### LZMALL 分布式商城系统
![https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square](https://img.shields.io/badge/license-Apache%202.0-blue.svg?longCache=true&style=flat-square)
![https://img.shields.io/badge/springcloud-Hoxton.RELEASE-yellow.svg?style=flat-square](https://img.shields.io/badge/springcloud-Hoxton.RELEASE-yellow.svg?style=flat-square)
![https://img.shields.io/badge/SpringCloudAlibaba-2.1.1.RELEASE-blueviolet.svg?style=flat-square](https://img.shields.io/badge/SpringCloudAlibaba-2.1.1.RELEASE-blueviolet.svg?style=flat-square)
![https://img.shields.io/badge/springboot-2.2.1.RELEASE-brightgreen.svg?style=flat-square](https://img.shields.io/badge/springboot-2.2.1.RELEASE-brightgreen.svg?style=flat-square)
![https://img.shields.io/badge/vue-2.6.10-orange.svg?style=flat-square](https://img.shields.io/badge/vue-2.6.10-orange.svg?style=flat-square)

lzmall是一款使用Spring Cloud Hoxton.RELEASE & Spring Cloud Alibaba构建的低耦合权限管理系统，前端基于
[lilishop](https://gitee.com/beijing_hongye_huicheng)前端项目改造。该系统具有如下特点：

序号 | 特点
---|---
1 | 前后端分离架构，客户端和服务端纯Token交互； 
2 | 认证服务器与资源服务器分离，方便接入自己的微服务系统
3 | 集成Prometheus，Skywalking APM
4 | 认证授权，前后端参数校验，Starter开箱即用等
5 | Doc Starter，几行配置自动生成系统api接口文档
6 | 基于flink-cdc + rocketma + es binlog自动同步,实现商品搜索功能
7 | 基于complateableFuture实现多线程任务编排

### 项目地址

 平台  | lzmall（后端）|lzmall_uniapp（前端）
---|---|---
Gitee  | [https://gitee.com/meloLh/lzmall](https://gitee.com/meloLh/lzmall)|[https://gitee.com/meloLh/lzmall-uniapp](https://gitee.com/meloLh/lzmall-uniapp)


### 服务模块

服务：

服务名称 | 端口 | 描述
---|---|---
lzmall-generator|6000| 代码生成服务
lzmall-auth| 7000| 微服务认证中心
lzmall-gateway| 8000| 微服务网关
lzmall-product| 8001| 商品中心
lzmall-marketing| 8002| 营销中心
lzmall-member| 8003| 会员中心
lzmall-order| 8004| 订单中心
lzmall-settlement| 8005| 结算中心
lzmall-trade| 8008| 交易中心

三方服务：

服务名称 | 端口 | 描述
---|---|---
Nacos| 8848|注册中心，配置中心 
MySQL| 3306 |MySQL 数据库 
Redis| 6379 | K-V 缓存数据库 
Elasticsearch|9200 | 日志存储
Logstash|4560|日志收集
Kibana|5601|日志展示
Prometheus|8403~8409|Prometheus APM
Skywalking|11800、12800、8080|Skywalking APM

### 目录结构
```
├─lzmall                       ------ 父项目
├─doc                             ------ 文档目录
│  ├─sql                          ------ SQL脚本
│  ├─es                           ------ es
│  └─puml                         ------ 流程图、时序图.....
├─lzmall-auth                     ------ 微服务认证中心
│  ├─lzmall-auth-api                   
│  ├─lzmall-auth-service                  ------ 认证服务
│  └─lzmall-auth-starter                  ------ 提供给外部服务统一认证授权starter
├─lzmall-common                   ------ 通用模块
│  ├─lzmall-common-core                   ------ 系统核心依赖包
│  ├─lzmall-common-datasource-starter     ------ 系统数据库自动装配starter
│  ├─lzmall-common-doc-starter            ------ 微服务子系统api文档自动装配starter
│  ├─lzmall-common-doc-starter            ------ 微服务子系统api文档自动装配starter
│  ├─lzmall-common-redis-starter          ------ 系统Redis自动装配starter
│  └─lzmall-common-framework-starter      ------ 微服务子系统统一日志处理、异常拦截等自动装配starter
├─lzmall-gateway                   ------ 微服务网关
├─lzmall-product                   ------ 商品中心
├─lzmall-member                    ------ 会员中心
├─lzmall-marketing                 ------ 营销中心
├─lzmall-order                     ------ 订单中心
├─lzmall-settlement                ------ 结算中心
├─lzmall-order                     ------ 交易中心
├─lzmall-generator                 ------ 代码生成器
```

## 技术栈

- 注册中心：Nacos
- 配置中心：Nacos
- 服务网关：Spring cloud Gateway
- 链路追踪：skyworking
- 消息队列：rocketmq
- 缓存：redis
- 搜索引擎：elasticsearch
- 日志收集：elk
- 数据同步：flinkcdc
- 同步锁框架：redission
- 常用工具：hutool、lombok、mapperStruct


## mall实现的功能概览
 -  用户端 
    - 商品模块  
        - 分类页展示
        - 商品搜索
        - 商品推荐
    - 购物车模块
        - 购物车展示
        - 购物车金额计算、优惠处理
        - 购物车去结算
    - 订单模块
        - 创建订单
        - 订单列表展示
    - 营销模块
        - 购物车优惠券识别
        - 领券中心
        - 使用优惠券
    - 用户模块
        - 地址管理
        - 积分账户管理
        - 商品评价管理

### 系统截图

<table>
  <tr>
     <td><img src="images/1.png"/></td>
     <td><img src="images/2.png"/></td>
  </tr>
  <tr>
     <td><img src="images/3.png"/></td>
     <td><img src="images/4.png"/></td>
  </tr>
  <tr>
     <td><img src="images/5.png"/></td>
     <td><img src="images/6.png"/></td>
  </tr>
  <tr>
     <td><img src="images/7.png"/></td>
  </tr>
</table>


