-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: lzmall_marketing
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_stock`
--

DROP TABLE IF EXISTS `activity_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_stock` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `real_stock` varchar(32) NOT NULL COMMENT '实际库存',
  `available_stock` varchar(32) NOT NULL COMMENT '可用库存',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='活动库存表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_stock`
--

LOCK TABLES `activity_stock` WRITE;
/*!40000 ALTER TABLE `activity_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_used_record`
--

DROP TABLE IF EXISTS `activity_used_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_used_record` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `biz_code` varchar(64) NOT NULL COMMENT '业务单号',
  `sku_code` varchar(32) NOT NULL COMMENT '商品编码',
  `product_name` varchar(64) NOT NULL DEFAULT '' COMMENT '商品名称',
  `shop_code` varchar(32) NOT NULL DEFAULT '' COMMENT '门店编码',
  `shop_name` varchar(64) NOT NULL DEFAULT '' COMMENT '门店名称',
  `purchase_qty` int NOT NULL DEFAULT '0' COMMENT '购买数量',
  `discount_qty` int NOT NULL DEFAULT '0' COMMENT '优惠数量',
  `account_code` varchar(32) NOT NULL DEFAULT '' COMMENT '使用者id',
  `account_name` varchar(64) NOT NULL DEFAULT '' COMMENT '使用者名称',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序值',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_biz_code` (`biz_code`) USING BTREE,
  KEY `idx_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='活动使用记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_used_record`
--

LOCK TABLES `activity_used_record` WRITE;
/*!40000 ALTER TABLE `activity_used_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_used_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount_activity_product`
--

DROP TABLE IF EXISTS `discount_activity_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `discount_activity_product` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `sku_code` varchar(32) NOT NULL COMMENT '商品编码',
  `product_name` varchar(64) NOT NULL COMMENT '商品名称',
  `image_url` varchar(128) NOT NULL COMMENT '商品图片',
  `shop_code` varchar(32) NOT NULL COMMENT '门店编码',
  `shop_name` varchar(64) NOT NULL COMMENT '门店名称',
  `discount_price` decimal(12,4) NOT NULL COMMENT '折扣价格',
  `discount_limit_qty` int NOT NULL COMMENT '商品折扣限制总数量',
  `user_limit_qty` int NOT NULL COMMENT '商品每人优惠限量',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序值',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code_sku_code` (`activity_code`,`sku_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='折扣活动商品明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount_activity_product`
--

LOCK TABLES `discount_activity_product` WRITE;
/*!40000 ALTER TABLE `discount_activity_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_activity_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_activity_product`
--

DROP TABLE IF EXISTS `gift_activity_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gift_activity_product` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `sku_code` varchar(32) NOT NULL COMMENT '商品编码',
  `product_name` varchar(64) NOT NULL COMMENT '商品名称',
  `image_url` varchar(128) NOT NULL COMMENT '商品图片',
  `shop_code` varchar(32) NOT NULL COMMENT '门店ID',
  `shop_name` varchar(64) NOT NULL COMMENT '门店名称',
  `send_qty` int NOT NULL COMMENT '商品每人优惠限量',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序值',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code_sku_code` (`activity_code`,`sku_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='赠品活动商品明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_activity_product`
--

LOCK TABLES `gift_activity_product` WRITE;
/*!40000 ALTER TABLE `gift_activity_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_activity_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_activity`
--

DROP TABLE IF EXISTS `marketing_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_activity` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `activity_widget` varchar(128) NOT NULL COMMENT '活动挂件',
  `activity_image` varchar(128) NOT NULL COMMENT '活动图片',
  `activity_source` varchar(16) NOT NULL COMMENT '优惠券来源 PLATEFORM-平台 SHOP-店铺',
  `subject_code` varchar(32) NOT NULL COMMENT '主体编号',
  `activity_type` varchar(16) NOT NULL COMMENT '活动类型  DISCOUNT-限时折扣  GIFT-满赠',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '活动结束时间',
  `discount_rule_type` varchar(16) NOT NULL COMMENT '优惠规则类型  MATCH_COUNT 满X件  MATCH_PRICE 满X元',
  `discount_rule_value` varchar(16) NOT NULL COMMENT '优惠规则满足值',
  `product_price_rule_type` varchar(16) NOT NULL COMMENT '商品价格优惠规则类型  RATIO-按照比例  PRICE-特定价格',
  `product_rule_type` varchar(16) NOT NULL COMMENT '优惠方式 ALL 所有商品 selected 选中',
  `status` varchar(16) NOT NULL DEFAULT 'WAIT' COMMENT '状态 WAIT 未开始 RUNNING 进行中  FINISH-已结束  CANCEL-已撤销',
  `remark` varchar(256) NOT NULL DEFAULT '' COMMENT '备注',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序值',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='营销活动模板表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_activity`
--

LOCK TABLES `marketing_activity` WRITE;
/*!40000 ALTER TABLE `marketing_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `marketing_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_activity`
--

DROP TABLE IF EXISTS `marketing_coupon_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_activity` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `coupon_amount` decimal(12,4) NOT NULL COMMENT '优惠券面值',
  `coupon_full_amount` decimal(12,4) DEFAULT NULL COMMENT '优惠券使用门槛金额',
  `coupon_source` varchar(16) NOT NULL COMMENT '优惠券来源 PLATFORM-平台券 STORE-店铺券',
  `subject_code` varchar(32) NOT NULL COMMENT '主体编号',
  `coupon_type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '优惠券类型 FULL_REDUCTION-满减券 NO_THRESHOLD-无门槛',
  `start_time` datetime NOT NULL COMMENT '优惠券活动开始时间',
  `end_time` datetime NOT NULL COMMENT '优惠券活动结束时间',
  `instance_start_time` datetime NOT NULL COMMENT '优惠券实例生效开始时间',
  `instance_end_time` datetime NOT NULL COMMENT '优惠券实例生效结束时间',
  `send_type` varchar(16) NOT NULL COMMENT '发放类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放',
  `use_type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '使用类型 ALL-全站商品均可用  SPECIFIC_PRODUCT-指定商品可用  SPECIFIC_CATEGORY3-指定分类可用 ORDER_AMOUNT_MATCH-订单满减',
  `user_send_limit` int NOT NULL COMMENT '限领张数',
  `send_count` int NOT NULL COMMENT '每次发几张优惠券',
  `mutex_flag` varchar(16) NOT NULL DEFAULT 'N' COMMENT '是否与其他活动互斥',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'WAIT' COMMENT '活动状态（状态 WAIT 未开始 RUNNING 进行中  FINISH-已结束  CANCEL-已撤销）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE,
  KEY `idx_subject_code` (`subject_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券模板表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_activity`
--

LOCK TABLES `marketing_coupon_activity` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_activity` DISABLE KEYS */;
INSERT INTO `marketing_coupon_activity` VALUES (1,'12345','测试优惠券',5.0000,10.0000,'SHOP','1','NO_THRESHOLD','2024-01-01 00:00:00','2025-03-10 00:00:00','2024-01-01 00:00:00','2024-03-10 00:00:00','ACTIVE_RECEIVE','SPECIFIC_PRODUCT',3,1,'N',1,'','','2024-01-30 11:01:14','','','2024-06-07 15:54:56','RUNNING'),(2,'234','平台券',4.0000,NULL,'PLATFORM','PLATFORM','NO_THRESHOLD','2024-01-01 00:00:00','2025-03-10 00:00:00','2024-01-01 00:00:00','2024-03-10 00:00:00','ACTIVE_RECEIVE','ALL',1,1,'N',1,'','','2024-01-30 11:09:32','','','2024-06-06 17:18:48','RUNNING');
/*!40000 ALTER TABLE `marketing_coupon_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_instance`
--

DROP TABLE IF EXISTS `marketing_coupon_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_instance` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `coupon_amount` decimal(12,4) NOT NULL COMMENT '优惠券面值',
  `instance_code` varchar(32) NOT NULL COMMENT '优惠券实例编号',
  `use_biz_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '使用关联单号(那笔订单使用)',
  `send_biz_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '发放关联单号(那个场景发放)',
  `use_time` datetime DEFAULT NULL COMMENT '使用时间',
  `send_time` datetime DEFAULT NULL COMMENT '发放时间',
  `instance_start_time` datetime NOT NULL COMMENT '优惠券实例生效开始时间',
  `instance_end_time` datetime NOT NULL COMMENT '优惠券实例生效结束时间',
  `account_code` varchar(32) NOT NULL COMMENT '所属人',
  `account_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属人名称',
  `status` varchar(16) NOT NULL COMMENT '状态  未使用  已使用  已失效',
  `mutex_flag` varchar(16) NOT NULL DEFAULT 'N' COMMENT '是否与其他活动互斥',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `send_type` varchar(16) NOT NULL DEFAULT '' COMMENT '发放方式',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_instance_code` (`instance_code`) USING BTREE,
  KEY `idx_activity_code` (`activity_code`) USING BTREE,
  KEY `idx_subject_code_` (`activity_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1800436294505963523 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券实例';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_instance`
--

LOCK TABLES `marketing_coupon_instance` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_instance` DISABLE KEYS */;
INSERT INTO `marketing_coupon_instance` VALUES (1798986705806475265,'12345','测试优惠券',5.0000,'1798986705305612288','','12345',NULL,'2024-06-07 15:53:49','2024-01-01 00:00:00','2024-07-10 00:00:00','123','LZMALL','UN_USE','N',1,'LZMALL','123','2024-06-07 15:53:48','LZMALL','123','2024-06-11 15:57:49','ACTIVE_RECEIVE'),(1798987003451064321,'12345','测试优惠券',5.0000,'1798987003461906432','','12345',NULL,'2024-06-07 15:55:00','2024-01-01 00:00:00','2024-07-10 00:00:00','123','LZMALL','UN_USE','N',1,'LZMALL','123','2024-06-07 15:54:59','LZMALL','123','2024-06-11 15:57:49','ACTIVE_RECEIVE'),(1800436284376719361,'12345','测试优惠券',5.0000,'1800436283972325376','','12345',NULL,'2024-06-11 15:53:55','2024-01-01 00:00:00','2024-07-10 00:00:00','123','LZMALL','UN_USE','N',1,'LZMALL','123','2024-06-11 15:53:55','LZMALL','123','2024-06-11 15:57:49','ACTIVE_RECEIVE'),(1800436294505963522,'234','平台券',4.0000,'1800436294520999936','','234',NULL,'2024-06-11 15:53:58','2024-01-01 00:00:00','2024-07-10 00:00:00','123','LZMALL','USED','N',1,'LZMALL','123','2024-06-11 15:53:57','LZMALL','123','2024-06-13 11:05:13','ACTIVE_RECEIVE');
/*!40000 ALTER TABLE `marketing_coupon_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_send_record`
--

DROP TABLE IF EXISTS `marketing_coupon_send_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_send_record` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `activity_name` varchar(64) NOT NULL COMMENT '活动名称',
  `coupon_amount` decimal(12,4) NOT NULL COMMENT '优惠券面值',
  `send_type` varchar(16) NOT NULL COMMENT '发放类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放',
  `instance_code` varchar(32) NOT NULL COMMENT '优惠券实例编号',
  `biz_code` varchar(32) NOT NULL COMMENT '使用关联单号(那个场景发放)',
  `send_time` datetime NOT NULL COMMENT '发放时间',
  `account_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接收人id',
  `account_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接收人名称',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_instance_code` (`instance_code`) USING BTREE,
  KEY `idx_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券发放记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_send_record`
--

LOCK TABLES `marketing_coupon_send_record` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_send_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `marketing_coupon_send_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_send_rule`
--

DROP TABLE IF EXISTS `marketing_coupon_send_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_send_rule` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `send_type` varchar(16) NOT NULL COMMENT '发放类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放',
  `user_send_limit` int NOT NULL COMMENT '限领张数',
  `send_count` int NOT NULL COMMENT '每次发几张优惠券',
  `rule_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发放规则json',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券发放规则';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_send_rule`
--

LOCK TABLES `marketing_coupon_send_rule` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_send_rule` DISABLE KEYS */;
INSERT INTO `marketing_coupon_send_rule` VALUES (1,'12345','ACTIVE_RECEIVE',4,4,'{}',1,'','','2024-06-06 17:21:11','','','2024-06-06 17:21:11'),(2,'234','ACTIVE_RECEIVE',5,4,'{}',1,'','','2024-06-06 17:21:36','','','2024-06-06 17:21:36');
/*!40000 ALTER TABLE `marketing_coupon_send_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_stock`
--

DROP TABLE IF EXISTS `marketing_coupon_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_stock` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` int NOT NULL COMMENT '活动编号',
  `real_stock` int NOT NULL DEFAULT '0' COMMENT '实际库存',
  `available_stock` int NOT NULL DEFAULT '0' COMMENT '可用库存',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券库存表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_stock`
--

LOCK TABLES `marketing_coupon_stock` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_stock` DISABLE KEYS */;
INSERT INTO `marketing_coupon_stock` VALUES (1,12345,100,97,1,'','','2024-01-30 11:01:37','','123','2024-06-11 15:53:55'),(2,234,50,49,1,'','','2024-01-30 11:09:54','','123','2024-06-11 15:53:57');
/*!40000 ALTER TABLE `marketing_coupon_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_coupon_use_rule`
--

DROP TABLE IF EXISTS `marketing_coupon_use_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_coupon_use_rule` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_code` varchar(32) NOT NULL COMMENT '活动编号',
  `use_type` varchar(16) NOT NULL COMMENT '使用类型 1.指定商品可用 2.指定分类可用 3.订单满减',
  `coupon_full_amount` decimal(12,4) DEFAULT NULL COMMENT '优惠券使用门槛金额',
  `mutex_flag` varchar(16) NOT NULL DEFAULT 'N' COMMENT '是否与其他活动互斥',
  `rule_json` text NOT NULL COMMENT '使用规则json',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_activity_code` (`activity_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券使用规则';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_coupon_use_rule`
--

LOCK TABLES `marketing_coupon_use_rule` WRITE;
/*!40000 ALTER TABLE `marketing_coupon_use_rule` DISABLE KEYS */;
INSERT INTO `marketing_coupon_use_rule` VALUES (1,'12345','SPECIFIC_PRODUCT',NULL,'N','[{\"imageUrl\":\"xxxxx\",\"skuId\":111111,\"skuName\":\"测试商品1\"}]',1,'','','2024-01-30 11:02:16','','','2024-01-30 13:55:33'),(2,'234','ALL',NULL,'N','[]',1,'','','2024-01-30 11:10:46','','','2024-01-30 11:10:46');
/*!40000 ALTER TABLE `marketing_coupon_use_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketing_preferential_record`
--

DROP TABLE IF EXISTS `marketing_preferential_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketing_preferential_record` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `preferential_type` varchar(32) NOT NULL COMMENT '活动编号',
  `preferential_code` varchar(32) NOT NULL COMMENT '优惠编号：优惠券指优惠券实例编号，活动指活动编号',
  `preferential_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '活动名称',
  `biz_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '业务单号',
  `sku_code` varchar(32) NOT NULL COMMENT '商品编码',
  `product_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `purchase_qty` int NOT NULL COMMENT '购买数量',
  `sale_price` decimal(12,4) NOT NULL COMMENT '商品价格',
  `origin_amount` decimal(12,4) NOT NULL COMMENT '商品原始总价',
  `preferential_amount` decimal(12,4) NOT NULL COMMENT '商品分摊优惠金额',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` varchar(16) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_biz_code` (`biz_code`) USING BTREE,
  KEY `idx_preferential_code` (`preferential_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1801088402213212163 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='营销分摊记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_preferential_record`
--

LOCK TABLES `marketing_preferential_record` WRITE;
/*!40000 ALTER TABLE `marketing_preferential_record` DISABLE KEYS */;
INSERT INTO `marketing_preferential_record` VALUES (1801067618832912385,'COUPON','234','','N234','KU123','',2,20.0000,40.0000,4.0000,1,'LZMALL','123','2024-06-13 09:42:37','LZMALL','123','2024-06-13 09:42:37','N'),(1801086830490062850,'COUPON','234','','N234','KU456','',1,60.0000,60.0000,4.0000,1,'LZMALL','123','2024-06-13 10:58:57','LZMALL','123','2024-06-13 10:58:57','N'),(1801087547518914561,'COUPON','234','','N234','KU456','',1,60.0000,60.0000,4.0000,1,'LZMALL','123','2024-06-13 11:01:48','LZMALL','123','2024-06-13 11:01:48','N'),(1801087818550644737,'COUPON','234','','N234','KU456','',1,60.0000,60.0000,4.0000,1,'LZMALL','123','2024-06-13 11:02:53','LZMALL','123','2024-06-13 11:02:53','N'),(1801088402213212162,'COUPON','234','','N234','KU456','',1,60.0000,60.0000,4.0000,1,'LZMALL','123','2024-06-13 11:05:12','LZMALL','123','2024-06-13 11:05:12','N');
/*!40000 ALTER TABLE `marketing_preferential_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'lzmall_marketing'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-14 10:33:32
