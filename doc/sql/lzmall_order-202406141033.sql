-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: lzmall_order
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_stock_record`
--

DROP TABLE IF EXISTS `product_stock_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_stock_record` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'sku编码',
  `biz_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '业务类型',
  `biz_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '业务单号',
  `operate_qty` int NOT NULL COMMENT '操作数量',
  `before_real_stock` int NOT NULL DEFAULT '0' COMMENT '操作前实际库存',
  `after_real_stock` int NOT NULL DEFAULT '0' COMMENT '操作后实际库存',
  `before_available_stock` int NOT NULL DEFAULT '0' COMMENT '操作前可用库存',
  `after_available_stock` int NOT NULL DEFAULT '0' COMMENT '操作后可用库存',
  `before_freeze_stock` int NOT NULL DEFAULT '0' COMMENT '操作前冻结库存',
  `after_freeze_stock` int NOT NULL DEFAULT '0' COMMENT '操作后冻结库存',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_biz_code` (`biz_code`,`sku_code`,`biz_type`) USING BTREE,
  KEY `idx_sku_code` (`sku_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='库存流水记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_stock_record`
--

LOCK TABLES `product_stock_record` WRITE;
/*!40000 ALTER TABLE `product_stock_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_stock_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_order`
--

DROP TABLE IF EXISTS `sale_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sale_order` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号',
  `trade_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '交易单编号',
  `client_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '订单来源',
  `shop_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '店铺名称',
  `shop_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '店铺编号',
  `member_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '会员编码',
  `member_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会员名称',
  `order_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '原价总额',
  `freight_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '运费',
  `coupon_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '优惠券优惠总额',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '折扣活动优惠总额',
  `payable_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '应付总额',
  `payment_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '实付总额',
  `cost_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '成本总额',
  `order_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'CREATED' COMMENT '订单状态',
  `pay_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'WAIT_PAY' COMMENT '支付状态',
  `pay_mode` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'ON_LINE' COMMENT '支付方式(ONLINE 线下支付  ONLINE线上支付)',
  `pay_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '支付方式(现金，支付宝，微信)',
  `pay_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '支付单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `cancel_time` datetime DEFAULT NULL COMMENT '取消时间',
  `refund_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '退款状态',
  `refund_time` datetime DEFAULT NULL COMMENT '退款成功时间',
  `cancel_reason` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '取消原因',
  `order_remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '订单备注',
  `sort` int NOT NULL DEFAULT '1' COMMENT '排序',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_order_code` (`order_code`) USING BTREE,
  KEY `idx_member_code` (`member_code`) USING BTREE,
  KEY `idx_trade_code` (`trade_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1801433433386205186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_order`
--

LOCK TABLES `sale_order` WRITE;
/*!40000 ALTER TABLE `sale_order` DISABLE KEYS */;
INSERT INTO `sale_order` VALUES (1801067385516408834,'1801067383769837568','1801067383245586432','CUSTOM','测试店铺','1','123','测试会员',20.0000,0.0000,0.0000,0.0000,20.0000,0.0000,10.0000,'WAIT_DELIVER','WAIT_PAY','ONLINE','','',NULL,NULL,'',NULL,'','',1,1,'测试会员','123','2024-06-13 09:41:41','测试会员','123','2024-06-13 10:43:44'),(1801067619285942273,'1801067618961240064','1801067618902556672','CUSTOM','测试店铺','1','123','测试会员',40.0000,0.0000,4.0000,0.0000,36.0000,0.0000,20.0000,'WAIT_DELIVER','WAIT_PAY','ONLINE','','',NULL,NULL,'',NULL,'','',1,1,'测试会员','123','2024-06-13 09:42:37','测试会员','123','2024-06-13 10:43:44'),(1801086294080598017,'1801086293470683136','1801086291629391872','CUSTOM','测试店铺','1','123','测试会员',20.0000,0.0000,0.0000,0.0000,20.0000,0.0000,10.0000,'CREATED','WAIT_PAY','ONLINE','','',NULL,NULL,'',NULL,'','',1,1,'测试会员','123','2024-06-13 10:56:49','测试会员','123','2024-06-13 10:56:49'),(1801088407082864641,'1801088404757479424','1801088402282856448','CUSTOM','测试店铺','1','123','测试会员',60.0000,0.0000,4.0000,0.0000,56.0000,0.0000,50.0000,'CREATED','WAIT_PAY','ONLINE','','',NULL,NULL,'',NULL,'','',1,1,'测试会员','123','2024-06-13 11:05:13','测试会员','123','2024-06-13 11:05:13'),(1801433433386205185,'1801433431249563648','1801433429118840832','CUSTOM','Lilishop自营店','1376369067769724928','123','测试会员',3799.0000,0.0000,0.0000,0.0000,3799.0000,0.0000,1000.0000,'CREATED','WAIT_PAY','ONLINE','','',NULL,NULL,'',NULL,'','',1,1,'测试会员','123','2024-06-14 09:56:14','测试会员','123','2024-06-14 09:56:14');
/*!40000 ALTER TABLE `sale_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_order_detail`
--

DROP TABLE IF EXISTS `sale_order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sale_order_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号',
  `spu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spu编码',
  `sku_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'sku编码',
  `buy_count` int NOT NULL DEFAULT '0' COMMENT '购买数量',
  `product_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'sku名称',
  `main_picture` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '商品主图',
  `category_code1` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '一级分类编号',
  `category_code2` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '二级分类编号',
  `category_code3` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '三级分类编号',
  `cost_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '成本价',
  `sale_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '商品价',
  `market_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '市场价',
  `sale_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '原始价格总额',
  `coupon_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '优惠券优惠总额',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '折扣活动优惠总额',
  `payable_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '应付总额',
  `cost_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '成本总额',
  `unit` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '单位',
  `spec_json` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '规格json',
  `sort` int NOT NULL DEFAULT '1' COMMENT '排序',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_sku_code` (`sku_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1801433433386205187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='订单明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_order_detail`
--

LOCK TABLES `sale_order_detail` WRITE;
/*!40000 ALTER TABLE `sale_order_detail` DISABLE KEYS */;
INSERT INTO `sale_order_detail` VALUES (1801067385516408835,'1801067383769837568','PU123','KU123',1,'华为手机MATE10金色','https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1e3ac5b7a32d4b569053aaedfdf7e264.jpg?x-oss-process=style/400X400','1','2','3',10.0000,20.0000,30.0000,20.0000,0.0000,0.0000,20.0000,10.0000,'','',1,1,'测试会员','123','2024-06-13 09:41:41','测试会员','123','2024-06-13 09:41:41'),(1801067619285942274,'1801067618961240064','PU123','KU123',2,'华为手机MATE10金色','https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1e3ac5b7a32d4b569053aaedfdf7e264.jpg?x-oss-process=style/400X400','1','2','3',10.0000,20.0000,30.0000,40.0000,4.0000,0.0000,36.0000,20.0000,'','',1,1,'测试会员','123','2024-06-13 09:42:37','测试会员','123','2024-06-13 09:42:37'),(1801086294101569537,'1801086293470683136','PU123','KU123',1,'华为手机MATE10金色','https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1e3ac5b7a32d4b569053aaedfdf7e264.jpg?x-oss-process=style/400X400','1','2','3',10.0000,20.0000,30.0000,20.0000,0.0000,0.0000,20.0000,10.0000,'','',1,1,'测试会员','123','2024-06-13 10:56:49','测试会员','123','2024-06-13 10:56:49'),(1801088407120613378,'1801088404757479424','PU123','KU456',1,'华为手机MATE10蓝色','https://lilishop-oss.oss-cn-beijing.aliyuncs.com/5417804420414d9092a965afff4b48df.jpg?x-oss-process=style/400X400','1','2','3',50.0000,60.0000,78.0000,60.0000,4.0000,0.0000,56.0000,50.0000,'','',1,1,'测试会员','123','2024-06-13 11:05:13','测试会员','123','2024-06-13 11:05:13'),(1801433433386205186,'1801433431249563648','PU1376373278360207360','KU1387977447483375616',1,'一加 OnePlus 9 ','https://lilishop-oss.oss-cn-beijing.aliyuncs.com/a4c2b5073fd84d60bfe38d6a6c0044c9.jpeg?x-oss-process=style/400X400','1348576427264204941','1348576427264204942','1348576427264204943',1000.0000,3799.0000,0.0000,3799.0000,0.0000,0.0000,3799.0000,1000.0000,'','',1,1,'测试会员','123','2024-06-14 09:56:14','测试会员','123','2024-06-14 09:56:14');
/*!40000 ALTER TABLE `sale_order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'lzmall_order'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-14 10:33:33
