-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: lzmall_trade
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `spu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'spu编码',
  `sku_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'sku编码',
  `product_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称',
  `member_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会员编码',
  `buy_count` int NOT NULL COMMENT '购买数量',
  `sale_price` decimal(12,4) NOT NULL COMMENT '加购时售价',
  `shop_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '店铺ID',
  `shop_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '店铺名称',
  `checked` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N' COMMENT '是否选中',
  `sort` int NOT NULL DEFAULT '1' COMMENT '排序',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_member_sku` (`member_code`,`sku_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1801433483231297538 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='购物车表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1801433483231297537,'PU1376373278360207360','KU1387977447483375616','一加 OnePlus 9 ','123',1,3799.0000,'1376369067769724928','Lilishop自营店','N',1,1,'','','2024-06-14 09:56:26','','','2024-06-14 09:56:26');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_order`
--

DROP TABLE IF EXISTS `trade_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_order` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `trade_code` varchar(32) NOT NULL COMMENT '交易单编号',
  `member_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会员编码',
  `member_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '会员名称',
  `payable_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '应付总额',
  `payment_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT '实付总额',
  `order_status` varchar(32) NOT NULL DEFAULT 'WAIT_PAY' COMMENT '订单状态',
  `pay_status` varchar(32) NOT NULL DEFAULT 'WAIT_PAY' COMMENT '支付状态',
  `pay_type` varchar(32) NOT NULL DEFAULT '' COMMENT '支付方式(现金，支付宝，微信)',
  `pay_code` varchar(32) NOT NULL DEFAULT '' COMMENT '支付单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `remark` varchar(512) NOT NULL DEFAULT '' COMMENT '订单备注',
  `sort` int NOT NULL DEFAULT '1' COMMENT '排序',
  `version` int NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `creator_code` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人编号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `modifier_code` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人编号',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_trade_code` (`trade_code`) USING BTREE,
  KEY `idx_member_code` (`member_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1801433429066055683 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='交易表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_order`
--

LOCK TABLES `trade_order` WRITE;
/*!40000 ALTER TABLE `trade_order` DISABLE KEYS */;
INSERT INTO `trade_order` VALUES (1797830886331203586,'1797830885977141248','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 11:21:00','测试会员','123','2024-06-04 11:21:00'),(1797830920615444481,'1797830920634675200','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 11:21:08','测试会员','123','2024-06-04 11:21:08'),(1797831020850921474,'1797831020853374976','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 11:21:32','测试会员','123','2024-06-04 11:21:32'),(1797870465088184321,'1797870464675401728','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 13:58:16','测试会员','123','2024-06-04 13:58:16'),(1797870619786698754,'1797870619831095296','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 13:58:53','测试会员','123','2024-06-04 13:58:53'),(1797870672723009538,'1797870672763211776','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 13:59:05','测试会员','123','2024-06-04 13:59:05'),(1797870834254045185,'1797870834302636032','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 13:59:44','测试会员','123','2024-06-04 13:59:44'),(1797870943658270721,'1797870943698472960','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:00:10','测试会员','123','2024-06-04 14:00:10'),(1797871045659549698,'1797871045649420288','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:00:34','测试会员','123','2024-06-04 14:00:34'),(1797871715561164801,'1797871715030941696','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:03:14','测试会员','123','2024-06-04 14:03:14'),(1797872066351779841,'1797872066383593472','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:04:38','测试会员','123','2024-06-04 14:04:38'),(1797872466165436418,'1797872465689739264','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:06:13','测试会员','123','2024-06-04 14:06:13'),(1797872629227393026,'1797872629284372480','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:06:52','测试会员','123','2024-06-04 14:06:52'),(1797873090173014017,'1797873090200633344','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:08:42','测试会员','123','2024-06-04 14:08:42'),(1797873303965077505,'1797873303996891136','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:09:33','测试会员','123','2024-06-04 14:09:33'),(1797874762420998146,'1797874761911820288','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:15:20','测试会员','123','2024-06-04 14:15:20'),(1797875538069442561,'1797875538126495744','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:18:25','测试会员','123','2024-06-04 14:18:25'),(1797876573991227393,'1797876574027309056','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:22:32','测试会员','123','2024-06-04 14:22:32'),(1797876751640973314,'1797876751693832192','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:23:15','测试会员','123','2024-06-04 14:23:15'),(1797877048463478785,'1797877048474394624','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:24:25','测试会员','123','2024-06-04 14:24:25'),(1797877825131139073,'1797877825133666304','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:27:31','测试会员','123','2024-06-04 14:27:31'),(1797879044432089089,'1797879044451393536','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:32:21','测试会员','123','2024-06-04 14:32:21'),(1797879097297096706,'1797879097345761280','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:32:34','测试会员','123','2024-06-04 14:32:34'),(1797879285126418434,'1797879285162500096','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:33:19','测试会员','123','2024-06-04 14:33:19'),(1797880209563602945,'1797880209616461824','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:36:59','测试会员','123','2024-06-04 14:36:59'),(1797881126392320001,'1797881126453567488','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:40:38','测试会员','123','2024-06-04 14:40:38'),(1797882878990635010,'1797882879047688192','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:47:36','测试会员','123','2024-06-04 14:47:36'),(1797883243479846914,'1797883243499151360','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:49:02','测试会员','123','2024-06-04 14:49:02'),(1797883589505732610,'1797883589550202880','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:50:25','测试会员','123','2024-06-04 14:50:25'),(1797884082978181121,'1797884082993291264','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:52:23','测试会员','123','2024-06-04 14:52:23'),(1797884634759843841,'1797884634766565376','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:54:34','测试会员','123','2024-06-04 14:54:34'),(1797885355911696386,'1797885355964555264','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 14:57:26','测试会员','123','2024-06-04 14:57:26'),(1797886182080532482,'1797886182125002752','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:00:43','测试会员','123','2024-06-04 15:00:43'),(1797887414325104641,'1797887414365380608','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:05:37','测试会员','123','2024-06-04 15:05:37'),(1797887973006397442,'1797887973017313280','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:07:50','测试会员','123','2024-06-04 15:07:50'),(1797888558564790274,'1797888558579900416','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:10:10','测试会员','123','2024-06-04 15:10:10'),(1797888631952527362,'1797888632001191936','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:10:27','测试会员','123','2024-06-04 15:10:27'),(1797888697840848898,'1797888697876930560','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:10:43','测试会员','123','2024-06-04 15:10:43'),(1797889141380108290,'1797889141428772864','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:12:29','测试会员','123','2024-06-04 15:12:29'),(1797889187945271298,'1797889187960381440','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:12:40','测试会员','123','2024-06-04 15:12:40'),(1797891187361599490,'1797891187397681152','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:20:36','测试会员','123','2024-06-04 15:20:36'),(1797891401099137026,'1797891401126830080','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:21:27','测试会员','123','2024-06-04 15:21:27'),(1797891975911723009,'1797891975960387584','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:23:44','测试会员','123','2024-06-04 15:23:44'),(1797894494633295874,'1797894493943689216','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:33:45','测试会员','123','2024-06-04 15:33:45'),(1797895555221782530,'1797895555228430336','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:37:58','测试会员','123','2024-06-04 15:37:58'),(1797896538899316737,'1797896538897575936','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:41:52','测试会员','123','2024-06-04 15:41:52'),(1797897182410407937,'1797897182425444352','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:44:26','测试会员','123','2024-06-04 15:44:26'),(1797897523352797186,'1797897523376222208','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:45:47','测试会员','123','2024-06-04 15:45:47'),(1797897809244946433,'1797897809259982848','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:46:55','测试会员','123','2024-06-04 15:46:55'),(1797898077718151170,'1797898077716410368','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 15:47:59','测试会员','123','2024-06-04 15:47:59'),(1797906997245345794,'1797906997256187904','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 16:23:26','测试会员','123','2024-06-04 16:23:26'),(1797907658657726466,'1797907658668568576','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 16:26:03','测试会员','123','2024-06-04 16:26:03'),(1797907896021778434,'1797907896061980672','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 16:27:00','测试会员','123','2024-06-04 16:27:00'),(1797908859130445825,'1797908859179036672','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 16:30:50','测试会员','123','2024-06-04 16:30:50'),(1797909410115162113,'1797909409769488384','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 16:33:01','测试会员','123','2024-06-04 16:33:01'),(1797919326024429569,'1797919325682950144','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:12:25','测试会员','123','2024-06-04 17:12:25'),(1797919751943417857,'1797919751945871360','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:14:07','测试会员','123','2024-06-04 17:14:07'),(1797919823582130177,'1797919823622332416','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:14:24','测试会员','123','2024-06-04 17:14:24'),(1797920293734248450,'1797920293732507648','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:16:16','测试会员','123','2024-06-04 17:16:16'),(1797920516355321858,'1797920516403912704','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:17:09','测试会员','123','2024-06-04 17:17:09'),(1797925986931556353,'1797925986573299712','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:38:53','测试会员','123','2024-06-04 17:38:53'),(1797930216211816450,'1797930215874531328','123','测试会员',180.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-04 17:55:42','测试会员','123','2024-06-04 17:55:42'),(1798161441388204034,'1798161441067696128','123','测试会员',200.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:14:30','测试会员','123','2024-06-05 09:14:30'),(1798162156483493889,'1798162156175511552','123','测试会员',200.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:17:21','测试会员','123','2024-06-05 09:17:21'),(1798162375472271362,'1798162375168614400','123','测试会员',200.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:18:13','测试会员','123','2024-06-05 09:18:13'),(1798162521245306881,'1798162521281388544','123','测试会员',200.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:18:47','测试会员','123','2024-06-05 09:18:47'),(1798162790628630530,'1798162790320705536','123','测试会员',200.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:19:52','测试会员','123','2024-06-05 09:19:52'),(1798163083873394690,'1798163083917791232','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 09:21:02','测试会员','123','2024-06-05 09:21:02'),(1798308524824932354,'1798308524869328896','123','测试会员',80.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-05 18:58:57','测试会员','123','2024-06-05 18:58:57'),(1798539000357564417,'1798539000364212224','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-06 10:14:47','测试会员','123','2024-06-06 10:14:47'),(1800808139923087362,'1800808139585802240','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 16:31:32','测试会员','123','2024-06-12 16:31:32'),(1800808578307547138,'1800808578305806336','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 16:33:17','测试会员','123','2024-06-12 16:33:17'),(1800808976107921409,'1800808976169095168','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 16:34:51','测试会员','123','2024-06-12 16:34:51'),(1800811798186639362,'1800811797857742848','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 16:46:04','测试会员','123','2024-06-12 16:46:04'),(1800819310260600834,'1800819310254665728','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 17:15:55','测试会员','123','2024-06-12 17:15:55'),(1800819441093525505,'1800819441121144832','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 17:16:27','测试会员','123','2024-06-12 17:16:27'),(1800820359839039489,'1800820359862464512','123','测试会员',136.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 17:20:06','测试会员','123','2024-06-12 17:20:06'),(1800836872948117506,'1800836872610775040','123','测试会员',16.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-12 18:25:43','测试会员','123','2024-06-12 18:25:43'),(1801064946419597314,'1801064946443022336','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 09:32:00','测试会员','123','2024-06-13 09:32:00'),(1801065677037961218,'1801065676696481792','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 09:34:54','测试会员','123','2024-06-13 09:34:54'),(1801066422785212418,'1801066422833803264','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 09:37:52','测试会员','123','2024-06-13 09:37:52'),(1801067383599648770,'1801067383245586432','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 09:41:41','测试会员','123','2024-06-13 09:41:41'),(1801067618891714561,'1801067618902556672','123','测试会员',36.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 09:42:37','测试会员','123','2024-06-13 09:42:37'),(1801086291597578242,'1801086291629391872','123','测试会员',20.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 10:56:49','测试会员','123','2024-06-13 10:56:49'),(1801086830490144770,'1801086830538735616','123','测试会员',56.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 10:58:57','测试会员','123','2024-06-13 10:58:57'),(1801087547518996482,'1801087547563393024','123','测试会员',56.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 11:01:48','测试会员','123','2024-06-13 11:01:48'),(1801087818550726658,'1801087818574151680','123','测试会员',56.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 11:02:53','测试会员','123','2024-06-13 11:02:53'),(1801088402276208641,'1801088402282856448','123','测试会员',56.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-13 11:05:12','测试会员','123','2024-06-13 11:05:12'),(1801433429066055682,'1801433429118840832','123','测试会员',3799.0000,0.0000,'WAIT_PAY','WAIT_PAY','','',NULL,'',1,1,'测试会员','123','2024-06-14 09:56:13','测试会员','123','2024-06-14 09:56:13');
/*!40000 ALTER TABLE `trade_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'lzmall_trade'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-14 10:33:33
