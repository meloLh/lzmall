package com.melo.lzmall.auth.api.feign;


import com.melo.lzmall.auth.api.request.CheckTokenRequest;
import com.melo.lzmall.auth.api.response.AuthAccountResponse;
import com.melo.lzmall.common.core.dto.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "lzmall-auth",contextId = "lzmall-auth-authFeignClient")
public interface AuthFeignClient {

    @ApiOperation("校验token")
    @PostMapping("/api/auth/checkToken")
    Result<AuthAccountResponse> checkToken(@Validated @RequestBody CheckTokenRequest request);
}
