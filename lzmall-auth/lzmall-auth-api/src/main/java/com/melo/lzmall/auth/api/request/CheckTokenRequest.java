package com.melo.lzmall.auth.api.request;

import lombok.Data;

//import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class CheckTokenRequest implements Serializable {

//    @NotBlank(message = "accessToken不能为空")
    private String accessToken;

//    @NotBlank(message = "请求的客户端类型")
    private String requestClientType;
}
