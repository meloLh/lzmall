package com.melo.lzmall.auth.api.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

//import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class UsernamePasswordLoginRequest implements Serializable {

//    @NotBlank(message = "用户名不能为空")
    private String username;

//    @NotBlank(message = "密码不能为空")
    @ApiModelProperty("已经加密的密码")
    private String password;
}
