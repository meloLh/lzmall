package com.melo.lzmall.auth.api.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class UsernamePasswordLoginResponse implements Serializable {

    private String accessToken;

    private String refreshToken;

    /**失效时间*/
    private Integer expireTime;
}
