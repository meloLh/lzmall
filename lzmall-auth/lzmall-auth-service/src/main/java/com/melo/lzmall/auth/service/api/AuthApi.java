package com.melo.lzmall.auth.service.api;

import com.melo.lzmall.auth.api.request.CheckTokenRequest;
import com.melo.lzmall.auth.api.response.AuthAccountResponse;
import com.melo.lzmall.auth.service.converter.AccountFaceConverter;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import com.melo.lzmall.auth.service.service.TokenService;
import com.melo.lzmall.common.core.dto.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class AuthApi {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AccountFaceConverter converter;

    @ApiOperation("校验token")
    @PostMapping("/api/auth/checkToken")
    public Result<AuthAccountResponse> checkToken(@Validated @RequestBody CheckTokenRequest request) {
        AuthAccountBO authAccountBO =  tokenService.checkToken(request.getAccessToken(),request.getRequestClientType());
        AuthAccountResponse response = converter.convertToAuthAccountResponse(authAccountBO);
        return Result.success(response);
    }
}
