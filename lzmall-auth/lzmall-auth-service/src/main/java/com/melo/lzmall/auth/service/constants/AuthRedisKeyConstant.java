package com.melo.lzmall.auth.service.constants;

public class AuthRedisKeyConstant {



    public static final String AUTH_CACHE_KEY_PREFIX = "LZMALL:AUTH:";

    public static final String TOKEN_KEY_PREFIX = AUTH_CACHE_KEY_PREFIX+"ACCOUNT_TOKEN:";

    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";


    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String ACCOUNT_LOGIN_COUNT = AUTH_CACHE_KEY_PREFIX+"ACCOUNT_LOGIN_COUNT";

//    public static final String ACCOUNT_LOGIN_COUNT = AUTH_CACHE_KEY_PREFIX+"ACCOUNT_LOGIN_COUNT_";




}
