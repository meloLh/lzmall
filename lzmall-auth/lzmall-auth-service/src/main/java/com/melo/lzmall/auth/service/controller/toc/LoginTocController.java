package com.melo.lzmall.auth.service.controller.toc;

import com.melo.lzmall.auth.api.request.UsernamePasswordLoginRequest;
import com.melo.lzmall.auth.api.response.UsernamePasswordLoginResponse;
import com.melo.lzmall.auth.service.converter.LoginFaceConverter;
import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.entity.dto.LoginDTO;
import com.melo.lzmall.auth.service.service.LoginService;
import com.melo.lzmall.common.core.dto.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
@Validated
public class LoginTocController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private LoginFaceConverter converter;

    @PostMapping("/buyer/auth/usernamePasswordLogin")
    public Result<UsernamePasswordLoginResponse> usernamePasswordLogin(@Validated  UsernamePasswordLoginRequest request){
        LoginDTO loginDTO =  converter.convertToLoginDTO(request);
        loginDTO.setAccountType("CUSTOM");
        LoginTokenDTO tokenDTO =  loginService.usernamePasswordLogin(loginDTO);
        return Result.success(converter.convertToUsernamePasswordLoginResponse(tokenDTO));
    }
}
