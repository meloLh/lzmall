package com.melo.lzmall.auth.service.converter;


import com.melo.lzmall.auth.api.response.AuthAccountResponse;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import org.mapstruct.Mapper;

import java.util.Map;


@Mapper(componentModel = "spring")
public interface  AccountFaceConverter {

    AuthAccountResponse convertToAuthAccountResponse(AuthAccountBO authAccountBO);
}

