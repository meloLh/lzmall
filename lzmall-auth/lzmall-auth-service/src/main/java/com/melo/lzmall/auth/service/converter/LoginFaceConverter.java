package com.melo.lzmall.auth.service.converter;


import com.melo.lzmall.auth.api.request.UsernamePasswordLoginRequest;
import com.melo.lzmall.auth.api.response.UsernamePasswordLoginResponse;
import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.entity.dto.LoginDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface LoginFaceConverter {

    @Mapping(source = "username",target = "accountName")
    LoginDTO convertToLoginDTO(UsernamePasswordLoginRequest request);

    UsernamePasswordLoginResponse convertToUsernamePasswordLoginResponse(LoginTokenDTO tokenDTO);
}

