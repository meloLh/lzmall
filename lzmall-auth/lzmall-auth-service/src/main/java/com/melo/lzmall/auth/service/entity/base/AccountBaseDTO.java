package com.melo.lzmall.auth.service.entity.base;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 账户表
 */
@Data
public class AccountBaseDTO implements Serializable {

    /**
     *
     */
    @TableId
    private Long id;
    /**
     * 账号
     */
    private String accountCode;
    /**
     * 用户名
     */
    private String accountName;

    /**
     * 账户类型
     */
    private String accountType;

    /**
     * 密码
     */
    private String password;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 盐
     */
    private String salt;
    /**
     * 用户状态
     */
    private String status;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 注册来源
     */
    private String clientType;
    /**
     * 最后一次登录时间
     */
    private Date lastLoginTime;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
