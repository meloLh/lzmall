package com.melo.lzmall.auth.service.entity.bo;

import lombok.Data;

import java.io.Serializable;
@Data
public class AuthAccountBO implements Serializable {

    private String accountName;

    private String accountCode;

    /**
     * 帐户类型
     */
    private String accountType;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 客户端类型
     */
    private String clientType;
}
