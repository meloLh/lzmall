package com.melo.lzmall.auth.service.entity.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginTokenDTO implements Serializable {

    private String accessToken;

    private String refreshToken;

    /**失效时间*/
    private Long expireTime;
}
