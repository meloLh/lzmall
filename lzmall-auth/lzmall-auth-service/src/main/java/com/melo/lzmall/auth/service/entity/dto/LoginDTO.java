package com.melo.lzmall.auth.service.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class LoginDTO implements Serializable {

    @NotBlank(message = "用户名不能为空")
    private String accountName;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "用户类型")
    private String accountType;

}
