package com.melo.lzmall.auth.service.enums;

public enum AccountAuthEnum {


    USER_NOT_EXIST("Z00000", "用户不存在"),

    USER_PASSWORD_ERROR("Z00001", "用户名或者密码不正确"),

    USER_LOGIN_COUNT_MAX("Z00001", "登录超过最大尝试次数"),

    USER_DISABLE("Z00002", "用户已禁用，请联系客服"),

    TOKEN_NOT_EXIST("T00001", "token不存在"),

    TOKEN_EXPIRED("T00002", "token已过期"),

    TOKEN_DISABLE("T00003", "token不合法"),

    ;

    private final String code;

    private final String msg;

    public String value() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    AccountAuthEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
