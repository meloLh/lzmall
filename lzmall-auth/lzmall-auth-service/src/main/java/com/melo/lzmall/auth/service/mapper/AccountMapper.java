package com.melo.lzmall.auth.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.auth.service.entity.po.AccountPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface AccountMapper extends BaseMapper<AccountPO> {
	
}
