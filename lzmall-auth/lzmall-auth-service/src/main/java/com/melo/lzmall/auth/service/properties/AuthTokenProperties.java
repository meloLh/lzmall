package com.melo.lzmall.auth.service.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "lzmall.auth.token")
@Data
@Configuration
public class AuthTokenProperties {

    /**token过期时间 默认分钟*/
    private Long expireTime= 60L;

    /**登录失败三次 禁止登录*/
    private Integer userLoginLimitCount = 5;

    /**登录失败三次 禁止登录 30分钟*/
    private Integer userLoginLimitCountTime = 60*60*3;
}
