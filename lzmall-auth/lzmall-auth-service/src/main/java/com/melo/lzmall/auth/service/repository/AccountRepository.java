package com.melo.lzmall.auth.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.auth.service.entity.base.AccountBaseDTO;
import com.melo.lzmall.auth.service.entity.po.AccountPO;
import com.melo.lzmall.auth.service.mapper.AccountMapper;
import com.melo.lzmall.auth.service.repository.converter.AccountRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Slf4j
@Component
public class AccountRepository {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountRepositoryConverter converter;

    public AccountBaseDTO getByAccountCodeAndAccountType(String accountCode, String accountType) {
        if(StringUtils.isAnyBlank(accountCode,accountType)){
            return null;
        }
        LambdaQueryWrapper<AccountPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AccountPO::getAccountCode,accountCode)
                    .eq(AccountPO::getAccountType,accountType);
        AccountPO accountPO = accountMapper.selectOne(queryWrapper);
        return converter.convertToAccountBaseDTO(accountPO);
    }

    public AccountBaseDTO getByAccountNameAndAccountType(String accountName, String accountType) {
        if(StringUtils.isAnyBlank(accountName,accountType)){
            return null;
        }
        LambdaQueryWrapper<AccountPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AccountPO::getAccountName,accountName)
                .eq(AccountPO::getAccountType,accountType);
        AccountPO accountPO = accountMapper.selectOne(queryWrapper);
        return converter.convertToAccountBaseDTO(accountPO);
    }
}

