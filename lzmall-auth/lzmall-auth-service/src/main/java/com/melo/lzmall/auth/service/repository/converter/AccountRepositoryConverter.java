package com.melo.lzmall.auth.service.repository.converter;


import com.melo.lzmall.auth.service.entity.base.AccountBaseDTO;
import com.melo.lzmall.auth.service.entity.po.AccountPO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface AccountRepositoryConverter {

    AccountBaseDTO convertToAccountBaseDTO(AccountPO accountPO);
}

