package com.melo.lzmall.auth.service.service;


import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.entity.dto.LoginDTO;

public interface LoginService {
    /**
     * @Description 用户名密码登录
     * @author liuhu
     * @param loginDTO
     * @date 2024/4/26 20:51
     * @return com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO
     */
    LoginTokenDTO usernamePasswordLogin(LoginDTO loginDTO);
}

