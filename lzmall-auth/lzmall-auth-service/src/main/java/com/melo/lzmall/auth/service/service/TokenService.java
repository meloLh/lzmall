package com.melo.lzmall.auth.service.service;


import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.entity.dto.LoginDTO;

public interface TokenService {
    /**
     * @Description 校验token
     * @author liuhu
     * @param accessToken
     * @param requestClientType
     * @date 2024/4/26 21:27
     * @return com.melo.lzmall.auth.service.entity.bo.AuthAccountBO
     */
    AuthAccountBO checkToken(String accessToken, String requestClientType);

    /**
     * @Description 创建token
     * @author liuhu
     * @param authAccountBO
     * @date 2024/4/26 21:27
     * @return com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO
     */
    LoginTokenDTO createToken(AuthAccountBO authAccountBO);
}

