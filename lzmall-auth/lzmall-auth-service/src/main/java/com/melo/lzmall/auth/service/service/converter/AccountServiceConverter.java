package com.melo.lzmall.auth.service.service.converter;


import com.melo.lzmall.auth.service.entity.base.AccountBaseDTO;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface AccountServiceConverter {
    AuthAccountBO convertToAuthAccountBO(AccountBaseDTO accountBaseDTO);
}

