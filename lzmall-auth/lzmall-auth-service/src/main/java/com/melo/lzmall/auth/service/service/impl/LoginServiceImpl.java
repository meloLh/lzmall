package com.melo.lzmall.auth.service.service.impl;

import com.melo.lzmall.auth.service.constants.AuthRedisKeyConstant;
import com.melo.lzmall.auth.service.entity.base.AccountBaseDTO;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.entity.dto.LoginDTO;
import com.melo.lzmall.auth.service.enums.AccountAuthEnum;
import com.melo.lzmall.auth.service.properties.AuthTokenProperties;
import com.melo.lzmall.auth.service.repository.AccountRepository;
import com.melo.lzmall.auth.service.service.LoginService;
import com.melo.lzmall.auth.service.service.TokenService;
import com.melo.lzmall.auth.service.service.converter.AccountServiceConverter;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RedisHelper redisHelper;

    @Autowired
    private AuthTokenProperties authTokenProperties;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AccountServiceConverter converter;

    @Override
    public LoginTokenDTO usernamePasswordLogin(LoginDTO loginDTO) {
        // 1.校验用户登录合法性
        AccountBaseDTO accountBaseDTO = accountRepository.getByAccountNameAndAccountType(loginDTO.getAccountName(), loginDTO.getAccountType());
        // 2.校验用户登录合法性
        validUsernamePasswordLogin(accountBaseDTO,loginDTO.getAccountName(),loginDTO.getPassword());
        // 3.获取accessToken
        AuthAccountBO authAccountBO = buildAuthAccountBO(accountBaseDTO);
        LoginTokenDTO token = tokenService.createToken(authAccountBO);
        // 清除次数
        redisHelper.del(AuthRedisKeyConstant.ACCOUNT_LOGIN_COUNT + accountBaseDTO.getAccountCode());
        // 4.TODO 用户登录事件异步流程
        return token;
    }

    private AuthAccountBO buildAuthAccountBO(AccountBaseDTO accountBaseDTO) {
        AuthAccountBO entity = new AuthAccountBO();
        entity.setAccountCode(accountBaseDTO.getAccountCode());
        entity.setAccountType(accountBaseDTO.getAccountType());
        entity.setClientType(accountBaseDTO.getClientType());
        entity.setAccountName(accountBaseDTO.getAccountName());
        entity.setRealName(accountBaseDTO.getRealName());
        return entity;
    }

    private void validUsernamePasswordLogin(AccountBaseDTO account, String username,String password) {
        //1.校验用户信息
        if(Objects.isNull(account)){
            throw new BusinessException(AccountAuthEnum.USER_NOT_EXIST.value(), AccountAuthEnum.USER_NOT_EXIST.getMsg());
        }
        //2.校验用户状态
        if (!Objects.equals(account.getStatus(), "ACTIVE")) {
            throw new BusinessException(AccountAuthEnum.USER_DISABLE.value(), AccountAuthEnum.USER_DISABLE.getMsg());
        }
        //3.校验用户登录次数
        String value = redisHelper.get(AuthRedisKeyConstant.ACCOUNT_LOGIN_COUNT + username);
        if(StringUtils.isNotBlank(value) && Integer.parseInt(value) > authTokenProperties.getUserLoginLimitCount()){
            throw new BusinessException(AccountAuthEnum.USER_LOGIN_COUNT_MAX.value(), AccountAuthEnum.USER_LOGIN_COUNT_MAX.getMsg());
        }
        //4.校验密码 前端传递的是已经MD5加密后的
        if(!Objects.equals(account.getPassword(), password)){
            redisHelper.set(AuthRedisKeyConstant.ACCOUNT_LOGIN_COUNT + username,
                    StringUtils.isNotBlank(value) ? Integer.parseInt(value) + 1 : 0,30L, TimeUnit.MINUTES);
            throw new BusinessException(AccountAuthEnum.USER_PASSWORD_ERROR.value(), AccountAuthEnum.USER_PASSWORD_ERROR.getMsg());
        }
    }
}