package com.melo.lzmall.auth.service.service.impl;

import com.melo.lzmall.auth.service.constants.AuthRedisKeyConstant;
import com.melo.lzmall.auth.service.entity.base.AccountBaseDTO;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import com.melo.lzmall.auth.service.entity.bo.LoginTokenDTO;
import com.melo.lzmall.auth.service.enums.AccountAuthEnum;
import com.melo.lzmall.auth.service.properties.AuthTokenProperties;
import com.melo.lzmall.auth.service.repository.AccountRepository;
import com.melo.lzmall.auth.service.service.TokenService;
import com.melo.lzmall.auth.service.service.converter.AccountServiceConverter;
import com.melo.lzmall.auth.service.utils.JWTUtil;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RedisHelper redisHelper;


    @Autowired
    private AuthTokenProperties authTokenProperties;

    @Autowired
    private AccountServiceConverter converter;

    @Override
    public AuthAccountBO checkToken(String accessToken, String requestClientType) {
        if(StringUtils.isBlank(accessToken)){
            throw new BusinessException(AccountAuthEnum.TOKEN_NOT_EXIST.value(), AccountAuthEnum.TOKEN_NOT_EXIST.getMsg());
        }
        String accountCode = JWTUtil.getAccountCodeFromToken(accessToken);
        Map<Object, Object> map = redisHelper.hGetAll(AuthRedisKeyConstant.TOKEN_KEY_PREFIX+accountCode);
        if(Objects.isNull(map)){
            throw new BusinessException(AccountAuthEnum.TOKEN_EXPIRED.value(), AccountAuthEnum.TOKEN_EXPIRED.getMsg());
        }
        Object cacheAccessToken = map.get(AuthRedisKeyConstant.ACCESS_TOKEN);
        if(Objects.isNull(cacheAccessToken)){
            throw new BusinessException(AccountAuthEnum.TOKEN_EXPIRED.value(), AccountAuthEnum.TOKEN_EXPIRED.getMsg());
        }
        AccountBaseDTO accountBaseDTO = accountRepository.getByAccountCodeAndAccountType(accountCode,requestClientType);
        if(Objects.isNull(accountBaseDTO)){
            throw new BusinessException(AccountAuthEnum.USER_NOT_EXIST.value(), AccountAuthEnum.USER_NOT_EXIST.getMsg());
        }
        if(!Objects.equals(accountBaseDTO.getStatus(),"ACTIVE")){
            throw new BusinessException(AccountAuthEnum.USER_DISABLE.value(), AccountAuthEnum.USER_DISABLE.getMsg());
        }
        return converter.convertToAuthAccountBO(accountBaseDTO);
    }

    @Override
    public LoginTokenDTO createToken(AuthAccountBO authAccountBO) {
        String token = JWTUtil.generateToken(authAccountBO);
        String refreshToken= JWTUtil.generateToken(authAccountBO);
        Map<String,Object> map = new HashMap<>();
        map.put(AuthRedisKeyConstant.ACCESS_TOKEN,token);
        map.put(AuthRedisKeyConstant.REFRESH_TOKEN,refreshToken);
        redisHelper.hSetAll(AuthRedisKeyConstant.TOKEN_KEY_PREFIX+authAccountBO.getAccountCode(),map);
        redisHelper.expire(AuthRedisKeyConstant.TOKEN_KEY_PREFIX+token,authTokenProperties.getExpireTime(), TimeUnit.MINUTES);
        return LoginTokenDTO.builder()
                .accessToken(token)
                .refreshToken(refreshToken)
                .expireTime(authTokenProperties.getExpireTime())
                .build();
    }
}