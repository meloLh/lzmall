package com.melo.lzmall.auth.service.utils;


import com.alibaba.fastjson.JSON;
import com.melo.lzmall.auth.service.entity.bo.AuthAccountBO;
import com.melo.lzmall.auth.service.enums.AccountAuthEnum;
import com.melo.lzmall.common.core.exception.BusinessException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

@Slf4j
public class JWTUtil {

    public static final String SECRET = "ukc8BDbRigUDaY6pZFfWus2jZWLPHO";

    /**
     * @Description 生成token
     * @author liuhu
     * @param authAccountBO
     * @return java.lang.String
     */
    public static String generateToken(AuthAccountBO authAccountBO) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("account", JSON.toJSONString(authAccountBO));
        return doGenerateToken(claims, authAccountBO.getAccountName());
    }

    private static String doGenerateToken(Map<String, Object> claims, String accountName) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(accountName)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(SECRET.getBytes()))
                .compact();
    }

    private static Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(Base64.getEncoder().encodeToString(SECRET.getBytes()))
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * @param token
     * @return com.salute.mall.auth.service.dto.auth.AuthUserEntity
     * @Description 获取用户信息
     * @author liuhu
     */
    public static String getAccountCodeFromToken(String token) {
        AuthAccountBO accountFromToken = getAuthAccountFromToken(token);
        return Objects.nonNull(accountFromToken) ? accountFromToken.getAccountCode():null;
    }

    public static AuthAccountBO getAuthAccountFromToken(String token) {
        try {
            String account = getAllClaimsFromToken(token).get("account").toString();
            if(StringUtils.isBlank(account)){
                throw new BusinessException(AccountAuthEnum.TOKEN_EXPIRED.value(), AccountAuthEnum.TOKEN_EXPIRED.getMsg());
            }
            return JSON.parseObject(account,AuthAccountBO.class);
        } catch (Exception e) {
            log.info("jwt解析token异常,token:{}",token,e);
            throw new BusinessException(AccountAuthEnum.TOKEN_EXPIRED.value(), AccountAuthEnum.TOKEN_EXPIRED.getMsg());
        }
    }
}
