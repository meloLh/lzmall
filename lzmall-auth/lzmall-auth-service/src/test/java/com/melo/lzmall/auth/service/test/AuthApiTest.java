package com.melo.lzmall.auth.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.auth.api.request.CheckTokenRequest;
import com.melo.lzmall.auth.api.response.AuthAccountResponse;
import com.melo.lzmall.auth.service.api.AuthApi;
import com.melo.lzmall.common.core.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class AuthApiTest extends AuthBaseTest {

    @Autowired
    private AuthApi authApi;

    @Test
    public void checkToken(){
        String aa= "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjMiLCJpYXQiOjE3MTQxMzc3MjgsImFjY291bnQiOiJ7XCJhY2NvdW50Q29kZVwiOlwiMTIzXCIsXCJhY2NvdW50TmFtZVwiOlwiTFpNQUxMXCIsXCJhY2NvdW50VHlwZVwiOlwiQ1VTVE9NXCIsXCJjbGllbnRUeXBlXCI6XCJDVVNUT01cIixcInJlYWxOYW1lXCI6XCJMWk1BTExcIn0ifQ.Q8YGnndJnMnegrm0s4SbHJkagpjwDfKImkO8a4VXF2mhKkPty__35z01EEPjXW8SpI272orn56q005-Xuo-aOw";
        CheckTokenRequest request = new CheckTokenRequest();
        request.setAccessToken(aa);
        request.setRequestClientType("CUSTOM");
        Result<AuthAccountResponse> result = authApi.checkToken(request);
        log.info(JSON.toJSONString(result));
    }
}
