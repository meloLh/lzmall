package com.melo.lzmall.auth.service.test;

import com.melo.lzmall.auth.service.AuthApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = AuthApplication.class)
@RunWith(SpringRunner.class)
public class AuthBaseTest {
}
