package com.melo.lzmall.auth.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.auth.api.request.UsernamePasswordLoginRequest;
import com.melo.lzmall.auth.api.response.UsernamePasswordLoginResponse;
import com.melo.lzmall.auth.service.controller.toc.LoginTocController;
import com.melo.lzmall.common.core.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class LoginTest extends AuthBaseTest{

    @Autowired
    private LoginTocController loginApi;


    @Test
    public void usernamePasswordLogin(){
        String encryptPassword = DigestUtils.md5Hex("LZMALL"+"_"+"LZMALL");
        log.info(JSON.toJSONString(encryptPassword));

        UsernamePasswordLoginRequest request = new UsernamePasswordLoginRequest();
        request.setUsername("LZMALL");
        request.setPassword("LZMALL");
        Result<UsernamePasswordLoginResponse> result = loginApi.usernamePasswordLogin(request);
        log.info(JSON.toJSONString(result));
    }
}
