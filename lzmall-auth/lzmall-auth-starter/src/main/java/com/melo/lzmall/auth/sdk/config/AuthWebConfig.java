package com.melo.lzmall.auth.sdk.config;

import com.melo.lzmall.auth.api.feign.AuthFeignClient;
import com.melo.lzmall.auth.sdk.interceptor.AuthInterceptor;
import com.melo.lzmall.auth.sdk.properties.AuthProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableConfigurationProperties(AuthProperties.class)
public class AuthWebConfig implements WebMvcConfigurer {

    @Autowired
    private AuthProperties authProperties;

    @Autowired
    @Lazy
    private AuthFeignClient authFeignClient;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor(authProperties,authFeignClient)).addPathPatterns("/**")
                .excludePathPatterns("/swagger-ui.html")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/swagger/**")
                .excludePathPatterns("/v2/api-docs")
                .excludePathPatterns("/swagger-resources/**");
    }
}