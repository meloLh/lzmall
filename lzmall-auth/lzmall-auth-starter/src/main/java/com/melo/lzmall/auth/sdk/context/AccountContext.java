package com.melo.lzmall.auth.sdk.context;

import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.common.core.utils.AssertUtil;

import java.util.Objects;

public class AccountContext {

    public static final ThreadLocal<AuthAccount> AUTH_ACCOUNT_THREAD_LOCAL = new ThreadLocal<>();

    public static void setAuthAccount(AuthAccount authAccount){
        AssertUtil.isTrue(Objects.nonNull(authAccount),"获取账户信息异常");
        AUTH_ACCOUNT_THREAD_LOCAL.set(authAccount);
    }

    public static AuthAccount getAuthAccount(){
        return AUTH_ACCOUNT_THREAD_LOCAL.get();
    }

    public static void remove(){
        if(Objects.nonNull(getAuthAccount())){
            AUTH_ACCOUNT_THREAD_LOCAL.remove();
        }
    }
}
