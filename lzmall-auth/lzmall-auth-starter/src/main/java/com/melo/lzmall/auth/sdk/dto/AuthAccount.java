package com.melo.lzmall.auth.sdk.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthAccount implements Serializable {

    /**
     * 账户类型
     */
    private String accountType;

    /**
     * 账户编号
     */
    private String accountCode;

    private String realName;

    private String nickName;

    /**
     * 账户名称
     */
    private String accountName;
}
