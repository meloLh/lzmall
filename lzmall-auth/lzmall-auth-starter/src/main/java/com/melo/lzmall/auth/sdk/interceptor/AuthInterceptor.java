package com.melo.lzmall.auth.sdk.interceptor;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.auth.api.feign.AuthFeignClient;
import com.melo.lzmall.auth.api.request.CheckTokenRequest;
import com.melo.lzmall.auth.api.response.AuthAccountResponse;
import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.auth.sdk.context.AccountContext;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.properties.AuthProperties;
import com.melo.lzmall.auth.sdk.utils.HttpResponseUtil;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *  @Description 权限校验
 *  @author liuhu
 *  @Date 2024/4/26
 */
@Slf4j
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private final AuthProperties authProperties;

    private final AuthFeignClient authFeignClient;

    private static final List<String> pageList = new ArrayList<>();
    static {
        pageList.add(".html");
    }

    public AuthInterceptor(AuthProperties authProperties,
                           AuthFeignClient authFeignClient){
        this.authProperties = authProperties;
        this.authFeignClient = authFeignClient;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.获取静态资源路径 白名单放行
        if(pageList.contains(request.getRequestURI())){
            if(isWhiteList(request.getRequestURI())){
                return true;
            }
            HttpResponseUtil.responseToWeb(response, HttpStatus.FORBIDDEN.value(),Result.success("403","请先登录"));
            return false;
        }
        //2.判断是否是方法维度的请求 不是则拒绝  静态资源需要白名单
        if (handler instanceof HandlerMethod) {
            //3.判断是否有权限校验注解 无则放行
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            MallAuth mallAuth = handlerMethod.getMethodAnnotation(MallAuth.class);
            if (Objects.isNull(mallAuth)) {
                return true;
            }
            //4.校验是否登录
            AuthAccount authAccount = new AuthAccount();
            String errorMsg = isAuthenticate(request, authAccount);
            if(StringUtils.isNotBlank(errorMsg)){
                HttpResponseUtil.responseToWeb(response, HttpStatus.FORBIDDEN.value(),Result.success("403","请先登录"));
                return false;
            }
            //5.校验是否授权
            if(!isAuthorize(request,authAccount,response)){
                HttpResponseUtil.responseToWeb(response, HttpStatus.FORBIDDEN.value(),Result.success("403","请先登录"));
                return false;
            }
            // 保存threadLocal
            AccountContext.setAuthAccount(authAccount);
            return true;
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AccountContext.remove();
    }

    private boolean isAuthorize(HttpServletRequest request,AuthAccount authAccount,HttpServletResponse response) {
        String clientType = request.getHeader("CLIENT_TYPE");
        // 如果是C端用户请求C端接口 则直接不校验接口权限
        if(Objects.equals(authAccount.getAccountType(),"CUSTOM") && Objects.equals(clientType,"C")){
             return true;
        }
        //
        return true;
    }

    private String isAuthenticate(HttpServletRequest request,
                                   AuthAccount authAccount) {
        String clientType = request.getHeader("Client_type");
        String token = request.getHeader("Accesstoken");
        if(StringUtils.isBlank(token) || StringUtils.isBlank(clientType)){
            return "请先登录";
        }
        // 校验token
        CheckTokenRequest checkTokenRequest = new CheckTokenRequest();
        checkTokenRequest.setAccessToken(token);
        checkTokenRequest.setRequestClientType(clientType);
        Result<AuthAccountResponse> result = null;
        try {
            result = authFeignClient.checkToken(checkTokenRequest);
            log.info("execute checkToken info,req:{},resp:{}", JSON.toJSONString(checkTokenRequest), JSON.toJSONString(result));
            if(Objects.isNull(result) || !result.getSuccess() || Objects.isNull(result.getResult())){
                log.info("execute checkToken error,req:{},resp:{}", JSON.toJSONString(checkTokenRequest), JSON.toJSONString(result));
                return result.getMessage();
            }
        } catch (Exception e) {
            log.info("execute checkToken error,req:{}", JSON.toJSONString(checkTokenRequest),e);
            return "服务开小差了，请稍后重试";
        }
        fillAuthAccount(result.getResult(),authAccount);
        return null;
    }

    private void fillAuthAccount(AuthAccountResponse response, AuthAccount authAccount) {
        authAccount.setAccountCode(response.getAccountCode());
        authAccount.setAccountName(response.getAccountName());
        authAccount.setRealName(response.getRealName());
        authAccount.setNickName(response.getNickName());
        authAccount.setAccountType(response.getAccountType());
    }

    private boolean isWhiteList(String uri) {
        if(StringUtils.isBlank(authProperties.getWhiteUrlList())){
            return false;
        }
        List<String> whiteUrlList = Arrays.stream(authProperties.getWhiteUrlList().split(",")).collect(Collectors.toList());
        AntPathMatcher pathMatcher = new AntPathMatcher();
        for (String whiteUrl : whiteUrlList) {
            if (pathMatcher.match(whiteUrl, uri)) {
                log.info("当前请求是白名单，whiteUrl：{},uri:{}",whiteUrl,uri);
                return true;
            }
        }
        return false;
    }
}
