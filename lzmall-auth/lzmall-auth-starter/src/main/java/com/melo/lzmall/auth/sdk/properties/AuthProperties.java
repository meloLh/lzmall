package com.melo.lzmall.auth.sdk.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;
@Data
@Configuration
@ConfigurationProperties(prefix = "mall.auth.config")
public class AuthProperties implements Serializable {

    /**白名单*/
    private String whiteUrlList;
}
