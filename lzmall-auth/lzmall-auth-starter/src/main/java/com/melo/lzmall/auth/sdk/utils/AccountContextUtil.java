package com.melo.lzmall.auth.sdk.utils;

import com.melo.lzmall.auth.sdk.context.AccountContext;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.common.core.utils.AssertUtil;

import java.util.Objects;

public class AccountContextUtil {

    public static AuthAccount getAccountInfoThrowE(){
        AuthAccount account = AccountContext.getAuthAccount();
        AssertUtil.isTrue(Objects.nonNull(account),"未获取到登录信息");
        return account;
    }

    public static AuthAccount getAccountInfo(){
        return AccountContext.getAuthAccount();
    }
}
