package com.melo.lzmall.common.core.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BaseModel implements Serializable {

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建人编号
     */
    private String creatorCode;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 更新人编号
     */
    private String modifierCode;

    /**
     * 更新时间
     */
    private Date modifiedTime;
}
