package com.melo.lzmall.common.core.dto;

import lombok.Data;

import java.util.List;

@Data
public class Page<T> {

    private Integer current;

    private Integer pageSize;

    private Integer pages;

    private Long total;

    private List<T> list;

    public Page() {
    }

    public Page(Integer current, Integer pageSize, Integer pages, Long total, List<T> list) {
        this.current = current;
        this.pageSize = pageSize;
        this.pages = pages;
        this.total = total;
        this.list = list;
    }
}
