package com.melo.lzmall.common.core.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageRequest implements Serializable {

    private Integer pageIndex = 1;

    private Integer pageSize = 10;

}
