package com.melo.lzmall.common.core.dto;


import com.melo.lzmall.common.core.enums.ResponseEnum;

import java.io.Serializable;


public class Result<T> implements Serializable {

    /**
     * 状态码
     */
    private String code;

    /**
     * 信息
     */
    private String message;

    /**
     * 数据
     */
    private T result;

    /**
     * 是否成功
     */
    private Boolean success;


    public Result() {
    }

    public static <T> Result<T> error(String statusCode, String message) {
        return new Result(false,message, (Object)null, statusCode);
    }

    public static <T> Result<T> error(String message) {
        return new Result(false,message, (Object)null, ResponseEnum.ERROR.value());
    }

    public static <T> Result<T> success(T data,String msg) {
        return new Result( true,msg, data, ResponseEnum.OK.value());
    }

    public static <T> Result<T> success(T data) {
        return new Result( true,ResponseEnum.OK.getMsg(), data, ResponseEnum.OK.value());
    }

    public static <T> Result<T> success() {
        return new Result(true, ResponseEnum.OK.getMsg(), (Object)null, ResponseEnum.OK.value());
    }


    public Result(boolean success, String message, T result, String statusCode) {
        this.success = success;
        this.message = message;
        this.result = result;
        this.code = statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
