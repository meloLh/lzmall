package com.melo.lzmall.common.core.enums;


public enum ResponseEnum {

	/**
	 * ok
	 */
	OK("200", "SUCCESS"),

	/**
	 * error
	 */
	ERROR("500", "业务异常")


	;

    private final String code;

	private final String msg;

	public String value() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	ResponseEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

}
