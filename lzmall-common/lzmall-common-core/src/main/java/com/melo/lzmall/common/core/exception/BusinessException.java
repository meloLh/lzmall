package com.melo.lzmall.common.core.exception;

import lombok.Data;

@Data
public class BusinessException extends RuntimeException{

    private String code;

    private String msg;

    public BusinessException(String code, String msg) {
        super(code + "=" + msg);
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(String code, String msg, Throwable e) {
        super(msg, e);
        this.code = code;
        this.msg = msg;
    }
}
