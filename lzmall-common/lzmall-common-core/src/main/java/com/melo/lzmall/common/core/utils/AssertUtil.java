package com.melo.lzmall.common.core.utils;


import com.melo.lzmall.common.core.enums.ResponseEnum;
import com.melo.lzmall.common.core.exception.BusinessException;

public class AssertUtil {

    public static void isTrue(boolean flag){
        if(!flag){
            throw new BusinessException(ResponseEnum.ERROR.value(), ResponseEnum.ERROR.getMsg());
        }
    }

    public static void isTrue(boolean flag,String msg){
        if(!flag){
            throw new BusinessException(ResponseEnum.ERROR.value(),msg);
        }
    }
}
