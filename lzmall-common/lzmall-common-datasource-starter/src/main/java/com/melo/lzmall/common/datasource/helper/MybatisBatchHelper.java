package com.melo.lzmall.common.datasource.helper;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.BatchResult;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

@Component
@Slf4j
public class MybatisBatchHelper {

   @Autowired
   private SqlSessionFactory sqlSessionFactory;

   /**
    * @Description mybatis 批量写入模式 注意 外层需要手动开始事务执行 (注意mysql 需要开启 rewriteBatchedStatements=true)
    * @author liuhu
    * @param dataList
    * @param mapperClazz
    * @param consumer
    * @date 2023/12/19 14:45
    * @return int
    */
    public <T,M>int batchSave(List<T> dataList, Class<M> mapperClazz, BiConsumer<T,M> consumer){
        if(CollectionUtils.isEmpty(dataList) || Objects.isNull(mapperClazz) || Objects.isNull(consumer)){
            return 0;
        }
        //开启batch模式 关闭自动提交
        try (SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false)) {
            M mapper = sqlSession.getMapper(mapperClazz);
            for (T data : dataList) {
                consumer.accept(data, mapper);
            }
            // 输出sql  batchResults 如果sql完全一样，只是值不一样则batchResults size为1
            // 如果sql 不一样 比如update table set entity where id = ? 如果entity中有属性为null则会导致sql不一样 则batchResults size会有多个
            List<BatchResult> batchResults = sqlSession.flushStatements();
            int batchUpdateCounts = batchResults.stream().flatMapToInt(v -> Arrays.stream(v.getUpdateCounts())).sum();
            // 非事务环境下强制commit，事务情况下该commit相当于无效
            sqlSession.commit(!TransactionSynchronizationManager.isSynchronizationActive());
            return batchUpdateCounts;
        }
    }
}
