package com.melo.lzmall.common.datasource.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.melo.lzmall.common.core.dto.Page;

import java.util.ArrayList;
import java.util.Objects;

public class PageUtil {

    public static Page emptyPage(Integer current, Integer pageSize){
         return new Page(current,pageSize,0,0L,new ArrayList<>());
    }

    public  static Page copyIPageNodata(IPage source){
        if(Objects.isNull(source)){
            return new Page(1,10,0,0L,new ArrayList<>());
        }
        Page page = new Page();
        page.setCurrent((int)source.getCurrent());
        page.setPages((int)source.getPages());
        page.setPageSize((int)source.getSize());
        page.setTotal(source.getTotal());
        return page;
    }

    public  static IPage copyPageNodata(Page source){
        if(Objects.isNull(source)){
            return new com.baomidou.mybatisplus.extension.plugins.pagination.Page(1,10,0);
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page page = new com.baomidou.mybatisplus.extension.plugins.pagination.Page();
        page.setCurrent(source.getCurrent());
        page.setPages(source.getPages());
        page.setTotal(source.getTotal());
        return page;
    }

    public  static Page copyPageData(Page source){
        if(Objects.isNull(source)){
            return new Page(1,10,0,0L,new ArrayList<>());
        }
        Page page = new Page();
        page.setCurrent((int)source.getCurrent());
        page.setPages((int)source.getPages());
        page.setTotal(source.getTotal());
        return page;
    }
}
