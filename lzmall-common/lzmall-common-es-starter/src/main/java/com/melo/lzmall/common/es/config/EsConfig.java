package com.melo.lzmall.common.es.config;

import com.melo.lzmall.common.es.helper.EsHelper;
import com.melo.lzmall.common.es.properties.EsProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@Slf4j
@EnableConfigurationProperties(value = EsProperties.class)
public class EsConfig {

    @Autowired
    private EsProperties esProperties;

    @Bean
    public RestHighLevelClient elasticsearchClient() {
        HttpHost[] hosts = Arrays.stream(esProperties.getHosts().split(",")).map(HttpHost::create).toArray(HttpHost[]::new);
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        if(StringUtils.isNotBlank(esProperties.getPassword())){
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(esProperties.getUsername(), esProperties.getPassword()));
        }
        RestClientBuilder builder = RestClient.builder(hosts).setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
        return new RestHighLevelClient(builder);
    }

    @Bean
    public EsHelper esHelper(){
        return new EsHelper();
    }
}
