package com.melo.lzmall.common.es.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 *  @Description es分页通用参数
 *  @author liuhu
 *  @Date 2022/9/5 13:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EsPageCommonParamDTO {

    /**索引名称*/
    private String indexName;

    private Integer pageIndex = 1;

    private Integer pageSize = 20;

    private List<EsSortParamDTO> esSortParamList;
}
