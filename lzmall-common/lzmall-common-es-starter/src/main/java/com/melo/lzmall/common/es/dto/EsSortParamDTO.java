package com.melo.lzmall.common.es.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  @Description es分页排序参数
 *  @author liuhu
 *  @Date 2022/9/5 13:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EsSortParamDTO {

    private String column;

    private Boolean desc;
}
