package com.melo.lzmall.common.es.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.es.dto.EsPageCommonParamDTO;
import com.melo.lzmall.common.es.dto.EsSortParamDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.alias.Alias;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.ParsedSum;
import org.elasticsearch.search.aggregations.metrics.SumAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 *  @Description es帮助类
 *  @author liuhu
 *  @Date 2022/9/5 16:41
 */
@Slf4j
public class EsHelper {


    @Autowired
    private RestHighLevelClient restHighLevelClient;


    public <T> Page<T> queryPage(BoolQueryBuilder queryBuilder, EsPageCommonParamDTO esPageCommonParam, Class<T> target){
        validPageParam(queryBuilder,esPageCommonParam,target);
        SearchSourceBuilder sourceBuilder = buildSearchSourceBuilder(queryBuilder, esPageCommonParam);
        log.info("execute queryPage query es doc info,sourceBuilder:{}", sourceBuilder.toString());
        SearchRequest searchRequest = new SearchRequest(esPageCommonParam.getIndexName());
        searchRequest.source(sourceBuilder);
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            List<T> dataList = Arrays.stream(searchHits).map(v -> JSONObject.parseObject(v.getSourceAsString(), target)).collect(Collectors.toList());
            long total = searchResponse.getHits().getTotalHits().value;
            int pageCount;
            if (total % esPageCommonParam.getPageSize() == 0) {
                pageCount = (int) (total / esPageCommonParam.getPageSize());
            } else {
                pageCount = (int) (total / esPageCommonParam.getPageSize() + 1);
            }
            return new Page<>(esPageCommonParam.getPageSize(), esPageCommonParam.getPageIndex(), pageCount,total,dataList);
        } catch (Exception e) {
            log.error("execute queryPage query es doc error,request:{}", queryBuilder.toString(),e);
            return buildEmptyPage(esPageCommonParam);
        }
    }


    public <T>List<T> queryList(BoolQueryBuilder queryBuilder, EsPageCommonParamDTO esPageCommonParam, Class<T> target){
        validPageParam(queryBuilder,esPageCommonParam,target);
        SearchSourceBuilder sourceBuilder = buildSearchSourceBuilder(queryBuilder, esPageCommonParam);
        log.info("execute queryList query es doc info,sourceBuilder:{}", sourceBuilder.toString());
        SearchRequest searchRequest = new SearchRequest(esPageCommonParam.getIndexName());
        searchRequest.source(sourceBuilder);
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            return Arrays.stream(searchHits).map(v -> JSONObject.parseObject(v.getSourceAsString(), target)).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("execute queryPage query es doc error,request:{}", queryBuilder.toString(),e);
            return new ArrayList<>();
        }
    }


    public Long count(BoolQueryBuilder queryBuilder, String indexName){
         AssertUtil.isTrue(Objects.nonNull(queryBuilder) && StringUtils.isNotBlank(indexName),"参数错误");
        try {
            log.info("execute count query es doc info,queryBuilder:{}", queryBuilder.toString());
            CountRequest countRequest = new CountRequest(indexName);
            countRequest.query(queryBuilder);
            CountResponse countResponse = restHighLevelClient.count(countRequest, RequestOptions.DEFAULT);
            return countResponse.getCount();
        } catch (Exception e) {
            log.error("execute count query es doc error,queryBuilder:{}", queryBuilder.toString(),e);
            return 0L;
        }
    }


    public Map<String,Object> sum(BoolQueryBuilder queryBuilder,List<String> keyList,String indexName){
        Map<String,Object> map = new HashMap<>();
        try {
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.query(queryBuilder);
            for (String key : keyList) {
                SumAggregationBuilder aggregationBuilder = AggregationBuilders.sum(key).field(key);
                sourceBuilder.aggregation(aggregationBuilder);
            }
            log.info("execute count query es doc info,sourceBuilder:{}", sourceBuilder.toString());
            SearchRequest searchRequest = new SearchRequest(indexName);
            searchRequest.source(sourceBuilder);
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            Map<String, Aggregation> aggregationMap = searchResponse.getAggregations().asMap();
            for (String key : keyList) {
                 ParsedSum parsedSum = (ParsedSum) aggregationMap.get(key);
                 if(Objects.nonNull(parsedSum)){
                     map.put(key,(long)parsedSum.getValue());
                 }
            }
            return map;
        } catch (Exception e) {
            log.error("execute count query es doc error,queryBuilder:{}", queryBuilder.toString(),e);
            return new HashMap<>();
        }
    }



    private  <T> Page<T> buildEmptyPage(EsPageCommonParamDTO esPageCommonParam){
        return new Page<T>(esPageCommonParam.getPageIndex(), esPageCommonParam.getPageSize(), 0,0L,new ArrayList<>());
    }


    private SearchSourceBuilder buildSearchSourceBuilder(BoolQueryBuilder queryBuilder, EsPageCommonParamDTO esPageCommonParam) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //跟踪真实命中数量
        sourceBuilder.trackTotalHits(true);
        sourceBuilder.query(queryBuilder);
        //1.分页参数 ES是从第0页开始的
        sourceBuilder.from((esPageCommonParam.getPageIndex() - 1) * esPageCommonParam.getPageSize())
                     .size(esPageCommonParam.getPageSize());
        // 构建排序值
        if(CollectionUtils.isNotEmpty(esPageCommonParam.getEsSortParamList())){
            for (EsSortParamDTO esSortParamDTO : esPageCommonParam.getEsSortParamList()) {
                sourceBuilder.sort(esSortParamDTO.getColumn(),Objects.equals(esSortParamDTO.getDesc(),Boolean.TRUE)? SortOrder.DESC:SortOrder.ASC);
            }
        }
        return sourceBuilder;
    }





    private <T> void validPageParam(BoolQueryBuilder queryBuilder, EsPageCommonParamDTO esPageCommonParam, Class<T> target) {
        AssertUtil.isTrue(Objects.nonNull(queryBuilder) && Objects.nonNull(esPageCommonParam) && Objects.nonNull(target) &&
                StringUtils.isNotBlank(esPageCommonParam.getIndexName()),"参数异常");
    }

    public void bulk(BulkRequest bulkRequest) {
        try {
            // 执行批量操作
            restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("批量写入ES异常，req:{}", JSON.toJSONString(bulkRequest), e);
        }
    }

    public void upsert(String indexName,String id,String dataJson){

        IndexRequest indexRequest = new IndexRequest(indexName)
                .id(id)
                .source(dataJson, XContentType.JSON);

        UpdateRequest updateRequest = new UpdateRequest(indexName, id)
                .doc(dataJson, XContentType.JSON)
                .upsert(indexRequest);

        log.info("execute upsert info, indexName:{},dataJson:{}", indexName,dataJson);
        try {
            UpdateResponse updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            log.error("execute upsert error，indexName:{},dataJson:{}", indexName, dataJson,e);
            throw new BusinessException("500","新增文档异常"+indexName);
        }
    }


    /**
     * @Description 创建索引  写入使用索引名写入  查询使用别名  这样可以支持多个索引挂一个别名 比如 product_2012  product_2013
     * @author liuhu
     * @param indexName
     * @param alias
     * @param mapping
     * @date 2024/6/14 17:25
     * @return void
     */
    public void createIndex(String indexName,String alias ,String mapping){
        // 1.创建Request对象
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        buildSetting(request);
        // 2.准备请求的参数：DSL语句
        request.source(mapping, XContentType.JSON);
        // 设置索引别名
        request.alias(new Alias(alias));
        // 3.发送请求
        try {
            CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
            if(!createIndexResponse.isAcknowledged()){
                throw new BusinessException("500","创建索引异常"+indexName);
            }
        } catch (Exception e) {
            log.error("创建ES索引异常，indexName:{},mapping:{}", mapping, e);
            throw new BusinessException("500","创建索引异常"+indexName);
        }
    }

    /**
     * 判断索引是否存在
     * @param indexName 索引名称
     * @return boolean
     * @throws
     */
    public boolean indexExist(String indexName)  {
        GetIndexRequest request = new GetIndexRequest(indexName);
        request.local(false);
        request.humanReadable(true);
        request.includeDefaults(false);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());
        try {
            return restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new BusinessException("500","创建索引异常"+indexName);
        }
    }

    /**
     * 设置3分片 2副本  刷盘时间1s
     * @param request
     * @return
     */
    public void buildSetting(CreateIndexRequest request){
        request.settings(Settings.builder().put("index.number_of_shards","3")
                .put("index.number_of_replicas","2").put("refresh_interval","1s"));
    }


}
