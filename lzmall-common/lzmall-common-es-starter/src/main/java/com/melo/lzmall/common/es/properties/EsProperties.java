package com.melo.lzmall.common.es.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "lzmall.es.config")
public class EsProperties {

    private String hosts;

    private String username;

    private String password;
}
