package com.melo.lzmall.common.framework.aspect;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
@Aspect
public class LogAspect {

    @Pointcut("@annotation(com.melo.lzmall.common.framework.annotation.SystemLog)")
    public void pointcut(){

    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
//        String className = joinPoint.getTarget().getClass().getName();
//        MethodSignature methodSignature =  (MethodSignature)joinPoint.getSignature();
//        Method method = methodSignature.getMethod();
//        long startTime = System.currentTimeMillis();
//        Map<String, Object> map = getNameAndValue(joinPoint);
//        log.info("\n"+"======================= 服务调用开始 ============= "+ "\n"+
//                "类：{}，方法：{}" + "\n"+
//                "入参：{}"+ "\n"+
//                 "================================================= "
//                ,className,method.getName(), JSON.toJSONString(map));
        Object proceed = joinPoint.proceed();
//        log.info("\n"+"======================= 服务调用结束 ============= "+ "\n"+
//                        "类：{},方法：{}" + "\n"+
//                        "入参：{}" + "\n"+
//                        "返回值：{}" + "\n"+
//                        "花费时间：{} "+ "\n"+
//                        "================================================= ",className,method.getName(),
//                JSON.toJSONString(map),JSON.toJSONString(proceed),System.currentTimeMillis()-startTime);
        return proceed;
    }


    Map<String, Object> getNameAndValue(ProceedingJoinPoint joinPoint) {
        Map<String, Object> param = new HashMap<>();
        Object[] paramValues = joinPoint.getArgs();
        String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
        if(Objects.isNull(paramNames)){
            return Maps.newHashMap();
        }
        for (int i = 0; i < paramNames.length; i++) {
            param.put(paramNames[i], paramValues[i]);
        }
        return param;
    }
}
