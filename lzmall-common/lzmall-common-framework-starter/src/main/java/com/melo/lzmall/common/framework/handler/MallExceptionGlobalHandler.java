package com.melo.lzmall.common.framework.handler;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class MallExceptionGlobalHandler {


    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result<Object> handleValid(ConstraintViolationException e){
        log.info("参数校验异常",e);
        return Result.error("400",e.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining("; ")));
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result<Object> handleValid(MethodArgumentNotValidException e){
        log.info("参数校验异常",e);
        return Result.error("400",e.getBindingResult().getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining("; ")));
    }

    @ExceptionHandler(value = BindException.class)
    public Result<Object> handleValid(BindException e){
        log.info("参数校验异常",e);
        return Result.error("400",e.getBindingResult().getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining("; ")));
    }

    @ExceptionHandler(value = BusinessException.class)
    public Result<Object> handleException(BusinessException e){
        log.info("业务异常",e);
        return Result.error(e.getCode(),e.getMsg());
    }

    @ExceptionHandler(value = Exception.class)
    public Result<Object> handleException(Exception e){
        log.info("系统异常",e);
        return Result.error("服务开小差了，请稍后重试");
    }
}
