//package com.melo.lzmall.marketing.api.fallback;
//
//import com.melo.lzmall.marketing.api.feign.MarketingDiscountTocFeignClient;
//import feign.hystrix.FallbackFactory;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Component
//public class MarketingDiscountTocFeignFallbackFactory implements FallbackFactory<MarketingDiscountTocFeignClient> {
//    @Override
//    public MarketingDiscountTocFeignClient create(Throwable throwable) {
//        log.error("execute marketingDiscountTocFeignFallbackFactory error",throwable);
//        return null;
//    }
//}