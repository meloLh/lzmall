package com.melo.lzmall.marketing.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponCenterRequest;
import com.melo.lzmall.marketing.api.request.coupon.UsedCouponInstanceRequest;
import com.melo.lzmall.marketing.api.response.coupon.CouponInstanceResponse;
import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
@FeignClient(value = "lzmall-marketing",contextId = "lzmall-marketing-couponInfoFeignClient")
public interface CouponTocFeignClient {

    @GetMapping("/api/marketing/getCouponInstanceByInstanceCode")
    @ApiOperation("通过优惠券实例编号获取实例信息")
    Result<CouponInstanceResponse> getCouponInstanceByInstanceCode(@RequestParam("instanceCode") String instanceCode);

    @PostMapping("/api/marketing/usedCouponInstance")
    @ApiOperation("使用优惠券")
    Result<Void> usedCouponInstance(@Validated @RequestBody UsedCouponInstanceRequest request);
}

