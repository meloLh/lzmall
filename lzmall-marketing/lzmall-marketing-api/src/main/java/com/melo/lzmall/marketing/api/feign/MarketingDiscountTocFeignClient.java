package com.melo.lzmall.marketing.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(value = "lzmall-marketing",contextId = "lzmall-marketing-marketingDiscountTocFeignClient")
@Validated
public interface MarketingDiscountTocFeignClient {

    @PostMapping("/api/marketing/submitOrder")
    @ApiOperation("提交订单计算")
    Result<SubmitOrderResponse> submitOrder(@Validated @RequestBody SubmitOrderRequest request);

    @PostMapping("/api//marketing/settleCart")
    @ApiOperation("购物车去结算")
    Result<MarketingSettleCartResponse> settleCart(@Validated @RequestBody SettleCartRequest request);
}
