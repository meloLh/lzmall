package com.melo.lzmall.marketing.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@FeignClient(value = "lzmall-marketing",contextId = "lzmall-marketing-marketingIdentifyTocFeignClient")
public interface MarketingIdentifyTocFeignClient {

    @RequestMapping("/api/marketing/querySkuMarketingList")
    @ApiOperation("商品集合查询参与的营销信息")
    Result<SkuMarketingResponse> querySkuMarketingList(@RequestBody QuerySkuMarketingRequest request);
}
