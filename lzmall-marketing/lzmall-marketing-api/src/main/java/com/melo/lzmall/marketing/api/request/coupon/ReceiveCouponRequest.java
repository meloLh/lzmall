package com.melo.lzmall.marketing.api.request.coupon;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
@Data
public class ReceiveCouponRequest implements Serializable {

    @NotBlank
    private String activityCode;
}
