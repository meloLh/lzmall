package com.melo.lzmall.marketing.api.request.coupon;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
@Data
public class UsedCouponInstanceRequest implements Serializable {

    @NotBlank
    private String instanceCode;

    private String accountName;

    private String bizCode;

    @NotBlank
    private String accountCode;
}
