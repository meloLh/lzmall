package com.melo.lzmall.marketing.api.request.discount;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SettleCartRequest implements Serializable {
    @NotBlank(message = "账号不能为空")
    private String accountCode;

    private String accountName;

    /**选中得优惠券实例编号*/
    private String instanceCode;

    @NotEmpty
    private List<SettleCartSkuRequest> skuList;
}
