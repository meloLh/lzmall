package com.melo.lzmall.marketing.api.request.discount;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class SubmitOrderRequest implements Serializable {

    @NotBlank
    private String accountCode;

    private String accountName;

    @NotBlank
    private String couponInstanceCode;

    @NotEmpty
    private List<SubmitOrderSkuRequest>  skuList;
}
