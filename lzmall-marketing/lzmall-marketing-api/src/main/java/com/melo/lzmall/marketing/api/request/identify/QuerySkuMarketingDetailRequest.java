package com.melo.lzmall.marketing.api.request.identify;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class QuerySkuMarketingDetailRequest implements Serializable {

    @NotBlank
    private String shopCode;

    private String shopName;

    @NotNull
    private String skuCode;

    @NotBlank
    private String categoryCode3;
}
