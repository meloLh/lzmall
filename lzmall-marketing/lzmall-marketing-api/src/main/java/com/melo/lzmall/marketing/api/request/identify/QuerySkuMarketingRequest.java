package com.melo.lzmall.marketing.api.request.identify;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class QuerySkuMarketingRequest implements Serializable {

    @NotBlank
    private String accountCode;

    private String accountName;

    @NotEmpty
    private List<QuerySkuMarketingDetailRequest> skuList;
}
