package com.melo.lzmall.marketing.api.request.identify;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SkuCanJoinMarketingRequest implements Serializable {

    @NotBlank
    private String accountCode;

    private String accountName;

    @NotBlank
    private String shopCode;

    private String shopName;

    @NotNull
    private String skuCode;

    @NotBlank
    private String categoryCode3;
}
