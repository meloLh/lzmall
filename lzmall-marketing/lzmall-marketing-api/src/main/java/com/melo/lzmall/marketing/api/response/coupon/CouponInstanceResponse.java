package com.melo.lzmall.marketing.api.response.coupon;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhu
 * @Description 优惠券实例
 * @Date 2024/1/3 17:08
 */
@Data
public class CouponInstanceResponse implements Serializable {

    private Long id;

    /**
     * 活动编号
     */
    private String activityCode;

    /**
     * 实例编号
     */
    private String instanceCode;

    /**
     * 活动编号
     */
    private String instanceName;

    /**
     * 状态  未使用 冻结中 已使用  已失效
     */
    private String status;

    /**
     * 使用时间
     */
    private Date useTime;

    /**
     * 关联单号  那笔订单使用
     */
    private String bizNo;

    /**
     * 关联单号  那个场景发放
     */
    private String sendBizNo;

    /**
     * 优惠券生效开始时间
     */
    private Date startTime;

    /**
     * 优惠券生效结束时间
     */
    private Date endTime;

    /**
     * 所属人
     */
    private String userId;

    private String userName;
}
