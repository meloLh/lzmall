package com.melo.lzmall.marketing.api.response.coupon;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ReceiveCouponCenterResponse implements Serializable {

    private List<CouponActivityBaseResponse>  canReceiveCouponActivityList;

    private List<CouponActivityBaseResponse>  cantReceiveCouponActivityList;
}
