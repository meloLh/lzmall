package com.melo.lzmall.marketing.api.response.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class MarketingSettleCartResponse implements Serializable {

    /**
     * 可用优惠券
     */
    private List<SettleCartCouponResponse> availableCouponInstanceList;

    private List<SettleCartCouponResponse> disableCouponInstanceList;

}
