package com.melo.lzmall.marketing.api.response.discount;

import lombok.Data;

import java.io.Serializable;

@Data
public class SettleCartDiscountResponse implements Serializable {

    /**订单编号*/
    private String orderCode;
}
