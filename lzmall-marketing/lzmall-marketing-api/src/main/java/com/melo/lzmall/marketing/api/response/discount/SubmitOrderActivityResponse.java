package com.melo.lzmall.marketing.api.response.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SubmitOrderActivityResponse implements Serializable {

    /**订单编号*/
    private Long id;

    /**
     * 活动编号
     */
    private String activityCode;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 活动类型
     */
    private String activityType;
}
