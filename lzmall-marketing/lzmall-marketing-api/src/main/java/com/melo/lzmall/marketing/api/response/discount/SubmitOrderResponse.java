package com.melo.lzmall.marketing.api.response.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SubmitOrderResponse implements Serializable {


    private String couponActivityCode;

    private String instanceCode;

    private String discountActivityCode;

    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;

    /**
     * 订单原价
     */
    private BigDecimal totalOriginAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal totalCouponPromotionAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal totalDiscountPromotionAmount;

    /**商品明细*/
    private List<SubmitOrderSkuCouponResponse> skuList;
}
