package com.melo.lzmall.marketing.api.response.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SubmitOrderSkuCouponResponse implements Serializable {

    private String skuCode;

    private String productName;

    /**
     * 原始销售价格
     */
    private BigDecimal salePrice;

    /**
     * 购买数量
     */
    private BigDecimal purchaseNum;

    /**
     * 商品原价总和
     */
    private BigDecimal totalOriginAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal couponPromotionAmount;
}
