package com.melo.lzmall.marketing.api.response.identify;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SkuMarketingActivityResponse implements Serializable {

    private String skuCode;

    /**
     * 商品可用券列表(1、按优惠券活动到期时间正序，如果到期时间一致，按照创建时间正序排列 ;)
     **/
    private List<SkuMarketingDetailResponse> activityList;
}
