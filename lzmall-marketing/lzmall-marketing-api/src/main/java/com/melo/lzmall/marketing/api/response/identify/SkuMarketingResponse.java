package com.melo.lzmall.marketing.api.response.identify;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SkuMarketingResponse implements Serializable {

    private List<SkuMarketingDetailResponse> disableCouponActivityList;

    private List<SkuMarketingDetailResponse> availableCouponActivityList;

    private List<SkuMarketingActivityResponse> skuActivityList;
}
