package com.melo.lzmall.marketing.service;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.melo.lzmall.marketing.service.mapper")
@EnableFeignClients(basePackages = {"com.melo.lzmall.*.api.feign"})
public class MarketingApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketingApplication.class,args);
    }
}
