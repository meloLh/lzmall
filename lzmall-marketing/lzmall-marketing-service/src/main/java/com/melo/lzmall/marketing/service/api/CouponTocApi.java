package com.melo.lzmall.marketing.service.api;



import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.feign.CouponTocFeignClient;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponCenterRequest;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponRequest;
import com.melo.lzmall.marketing.api.request.coupon.UsedCouponInstanceRequest;
import com.melo.lzmall.marketing.api.response.coupon.CouponInstanceResponse;
import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import com.melo.lzmall.marketing.service.convert.CouponConvert;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponBO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponCenterBO;
import com.melo.lzmall.marketing.service.service.CouponInstanceService;
import com.melo.lzmall.marketing.service.service.CouponService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping
public class CouponTocApi implements CouponTocFeignClient {

    @Autowired
    private CouponInstanceService couponInstanceService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private CouponConvert convert;


    @GetMapping("/api/marketing/getCouponInstanceByInstanceCode")
    @ApiOperation("通过优惠券实例编号获取实例信息")
    public Result<CouponInstanceResponse> getCouponInstanceByInstanceCode(@RequestParam("instanceCode") String instanceCode) {
//        CouponActivityBaseDTO couponActivityBaseDTO = couponInstanceService.getCouponInstanceByInstanceCode(instanceCode);
//        CouponInstanceResponse response = convert.convertToCouponInstanceResponse(couponActivityBaseDTO);
//        return Result.success(response);
        return null;
    }

    @PostMapping("/api/marketing/usedCouponInstance")
    @ApiOperation("使用优惠券")
    public Result<Void> usedCouponInstance(@Validated @RequestBody UsedCouponInstanceRequest request) {
        couponService.usedCouponInstance(request.getInstanceCode(),request.getAccountCode(),request.getBizCode());
        return Result.success();
    }
}
