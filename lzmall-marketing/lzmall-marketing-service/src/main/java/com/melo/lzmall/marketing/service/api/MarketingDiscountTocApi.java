package com.melo.lzmall.marketing.service.api;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.framework.annotation.SystemLog;
import com.melo.lzmall.marketing.api.feign.MarketingDiscountTocFeignClient;
import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import com.melo.lzmall.marketing.service.convert.DiscountConvert;
import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartDTO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SettleCartBO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SubmitOrderInputBO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO;
import com.melo.lzmall.marketing.service.service.DiscountService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping
public class MarketingDiscountTocApi implements MarketingDiscountTocFeignClient{

    @Autowired
    private DiscountConvert convert;

    @Autowired
    private DiscountService discountService;


    @PostMapping("/api/marketing/settleCart")
    @ApiOperation("购物车去结算")
    @SystemLog
    public Result<MarketingSettleCartResponse> settleCart(@Validated @RequestBody SettleCartRequest request){
        SettleCartBO settleCartBO = convert.convertToSettleCartBO(request);
        SettleCartDTO settleCartDTO = discountService.settleCart(settleCartBO);
        MarketingSettleCartResponse response = convert.convertToSettleCartResponse(settleCartDTO);
        return Result.success(response);
    }

    @PostMapping("/api/marketing/submitOrder")
    @ApiOperation("提交订单计算")
    @SystemLog
    public Result<SubmitOrderResponse> submitOrder(@Validated @RequestBody SubmitOrderRequest request){
        SubmitOrderInputBO submitOrderInputBO = convert.convertToSubmitOrderInputBO(request);
        MarketingCouponDiscountDTO discountDTO = discountService.submitOrder(submitOrderInputBO);
        SubmitOrderResponse response = convert.convertToSubmitOrderResponse(discountDTO);
        return Result.success(response);
    }



}
