package com.melo.lzmall.marketing.service.api;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.framework.annotation.SystemLog;
import com.melo.lzmall.marketing.api.feign.MarketingIdentifyTocFeignClient;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import com.melo.lzmall.marketing.service.convert.MarketingIdentifyFaceConvert;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyBO;
import com.melo.lzmall.marketing.service.service.MarketingIdentifyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping
public class MarketingIdentifyTocApi implements MarketingIdentifyTocFeignClient {

    @Autowired
    private MarketingIdentifyService marketingIdentifyService;

    @Autowired
    private MarketingIdentifyFaceConvert identifyConvert;

    @RequestMapping("/api/marketing/querySkuMarketingList")
    @ApiOperation("商品集合查询参与的营销信息")
    @SystemLog
    public Result<SkuMarketingResponse> querySkuMarketingList(@RequestBody QuerySkuMarketingRequest request){
        SkuPreferentialIdentifyBO skuPreferentialIdentifyBO = identifyConvert.convertToSkuPreferentialIdentifyBO(request);
        SkuMarketingIdentifyBO skuMarketingIdentifyBO = marketingIdentifyService.querySkuMarketingInfo(skuPreferentialIdentifyBO);
        SkuMarketingResponse response = identifyConvert.convertToSkuMarketingResponse(skuMarketingIdentifyBO);
        return Result.success(response);
    }
}
