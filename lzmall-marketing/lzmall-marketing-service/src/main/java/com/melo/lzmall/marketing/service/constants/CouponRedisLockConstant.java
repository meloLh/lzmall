package com.melo.lzmall.marketing.service.constants;

public class CouponRedisLockConstant {

    private static final String LZMALL_MARKETING_KEY_PREFIX = "LZMALL_MARKETING_LOCK:";

    public static final String USE_COUPON_INSTANCE_LOCK_KEY = LZMALL_MARKETING_KEY_PREFIX+"COUPON_INSTANCE_";

    public static final String USE_COUPON__LOCK_KEY = LZMALL_MARKETING_KEY_PREFIX+"COUPON_ACTIVITY_";
}
