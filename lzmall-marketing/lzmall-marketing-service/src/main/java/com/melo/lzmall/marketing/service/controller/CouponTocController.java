package com.melo.lzmall.marketing.service.controller;



import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.feign.CouponTocFeignClient;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponCenterRequest;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponRequest;
import com.melo.lzmall.marketing.api.response.coupon.CouponInstanceResponse;
import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import com.melo.lzmall.marketing.service.convert.CouponConvert;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponBO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponCenterBO;
import com.melo.lzmall.marketing.service.service.CouponInstanceService;
import com.melo.lzmall.marketing.service.service.CouponService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping
public class CouponTocController{

    @Autowired
    private CouponInstanceService couponInstanceService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private CouponConvert convert;


    @GetMapping("/buyer/marketing/queryReceiveCouponCenter")
    @ApiOperation("领券中心")
    @MallAuth
    public Result<ReceiveCouponCenterResponse> queryReceiveCouponCenter(ReceiveCouponCenterRequest request) {
        ReceiveCouponCenterBO receiveCouponCenterBO = convert.convertToReceiveCouponCenterBO(request);
        ReceiveCouponCenterResponse response = couponService.queryReceiveCouponCenter(receiveCouponCenterBO);
        return Result.success(response);
    }

    @GetMapping("/buyer/marketing/receiveCoupon")
    @ApiOperation("领券")
    @MallAuth
    public Result<Void> receiveCoupon(ReceiveCouponRequest request) {
        ReceiveCouponBO receiveCouponBO = convert.convertToReceiveCouponBO(request);
        couponService.receiveCoupon(receiveCouponBO);
        return Result.success();
    }
}
