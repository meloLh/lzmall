package com.melo.lzmall.marketing.service.convert;


import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponCenterRequest;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponRequest;
import com.melo.lzmall.marketing.api.response.coupon.CouponActivityBaseResponse;
import com.melo.lzmall.marketing.api.response.coupon.CouponInstanceResponse;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponBO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponCenterBO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CouponConvert {

    CouponInstanceResponse convertToCouponInstanceResponse(CouponActivityBaseDTO couponActivityBaseDTO);

    ReceiveCouponBO convertToReceiveCouponBO(ReceiveCouponRequest request);

    ReceiveCouponCenterBO convertToReceiveCouponCenterBO(ReceiveCouponCenterRequest request);

    List<CouponActivityBaseResponse> convertToCouponActivityBaseResponse(List<CouponActivityBaseDTO> canReceiveCouponActivityList);
}
