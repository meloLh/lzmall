package com.melo.lzmall.marketing.service.convert;


import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartCouponDTO;
import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponInstanceBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SettleCartBO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SubmitOrderInputBO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DiscountConvert {

    SubmitOrderResponse convertToSubmitOrderResponse(MarketingCouponDiscountDTO discountDTO);

    SettleCartBO convertToSettleCartBO(SettleCartRequest request);


    MarketingSettleCartResponse convertToSettleCartResponse(SettleCartDTO settleCartDTO);

    SubmitOrderInputBO convertToSubmitOrderInputBO(SubmitOrderRequest request);

    SettleCartCouponDTO convertToSettleCartCouponDTO(CouponActivityBaseDTO couponActivityBaseDTO);
}
