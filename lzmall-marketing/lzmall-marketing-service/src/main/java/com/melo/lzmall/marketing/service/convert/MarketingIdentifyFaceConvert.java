package com.melo.lzmall.marketing.service.convert;


import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.request.identify.SkuCanJoinMarketingRequest;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyBO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MarketingIdentifyFaceConvert {

    List<SkuPreferentialIdentifyBO> convertToSkuPreferentialIdentifyBOList(List<SkuCanJoinMarketingRequest> request);


    SkuPreferentialIdentifyBO convertToSkuPreferentialIdentifyBO(QuerySkuMarketingRequest request);

    SkuMarketingResponse convertToSkuMarketingResponse(SkuMarketingIdentifyBO skuMarketingIdentifyBO);
}
