package com.melo.lzmall.marketing.service.core;

import com.alibaba.fastjson.JSON;

import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.service.convert.DiscountConvert;
import com.melo.lzmall.marketing.service.core.context.discount.MarketingDiscountContext;
import com.melo.lzmall.marketing.service.core.context.discount.MarketingDiscountDetailContext;
import com.melo.lzmall.marketing.service.entity.base.CouponUseRuleBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.userule.SpecificCategoryCode3UseRuleJsonBO;
import com.melo.lzmall.marketing.service.entity.bo.userule.SpecificProductUserRuleJsonBO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountSkuDTO;
import com.melo.lzmall.marketing.service.enums.UseTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *  @Description 营销识别
 *  @author liuhu
 *  @Date 2024/1/4 14:02
 */
@Component
@Slf4j
public class MarketingDiscountCore {

    @Autowired
    private DiscountConvert convert;

    /**
     * @Description 计算优惠券实例的分摊金额
     * @author liuhu
     * @param marketingDiscountContext
     * @date 2024/6/7 17:16
     * @return com.melo.lzmall.marketing.service.entity.dto.discount.MarketingDiscountDTO
     */
    public MarketingCouponDiscountDTO calculateSkuDiscount(MarketingDiscountContext marketingDiscountContext) {
        AssertUtil.isTrue(Objects.nonNull(marketingDiscountContext) &&
                !StringUtils.isAnyBlank(marketingDiscountContext.getAccountCode(),marketingDiscountContext.getActivityCode(),marketingDiscountContext.getInstanceCode()),"参数异常");
        AssertUtil.isTrue(Objects.nonNull(marketingDiscountContext.getCouponUseRule()),"使用规则不存在");
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(marketingDiscountContext.getSkuList()),"商品信息不存在");

        List<MarketingCouponDiscountSkuDTO> marketingCouponDiscountSkuDTOS = calculateSkuCouponApportionAmount(marketingDiscountContext);
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(marketingCouponDiscountSkuDTOS),"计算分摊异常");

        MarketingCouponDiscountDTO marketingCouponDiscountDTO = new MarketingCouponDiscountDTO();
        marketingCouponDiscountDTO.setActivityCode(marketingDiscountContext.getActivityCode());
        marketingCouponDiscountDTO.setInstanceCode(marketingDiscountContext.getInstanceCode());
        marketingCouponDiscountDTO.setCouponAmount(marketingDiscountContext.getCouponAmount());
        BigDecimal totalOriginAmount = marketingCouponDiscountSkuDTOS.stream().map(MarketingCouponDiscountSkuDTO::getOriginAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalPromotionAmount = marketingCouponDiscountSkuDTOS.stream().map(MarketingCouponDiscountSkuDTO::getCouponPromotionAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

        marketingCouponDiscountDTO.setTotalDiscountPromotionAmount(BigDecimal.ZERO);
        marketingCouponDiscountDTO.setTotalOriginAmount(totalOriginAmount);
        marketingCouponDiscountDTO.setTotalCouponPromotionAmount(totalPromotionAmount);
        marketingCouponDiscountDTO.setSkuList(marketingCouponDiscountSkuDTOS);
        return marketingCouponDiscountDTO;
    }


    /**
     * @Description 计算商品分摊
     * @author liuhu
     * @param marketingDiscountContext
     * @date 2024/1/26 17:48
     * @return java.util.List<com.lz.mall.marketing.service.entity.dto.discount.MarketingSkuDiscountDTO>
     */
    private List<MarketingCouponDiscountSkuDTO> calculateSkuCouponApportionAmount(MarketingDiscountContext marketingDiscountContext) {
        CouponUseRuleBaseDTO couponUseRule = marketingDiscountContext.getCouponUseRule();
        UseTypeEnum useTypeEnum = UseTypeEnum.getByValue(couponUseRule.getUseType());
        AssertUtil.isTrue(Objects.nonNull(useTypeEnum), "暂不支持");
        List<MarketingCouponDiscountSkuDTO> list = calculateCouponApportionAmount(couponUseRule, marketingDiscountContext, useTypeEnum);
        Map<String, MarketingCouponDiscountSkuDTO> skuDiscountDTOMap = list.stream().collect(Collectors.toMap(MarketingCouponDiscountSkuDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));
        return marketingDiscountContext.getSkuList().stream().map(sku -> {
            MarketingCouponDiscountSkuDTO couponSkuDiscountDTO = skuDiscountDTOMap.get(sku.getSkuCode());
            if (Objects.isNull(couponSkuDiscountDTO)) {
                MarketingCouponDiscountSkuDTO dto = new MarketingCouponDiscountSkuDTO();
                dto.setSkuCode(sku.getSkuCode());
                dto.setPurchaseQty(sku.getPurchaseQty());
                dto.setSalePrice(sku.getSalePrice());
                dto.setProductName(sku.getProductName());
                dto.setOriginAmount(new BigDecimal(sku.getPurchaseQty()).multiply(sku.getSalePrice()));
                dto.setCouponPromotionAmount(BigDecimal.ZERO);
                return dto;
            }
            return couponSkuDiscountDTO;
        }).collect(Collectors.toList());
    }

    private List<MarketingCouponDiscountSkuDTO> calculateCouponApportionAmount(CouponUseRuleBaseDTO useRuleDO,
                                                                               MarketingDiscountContext marketingDiscountContext,
                                                                               UseTypeEnum useTypeEnum) {
        switch (useTypeEnum) {
            case ALL:
                // 所有商品参与分摊
                return calculateCouponApportionAmount(marketingDiscountContext.getSkuList(), marketingDiscountContext.getCouponAmount());
            case SPECIFIC_CATEGORY3:
                // 指定分类商品参与分摊
                List<SpecificCategoryCode3UseRuleJsonBO> specificCategoryCode3SendRuleJsonBOS = JSON.parseArray(useRuleDO.getRuleJson(), SpecificCategoryCode3UseRuleJsonBO.class);
                AssertUtil.isTrue(CollectionUtils.isNotEmpty(specificCategoryCode3SendRuleJsonBOS), "指定分类使用规则不存在");
                List<String> categoryCode3List = specificCategoryCode3SendRuleJsonBOS.stream().map(SpecificCategoryCode3UseRuleJsonBO::getSkuCode).collect(Collectors.toList());
                List<MarketingDiscountDetailContext> categoryMatchProductList = marketingDiscountContext.getSkuList().stream().filter(sku -> categoryCode3List.contains(sku.getCategoryCode3())).collect(Collectors.toList());
                return calculateCouponApportionAmount(categoryMatchProductList, marketingDiscountContext.getCouponAmount());
            case SPECIFIC_PRODUCT:
                // 指定商品参与分摊
                List<SpecificProductUserRuleJsonBO> specificProductSendRuleJsonBOS = JSON.parseArray(useRuleDO.getRuleJson(), SpecificProductUserRuleJsonBO.class);
                AssertUtil.isTrue(CollectionUtils.isNotEmpty(specificProductSendRuleJsonBOS), "指定商品使用规则不存在");
                List<String> skuIdList = specificProductSendRuleJsonBOS.stream().map(SpecificProductUserRuleJsonBO::getSkuCode).collect(Collectors.toList());
                List<MarketingDiscountDetailContext> skuList = marketingDiscountContext.getSkuList().stream().filter(sku -> skuIdList.contains(sku.getSkuCode())).collect(Collectors.toList());
                return calculateCouponApportionAmount(skuList, marketingDiscountContext.getCouponAmount());
            default:
                return Lists.newArrayList();
        }
    }

    /**
     * @Description 存在差异过大情况 导致分摊 不尽
     * @author liuhu
     * @param list
     * @date 2024/1/26 17:09
     * @return java.util.List<com.lz.mall.marketing.service.entity.dto.discount.MarketingSkuDiscountDTO>
     */
    private void reBalanceCouponDiscountAmount(List<MarketingCouponDiscountSkuDTO> list) {
        BigDecimal needReApportionAmount = list.stream().filter(sku -> sku.getCouponPromotionAmount().compareTo(sku.getOriginAmount()) > 0)
                .map(sku->sku.getCouponPromotionAmount().subtract(sku.getOriginAmount())).reduce(BigDecimal.ZERO, BigDecimal::add);

        if (needReApportionAmount.compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        for (int i = 0; i < list.size(); i++) {
            MarketingCouponDiscountSkuDTO dto = list.get(i);
            BigDecimal promotionAmount = dto.getCouponPromotionAmount();

            if (needReApportionAmount.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }

            if (promotionAmount.compareTo(dto.getOriginAmount()) > 0) {
                dto.setCouponPromotionAmount(dto.getOriginAmount());
                needReApportionAmount = needReApportionAmount.subtract(dto.getOriginAmount());
            }
        }
    }

    private List<MarketingCouponDiscountSkuDTO> calculateCouponApportionAmount(List<MarketingDiscountDetailContext> detailList,
                                                                               BigDecimal couponAmount) {
        BigDecimal orderOriginAmount = detailList.stream().map(v -> new BigDecimal(v.getPurchaseQty()).multiply(v.getSalePrice())).reduce(BigDecimal.ZERO, BigDecimal::add);
        // 订单可用优惠金额
        BigDecimal orderCanUseCouponAmount = orderOriginAmount.compareTo(couponAmount) > 0 ? couponAmount : orderOriginAmount;
        // 计算商品分摊金额
        List<MarketingCouponDiscountSkuDTO> skuDiscountDTOS = calculateProductApportionAmount(detailList, orderOriginAmount, orderCanUseCouponAmount);
        // 重平衡  商品金额差异太大  导致最后一笔分摊的金额 可能比他本身的金额还大 举例  A  200   B  1  C  1  优惠券 100  分摊 A 98 B 0 C 2 这样就会有问题
        reBalanceCouponDiscountAmount(skuDiscountDTOS);
        // 最终校验金额分摊
        reCheckCalculateCouponDiscountAmount(skuDiscountDTOS, couponAmount);
        return skuDiscountDTOS;
    }

    private void reCheckCalculateCouponDiscountAmount(List<MarketingCouponDiscountSkuDTO> skuDiscountDTOS, BigDecimal couponAmount) {
        BigDecimal totalCouponActivityAmount = skuDiscountDTOS.stream().map(MarketingCouponDiscountSkuDTO::getCouponPromotionAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        if(totalCouponActivityAmount.compareTo(couponAmount) > 0){
            throw new BusinessException("500","优惠券分摊金额异常");
        }
        for (int i = 0; i < skuDiscountDTOS.size(); i++) {
            MarketingCouponDiscountSkuDTO dto = skuDiscountDTOS.get(i);
            BigDecimal productCouponActivityAmount = dto.getCouponPromotionAmount();
            if (productCouponActivityAmount.compareTo(dto.getOriginAmount()) > 0) {
                throw new BusinessException("500","优惠券分摊金额异常");
            }
        }
    }

    /**
     * @Description 按照商品占比计算金额  截断2位小数  注意最后一笔用减法
     * @author liuhu
     * @param detailList
     * @param orderOriginAmount
     * @param orderCanUseCouponAmount
     * @date 2024/1/26 17:22
     * @return java.util.List<com.lz.mall.marketing.service.entity.dto.discount.MarketingSkuDiscountDTO>
     */
    private List<MarketingCouponDiscountSkuDTO> calculateProductApportionAmount(List<MarketingDiscountDetailContext> detailList,
                                                                                BigDecimal orderOriginAmount,
                                                                                BigDecimal orderCanUseCouponAmount) {

        List<MarketingCouponDiscountSkuDTO> list = new ArrayList<>();

        BigDecimal existAmount = BigDecimal.ZERO;

        for (int i = 0; i < detailList.size(); i++) {

            MarketingDiscountDetailContext sku = detailList.get(i);

            MarketingCouponDiscountSkuDTO dto = new MarketingCouponDiscountSkuDTO();

            BigDecimal productOriginAmount = new BigDecimal(sku.getPurchaseQty()).multiply(sku.getSalePrice());
            // 商品在整个订单中的比例
            BigDecimal proportion = productOriginAmount.divide(orderOriginAmount, 2, BigDecimal.ROUND_DOWN);

            // 最后一笔用减法  不然会分摊不均
            BigDecimal totalCouponActivityAmount;
            if(i == detailList.size() -1){
                totalCouponActivityAmount = orderCanUseCouponAmount.subtract(existAmount);
            }else {
                totalCouponActivityAmount = orderCanUseCouponAmount.multiply(proportion);
                existAmount = existAmount.add(totalCouponActivityAmount);
            }
            dto.setSkuCode(sku.getSkuCode());
            dto.setPurchaseQty(sku.getPurchaseQty());
            dto.setSalePrice(sku.getSalePrice());
            dto.setProductName(sku.getProductName());
            dto.setOriginAmount(productOriginAmount);
            dto.setCouponPromotionAmount(totalCouponActivityAmount);
            list.add(dto);
        }

        return list;
    }
}
