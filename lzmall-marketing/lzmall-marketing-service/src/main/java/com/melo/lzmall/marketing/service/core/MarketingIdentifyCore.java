package com.melo.lzmall.marketing.service.core;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.service.core.context.identity.MarketingIdentifyContext;
import com.melo.lzmall.marketing.service.core.context.identity.CouponInstanceIdentifyContext;
import com.melo.lzmall.marketing.service.core.context.identity.CouponInstanceIdentifySkuContext;
import com.melo.lzmall.marketing.service.core.context.identity.MatchUseRuleContext;
import com.melo.lzmall.marketing.service.entity.base.*;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyActivityBO;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyBO;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyDetailBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.OrderAmountMatchSendRuleJsonBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SpecificCategoryCode3SendRuleJsonBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SpecificProductSendRuleJsonBO;
import com.melo.lzmall.marketing.service.enums.SendTypeEnum;
import com.melo.lzmall.marketing.service.enums.UseTypeEnum;
import com.melo.lzmall.marketing.service.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *  @Description 营销识别
 *  @author liuhu
 *  @Date 2024/1/4 14:02
 */
@Component
@Slf4j
public class MarketingIdentifyCore {

    @Autowired
    private CouponActivityRepository couponActivityRepository;

    @Autowired
    private CouponStockRepository couponStockRepository;


    @Autowired
    private CouponInstanceRepository couponInstanceRepository;


    @Autowired
    private CouponSendRuleRepository sendRuleRepository;

    @Autowired
    private CouponUseRuleRepository couponUseRuleRepository;



    /**
     * @Description 识别商品可参与优惠券活动
     * @author liuhu
     * @param marketingIdentifyContext
     * @date 2024/1/4 14:08
     * @return void
     */
    public SkuMarketingIdentifyBO identifySkuAvailableCouponActivity(MarketingIdentifyContext marketingIdentifyContext){
        List<String> shopCodeList = marketingIdentifyContext.getSkuList().stream().map(CouponInstanceIdentifySkuContext::getShopCode).collect(Collectors.toList());
        // 1.识别基本状态是否满足
        List<CouponActivityBaseDTO> allCouponActivityBaseDTOList = couponActivityRepository.queryAvailableCouponActivity(shopCodeList);
        List<CouponActivityBaseDTO> couponActivityBaseDTOList = identifyAvailableCouponActivity(allCouponActivityBaseDTOList, marketingIdentifyContext.getAccountCode());
        //2.识别商品可用优惠券
        List<SkuMarketingIdentifyActivityBO> identifyActivityBOS = identifySkuUseRuleAvailableCouponActivity(couponActivityBaseDTOList, marketingIdentifyContext);
        SkuMarketingIdentifyBO skuMarketingIdentifyBO = new SkuMarketingIdentifyBO();
        skuMarketingIdentifyBO.setSkuActivityList(identifyActivityBOS);
        List<SkuMarketingIdentifyDetailBO> availableActivityList = identifyActivityBOS.stream().flatMap(v -> v.getActivityList().stream()).collect(Collectors.toList());
        skuMarketingIdentifyBO.setAvailableCouponActivityList(availableActivityList);
        List<String> availableActivityCodeList = availableActivityList.stream().map(SkuMarketingIdentifyDetailBO::getActivityCode).collect(Collectors.toList());
        //3.过滤不可使用优惠券活动
        List<CouponActivityBaseDTO> disableCouponActivity = couponActivityBaseDTOList.stream().filter(v -> !availableActivityCodeList.contains(v.getActivityCode())).collect(Collectors.toList());
        List<SkuMarketingIdentifyDetailBO> disableIdentifyDetail = buildSkuMarketingIdentifyDetailBO(disableCouponActivity);
        skuMarketingIdentifyBO.setDisableCouponActivityList(disableIdentifyDetail);
        return skuMarketingIdentifyBO;
    }

    private List<SkuMarketingIdentifyDetailBO> buildSkuMarketingIdentifyDetailBO(List<CouponActivityBaseDTO> disableCouponActivity) {
      return   disableCouponActivity.stream().map(couponActivity->{
            SkuMarketingIdentifyDetailBO detailDTO = new SkuMarketingIdentifyDetailBO();
            detailDTO.setActivityCode(couponActivity.getActivityCode());
            detailDTO.setActivityName(couponActivity.getActivityName());
            detailDTO.setStartTime(couponActivity.getStartTime());
            detailDTO.setEndTime(couponActivity.getEndTime());
            detailDTO.setCouponAmount(couponActivity.getCouponAmount());

            return detailDTO;
        }).collect(Collectors.toList());
    }

    /**
     * @Description 优惠券实例识别是否可用
     * @author liuhu
     * @param marketingIdentifyContext
     * @date 2024/6/7 16:47
     * @return java.util.List<com.melo.lzmall.marketing.service.entity.base.CouponInstanceBaseDTO>
     */
    public boolean identifyCouponInstanceAvailable(CouponInstanceIdentifyContext marketingIdentifyContext) {
        AssertUtil.isTrue(Objects.nonNull(marketingIdentifyContext) &&
                !StringUtils.isAnyBlank(marketingIdentifyContext.getAccountCode(),marketingIdentifyContext.getActivityCode(),marketingIdentifyContext.getInstanceCode()),"参数异常");
        AssertUtil.isTrue(Objects.nonNull(marketingIdentifyContext.getCouponUseRule()),"优惠券使用规则不存在");
        // 只要这一批商品有一个满足  则可以使用优惠券
        for (CouponInstanceIdentifySkuContext context : marketingIdentifyContext.getSkuList()) {
            MatchUseRuleContext ruleContext = MatchUseRuleContext.builder()
                    .accountCode(marketingIdentifyContext.getAccountCode())
                    .skuCode(context.getSkuCode())
                    .categoryCode3(context.getCategoryCode3()).build();
            boolean matchUseRule = matchUseRule(marketingIdentifyContext.getCouponUseRule(), ruleContext);
            if (matchUseRule) {
                return true;
            }
        }
        return false;
    }


    /**
     * @Description 识别商品使用规则可参与优惠券活动
     * @author liuhu
     * @param marketingIdentifyContext
     * @date 2024/1/4 14:08
     * @return void
     */
    private List<SkuMarketingIdentifyActivityBO> identifySkuUseRuleAvailableCouponActivity(List<CouponActivityBaseDTO> CouponActivityBaseDTOList,
                                                                             MarketingIdentifyContext marketingIdentifyContext) {

        List<String> activityCodeList = CouponActivityBaseDTOList.stream().map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());

        List<CouponUseRuleBaseDTO> couponUseRuleBaseDTOS = couponUseRuleRepository.queryByActivityCodeList(activityCodeList);

        return marketingIdentifyContext.getSkuList().stream().map(identifyContext -> {
            SkuMarketingIdentifyActivityBO activityBO = new SkuMarketingIdentifyActivityBO();
            activityBO.setSkuCode(identifyContext.getSkuCode());
            List<SkuMarketingIdentifyDetailBO> detailDTOList = buildSkuMarketingIdentifyDetailDTO(CouponActivityBaseDTOList, couponUseRuleBaseDTOS, identifyContext);
            activityBO.setActivityList(detailDTOList);
            return activityBO;
        }).collect(Collectors.toList());

    }

    /**
     * @Description 商品使用规则识别
     * @author liuhu
     * @param couponActivityBaseDTOList
     * @param CouponUseRuleBaseDTOS
     * @param identifyContext
     * @date 2024/1/22 17:20
     * @return java.util.List<com.lz.mall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyDetailResultBO>
     */
    private List<SkuMarketingIdentifyDetailBO> buildSkuMarketingIdentifyDetailDTO(
                                                List<CouponActivityBaseDTO> couponActivityBaseDTOList,
                                                List<CouponUseRuleBaseDTO> CouponUseRuleBaseDTOS,
                                                CouponInstanceIdentifySkuContext identifyContext) {
        Map<String, CouponUseRuleBaseDTO> CouponUseRuleBaseDTOMap = CouponUseRuleBaseDTOS.stream().collect(Collectors.toMap(CouponUseRuleBaseDTO::getActivityCode, Function.identity(), (k1, k2) -> k1));
        return couponActivityBaseDTOList.stream().map(activityDO -> {
            CouponUseRuleBaseDTO useRuleDO = CouponUseRuleBaseDTOMap.get(activityDO.getActivityCode());
            AssertUtil.isTrue(Objects.nonNull(useRuleDO), "使用规则不存在");

            MatchUseRuleContext useRuleContext = MatchUseRuleContext.builder()
                    .skuCode(identifyContext.getSkuCode())
                    .categoryCode3(identifyContext.getCategoryCode3()).build();

            boolean matchUseRule = matchUseRule(useRuleDO, useRuleContext);
            if (!matchUseRule) {
                return null;
            }
            SkuMarketingIdentifyDetailBO detailDTO = new SkuMarketingIdentifyDetailBO();
            detailDTO.setActivityCode(activityDO.getActivityCode());
            detailDTO.setActivityName(activityDO.getActivityName());
            detailDTO.setStartTime(activityDO.getStartTime());
            detailDTO.setEndTime(activityDO.getEndTime());
            detailDTO.setCouponAmount(activityDO.getCouponAmount());

            return detailDTO;
        }).filter(Objects::nonNull).collect(Collectors.toList());

    }


    /**
     * @Description 使用规则识别
     * @author liuhu
     * @param useRuleDO
     * @param useRuleContext
     * @date 2024/1/22 17:19
     * @return boolean
     */
    private boolean matchUseRule(CouponUseRuleBaseDTO useRuleDO,MatchUseRuleContext useRuleContext) {
        if(Objects.equals(useRuleDO.getUseType(), UseTypeEnum.ALL.getValue())){
            return true;
        }
        if(Objects.equals(useRuleDO.getUseType(),UseTypeEnum.SPECIFIC_PRODUCT.getValue())){
            List<SpecificProductSendRuleJsonBO> specificProductSendRuleJsonBOS = JSON.parseArray(useRuleDO.getRuleJson(), SpecificProductSendRuleJsonBO.class);
            if(CollectionUtils.isEmpty(specificProductSendRuleJsonBOS)){
                return true;
            }
            List<String> skuCodeList = specificProductSendRuleJsonBOS.stream().map(SpecificProductSendRuleJsonBO::getSkuCode).collect(Collectors.toList());
            return skuCodeList.contains(useRuleContext.getSkuCode());
        }

        if(Objects.equals(useRuleDO.getUseType(),UseTypeEnum.SPECIFIC_CATEGORY3.getValue())){
            List<SpecificCategoryCode3SendRuleJsonBO> specificCategoryCode3SendRuleJsonBOS = JSON.parseArray(useRuleDO.getRuleJson(), SpecificCategoryCode3SendRuleJsonBO.class);
            if(CollectionUtils.isEmpty(specificCategoryCode3SendRuleJsonBOS)){
                return true;
            }
            List<String> categoryCode3List = specificCategoryCode3SendRuleJsonBOS.stream().map(SpecificCategoryCode3SendRuleJsonBO::getSkuCode).collect(Collectors.toList());
            return categoryCode3List.contains(useRuleContext.getCategoryCode3());
        }

        if(Objects.equals(useRuleDO.getUseType(),UseTypeEnum.ORDER_AMOUNT_MATCH.getValue())){
            OrderAmountMatchSendRuleJsonBO orderAmountMatchSendRuleJsonBO = JSON.parseObject(useRuleDO.getRuleJson(), OrderAmountMatchSendRuleJsonBO.class);
            if(Objects.isNull(orderAmountMatchSendRuleJsonBO)){
                return true;
            }
            return orderAmountMatchSendRuleJsonBO.getOrderAmount() > useRuleContext.getOrderAmount();
        }
        return false;
    }

    /**
     * @Description 识别可用的优惠券活动
     * @author liuhu
     * @param couponActivityBaseDTOList
     * @param accountCode
     * @date 2024/1/5 17:28
     * @return java.util.List<com.lz.mall.marketing.service.entity.base.CouponActivityDO>
     */
    public List<CouponActivityBaseDTO> identifyAvailableCouponActivity(List<CouponActivityBaseDTO> couponActivityBaseDTOList, String accountCode) {
        //2.查询平台可用优惠券
        if(CollectionUtils.isEmpty(couponActivityBaseDTOList)){
            return Lists.newArrayList();
        }
        //2.过滤库存是否满足
        List<String> stockAvailableActivityCodeList = getStockAvailableActivityCodeList(accountCode, couponActivityBaseDTOList);
        List<CouponActivityBaseDTO> stockAvailableActivityList = couponActivityBaseDTOList.stream().filter(couponActivityDO -> stockAvailableActivityCodeList.contains(couponActivityDO.getActivityCode())).collect(Collectors.toList());

        //3.过滤已领取达到上限
        List<String> instanceAvailableQueryActivityCodeList = getInstanceAvailableQueryActivityCodeList(accountCode, stockAvailableActivityList);
        List<CouponActivityBaseDTO> instanceAvailableQueryActivityList = stockAvailableActivityList.stream()
                .filter(couponActivityDO -> instanceAvailableQueryActivityCodeList.contains(couponActivityDO.getActivityCode()))
                .collect(Collectors.toList());
        //4.过滤发放规则不满足
        List<String> ruleAvailableActivityCodeList = getSendRuleAvailableActivityCodeList(accountCode, instanceAvailableQueryActivityList);
        return instanceAvailableQueryActivityList.stream()
                .filter(couponActivityDO -> ruleAvailableActivityCodeList.contains(couponActivityDO.getActivityCode()))
                .collect(Collectors.toList());
    }

    private List<String> getSendRuleAvailableActivityCodeList(String accountCode,List<CouponActivityBaseDTO> CouponActivityBaseDTOList) {
        List<String> activityCodeList = CouponActivityBaseDTOList.stream().map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
        List<CouponSendRuleBaseDTO> CouponSendRuleBaseDTOS = sendRuleRepository.queryByActivityCodeList(activityCodeList);
        Map<String, CouponSendRuleBaseDTO> CouponSendRuleBaseDTOMap = CouponSendRuleBaseDTOS.stream().collect(Collectors.toMap(CouponSendRuleBaseDTO::getActivityCode, Function.identity(), (k1, k2) -> k1));

       return CouponActivityBaseDTOList.stream().map(couponActivityDO -> {

            CouponSendRuleBaseDTO sendRuleDO = CouponSendRuleBaseDTOMap.get(couponActivityDO.getActivityCode());

            if(Objects.isNull(sendRuleDO) || !Objects.equals(sendRuleDO.getSendType(), SendTypeEnum.ACTIVE_RECEIVE.getValue())){
                log.info("当前用户领取活动规则不是主动领取，userId:{},activityCode:{}", accountCode, couponActivityDO.getActivityCode());
                return  null;
            }
            return couponActivityDO;
        }).filter(Objects::nonNull).map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
    }

    private List<String> getStockAvailableActivityCodeList(String accountCode,List<CouponActivityBaseDTO> CouponActivityBaseDTOList) {
        List<String> activityCodeList = CouponActivityBaseDTOList.stream().map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
        List<CouponStockBaseDTO> CouponStockBaseDTOS = couponStockRepository.queryByActivityCodeList(activityCodeList);
        return CouponStockBaseDTOS.stream()
                .map(CouponStockBaseDTO -> {
                    if(Objects.isNull(CouponStockBaseDTO.getAvailableStock()) || CouponStockBaseDTO.getAvailableStock() <= 0){
                        log.info("当前用户领取活动库存不足，userId:{},activityCode:{}", accountCode, CouponStockBaseDTO.getActivityCode());
                        return null;
                    }
                    return CouponStockBaseDTO.getActivityCode();
                }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<String> getInstanceAvailableQueryActivityCodeList(String accountCode, List<CouponActivityBaseDTO> availableActivityList) {
        List<String> stockAvailableQueryActivityCodeList = availableActivityList.stream().map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
        List<CouponActivityBaseDTO> couponActivityBaseDTOS = couponInstanceRepository.queryListByActivityCodeAndAccountCode(stockAvailableQueryActivityCodeList, accountCode);
        Map<String, List<CouponActivityBaseDTO>> couponInstanceMap = couponActivityBaseDTOS.stream().collect(Collectors.groupingBy(CouponActivityBaseDTO::getActivityCode));
        return availableActivityList.stream().map(couponActivityDO -> {
            List<CouponActivityBaseDTO> existCouponInstance = couponInstanceMap.get(couponActivityDO.getActivityCode());
            if (couponActivityDO.getUserSendLimit() == -1) {
                return couponActivityDO;
            }
            Integer existReceiveCount = Objects.isNull(existCouponInstance) ? 0 :  CollectionUtils.size(existCouponInstance);
            if (couponActivityDO.getUserSendLimit() <= existReceiveCount) {
                log.info("当前用户领取活动已达上限，userId:{},activityCode:{}", accountCode, couponActivityDO.getActivityCode());
                return null;
            }
            return couponActivityDO;
        }).filter(Objects::nonNull).map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
    }
}
