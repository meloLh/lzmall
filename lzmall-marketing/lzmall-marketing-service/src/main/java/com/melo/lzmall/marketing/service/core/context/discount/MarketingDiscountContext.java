package com.melo.lzmall.marketing.service.core.context.discount;

import com.melo.lzmall.marketing.service.entity.base.CouponUseRuleBaseDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class MarketingDiscountContext implements Serializable {

    private String accountCode;

    private String accountName;

    private BigDecimal couponAmount;

    private String instanceCode;

    private String activityCode;

    private String activityName;

    private CouponUseRuleBaseDTO couponUseRule;

    private List<MarketingDiscountDetailContext> skuList;

}
