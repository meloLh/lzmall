package com.melo.lzmall.marketing.service.core.context.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MarketingDiscountDetailContext implements Serializable {

    private String categoryCode3;

    private String shopCode;

    private String shopName;

    private String skuCode;

    private String productName;

    private Integer purchaseQty;

    private BigDecimal salePrice;

}
