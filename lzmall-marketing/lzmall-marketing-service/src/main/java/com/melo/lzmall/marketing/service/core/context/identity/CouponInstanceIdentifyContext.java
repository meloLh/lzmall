package com.melo.lzmall.marketing.service.core.context.identity;

import com.melo.lzmall.marketing.service.entity.base.CouponUseRuleBaseDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CouponInstanceIdentifyContext implements Serializable {

    private String accountCode;

    private String accountName;

    private String activityCode;

    private String instanceCode;

    private CouponUseRuleBaseDTO couponUseRule;

    /**sku识别上下文*/
    private List<CouponInstanceIdentifySkuContext> skuList;
}
