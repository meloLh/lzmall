package com.melo.lzmall.marketing.service.core.context.identity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CouponInstanceIdentifySkuContext implements Serializable {

    private String categoryCode3;

    private String shopCode;

    private String skuCode;
}
