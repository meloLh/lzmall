package com.melo.lzmall.marketing.service.core.context.identity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MarketingIdentifyContext implements Serializable {

    private String accountCode;

    /**sku识别上下文*/
    private List<CouponInstanceIdentifySkuContext> skuList;
}
