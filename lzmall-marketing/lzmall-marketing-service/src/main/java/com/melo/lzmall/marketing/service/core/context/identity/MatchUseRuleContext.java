package com.melo.lzmall.marketing.service.core.context.identity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *  @Description 使用规则匹配上下文
 *  @author liuhu
 *  @Date 2024/1/22 16:34
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MatchUseRuleContext implements Serializable {

    private String skuCode;

    private String accountCode;

    private String categoryCode3;

    private Long orderAmount;
}
