package com.melo.lzmall.marketing.service.core.dto.discount;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SettleCartDTO implements Serializable {

    /**
     * 可用优惠券活动
     */
    private List<SettleCartCouponDTO> availableCouponInstanceList;


    /**
     * 不可用优惠券活动
     */
    private List<SettleCartCouponDTO> disableCouponInstanceList;
}
