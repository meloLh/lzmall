package com.melo.lzmall.marketing.service.core.dto.discount;

import lombok.Data;

import java.io.Serializable;

@Data
public class SettleCartDiscountDTO implements Serializable {

    /**订单编号*/
    private String orderCode;
}
