package com.melo.lzmall.marketing.service.entity.base;


import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 营销活动模板表
 */
@Data
public class ActivityTemplateBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 活动名称
     */
    private String activityName;

        /**
     * 活动挂件
     */
    private String activityWidget;

        /**
     * 活动图片
     */
    private String activityImage;

        /**
     * 优惠券来源 PLATEFORM-平台 SHOP-店铺
     */
    private String activitySource;

        /**
     * 主体编号
     */
    private String subjectCode;

        /**
     * 活动类型  DISCOUNT-限时折扣  GIFT-满赠
     */
    private String activityType;

        /**
     * 活动开始时间
     */
    private Date startTime;

        /**
     * 活动结束时间
     */
    private Date endTime;

        /**
     * 优惠规则类型  MATCH_COUNT 满X件  MATCH_PRICE 满X元
     */
    private String discountRuleType;

        /**
     * 优惠规则满足值
     */
    private String discountRuleValue;

        /**
     * 商品价格优惠规则类型  RATIO-按照比例  PRICE-特定价格
     */
    private String productPriceRuleType;

        /**
     * 优惠方式 ALL 所有商品 selected 选中
     */
    private String productRuleType;

        /**
     * 状态 WAIT 未开始 RUNNING 进行中  FINISH-已结束  CANCEL-已撤销
     */
    private String status;

        /**
     * 备注
     */
    private String remark;

        /**
     * 排序值
     */
    private Integer sort;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
