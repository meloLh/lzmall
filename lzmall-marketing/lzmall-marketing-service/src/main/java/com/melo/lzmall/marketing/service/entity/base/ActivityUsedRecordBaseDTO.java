package com.melo.lzmall.marketing.service.entity.base;


import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 活动使用记录表
 */
@Data
public class ActivityUsedRecordBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 活动名称
     */
    private String activityName;

        /**
     * 业务单号
     */
    private String bizCode;

        /**
     * 商品编码
     */
    private String skuCode;

        /**
     * 商品名称
     */
    private String productName;

        /**
     * 门店编码
     */
    private String shopCode;

        /**
     * 门店名称
     */
    private String shopName;

        /**
     * 购买数量
     */
    private Integer purchaseQty;

        /**
     * 优惠数量
     */
    private Integer discountQty;

        /**
     * 使用者id
     */
    private String accountCode;

        /**
     * 使用者名称
     */
    private String accountName;

        /**
     * 排序值
     */
    private Integer sort;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
