package com.melo.lzmall.marketing.service.entity.base;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券实例
 */
@Data
public class CouponActivityBaseDTO implements Serializable {

    /**
     * 活动编号
     */
    private String activityCode;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;
    /**
     * 优惠券使用门槛金额
     */
    private BigDecimal couponFullAmount;
    /**
     * 优惠券来源 PLATFORM-平台券 SHOP-店铺券
     */
    private String couponSource;
    /**
     * 主体编号
     */
    private String subjectCode;
    /**
     * 优惠券类型 FULL_REDUCTION-满减券 NO_THRESHOLD-无门槛
     */
    private String couponType;
    /**
     * 优惠券活动开始时间
     */
    private Date startTime;
    /**
     * 优惠券活动结束时间
     */
    private Date endTime;
    /**
     * 优惠券实例生效开始时间
     */
    private Date instanceStartTime;
    /**
     * 优惠券实例生效结束时间
     */
    private Date instanceEndTime;
    /**
     * 发放类型 com.melo.lzmall.marketing.service.enums.SendTypeEnum
     */
    private String sendType;
    /**
     * 使用类型 ALL-全站商品均可用  SPECIFIC_PRODUCT-指定商品可用  SPECIFIC_CATEGORY3-指定分类可用 ORDER_AMOUNT_MATCH-订单满减
     */
    private String useType;
    /**
     * 限领张数
     */
    private Integer userSendLimit;
    /**
     * 每次发几张优惠券
     */
    private Integer sendCount;
    /**
     * 是否与其他活动互斥
     */
    private String mutexFlag;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;
    /**
     * 活动状态（状态 WAIT 未开始 RUNNING 进行中  FINISH-已结束  CANCEL-已撤销）
     */
    private String status;


}
