package com.melo.lzmall.marketing.service.entity.base;


import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券发放规则
 */
@Data
public class CouponSendRuleBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 发放类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放
     */
    private String sendType;

        /**
     * 限领张数
     */
    private Integer userSendLimit;

        /**
     * 每次发几张优惠券
     */
    private Integer sendCount;

        /**
     * 发放规则json
     */
    private String ruleJson;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
