package com.melo.lzmall.marketing.service.entity.base;


import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券库存表
 */
@Data
public class CouponStockBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 实际库存
     */
    private Integer realStock;

    private Integer freezeStock;

        /**
     * 可用库存
     */
    private Integer availableStock;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
