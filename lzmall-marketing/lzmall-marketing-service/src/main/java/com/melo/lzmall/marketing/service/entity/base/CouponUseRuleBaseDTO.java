package com.melo.lzmall.marketing.service.entity.base;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券使用规则
 */
@Data
public class CouponUseRuleBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 使用类型 1.指定商品可用 2.指定分类可用 3.订单满减
     */
    private String useType;

        /**
     * 优惠券使用门槛金额
     */
    private BigDecimal couponFullAmount;

        /**
     * 是否与其他活动互斥
     */
    private String mutexFlag;

        /**
     * 使用规则json
     */
    private String ruleJson;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
