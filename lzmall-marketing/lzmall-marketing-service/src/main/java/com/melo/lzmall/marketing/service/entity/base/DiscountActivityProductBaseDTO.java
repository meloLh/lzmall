package com.melo.lzmall.marketing.service.entity.base;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 折扣活动商品明细表
 */
@Data
public class DiscountActivityProductBaseDTO implements Serializable {

        /**
     * id
     */
    private Long id;

        /**
     * 活动编号
     */
    private String activityCode;

        /**
     * 活动名称
     */
    private String activityName;

        /**
     * 商品编码
     */
    private String skuCode;

        /**
     * 商品名称
     */
    private String productName;

        /**
     * 商品图片
     */
    private String imageUrl;

        /**
     * 门店编码
     */
    private String shopCode;

        /**
     * 门店名称
     */
    private String shopName;

        /**
     * 折扣价格
     */
    private BigDecimal discountPrice;

        /**
     * 商品折扣限制总数量
     */
    private Integer discountLimitQty;

        /**
     * 商品每人优惠限量
     */
    private Integer userLimitQty;

        /**
     * 排序值
     */
    private Integer sort;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
