package com.melo.lzmall.marketing.service.entity.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class ReceiveCouponBO implements Serializable {

    @NotBlank
    private String activityCode;

    private String accountName;

    @NotBlank
    private String accountCode;
}
