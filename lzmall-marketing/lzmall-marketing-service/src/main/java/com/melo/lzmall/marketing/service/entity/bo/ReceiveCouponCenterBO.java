package com.melo.lzmall.marketing.service.entity.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class ReceiveCouponCenterBO implements Serializable {

    @NotBlank
    private String shopCode;

    private String accountName;

    @NotBlank
    private String accountCode;
}
