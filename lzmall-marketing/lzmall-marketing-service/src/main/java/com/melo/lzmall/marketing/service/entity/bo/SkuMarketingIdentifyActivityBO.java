package com.melo.lzmall.marketing.service.entity.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SkuMarketingIdentifyActivityBO implements Serializable {


    private String skuCode;

    private List<SkuMarketingIdentifyDetailBO> activityList;
}
