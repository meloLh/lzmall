package com.melo.lzmall.marketing.service.entity.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SkuMarketingIdentifyBO implements Serializable {

    private List<SkuMarketingIdentifyDetailBO> disableCouponActivityList;

    private List<SkuMarketingIdentifyDetailBO> availableCouponActivityList;

    private List<SkuMarketingIdentifyActivityBO> skuActivityList;
}
