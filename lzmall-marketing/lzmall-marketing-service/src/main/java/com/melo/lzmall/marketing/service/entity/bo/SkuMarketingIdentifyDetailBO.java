package com.melo.lzmall.marketing.service.entity.bo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SkuMarketingIdentifyDetailBO implements Serializable {

    private String activityCode;

    private String activityName;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;
}
