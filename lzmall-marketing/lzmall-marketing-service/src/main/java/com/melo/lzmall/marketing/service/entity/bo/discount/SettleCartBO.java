package com.melo.lzmall.marketing.service.entity.bo.discount;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
public class SettleCartBO implements Serializable {

    private String accountCode;

    private String accountName;

    /**选中得优惠券实例编号*/
    private String instanceCode;

    private List<SettleCartSkuBO>  skuList;
}
