package com.melo.lzmall.marketing.service.entity.bo.discount;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SettleCartSkuBO implements Serializable {

    @NotBlank
    private String skuCode;

    private String productName;

    @NotNull
    private BigDecimal salePrice;

    @NotBlank
    private String categoryCode3;

    @NotBlank
    private String shopCode;

    private String shopName;

    @NotNull
    private Integer purchaseNum;
}
