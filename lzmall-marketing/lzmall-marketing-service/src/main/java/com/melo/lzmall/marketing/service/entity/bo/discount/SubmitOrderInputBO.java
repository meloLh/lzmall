package com.melo.lzmall.marketing.service.entity.bo.discount;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
public class SubmitOrderInputBO implements Serializable {

    private String accountCode;

    private String accountName;

    private String couponInstanceCode;

    private List<SubmitOrderSkuInputBO> skuList;
}
