package com.melo.lzmall.marketing.service.entity.bo.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SubmitOrderSkuInputBO implements Serializable {

    private String skuCode;

    private String productName;

    private String shopCode;

    private String shopName;

    private String categoryCode3;

    private BigDecimal salePrice;

    private Integer purchaseNum;
}
