package com.melo.lzmall.marketing.service.entity.bo.sendrule;

import lombok.Data;

import java.io.Serializable;

/**
 *  @Description 订单金额满足
 *  @author liuhu
 *  @Date 2024/1/22 16:37
 */
@Data
public class OrderAmountMatchSendRuleJsonBO implements Serializable {


    private Long orderAmount;
}
