package com.melo.lzmall.marketing.service.entity.bo.sendrule;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SkuPreferentialIdentifyDetailBO implements Serializable {

    @NotBlank
    private String shopCode;

    private String shopName;

    @NotNull
    private String skuCode;

    @NotBlank
    private String categoryCode3;


}
