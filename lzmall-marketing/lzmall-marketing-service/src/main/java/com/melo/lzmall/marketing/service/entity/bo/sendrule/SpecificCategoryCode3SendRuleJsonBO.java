package com.melo.lzmall.marketing.service.entity.bo.sendrule;

import lombok.Data;

import java.io.Serializable;

/**
 *  @Description 指定类目匹配
 *  @author liuhu
 *  @Date 2024/1/22 16:37
 */
@Data
public class SpecificCategoryCode3SendRuleJsonBO implements Serializable {

    private String skuCode;

    private String skuName;

    private String imageUrl;

    private String categoryCode3;

    private String categoryName3;
}
