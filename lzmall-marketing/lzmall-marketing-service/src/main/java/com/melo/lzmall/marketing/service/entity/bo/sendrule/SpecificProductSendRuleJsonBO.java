package com.melo.lzmall.marketing.service.entity.bo.sendrule;

import lombok.Data;

import java.io.Serializable;
/**
 *  @Description 指定商品匹配
 *  @author liuhu
 *  @Date 2024/1/22 16:37
 */
@Data
public class SpecificProductSendRuleJsonBO implements Serializable {

    private String skuCode;

    private String productName;

    private String imageUrl;
}
