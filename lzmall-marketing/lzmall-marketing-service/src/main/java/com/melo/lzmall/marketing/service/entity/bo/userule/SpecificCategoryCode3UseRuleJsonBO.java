package com.melo.lzmall.marketing.service.entity.bo.userule;

import lombok.Data;

import java.io.Serializable;

/**
 *  @Description 指定类目可用
 *  @author liuhu
 *  @Date 2024/1/22 16:37
 */
@Data
public class SpecificCategoryCode3UseRuleJsonBO implements Serializable {

    private String skuCode;

    private String skuName;

    private String imageUrl;

    private String categoryCode3;

    private String categoryName3;
}
