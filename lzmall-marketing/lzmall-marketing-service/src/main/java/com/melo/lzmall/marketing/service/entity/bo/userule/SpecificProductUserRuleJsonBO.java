package com.melo.lzmall.marketing.service.entity.bo.userule;

import lombok.Data;

import java.io.Serializable;

/**
 *  @Description 指定商品可用
 *  @author liuhu
 *  @Date 2024/1/22 16:37
 */
@Data
public class SpecificProductUserRuleJsonBO implements Serializable {

    private String skuCode;

    private String productName;

    private String imageUrl;
}
