package com.melo.lzmall.marketing.service.entity.dto.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class MarketingCouponDiscountDTO implements Serializable {

    private String activityCode;

    private String activityName;

    private String instanceCode;

    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;

    /**
     * 订单原价总额
     */
    private BigDecimal totalOriginAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal totalCouponPromotionAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal totalDiscountPromotionAmount;


    /**商品明细*/
    private List<MarketingCouponDiscountSkuDTO> skuList;
}
