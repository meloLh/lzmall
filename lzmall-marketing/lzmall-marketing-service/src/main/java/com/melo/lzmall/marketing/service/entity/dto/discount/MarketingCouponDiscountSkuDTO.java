package com.melo.lzmall.marketing.service.entity.dto.discount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MarketingCouponDiscountSkuDTO implements Serializable {

    private String skuCode;

    private String productName;

    /**
     * 原始销售价格
     */
    private BigDecimal salePrice;

    /**
     * 购买数量
     */
    private Integer purchaseQty;

    /**
     * 商品原价总和
     */
    private BigDecimal originAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal couponPromotionAmount;
}
