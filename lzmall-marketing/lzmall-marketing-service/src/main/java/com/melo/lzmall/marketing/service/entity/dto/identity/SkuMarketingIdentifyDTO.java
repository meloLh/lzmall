package com.melo.lzmall.marketing.service.entity.dto.identity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class SkuMarketingIdentifyDTO implements Serializable {


    private String skuCode;

    private List<SkuMarketingIdentifyDetailDTO> skuList;
}
