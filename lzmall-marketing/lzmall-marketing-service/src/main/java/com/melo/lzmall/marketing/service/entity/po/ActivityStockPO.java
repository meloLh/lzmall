package com.melo.lzmall.marketing.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 活动库存表
 */
@Data
@TableName("activity_stock")
public class ActivityStockPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 活动编号
     */
    private String activityCode;
    /**
     * 实际库存
     */
    private String realStock;
    /**
     * 可用库存
     */
    private String availableStock;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
