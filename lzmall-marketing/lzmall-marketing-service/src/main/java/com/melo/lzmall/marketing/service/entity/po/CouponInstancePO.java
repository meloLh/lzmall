package com.melo.lzmall.marketing.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券实例
 */
@Data
@TableName("marketing_coupon_instance")
public class CouponInstancePO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 活动编号
     */
    private String activityCode;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;
    /**
     * 优惠券实例编号
     */
    private String instanceCode;
    /**
     * 使用关联单号(那笔订单使用)
     */
    private String useBizCode;
    /**
     * 发放关联单号(那个场景发放)
     */
    private String sendBizCode;
    /**
     * 使用时间
     */
    private Date useTime;
    /**
     * 发放时间
     */
    private Date sendTime;

    /**
     * 发放类型 com.melo.lzmall.marketing.service.enums.SendTypeEnum
     */
    private String sendType;
    /**
     * 优惠券实例生效开始时间
     */
    private Date instanceStartTime;
    /**
     * 优惠券实例生效结束时间
     */
    private Date instanceEndTime;
    /**
     * 所属人
     */
    private String accountCode;
    /**
     * 所属人名称
     */
    private String accountName;
    /**
     * 状态  未使用  已使用  已失效
     */
    private String status;
    /**
     * 是否与其他活动互斥
     */
    private String mutexFlag;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
