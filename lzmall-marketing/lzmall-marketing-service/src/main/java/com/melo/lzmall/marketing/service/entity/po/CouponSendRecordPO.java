package com.melo.lzmall.marketing.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 优惠券发放记录表
 */
@Data
@TableName("marketing_coupon_send_record")
public class CouponSendRecordPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 活动编号
     */
    private String activityCode;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;
    /**
     * 发放类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放
     */
    private String sendType;
    /**
     * 优惠券实例编号
     */
    private String instanceCode;
    /**
     * 使用关联单号(那个场景发放)
     */
    private String bizCode;
    /**
     * 发放时间
     */
    private Date sendTime;
    /**
     * 接收人id
     */
    private String accountCode;
    /**
     * 接收人名称
     */
    private String accountName;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
