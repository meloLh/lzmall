package com.melo.lzmall.marketing.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 赠品活动商品明细表
 */
@Data
@TableName("gift_activity_product")
public class GiftActivityProductPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 活动编号
     */
    private String activityCode;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 商品编码
     */
    private String skuCode;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品图片
     */
    private String imageUrl;
    /**
     * 门店ID
     */
    private String shopCode;
    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 商品每人优惠限量
     */
    private Integer sendQty;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
