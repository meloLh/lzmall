package com.melo.lzmall.marketing.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 营销分摊记录表
 */
@Data
@TableName("marketing_preferential_record")
public class PreferentialRecordPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 活动编号
     */
    private String preferentialType;
    /**
     * 优惠编号：优惠券指优惠券实例编号，活动指活动编号
     */
    private String preferentialCode;
    /**
     * 活动名称
     */
    private String preferentialName;
    /**
     * 业务单号
     */
    private String bizCode;
    /**
     * 商品编码
     */
    private String skuCode;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 购买数量
     */
    private Integer purchaseQty;
    /**
     * 商品价格
     */
    private BigDecimal salePrice;
    /**
     * 商品原始总价
     */
    private BigDecimal originAmount;
    /**
     * 商品分摊优惠金额
     */
    private BigDecimal preferentialAmount;

    private String status;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
