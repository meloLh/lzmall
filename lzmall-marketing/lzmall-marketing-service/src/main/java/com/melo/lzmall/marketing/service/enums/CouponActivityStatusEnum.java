package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CouponActivityStatusEnum {

    WAIT("未开始","WAIT"),

    RUNNING("运行中","RUNNING"),

    FINISH("已结束","FINISH"),

    CANCEL("已撤销","CANCEL"),

    ;

    private String name;

    private String value;
}
