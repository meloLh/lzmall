package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CouponInstanceStatusEnum {

    UN_USE("未使用","UN_USE"),

    FROZEN("已冻结","FROZEN"),

    USED("已使用","USED"),

    CANCEL("已撤销","CANCEL"),

    ;

    private final String name;

    private final String value;

}
