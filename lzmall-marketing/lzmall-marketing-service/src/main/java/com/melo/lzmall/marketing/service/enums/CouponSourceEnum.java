package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@AllArgsConstructor
@Getter
public enum CouponSourceEnum {

    SHOP("门店券","SHOP"),

    PLATFORM("平台券","PLATFORM"),

    ;

    private String name;

    private String value;
}
