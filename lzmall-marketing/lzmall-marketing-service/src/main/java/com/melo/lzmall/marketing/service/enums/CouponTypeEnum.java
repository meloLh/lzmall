package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@AllArgsConstructor
@Getter
public enum CouponTypeEnum {

    FULL_REDUCTION("满减","FULL_REDUCTION"),

    NO_THRESHOLD("无门槛","NO_THRESHOLD"),

    ;

    private String name;

    private String value;

}
