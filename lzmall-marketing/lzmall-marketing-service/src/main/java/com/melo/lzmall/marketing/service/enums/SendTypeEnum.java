package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@AllArgsConstructor
@Getter
public enum SendTypeEnum {

    ACTIVE_RECEIVE("主动领取","ACTIVE_RECEIVE"),

    ORDER_SATISFY("订单满赠","ORDER_SATISFY"),

    SPECIFIC_USER("特定用户可领","SPECIFIC_USER"),

    NEW_USER("新用户可领","ORDER_SATISFY"),
    ;

    private String name;

    private String value;

    public static SendTypeEnum getByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (SendTypeEnum sendTypeEnum : SendTypeEnum.values()) {
            if (Objects.equals(sendTypeEnum.value, value)) {
                return sendTypeEnum;
            }
        }
        return null;
    }

}
