package com.melo.lzmall.marketing.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@AllArgsConstructor
@Getter
public enum UseTypeEnum {

    SPECIFIC_PRODUCT("指定商品可用","SPECIFIC_PRODUCT"),

    SPECIFIC_CATEGORY3("指定分类可用","SPECIFIC_CATEGORY3"),

    ORDER_AMOUNT_MATCH("订单金额满足","ORDER_AMOUNT_MATCH"),

    ALL("全站商品均可用","ALL"),
    ;

    private String name;

    private String value;

    public static UseTypeEnum getByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (UseTypeEnum useTypeEnum : UseTypeEnum.values()) {
            if (Objects.equals(useTypeEnum.value, value)) {
                return useTypeEnum;
            }
        }
        return null;
    }

}
