package com.melo.lzmall.marketing.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.marketing.service.entity.po.CouponSendRulePO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CouponSendRuleMapper extends BaseMapper<CouponSendRulePO> {
	
}
