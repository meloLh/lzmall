package com.melo.lzmall.marketing.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.marketing.service.entity.po.CouponStockPO;
import com.melo.lzmall.marketing.service.repository.dto.DeductionCouponStockDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface CouponStockMapper extends BaseMapper<CouponStockPO> {

    int deductionCouponStock(@Param("entity") DeductionCouponStockDTO deductionCouponStockDTO);
}
