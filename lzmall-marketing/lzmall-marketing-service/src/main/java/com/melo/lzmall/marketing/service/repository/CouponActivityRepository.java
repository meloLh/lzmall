package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponActivityPO;
import com.melo.lzmall.marketing.service.enums.CouponActivityStatusEnum;
import com.melo.lzmall.marketing.service.enums.CouponSourceEnum;
import com.melo.lzmall.marketing.service.mapper.CouponActivityMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
@Slf4j
public class CouponActivityRepository {

    @Autowired
    private CouponActivityMapper couponActivityMapper;

    @Autowired
    private CouponRepositoryConvert convert;


    public List<CouponActivityBaseDTO> queryAvailableCouponActivity(List<String> subjectCodeList) {
        List<CouponActivityBaseDTO> shopCouponTemplateDOList = queryShopAvailableCouponActivity(subjectCodeList);
        List<CouponActivityBaseDTO> platformCouponTemplateDOList = queryPlatformAvailableCouponActivity();
        shopCouponTemplateDOList.addAll(platformCouponTemplateDOList);
        return shopCouponTemplateDOList;
    }

    public List<CouponActivityBaseDTO> queryShopAvailableCouponActivity(List<String> subjectCodeList) {
        if(CollectionUtils.isEmpty(subjectCodeList)){
            return Lists.newArrayList();
        }
        Date now = new Date();
        LambdaQueryWrapper<CouponActivityPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponActivityPO::getSubjectCode, subjectCodeList)
                .le(CouponActivityPO::getStartTime, now)
                .ge(CouponActivityPO::getEndTime, now)
                .eq(CouponActivityPO::getCouponSource, CouponSourceEnum.SHOP.getValue())
                .eq(CouponActivityPO::getStatus, CouponActivityStatusEnum.RUNNING.getValue());

        List<CouponActivityPO> activityList = couponActivityMapper.selectList(queryWrapper);
        return   convert.convertToCouponActivityDOList(activityList);
    }

    public List<CouponActivityBaseDTO> queryPlatformAvailableCouponActivity() {
        Date now = new Date();
        LambdaQueryWrapper<CouponActivityPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.le(CouponActivityPO::getStartTime, now)
                .ge(CouponActivityPO::getEndTime, now)
                .eq(CouponActivityPO::getCouponSource, CouponSourceEnum.PLATFORM.getValue())
                .eq(CouponActivityPO::getStatus, CouponActivityStatusEnum.RUNNING.getValue());

        List<CouponActivityPO> activityList = couponActivityMapper.selectList(queryWrapper);
        return   convert.convertToCouponActivityDOList(activityList);
    }

    public CouponActivityBaseDTO getByActivityCode(String activityCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(activityCode),"参数异常");
        LambdaQueryWrapper<CouponActivityPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponActivityPO::getActivityCode, activityCode);
        CouponActivityPO couponTemplate = couponActivityMapper.selectOne(queryWrapper);
        return   convert.convertToCouponActivityDO(couponTemplate);
    }


    public List<CouponActivityBaseDTO> queryByActivityCodeList(List<String> activityCodeList) {
        if(CollectionUtils.isEmpty(activityCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<CouponActivityPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponActivityPO::getActivityCode, activityCodeList);
        List<CouponActivityPO> couponActivityPOS = couponActivityMapper.selectList(queryWrapper);
        return   convert.convertToCouponActivityDOList(couponActivityPOS);
    }
}
