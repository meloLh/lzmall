package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.helper.MybatisBatchHelper;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponInstanceBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponInstancePO;
import com.melo.lzmall.marketing.service.enums.CouponInstanceStatusEnum;
import com.melo.lzmall.marketing.service.mapper.CouponInstanceMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
@Slf4j
public class CouponInstanceRepository {

    @Autowired
    private CouponInstanceMapper couponInstanceMapper;

    @Autowired
    private MybatisBatchHelper mybatisBatchHelper;

    @Autowired
    private CouponRepositoryConvert convert;

    public List<CouponActivityBaseDTO> queryListByActivityCodeAndAccountCode(List<String> activityCodeList, String accountCode) {
         if(CollectionUtils.isEmpty(activityCodeList) || StringUtils.isBlank(accountCode)){
             return Lists.newArrayList();
         }
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponInstancePO::getActivityCode, activityCodeList)
                    .eq(CouponInstancePO::getAccountCode,accountCode);
        List<CouponInstancePO> CouponInstancePOList = couponInstanceMapper.selectList(queryWrapper);
        return convert.convertToCouponActivityBaseDTOList(CouponInstancePOList);
    }

    public List<CouponActivityBaseDTO> queryListByActivityCodeAndAccountCode(String activityCode, String accountCode) {
        if(StringUtils.isAnyBlank(activityCode,accountCode)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getActivityCode, activityCode)
                .eq(CouponInstancePO::getAccountCode,accountCode);
        List<CouponInstancePO> CouponInstancePOList = couponInstanceMapper.selectList(queryWrapper);
        return convert.convertToCouponActivityBaseDTOList(CouponInstancePOList);
    }

    public CouponInstanceBaseDTO getByInstanceCode(String instanceCode) {
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getInstanceCode,instanceCode);
        CouponInstancePO CouponInstancePO = couponInstanceMapper.selectOne(queryWrapper);
        return convert.convertToCouponInstanceBaseDTO(CouponInstancePO);
    }

    public CouponInstanceBaseDTO getUserAvailableCouponInstanceByInstanceCode(String instanceCode, String accountCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(instanceCode,accountCode),"参数异常");
        Date now = new Date();
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getInstanceCode,instanceCode)
                    .eq(CouponInstancePO::getAccountCode,accountCode)
                    .eq(CouponInstancePO::getStatus, CouponInstanceStatusEnum.UN_USE.getValue())
                    .ge(CouponInstancePO::getInstanceEndTime,now)
                    .le(CouponInstancePO::getInstanceStartTime,now);
        CouponInstancePO CouponInstancePO = couponInstanceMapper.selectOne(queryWrapper);
        return convert.convertToCouponInstanceBaseDTO(CouponInstancePO);
    }

    public void useCouponInstance(String couponInstanceCode, String accountCode,String bizCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(couponInstanceCode,accountCode),"参数异常");
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getInstanceCode,couponInstanceCode)
                .eq(CouponInstancePO::getAccountCode,accountCode)
                .eq(CouponInstancePO::getStatus, CouponInstanceStatusEnum.FROZEN.getValue());
        CouponInstancePO update = new CouponInstancePO();
        update.setUseBizCode(bizCode);
        update.setStatus(CouponInstanceStatusEnum.USED.getValue());
        int updateRows = couponInstanceMapper.update(update, queryWrapper);
        AssertUtil.isTrue(updateRows == 1,"使用优惠券异常");
    }

    public void freezeCouponInstance(String couponInstanceCode, String accountCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(couponInstanceCode,accountCode),"参数异常");
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getInstanceCode,couponInstanceCode)
                .eq(CouponInstancePO::getAccountCode,accountCode)
                .eq(CouponInstancePO::getStatus, CouponInstanceStatusEnum.UN_USE.getValue());
        CouponInstancePO update = new CouponInstancePO();
        update.setStatus(CouponInstanceStatusEnum.FROZEN.getValue());
        int updateRows = couponInstanceMapper.update(update, queryWrapper);
        AssertUtil.isTrue(updateRows == 1,"冻结用优惠券异常");
    }

    public void batchInsert(List<CouponInstanceBaseDTO> couponInstanceBaseDTOS) {
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(couponInstanceBaseDTOS),"参数异常");
        List<CouponInstancePO> couponInstancePOS = convert.convertToCouponInstancePO(couponInstanceBaseDTOS);
        mybatisBatchHelper.batchSave(couponInstancePOS,CouponInstanceMapper.class,(record,mapper)->{
            mapper.insert(record);
        });
    }

    public List<CouponInstanceBaseDTO> queryAvailableCouponInstanceByAccountCode(String accountCode) {
        if(StringUtils.isBlank(accountCode)){
            return Lists.newArrayList();
        }
        Date now = new Date();
        LambdaQueryWrapper<CouponInstancePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponInstancePO::getAccountCode,accountCode)
                .eq(CouponInstancePO::getStatus, CouponInstanceStatusEnum.UN_USE.getValue())
                .ge(CouponInstancePO::getInstanceEndTime,now)
                .le(CouponInstancePO::getInstanceStartTime,now);
        List<CouponInstancePO> couponInstancePOS = couponInstanceMapper.selectList(queryWrapper);
        return convert.convertToCouponInstanceBaseDTOList(couponInstancePOS);
    }
}
