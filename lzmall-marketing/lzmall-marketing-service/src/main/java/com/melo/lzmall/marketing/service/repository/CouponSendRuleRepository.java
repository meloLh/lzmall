package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.marketing.service.entity.base.CouponSendRuleBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponSendRulePO;
import com.melo.lzmall.marketing.service.mapper.CouponSendRuleMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Slf4j
public class CouponSendRuleRepository {

    @Autowired
    private CouponSendRuleMapper couponSendRuleMapper;

    @Autowired
    private CouponRepositoryConvert convert;

    public List<CouponSendRuleBaseDTO> queryByActivityCodeList(List<String> activityCodeList) {
        if(CollectionUtils.isEmpty(activityCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<CouponSendRulePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponSendRulePO::getActivityCode, activityCodeList);
        List<CouponSendRulePO> couponSendRuleList = couponSendRuleMapper.selectList(queryWrapper);
        return convert.convertToCouponSendRuleBaseDTO(couponSendRuleList);
    }
}
