package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.service.entity.base.CouponStockBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponStockPO;
import com.melo.lzmall.marketing.service.mapper.CouponStockMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import com.melo.lzmall.marketing.service.repository.dto.DeductionCouponStockDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;


@Component
@Slf4j
public class CouponStockRepository {

    @Autowired
    private CouponStockMapper couponStockMapper;

    @Autowired
    private CouponRepositoryConvert convert;

    public List<CouponStockBaseDTO> queryByActivityCodeList(List<String> activityCodeList) {
        if(CollectionUtils.isEmpty(activityCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<CouponStockPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponStockPO::getActivityCode, activityCodeList);
        List<CouponStockPO> activityStocks = couponStockMapper.selectList(queryWrapper);
        return convert.convertToCouponStockBaseDTOList(activityStocks);
    }

    public CouponStockBaseDTO getByActivityCode(String activityCode) {
        if(StringUtils.isBlank(activityCode)){
            return null;
        }
        LambdaQueryWrapper<CouponStockPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponStockPO::getActivityCode, activityCode);
        CouponStockPO couponStockPO = couponStockMapper.selectOne(queryWrapper);
        return convert.convertToCouponStockBaseDTO(couponStockPO);
    }

    public void deductionCouponStock(DeductionCouponStockDTO deductionCouponStockDTO) {
        AssertUtil.isTrue(StringUtils.isNotBlank(deductionCouponStockDTO.getActivityCode()) && Objects.nonNull(deductionCouponStockDTO.getStockNum()));
        int rows = couponStockMapper.deductionCouponStock(deductionCouponStockDTO);
        AssertUtil.isTrue(rows == 1,"扣减优惠券库存异常");
    }
}
