package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.marketing.service.convert.CouponUseRuleConvert;
import com.melo.lzmall.marketing.service.entity.base.CouponUseRuleBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponUseRulePO;
import com.melo.lzmall.marketing.service.mapper.CouponUseRuleMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Slf4j
public class CouponUseRuleRepository {

    @Autowired
    private CouponUseRuleMapper couponUseRuleMapper;

    @Autowired
    private CouponRepositoryConvert convert;

    public List<CouponUseRuleBaseDTO> queryByActivityCodeList(List<String> activityCodeList) {
        if(CollectionUtils.isEmpty(activityCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<CouponUseRulePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CouponUseRulePO::getActivityCode, activityCodeList);
        List<CouponUseRulePO> couponUseRuleList = couponUseRuleMapper.selectList(queryWrapper);
        return convert.convertToCouponUseRuleDOList(couponUseRuleList);
    }

    public CouponUseRuleBaseDTO getByActivityCode(String activityCode) {
        LambdaQueryWrapper<CouponUseRulePO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CouponUseRulePO::getActivityCode, activityCode);
        CouponUseRulePO couponUseRule = couponUseRuleMapper.selectOne(queryWrapper);
        return convert.convertToCouponUseRuleDO(couponUseRule);
    }
}
