package com.melo.lzmall.marketing.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.helper.MybatisBatchHelper;
import com.melo.lzmall.marketing.service.entity.base.PreferentialRecordBaseDTO;
import com.melo.lzmall.marketing.service.entity.po.CouponInstancePO;
import com.melo.lzmall.marketing.service.entity.po.PreferentialRecordPO;
import com.melo.lzmall.marketing.service.enums.CouponInstanceStatusEnum;
import com.melo.lzmall.marketing.service.mapper.PreferentialRecordMapper;
import com.melo.lzmall.marketing.service.repository.convert.CouponRepositoryConvert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;


@Component
@Slf4j
public class PreferentialRecordRepository {

    @Autowired
    private PreferentialRecordMapper recordMapper;

    @Autowired
    private CouponRepositoryConvert convert;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private MybatisBatchHelper mybatisBatchHelper;

    public void insert(PreferentialRecordBaseDTO recordDO) {
        PreferentialRecordPO record =   convert.convertToPreferentialRecordPO(recordDO);
        recordMapper.insert(record);
    }

    public void batchInsert(List<PreferentialRecordBaseDTO> recordDOList) {
        List<PreferentialRecordPO> recordList = convert.convertToPreferentialRecordPOList(recordDOList);
        int save = mybatisBatchHelper.batchSave(recordList, PreferentialRecordMapper.class, (record, mapper) -> {
            mapper.insert(record);
        });
        AssertUtil.isTrue(save == CollectionUtils.size(recordList), "批量保存营销信息异常");
    }

    public void updateStatusByPreferentialCode(String preferentialCode,String accountCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(preferentialCode,accountCode),"参数异常");
        LambdaQueryWrapper<PreferentialRecordPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PreferentialRecordPO::getPreferentialCode,preferentialCode)
                .eq(PreferentialRecordPO::getCreatorCode,accountCode);
        PreferentialRecordPO update = new PreferentialRecordPO();
        update.setStatus("Y");
        int updateRows = recordMapper.update(update, queryWrapper);
//        AssertUtil.isTrue(updateRows == 1,"冻结用优惠券异常");
    }
}
