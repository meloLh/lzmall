package com.melo.lzmall.marketing.service.repository.convert;


import com.melo.lzmall.marketing.service.entity.base.*;
import com.melo.lzmall.marketing.service.entity.po.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CouponRepositoryConvert {


    List<CouponSendRuleBaseDTO> convertToCouponSendRuleBaseDTO(List<CouponSendRulePO> couponSendRuleList);

    List<CouponStockBaseDTO> convertToCouponStockBaseDTOList(List<CouponStockPO> activityStocks);

    CouponActivityBaseDTO convertToCouponActivityDO(CouponActivityPO couponTemplate);

    List<CouponActivityBaseDTO> convertToCouponActivityDOList(List<CouponActivityPO> activityList);

    List<CouponUseRuleBaseDTO> convertToCouponUseRuleDOList(List<CouponUseRulePO> couponUseRuleList);

    CouponUseRuleBaseDTO convertToCouponUseRuleDO(CouponUseRulePO couponUseRule);

    PreferentialRecordPO convertToPreferentialRecordPO(PreferentialRecordBaseDTO recordDO);

    List<PreferentialRecordPO> convertToPreferentialRecordPOList(List<PreferentialRecordBaseDTO> recordDOList);

    CouponStockBaseDTO convertToCouponStockBaseDTO(CouponStockPO couponStockPO);

    List<CouponInstancePO> convertToCouponInstancePO(List<CouponInstanceBaseDTO> couponInstanceBaseDTOS);

    List<CouponInstanceBaseDTO> convertToCouponInstanceBaseDTOList(List<CouponInstancePO> couponInstancePOS);

    List<CouponActivityBaseDTO> convertToCouponActivityBaseDTOList(List<CouponInstancePO> couponInstancePOList);

    CouponInstanceBaseDTO convertToCouponInstanceBaseDTO(CouponInstancePO couponInstancePO);
}
