package com.melo.lzmall.marketing.service.repository.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class DeductionCouponStockDTO implements Serializable {

    private String activityCode;

    private String accountCode;

    private String accountName;

    private Integer stockNum;
}
