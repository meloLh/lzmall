package com.melo.lzmall.marketing.service.service;


import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;

public interface CouponInstanceService {
    CouponActivityBaseDTO getCouponInstanceByInstanceCode(String instanceCode);

    void receiveCouponInstance(String accountCode, String activityCode);
}
