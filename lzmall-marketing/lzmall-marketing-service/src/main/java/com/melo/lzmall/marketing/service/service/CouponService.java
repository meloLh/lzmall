package com.melo.lzmall.marketing.service.service;


import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponBO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponCenterBO;

public interface CouponService {

    /**
     * @Description 领券
     * @author liuhu
     * @param receiveCouponBO
     * @date 2024/6/7 19:35
     * @return void
     */
    void receiveCoupon(ReceiveCouponBO receiveCouponBO);

    /**
     * @Description 领券中心
     * @author liuhu
     * @param receiveCouponCenterBO
     * @date 2024/6/7 19:35
     * @return com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse
     */
    ReceiveCouponCenterResponse queryReceiveCouponCenter(ReceiveCouponCenterBO receiveCouponCenterBO);

    /**
     * @Description 使用优惠券
     * @author liuhu
     * @param instanceCode
     * @param accountCode
     * @date 2024/6/11 17:09
     * @return void
     */
    void usedCouponInstance(String instanceCode, String accountCode,String bizCode);
}
