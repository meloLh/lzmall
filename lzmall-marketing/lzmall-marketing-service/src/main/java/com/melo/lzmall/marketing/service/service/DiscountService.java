package com.melo.lzmall.marketing.service.service;


import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartDTO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SettleCartBO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SubmitOrderInputBO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO;

public interface DiscountService{
    /**
     * @Description 购物车去结算 可用优惠券实例和不可用优惠券实例
     * @author liuhu
     * @param settleCartBO
     * @date 2024/1/29 16:41
     * @return com.lz.mall.marketing.service.core.dto.discount.SettleCartDTO
     */
    SettleCartDTO settleCart(SettleCartBO settleCartBO);

    /**
     * @Description 提交订单 计算分摊
     * @author liuhuDiscountService
     * @param submitOrderInputBO
     * @date 2024/1/29 11:06
     * @return com.lz.mall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO
     */
    MarketingCouponDiscountDTO submitOrder(SubmitOrderInputBO submitOrderInputBO);
}
