package com.melo.lzmall.marketing.service.service;


import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyBO;
import com.melo.lzmall.marketing.service.entity.dto.identity.SkuMarketingIdentifyDTO;

import java.util.List;

/**
 *  @Description 营销识别
 *  @author liuhu
 *  @Date 2024/1/4 14:02
 */
public interface MarketingIdentifyService {

    /**
     * @Description 购物车商品营销识别
     * @author liuhu
     * @param skuPreferentialIdentifyBO
     * @date 2024/1/22 17:22
     * @return java.util.List<com.lz.mall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyResultBO>
     */
    SkuMarketingIdentifyBO querySkuMarketingInfo(SkuPreferentialIdentifyBO skuPreferentialIdentifyBO);
}
