package com.melo.lzmall.marketing.service.service.impl;


import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.api.response.coupon.CouponActivityBaseResponse;
import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import com.melo.lzmall.marketing.service.constants.CouponRedisLockConstant;
import com.melo.lzmall.marketing.service.convert.CouponConvert;
import com.melo.lzmall.marketing.service.core.MarketingIdentifyCore;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponInstanceBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponStockBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponBO;
import com.melo.lzmall.marketing.service.entity.bo.ReceiveCouponCenterBO;
import com.melo.lzmall.marketing.service.enums.CouponInstanceStatusEnum;
import com.melo.lzmall.marketing.service.repository.CouponActivityRepository;
import com.melo.lzmall.marketing.service.repository.CouponInstanceRepository;
import com.melo.lzmall.marketing.service.repository.CouponStockRepository;
import com.melo.lzmall.marketing.service.repository.PreferentialRecordRepository;
import com.melo.lzmall.marketing.service.repository.dto.DeductionCouponStockDTO;
import com.melo.lzmall.marketing.service.service.CouponService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponActivityRepository couponActivityRepository;

    @Autowired
    private MarketingIdentifyCore marketingIdentifyCore;

    @Autowired
    private CouponStockRepository couponStockRepository;

    @Autowired
    private CouponInstanceRepository couponInstanceRepository;

    @Autowired
    private PreferentialRecordRepository preferentialRecordRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private CouponConvert convert;

    @Override
    public void receiveCoupon(ReceiveCouponBO receiveCouponBO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        receiveCouponBO.setAccountCode(authAccount.getAccountCode());
        receiveCouponBO.setAccountName(authAccount.getAccountName());

        CouponActivityBaseDTO couponActivityBaseDTO = couponActivityRepository.getByActivityCode(receiveCouponBO.getActivityCode());
        AssertUtil.isTrue(Objects.nonNull(couponActivityBaseDTO),"优惠券活动不存在"+receiveCouponBO.getActivityCode());
        //1.获取可领
        List<CouponActivityBaseDTO> couponActivityBaseDTOS = marketingIdentifyCore.identifyAvailableCouponActivity(Lists.newArrayList(couponActivityBaseDTO), receiveCouponBO.getAccountCode());
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(couponActivityBaseDTOS),"当前优惠券活动已过期已达领取上限");
        //2.领券
        RLock lock = redissonClient.getLock(CouponRedisLockConstant.USE_COUPON__LOCK_KEY+receiveCouponBO.getActivityCode());
        try {
            boolean tryLock = lock.tryLock(500, 1000L, TimeUnit.MILLISECONDS);
            AssertUtil.isTrue(tryLock,"请稍后重试");
            //1.校验库存
            CouponStockBaseDTO couponStockBaseDTO = couponStockRepository.getByActivityCode(receiveCouponBO.getActivityCode());
            //2.查询已领数量
            List<CouponActivityBaseDTO> activityBaseDTOS = couponInstanceRepository.queryListByActivityCodeAndAccountCode(receiveCouponBO.getActivityCode(), receiveCouponBO.getAccountCode());
            Integer hasReceiveCount = CollectionUtils.size(activityBaseDTOS);
            Integer canReceiveAgainCount = couponActivityBaseDTO.getUserSendLimit() - hasReceiveCount;
            //2.获取发放数量
            Integer canReceiveCount = couponStockBaseDTO.getAvailableStock() >= canReceiveAgainCount ? canReceiveAgainCount : couponStockBaseDTO.getAvailableStock();
            //3.最终发放数量 = 允许发放数量 和 一次最多发放数量比较
            Integer finalCanReceiveCount = canReceiveCount >= couponActivityBaseDTO.getSendCount() ? couponActivityBaseDTO.getSendCount() : canReceiveCount;
            AssertUtil.isTrue(finalCanReceiveCount >=0,"当前优惠券活动已领取达到上限"+receiveCouponBO.getActivityCode());
            //3.扣减库存
            DeductionCouponStockDTO deductionCouponStockDTO = buildDeductionCouponStockDTO(receiveCouponBO,finalCanReceiveCount);
            //4.保存券实例
            List<CouponInstanceBaseDTO> couponInstanceBaseDTOS = buildInsertCouponInstance(finalCanReceiveCount, couponActivityBaseDTO, receiveCouponBO);
            transactionTemplate.execute(ts->{
                couponStockRepository.deductionCouponStock(deductionCouponStockDTO);
                couponInstanceRepository.batchInsert(couponInstanceBaseDTOS);
                return null;
            });
        }catch (BusinessException e){
            throw e;
        } catch (Exception e) {
           log.error("领取优惠券异常,req:{}", JSON.toJSONString(receiveCouponBO),e);
           throw new BusinessException("500","领取优惠券异常");
        } finally {
            if(lock.isLocked() && lock.isHeldByCurrentThread()){
                lock.unlock();
            }
        }
    }

    @Override
    public ReceiveCouponCenterResponse queryReceiveCouponCenter(ReceiveCouponCenterBO receiveCouponCenterBO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        receiveCouponCenterBO.setAccountCode(authAccount.getAccountCode());
        receiveCouponCenterBO.setAccountName(authAccount.getAccountName());
        // 1.查询可用优惠券活动 门店券和平台券
        List<CouponActivityBaseDTO> allCouponActivityBaseDTOList = couponActivityRepository.queryAvailableCouponActivity(Lists.newArrayList(receiveCouponCenterBO.getShopCode(),"PLATFORM"));
        // 2.识别可领
        List<CouponActivityBaseDTO> canReceiveCouponActivityList = marketingIdentifyCore.identifyAvailableCouponActivity(allCouponActivityBaseDTOList, receiveCouponCenterBO.getAccountCode());
        List<String> canReceiveActivityCodeList = canReceiveCouponActivityList.stream().map(CouponActivityBaseDTO::getActivityCode).collect(Collectors.toList());
        // 3.过滤不可领
        List<CouponActivityBaseDTO> cantReceiveCouponActivityList = allCouponActivityBaseDTOList.stream().filter(v -> !canReceiveActivityCodeList.contains(v.getActivityCode())).collect(Collectors.toList());

        ReceiveCouponCenterResponse response = new ReceiveCouponCenterResponse();
        List<CouponActivityBaseResponse>  canReceiveCouponResponse =  convert.convertToCouponActivityBaseResponse(canReceiveCouponActivityList);
        response.setCanReceiveCouponActivityList(canReceiveCouponResponse);

        List<CouponActivityBaseResponse>  cantReceiveCouponResponse =  convert.convertToCouponActivityBaseResponse(cantReceiveCouponActivityList);
        response.setCantReceiveCouponActivityList(cantReceiveCouponResponse);
        return response;
    }

    @Override
    public void usedCouponInstance(String instanceCode, String accountCode,String bizCode) {
        //2.领券
        RLock lock = redissonClient.getLock(CouponRedisLockConstant.USE_COUPON_INSTANCE_LOCK_KEY+instanceCode);
        try {
            boolean tryLock = lock.tryLock(500, 1000L, TimeUnit.MILLISECONDS);
            AssertUtil.isTrue(tryLock,"请稍后重试");
            //1.查询实例
            CouponInstanceBaseDTO couponInstance = couponInstanceRepository.getByInstanceCode(instanceCode);
            AssertUtil.isTrue(Objects.nonNull(couponInstance),"优惠券实例已失效"+instanceCode);
            AssertUtil.isTrue(!Objects.equals(couponInstance.getStatus(),CouponInstanceStatusEnum.USED.getValue()),"优惠券实例已失效"+instanceCode);
            transactionTemplate.execute(ts->{
                couponInstanceRepository.useCouponInstance(instanceCode,accountCode,bizCode);
                preferentialRecordRepository.updateStatusByPreferentialCode(instanceCode,accountCode);
                return null;
            });
        }catch (BusinessException e){
            throw e;
        } catch (Exception e) {
            log.error("领取优惠券异常,instanceCode:{},accountCode:{}", instanceCode,accountCode,e);
            throw new BusinessException("500","领取优惠券异常");
        } finally {
            if(lock.isLocked() && lock.isHeldByCurrentThread()){
                lock.unlock();
            }
        }
    }

    private DeductionCouponStockDTO buildDeductionCouponStockDTO(ReceiveCouponBO receiveCouponBO, Integer canReceiveCount) {
        DeductionCouponStockDTO couponStockDTO = new DeductionCouponStockDTO();
        couponStockDTO.setActivityCode(receiveCouponBO.getActivityCode());
        couponStockDTO.setAccountCode(receiveCouponBO.getAccountCode());
        couponStockDTO.setAccountName(receiveCouponBO.getAccountName());
        couponStockDTO.setStockNum(canReceiveCount);
        return couponStockDTO;
    }

    private List<CouponInstanceBaseDTO> buildInsertCouponInstance(Integer canReceiveCount,
                                    CouponActivityBaseDTO couponActivityBaseDTO,
                                    ReceiveCouponBO receiveCouponBO) {
        List<CouponInstanceBaseDTO> list = new ArrayList<>();
        for (int i = 0; i < canReceiveCount; i++) {
            CouponInstanceBaseDTO baseDTO = new CouponInstanceBaseDTO();
            baseDTO.setActivityCode(couponActivityBaseDTO.getActivityCode());
            baseDTO.setActivityName(couponActivityBaseDTO.getActivityName());
            baseDTO.setCouponAmount(couponActivityBaseDTO.getCouponAmount());
            baseDTO.setInstanceCode(IdUtil.getSnowflakeNextIdStr());
            baseDTO.setInstanceStartTime(couponActivityBaseDTO.getInstanceStartTime());
            baseDTO.setInstanceEndTime(couponActivityBaseDTO.getInstanceEndTime());
            baseDTO.setSendType(couponActivityBaseDTO.getSendType());
            baseDTO.setSendTime(new Date());
            baseDTO.setSendBizCode(couponActivityBaseDTO.getActivityCode());
            baseDTO.setAccountCode(receiveCouponBO.getAccountCode());
            baseDTO.setAccountName(receiveCouponBO.getAccountName());
            baseDTO.setCreator(receiveCouponBO.getAccountName());
            baseDTO.setCreatorCode(receiveCouponBO.getAccountCode());
            baseDTO.setModifier(receiveCouponBO.getAccountName());
            baseDTO.setModifierCode(receiveCouponBO.getAccountCode());
            baseDTO.setStatus(CouponInstanceStatusEnum.UN_USE.getValue());
            list.add(baseDTO);
        }
        return list;
    }
}
