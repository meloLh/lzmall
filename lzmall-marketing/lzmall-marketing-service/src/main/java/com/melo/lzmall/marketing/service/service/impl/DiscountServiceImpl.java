package com.melo.lzmall.marketing.service.service.impl;

import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.service.convert.DiscountConvert;
import com.melo.lzmall.marketing.service.core.MarketingDiscountCore;
import com.melo.lzmall.marketing.service.core.MarketingIdentifyCore;
import com.melo.lzmall.marketing.service.core.context.discount.MarketingDiscountContext;
import com.melo.lzmall.marketing.service.core.context.discount.MarketingDiscountDetailContext;
import com.melo.lzmall.marketing.service.core.context.identity.CouponInstanceIdentifyContext;
import com.melo.lzmall.marketing.service.core.context.identity.CouponInstanceIdentifySkuContext;
import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartCouponDTO;
import com.melo.lzmall.marketing.service.core.dto.discount.SettleCartDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponActivityBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponInstanceBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.CouponUseRuleBaseDTO;
import com.melo.lzmall.marketing.service.entity.base.PreferentialRecordBaseDTO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SettleCartBO;
import com.melo.lzmall.marketing.service.entity.bo.discount.SubmitOrderInputBO;
import com.melo.lzmall.marketing.service.entity.dto.discount.MarketingCouponDiscountDTO;
import com.melo.lzmall.marketing.service.repository.CouponInstanceRepository;
import com.melo.lzmall.marketing.service.repository.CouponActivityRepository;
import com.melo.lzmall.marketing.service.repository.CouponUseRuleRepository;
import com.melo.lzmall.marketing.service.repository.PreferentialRecordRepository;
import com.melo.lzmall.marketing.service.service.DiscountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private MarketingIdentifyCore identifyCore;

    @Autowired
    private MarketingDiscountCore marketingDiscountCore;

    @Autowired
    private PreferentialRecordRepository preferentialRecordRepository;

    @Autowired
    private CouponInstanceRepository couponInstanceRepository;

    @Autowired
    private CouponUseRuleRepository couponUseRuleRepository;

    @Autowired
    private CouponActivityRepository couponActivityRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private DiscountConvert convert;

    @Override
    public MarketingCouponDiscountDTO submitOrder(SubmitOrderInputBO submitOrderInputBO) {
        //1.获取商品可用优惠券信息
        CouponInstanceBaseDTO couponInstanceBaseDTO = couponInstanceRepository.getUserAvailableCouponInstanceByInstanceCode(submitOrderInputBO.getCouponInstanceCode(), submitOrderInputBO.getAccountCode());
        AssertUtil.isTrue(Objects.nonNull(couponInstanceBaseDTO),"优惠券实例已失效"+submitOrderInputBO.getCouponInstanceCode());
        //2.查询使用规则
        CouponUseRuleBaseDTO couponUseRuleBaseDTO = couponUseRuleRepository.getByActivityCode(couponInstanceBaseDTO.getActivityCode());
        //3.识别是否可用
        CouponInstanceIdentifyContext context = buildCouponInstanceIdentifyContext(couponInstanceBaseDTO,submitOrderInputBO,couponUseRuleBaseDTO);
        boolean couponInstanceAvailable = identifyCore.identifyCouponInstanceAvailable(context);
        AssertUtil.isTrue(couponInstanceAvailable,"优惠券实例已失效"+submitOrderInputBO.getCouponInstanceCode());
        //4.计算分摊
        MarketingDiscountContext discountContext =  buildSubmitOrderDiscountContext(couponInstanceBaseDTO,couponUseRuleBaseDTO,submitOrderInputBO);
        MarketingCouponDiscountDTO discountDTO = marketingDiscountCore.calculateSkuDiscount(discountContext);
        //5.保存记录
        transactionTemplate.execute(ts->{
            //3.优惠券实例状态变为已冻结
            couponInstanceRepository.freezeCouponInstance(submitOrderInputBO.getCouponInstanceCode(),submitOrderInputBO.getAccountCode());
            //3.保存分摊记录为未生效
            savePreferentialRecord(discountDTO,submitOrderInputBO);
            return null;
        });
        return discountDTO;
    }

    private CouponInstanceIdentifyContext buildCouponInstanceIdentifyContext(CouponInstanceBaseDTO couponInstance,
                                                                                   SubmitOrderInputBO submitOrderInputBO,
                                                                                   CouponUseRuleBaseDTO couponUseRuleBaseDTO) {

        CouponInstanceIdentifyContext context = new CouponInstanceIdentifyContext();
        context.setAccountCode(submitOrderInputBO.getAccountCode());
        context.setAccountName(submitOrderInputBO.getAccountName());
        context.setActivityCode(couponInstance.getActivityCode());
        context.setInstanceCode(couponInstance.getInstanceCode());
        context.setCouponUseRule(couponUseRuleBaseDTO);
        List<CouponInstanceIdentifySkuContext> identifySkuContexts = submitOrderInputBO.getSkuList().stream().map(sku -> {
            CouponInstanceIdentifySkuContext skuContext = new CouponInstanceIdentifySkuContext();
            skuContext.setSkuCode(sku.getSkuCode());
            skuContext.setCategoryCode3(sku.getCategoryCode3());
            skuContext.setShopCode(sku.getShopCode());
            skuContext.setCategoryCode3(sku.getCategoryCode3());
            return skuContext;
        }).collect(Collectors.toList());
        context.setSkuList(identifySkuContexts);
        return context;
    }

    private MarketingDiscountContext buildSubmitOrderDiscountContext(CouponInstanceBaseDTO instanceBaseDTO,
                                                                     CouponUseRuleBaseDTO couponUseRuleBaseDTO,
                                                                     SubmitOrderInputBO submitOrderInputBO) {
        MarketingDiscountContext discountContext = new MarketingDiscountContext();
        discountContext.setCouponAmount(instanceBaseDTO.getCouponAmount());
        discountContext.setAccountCode(instanceBaseDTO.getAccountCode());
        discountContext.setAccountName(instanceBaseDTO.getAccountName());
        discountContext.setActivityCode(instanceBaseDTO.getActivityCode());
        discountContext.setActivityName(instanceBaseDTO.getActivityName());
        discountContext.setActivityCode(instanceBaseDTO.getActivityCode());
        discountContext.setInstanceCode(instanceBaseDTO.getInstanceCode());

        discountContext.setCouponUseRule(couponUseRuleBaseDTO);


        List<MarketingDiscountDetailContext> detailContexts = submitOrderInputBO.getSkuList().stream().map(sku -> {
            MarketingDiscountDetailContext detailContext = new MarketingDiscountDetailContext();
            detailContext.setCategoryCode3(sku.getCategoryCode3());
            detailContext.setShopCode(sku.getShopCode());
            detailContext.setShopName(sku.getShopName());
            detailContext.setSkuCode(sku.getSkuCode());
            detailContext.setProductName(sku.getProductName());
            detailContext.setPurchaseQty(sku.getPurchaseNum());
            detailContext.setSalePrice(sku.getSalePrice());
            return detailContext;
        }).collect(Collectors.toList());

        discountContext.setSkuList(detailContexts);
        return discountContext;
    }




    private void savePreferentialRecord(MarketingCouponDiscountDTO discountDTO,
                                        SubmitOrderInputBO submitOrderInputBO) {
        List<PreferentialRecordBaseDTO> recordDOList = discountDTO.getSkuList().stream().map(sku->{
            PreferentialRecordBaseDTO recordDO = new PreferentialRecordBaseDTO();
            // 使用优惠券时会变为对应订单号
            recordDO.setBizCode("N"+discountDTO.getActivityCode());
            recordDO.setPreferentialCode(discountDTO.getActivityCode());
            recordDO.setPreferentialName(discountDTO.getActivityName());
            recordDO.setPreferentialType("COUPON");
            recordDO.setPurchaseQty(sku.getPurchaseQty());
            recordDO.setSalePrice(sku.getSalePrice());
            recordDO.setSkuCode(sku.getSkuCode());
            recordDO.setProductName(sku.getProductName());
            recordDO.setOriginAmount(sku.getOriginAmount());
            recordDO.setPreferentialAmount(sku.getCouponPromotionAmount());
            recordDO.setCreatorCode(submitOrderInputBO.getAccountCode());
            recordDO.setCreator(submitOrderInputBO.getAccountName());
            recordDO.setModifier(submitOrderInputBO.getAccountName());
            recordDO.setModifierCode(submitOrderInputBO.getAccountCode());
            recordDO.setStatus("N");
            return recordDO;
        }).collect(Collectors.toList());
        preferentialRecordRepository.batchInsert(recordDOList);
    }



    @Override
    public SettleCartDTO settleCart(SettleCartBO settleCartBO) {
        //1.获取当前用户所有优惠券实例
        List<CouponInstanceBaseDTO> couponInstanceList = getInstanceBaseDTOS(settleCartBO);
        if(CollectionUtils.isEmpty(couponInstanceList)){
            return SettleCartDTO.builder().availableCouponInstanceList(Lists.newArrayList()).disableCouponInstanceList(Lists.newArrayList()).build();
        }
        //2.查询活动信息
        List<String> activityCodeList = couponInstanceList.stream().map(CouponInstanceBaseDTO::getActivityCode).distinct().collect(Collectors.toList());
        List<CouponActivityBaseDTO> couponActivityBaseDTOS = couponActivityRepository.queryByActivityCodeList(activityCodeList);
        Map<String, CouponActivityBaseDTO> couponActivityBaseDTOMap = couponActivityBaseDTOS.stream().collect(Collectors.toMap(CouponActivityBaseDTO::getActivityCode, Function.identity(), (k1, k2) -> k1));
        //3.查询使用规则
        List<CouponUseRuleBaseDTO> couponUseRuleBaseDTOS = couponUseRuleRepository.queryByActivityCodeList(activityCodeList);
        Map<String, CouponUseRuleBaseDTO> couponUseRuleBaseDTOMap = couponUseRuleBaseDTOS.stream().collect(Collectors.toMap(CouponUseRuleBaseDTO::getActivityCode, Function.identity(), (k1, k2) -> k1));
        //3.识别出可用的优惠券实例
        List<CouponInstanceIdentifyContext> couponInstanceIdentifyContexts = buildCouponInstanceIdentifyContext(couponInstanceList, settleCartBO, couponUseRuleBaseDTOMap);
        List<String> availableCouponInstanceCodeList= couponInstanceIdentifyContexts.stream().filter(context -> identifyCore.identifyCouponInstanceAvailable(context))
                .map(CouponInstanceIdentifyContext::getInstanceCode).collect(Collectors.toList());
        List<CouponInstanceBaseDTO> availableCouponInstanceList = couponInstanceList.stream().filter(couponInstanceBaseDTO -> availableCouponInstanceCodeList.contains(couponInstanceBaseDTO.getInstanceCode())).collect(Collectors.toList());
        //4.计算优惠券实例分摊金额
        List<MarketingDiscountContext> marketingDiscountContexts = buildSettleCartMarketingDiscountContext(availableCouponInstanceList,couponUseRuleBaseDTOMap,settleCartBO);
        List<MarketingCouponDiscountDTO> discountDTOList = marketingDiscountContexts.stream()
                .map(marketingDiscountContext -> marketingDiscountCore.calculateSkuDiscount(marketingDiscountContext)).filter(Objects::nonNull).collect(Collectors.toList());
        //4.构建可参与优惠券活动
        List<SettleCartCouponDTO> availableSettleCartCouponDTOList = buildSettleCartCouponDTOList(availableCouponInstanceList, discountDTOList,couponActivityBaseDTOMap);
        SettleCartDTO settleCartDTO = new SettleCartDTO();
        settleCartDTO.setAvailableCouponInstanceList(availableSettleCartCouponDTOList);
        //5.获取不可参与优惠券活动
        List<SettleCartCouponDTO> disableSettleCartCouponDTOList = couponInstanceList.stream()
                .filter(couponInstanceBaseDTO -> !availableCouponInstanceCodeList.contains(couponInstanceBaseDTO.getInstanceCode()))
                .map(couponInstanceBaseDTO -> {
                    CouponActivityBaseDTO couponActivityBaseDTO = couponActivityBaseDTOMap.get(couponInstanceBaseDTO.getActivityCode());
                    SettleCartCouponDTO cartCouponDTO = convert.convertToSettleCartCouponDTO(couponActivityBaseDTO);
                    cartCouponDTO.setInstanceCode(couponInstanceBaseDTO.getInstanceCode());
                    cartCouponDTO.setTotalCouponPromotionAmount(BigDecimal.ZERO);
                    return cartCouponDTO;
                }).collect(Collectors.toList());
        settleCartDTO.setDisableCouponInstanceList(disableSettleCartCouponDTOList);
        return settleCartDTO;
    }

    private List<CouponInstanceBaseDTO> getInstanceBaseDTOS(SettleCartBO settleCartBO) {
        if(StringUtils.isNotBlank(settleCartBO.getInstanceCode())){
            CouponInstanceBaseDTO instance = couponInstanceRepository.getUserAvailableCouponInstanceByInstanceCode(settleCartBO.getInstanceCode(), settleCartBO.getAccountCode());
            if(Objects.isNull(instance)){
                return Lists.newArrayList();
            }
            return Lists.newArrayList(instance);
        }
        return couponInstanceRepository.queryAvailableCouponInstanceByAccountCode(settleCartBO.getAccountCode());
    }

    private List<CouponInstanceIdentifyContext> buildCouponInstanceIdentifyContext(List<CouponInstanceBaseDTO> couponInstanceList,
                                                                                   SettleCartBO settleCartBO,
                                                                                   Map<String, CouponUseRuleBaseDTO> couponUseRuleBaseDTOMap) {
        return couponInstanceList.stream().map(couponInstance -> {
            CouponInstanceIdentifyContext context = new CouponInstanceIdentifyContext();
            context.setAccountCode(settleCartBO.getAccountCode());
            context.setAccountName(settleCartBO.getAccountName());
            context.setActivityCode(couponInstance.getActivityCode());
            context.setInstanceCode(couponInstance.getInstanceCode());
            CouponUseRuleBaseDTO couponUseRuleBaseDTO = couponUseRuleBaseDTOMap.get(couponInstance.getActivityCode());
            context.setCouponUseRule(couponUseRuleBaseDTO);
            List<CouponInstanceIdentifySkuContext> identifySkuContexts = settleCartBO.getSkuList().stream().map(sku -> {
                CouponInstanceIdentifySkuContext skuContext = new CouponInstanceIdentifySkuContext();
                skuContext.setSkuCode(sku.getSkuCode());
                skuContext.setCategoryCode3(sku.getCategoryCode3());
                skuContext.setShopCode(sku.getShopCode());
                return skuContext;
            }).collect(Collectors.toList());
            context.setSkuList(identifySkuContexts);
            return context;
        }).collect(Collectors.toList());
    }

    private List<SettleCartCouponDTO> buildSettleCartCouponDTOList(List<CouponInstanceBaseDTO> availableCouponInstanceList,
                                                                   List<MarketingCouponDiscountDTO> discountDTOList,
                                                                   Map<String, CouponActivityBaseDTO> couponActivityBaseDTOMap) {
        Map<String, MarketingCouponDiscountDTO> discountDTOMap = discountDTOList.stream().collect(Collectors.toMap(MarketingCouponDiscountDTO::getInstanceCode, Function.identity(), (k1, k2) -> k1));
        return availableCouponInstanceList.stream().map(couponInstanceBaseDTO -> {
            CouponActivityBaseDTO couponActivityBaseDTO = couponActivityBaseDTOMap.get(couponInstanceBaseDTO.getActivityCode());

            MarketingCouponDiscountDTO marketingCouponDiscountDTO = discountDTOMap.get(couponInstanceBaseDTO.getInstanceCode());
            SettleCartCouponDTO cartCouponDTO = convert.convertToSettleCartCouponDTO(couponActivityBaseDTO);
            cartCouponDTO.setTotalCouponPromotionAmount(marketingCouponDiscountDTO.getTotalCouponPromotionAmount());
            cartCouponDTO.setInstanceCode(couponInstanceBaseDTO.getInstanceCode());
            return cartCouponDTO;
        }).collect(Collectors.toList());
    }
    private List<MarketingDiscountContext> buildSettleCartMarketingDiscountContext(List<CouponInstanceBaseDTO> availableCouponInstanceList,
                                                                                   Map<String, CouponUseRuleBaseDTO> couponUseRuleBaseDTOMap,
                                                                                   SettleCartBO settleCartBO) {
        return availableCouponInstanceList.stream().map(couponActivity -> {
            MarketingDiscountContext discountContext = new MarketingDiscountContext();
            discountContext.setCouponAmount(couponActivity.getCouponAmount());
            discountContext.setAccountCode(settleCartBO.getAccountCode());
            discountContext.setAccountName(settleCartBO.getAccountName());
            discountContext.setActivityCode(couponActivity.getActivityCode());
            discountContext.setActivityName(couponActivity.getActivityName());
            discountContext.setActivityCode(couponActivity.getActivityCode());
            discountContext.setInstanceCode(couponActivity.getInstanceCode());

            CouponUseRuleBaseDTO couponUseRuleBaseDTO = couponUseRuleBaseDTOMap.get(couponActivity.getActivityCode());
            discountContext.setCouponUseRule(couponUseRuleBaseDTO);


            List<MarketingDiscountDetailContext> detailContexts = settleCartBO.getSkuList().stream().map(sku -> {
                MarketingDiscountDetailContext detailContext = new MarketingDiscountDetailContext();
                detailContext.setCategoryCode3(sku.getCategoryCode3());
                detailContext.setShopCode(sku.getShopCode());
                detailContext.setShopName(sku.getShopName());
                detailContext.setSkuCode(sku.getSkuCode());
                detailContext.setProductName(sku.getProductName());
                detailContext.setPurchaseQty(sku.getPurchaseNum());
                detailContext.setSalePrice(sku.getSalePrice());
                return detailContext;
            }).collect(Collectors.toList());

            discountContext.setSkuList(detailContexts);
            return discountContext;
        }).collect(Collectors.toList());
    }
}
