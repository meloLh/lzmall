package com.melo.lzmall.marketing.service.service.impl;


import com.melo.lzmall.marketing.service.core.MarketingIdentifyCore;
import com.melo.lzmall.marketing.service.core.context.identity.MarketingIdentifyContext;
import com.melo.lzmall.marketing.service.core.context.identity.CouponInstanceIdentifySkuContext;
import com.melo.lzmall.marketing.service.entity.bo.SkuMarketingIdentifyBO;
import com.melo.lzmall.marketing.service.entity.bo.sendrule.SkuPreferentialIdentifyBO;
import com.melo.lzmall.marketing.service.service.MarketingIdentifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class MarketingIdentifyServiceImpl implements MarketingIdentifyService {

    @Autowired
    private MarketingIdentifyCore identifyCore;

    @Override
    public SkuMarketingIdentifyBO querySkuMarketingInfo(SkuPreferentialIdentifyBO skuPreferentialIdentifyBO) {
        MarketingIdentifyContext context = buildMarketingIdentifyContext(skuPreferentialIdentifyBO);
        return identifyCore.identifySkuAvailableCouponActivity(context);
    }

    private MarketingIdentifyContext buildMarketingIdentifyContext(SkuPreferentialIdentifyBO skuPreferentialIdentifyBO) {
        MarketingIdentifyContext context = new MarketingIdentifyContext();
        context.setAccountCode(skuPreferentialIdentifyBO.getAccountCode());

        List<CouponInstanceIdentifySkuContext> skuIdentifyContexts = skuPreferentialIdentifyBO.getSkuList().stream().map(detailBO -> {
            CouponInstanceIdentifySkuContext skuIdentifyContext = new CouponInstanceIdentifySkuContext();
            skuIdentifyContext.setSkuCode(detailBO.getSkuCode());
            skuIdentifyContext.setCategoryCode3(detailBO.getCategoryCode3());
            skuIdentifyContext.setShopCode(detailBO.getShopCode());
            return skuIdentifyContext;
        }).collect(Collectors.toList());

        context.setSkuList(skuIdentifyContexts);
        return context;
    }
}
