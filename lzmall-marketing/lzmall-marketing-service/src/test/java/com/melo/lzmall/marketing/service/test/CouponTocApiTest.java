package com.melo.lzmall.marketing.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponCenterRequest;
import com.melo.lzmall.marketing.api.request.coupon.ReceiveCouponRequest;
import com.melo.lzmall.marketing.api.response.coupon.ReceiveCouponCenterResponse;
import com.melo.lzmall.marketing.service.api.CouponTocApi;
import com.melo.lzmall.marketing.service.controller.CouponTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class CouponTocApiTest extends MarketingBaseTest{

    @Autowired
    private CouponTocController couponTocApi;

    @Test
    public void receiveCouponInstance(){
        ReceiveCouponRequest request = new ReceiveCouponRequest();
        request.setActivityCode("12345");
        Result<Void> result = couponTocApi.receiveCoupon(request);
        log.info(JSON.toJSONString(result));
    }


    @Test
    public void queryReceiveCouponCenter(){
        ReceiveCouponCenterRequest request = new ReceiveCouponCenterRequest();
        request.setShopCode("1");
        Result<ReceiveCouponCenterResponse> result = couponTocApi.queryReceiveCouponCenter(request);
        log.info(JSON.toJSONString(result));
    }
}
