package com.melo.lzmall.marketing.service.test;

import com.melo.lzmall.marketing.service.MarketingApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = MarketingApplication.class)
@RunWith(SpringRunner.class)
public class MarketingBaseTest {
}
