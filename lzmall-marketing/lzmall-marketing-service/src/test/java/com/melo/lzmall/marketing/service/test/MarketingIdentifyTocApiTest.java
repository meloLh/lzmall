package com.melo.lzmall.marketing.service.test;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingDetailRequest;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import com.melo.lzmall.marketing.service.api.MarketingIdentifyTocApi;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class MarketingIdentifyTocApiTest extends MarketingBaseTest {

    @Autowired
    private MarketingIdentifyTocApi identifyTocApi;

    @Test
    public void queryAllCategory(){
        QuerySkuMarketingRequest request = new QuerySkuMarketingRequest();
        request.setAccountCode("xxx");

        QuerySkuMarketingDetailRequest detailRequest = new QuerySkuMarketingDetailRequest();
        detailRequest.setCategoryCode3("1");
        detailRequest.setShopCode("1");
        detailRequest.setSkuCode("KU123");
        detailRequest.setShopName("xxx");
        request.setSkuList(Lists.newArrayList(detailRequest));
        Result<SkuMarketingResponse> result = identifyTocApi.querySkuMarketingList(request);
        log.info(JSON.toJSONString(result));
    }
}
