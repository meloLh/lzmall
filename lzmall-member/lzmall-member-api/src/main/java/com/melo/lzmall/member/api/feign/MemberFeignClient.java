package com.melo.lzmall.member.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
//import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "lzmall-member",contextId = "lzmall-member-memberFeignClient")
public interface MemberFeignClient {

//    @ApiOperation("通过账号获取会员基本信息")
    @GetMapping("/buyer/member/getMemberInfoByAccountCode")
    Result<MemberInfoResponse> getMemberInfoByAccountCode(@RequestParam(name = "accountCode") String accountCode);
}
