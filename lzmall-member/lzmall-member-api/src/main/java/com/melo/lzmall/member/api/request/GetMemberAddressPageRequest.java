package com.melo.lzmall.member.api.request;

import com.melo.lzmall.common.core.dto.PageRequest;
import lombok.Data;

import java.io.Serializable;
@Data
public class GetMemberAddressPageRequest extends PageRequest implements Serializable {

}
