package com.melo.lzmall.member.api.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class UpdateMemberAddressRequest implements Serializable {

    @NotBlank
    private String id;

    /**
     * 手机号码
     */
    @NotBlank
    private String mobile;
    /**
     * 收货人姓名
     */
    @NotBlank
    private String name;
    /**
     * 详细地址
     */
    private String addressDetail;
    /**
     * 地址链路编码
     */
    @NotEmpty
    private List<String> addressCodePath;
    /**
     * 地址链路名称
     */
    @NotEmpty
    private  List<String> addressNamePath;
    /**
     * 是否为默认收货地址
     */
    private Boolean isDefault;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 经度
     */
    private String lon;
}
