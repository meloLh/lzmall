package com.melo.lzmall.member.api.response;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会员收货地址表
 */
@Data
public class MemberAddressResponse implements Serializable {

    /**
     * ID
     */
    private String id;
    /**
     * 会员编码
     */
    private String memberCode;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 详细地址
     */
    private String addressDetail;
    /**
     * 地址链路编码
     */
    private String addressCodePath;
    /**
     * 地址链路名称
     */
    private String addressNamePath;
    /**
     * 是否为默认收货地址
     */
    private Boolean isDefault;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 经度
     */
    private String lon;
}
