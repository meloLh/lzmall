package com.melo.lzmall.member.api.response;




import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class MemberInfoResponse implements Serializable {

    /**
     * 会员编码
     */
    private String memberCode;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 会员头像
     */
    private String avatar;
    /**
     * 会员出生日期
     */
    private Date birthday;
    /**
     * 会员手机号
     */
    private String mobile;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 会员性别
     */
    private Integer sex;
    /**
     * 会员积分
     */
    private Integer memberPoint;
    /**
     * 会员等级
     */
    private String memberGrade;
    /**
     * 会员状态
     */
    private String status;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
