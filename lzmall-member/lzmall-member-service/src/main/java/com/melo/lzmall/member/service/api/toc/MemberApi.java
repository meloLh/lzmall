package com.melo.lzmall.member.service.api.toc;

import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.member.service.converter.MemberFaceConverter;
import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.service.MemberService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
public class MemberApi {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberFaceConverter converter;


    @ApiOperation("通过账号获取会员基本信息")
    @GetMapping("/buyer/member/getMemberInfoByAccountCode")
    public Result<MemberInfoResponse> getMemberInfoByAccountCode(@RequestParam(name = "accountCode") String accountCode) {
        MemberBaseDTO  memberBaseDTO = memberService.getMemberByAccountCode(accountCode);
        MemberInfoResponse response = converter.convertToMemberInfoResponse(memberBaseDTO);
        return Result.success(response);
    }
}
