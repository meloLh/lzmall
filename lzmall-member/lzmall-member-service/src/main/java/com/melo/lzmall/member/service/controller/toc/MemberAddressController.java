package com.melo.lzmall.member.service.controller.toc;

import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.member.api.request.AddMemberAddressRequest;
import com.melo.lzmall.member.api.request.UpdateMemberAddressRequest;
import com.melo.lzmall.member.api.response.MemberAddressResponse;
import com.melo.lzmall.member.service.converter.MemberAddressFaceConverter;
import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.dto.SaveMemberAddressDTO;
import com.melo.lzmall.member.service.service.MemberAddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping
public class MemberAddressController {

    @Autowired
    private MemberAddressService memberAddressService;

    @Autowired
    private MemberAddressFaceConverter converter;

    @ApiOperation("获取会员默认收货地址")
    @GetMapping("/buyer/member/address/getMemberDefaultAddress")
    @MallAuth
    public Result<MemberAddressResponse> getMemberDefaultAddress() {
        MemberAddressBaseDTO memberAddressBaseDTO = memberAddressService.getMemberDefaultAddress();
        MemberAddressResponse response = converter.convertToMemberAddressResponse(memberAddressBaseDTO);
        return Result.success(response);
    }


    @ApiOperation("获取会员收货地址列表")
    @GetMapping("/buyer/member/address/getMemberAddressPage")
    @MallAuth
    public Result<Page<MemberAddressResponse>> getMemberAddressPage(@RequestParam(name = "pageIndex")Integer pageIndex,
                                                                    @RequestParam(name = "pageSize")Integer pageSize) {
        Page<MemberAddressBaseDTO> memberAddressBaseDTOPage = memberAddressService.getMemberAddressPage(pageIndex,pageSize);
        Page<MemberAddressResponse> response = converter.convertToMemberAddressResponsePage(memberAddressBaseDTOPage);
        return Result.success(response);
    }

    @ApiOperation("保存会员收货地址列表")
    @PostMapping("/buyer/member/address/addMemberAddress")
    @MallAuth
    public Result<Void> addMemberAddress(@Validated @RequestBody AddMemberAddressRequest request) {
        SaveMemberAddressDTO saveMemberAddressDTO = converter.convertToSaveMemberAddressDTO(request);
        memberAddressService.addMemberAddress(saveMemberAddressDTO);
        return Result.success();
    }

    @ApiOperation("修改会员收货地址列表")
    @PostMapping("/buyer/member/address/updateMemberAddress")
    @MallAuth
    public Result<Void> updateMemberAddress(@Validated @RequestBody UpdateMemberAddressRequest request) {
        SaveMemberAddressDTO saveMemberAddressDTO = converter.convertToSaveMemberAddressDTO(request);
        memberAddressService.updateMemberAddress(saveMemberAddressDTO);
        return Result.success();
    }

    @ApiOperation("保存会员收货地址列表")
    @GetMapping("/buyer/member/address/getMemberAddress")
    @MallAuth
    public Result<MemberAddressResponse> getMemberAddress(@Validated @RequestParam String id) {
        MemberAddressBaseDTO memberAddressBaseDTO =  memberAddressService.getMemberAddress(Long.valueOf(id));
        MemberAddressResponse response = converter.convertToMemberAddressResponse(memberAddressBaseDTO);
        return Result.success(response);
    }

    @ApiOperation("保存会员收货地址列表")
    @DeleteMapping("/buyer/member/address/deleteMemberAddress")
    @MallAuth
    public Result<Void> deleteMemberAddress(@Validated @RequestParam String id) {
        memberAddressService.deleteMemberAddress(Long.valueOf(id));
        return Result.success();
    }
}
