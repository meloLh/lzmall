package com.melo.lzmall.member.service.controller.toc;

import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.member.service.converter.MemberFaceConverter;
import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.service.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;



@RestController
@RequestMapping
public class MemberTocController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberFaceConverter converter;


    @ApiOperation("登录成功获取会员信息")
    @GetMapping("/buyer/member/getInitMemberInfo")
    @MallAuth
    public Result<MemberInfoResponse> getInitMemberInfo() {
        AuthAccount accountInfo = AccountContextUtil.getAccountInfo();
        MemberBaseDTO  memberBaseDTO = memberService.getMemberByAccountCode(accountInfo.getAccountCode());
        MemberInfoResponse response = converter.convertToMemberInfoResponse(memberBaseDTO);
        return Result.success(response);
    }
}
