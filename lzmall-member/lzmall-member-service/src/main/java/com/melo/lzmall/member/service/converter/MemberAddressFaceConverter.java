package com.melo.lzmall.member.service.converter;


import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.member.api.request.AddMemberAddressRequest;
import com.melo.lzmall.member.api.request.UpdateMemberAddressRequest;
import com.melo.lzmall.member.api.response.MemberAddressResponse;
import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.dto.SaveMemberAddressDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface MemberAddressFaceConverter {

    MemberAddressResponse convertToMemberAddressResponse(MemberAddressBaseDTO memberAddressBaseDTO);

    Page<MemberAddressResponse> convertToMemberAddressResponsePage(Page<MemberAddressBaseDTO> memberAddressBaseDTOPage);

    SaveMemberAddressDTO convertToSaveMemberAddressDTO(AddMemberAddressRequest request);

    SaveMemberAddressDTO convertToSaveMemberAddressDTO(UpdateMemberAddressRequest request);
}

