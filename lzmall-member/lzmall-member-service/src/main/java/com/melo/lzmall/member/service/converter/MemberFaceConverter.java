package com.melo.lzmall.member.service.converter;


import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import org.mapstruct.Mapper;

import java.util.Map;


@Mapper(componentModel = "spring")
public interface  MemberFaceConverter {

    MemberInfoResponse convertToMemberInfoResponse(MemberBaseDTO memberBaseDTO);
}

