package com.melo.lzmall.member.service.entity.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class AddCartDTO implements Serializable {

    /**商品编码不能为空*/
    private String skuCode;

    private Integer buyCount;

    private String memberCode;
}
