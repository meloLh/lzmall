package com.melo.lzmall.member.service.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SaveMemberAddressDTO implements Serializable {


    private String id;

    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 详细地址
     */
    private String addressDetail;

    private List<String> addressCodePath;
    /**
     * 地址链路名称
     */
    private List<String> addressNamePath;
    /**
     * 是否为默认收货地址
     */
    private Boolean isDefault;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 经度
     */
    private String lon;
}
