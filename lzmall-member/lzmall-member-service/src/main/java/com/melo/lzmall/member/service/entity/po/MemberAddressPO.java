package com.melo.lzmall.member.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员收货地址表
 */
@Data
@TableName("member_address")
public class MemberAddressPO implements Serializable {
private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 会员编码
	 */
	private String memberCode;
	/**
	 * 手机号码
	 */
	private String mobile;
	/**
	 * 收货人姓名
	 */
	private String name;
	/**
	 * 
	 */
	private String provinceCode;
	/**
	 * 
	 */
	private String provinceName;
	/**
	 * 
	 */
	private String cityCode;
	/**
	 * 
	 */
	private String cityName;
	/**
	 * 
	 */
	private String districtCode;
	/**
	 * 
	 */
	private String districtName;

	private String townCode;

	private String townName;

	/**
	 * 详细地址
	 */
	private String addressDetail;
	/**
	 * 地址链路编码
	 */
	private String addressCodePath;
	/**
	 * 地址链路名称
	 */
	private String addressNamePath;
	/**
	 * 是否为默认收货地址
	 */
	private Boolean isDefault;
	/**
	 * 纬度
	 */
	private String lat;
	/**
	 * 经度
	 */
	private String lon;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
