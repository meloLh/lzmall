package com.melo.lzmall.member.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 会员表
 */
@Data
@TableName("member")
public class MemberPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Long id;
    /**
     * 会员编码
     */
    private String memberCode;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 会员头像
     */
    private String avatar;
    /**
     * 会员出生日期
     */
    private Date birthday;
    /**
     * 会员手机号
     */
    private String mobile;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 会员性别
     */
    private Integer sex;

    /**账户编号*/
    private String accountCode;
    /**
     * 会员积分
     */
    private Integer memberPoint;
    /**
     * 会员等级
     */
    private String memberGrade;
    /**
     * 会员状态
     */
    private String status;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
