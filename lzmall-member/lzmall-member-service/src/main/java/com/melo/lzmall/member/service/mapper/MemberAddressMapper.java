package com.melo.lzmall.member.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.member.service.entity.po.MemberAddressPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface MemberAddressMapper extends BaseMapper<MemberAddressPO> {
	
}
