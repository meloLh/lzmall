package com.melo.lzmall.member.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.utils.PageUtil;
import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.po.MemberAddressPO;
import com.melo.lzmall.member.service.mapper.MemberAddressMapper;
import com.melo.lzmall.member.service.repository.converter.MemberAddressRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
public class MemberAddressRepository {

    @Autowired
    private MemberAddressMapper memberAddressMapper;

    @Autowired
    private MemberAddressRepositoryConverter converter;

    public MemberAddressBaseDTO getMemberDefaultAddress(String memberCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员信息不能为空");
        LambdaQueryWrapper<MemberAddressPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberAddressPO::getMemberCode,memberCode)
                .eq(MemberAddressPO::getIsDefault,Boolean.TRUE);
        MemberAddressPO memberAddressPO = memberAddressMapper.selectOne(queryWrapper);
        return   converter.convertToMemberAddressBaseDTO(memberAddressPO);
    }

    public List<MemberAddressBaseDTO> queryMemberAddressPage(String memberCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员信息不能为空");
        LambdaQueryWrapper<MemberAddressPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberAddressPO::getMemberCode,memberCode);
        List<MemberAddressPO> memberAddressPOS = memberAddressMapper.selectList(queryWrapper);
        return   converter.convertToMemberAddressBaseDTOList(memberAddressPOS);
    }

    public Page<MemberAddressBaseDTO> getMemberAddressPage(String memberCode,Integer pageIndex,Integer pageSize) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员信息不能为空");
        IPage<MemberAddressPO> objectPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageIndex,pageSize);

        LambdaQueryWrapper<MemberAddressPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberAddressPO::getMemberCode,memberCode);
        IPage<MemberAddressPO> iPage = memberAddressMapper.selectPage(objectPage, queryWrapper);

        Page page = PageUtil.copyIPageNodata(iPage);
        List<MemberAddressBaseDTO> memberAddressPOS = converter.convertToMemberAddressBaseDTOList(iPage.getRecords());
        page.setList(memberAddressPOS);
        return page;
    }

    public void insert(MemberAddressBaseDTO addressBaseDTO) {
        MemberAddressPO memberAddressPO = converter.convertToMemberAddressPO(addressBaseDTO);
        memberAddressMapper.insert(memberAddressPO);
    }

    public void updateById(MemberAddressBaseDTO addressBaseDTO) {
        MemberAddressPO memberAddressPO = converter.convertToMemberAddressPO(addressBaseDTO);
        int rows = memberAddressMapper.updateById(memberAddressPO);
        AssertUtil.isTrue(rows == 1,"更新会员地址信息异常");
    }

    public MemberAddressBaseDTO getMemberAddressById(Long id) {
        AssertUtil.isTrue(Objects.nonNull(id),"id不能为空");
        MemberAddressPO memberAddressPO = memberAddressMapper.selectById(id);
        return   converter.convertToMemberAddressBaseDTO(memberAddressPO);
    }

    public void clearMemberDefaultAddress(String memberCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员信息不能为空");
        LambdaQueryWrapper<MemberAddressPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberAddressPO::getMemberCode,memberCode);
        MemberAddressPO update =  new MemberAddressPO();
        update.setMemberCode(memberCode);
        update.setIsDefault(Boolean.FALSE);
        memberAddressMapper.update(update,queryWrapper);
    }

    public void deleteMemberAddressById(Long id) {
        AssertUtil.isTrue(Objects.nonNull(id),"id不能为空");
        memberAddressMapper.deleteById(id);
    }
}

