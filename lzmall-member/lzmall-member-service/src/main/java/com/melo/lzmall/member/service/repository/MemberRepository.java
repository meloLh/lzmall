package com.melo.lzmall.member.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.entity.po.MemberPO;
import com.melo.lzmall.member.service.mapper.MemberMapper;
import com.melo.lzmall.member.service.repository.converter.MemberRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class MemberRepository {

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private MemberRepositoryConverter converter;

    public MemberBaseDTO getMemberByAccountCode(String accountCode) {
        if(StringUtils.isBlank(accountCode)){
            return null;
        }
        LambdaQueryWrapper<MemberPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(MemberPO::getAccountCode,accountCode);
        MemberPO memberPO = memberMapper.selectOne(queryWrapper);
        return   converter.convertToMemberBaseDTO(memberPO);
    }

    public MemberBaseDTO getMemberByMemberCode(String memberCode) {
        if(StringUtils.isBlank(memberCode)){
            return null;
        }
        LambdaQueryWrapper<MemberPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(MemberPO::getMemberCode,memberCode);
        MemberPO memberPO = memberMapper.selectOne(queryWrapper);
        return   converter.convertToMemberBaseDTO(memberPO);
    }
}

