package com.melo.lzmall.member.service.repository.converter;


import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.po.MemberAddressPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface MemberAddressRepositoryConverter {

    MemberAddressBaseDTO convertToMemberAddressBaseDTO(MemberAddressPO memberAddressPO);

    List<MemberAddressBaseDTO> convertToMemberAddressBaseDTOList(List<MemberAddressPO> records);

    MemberAddressPO convertToMemberAddressPO(MemberAddressBaseDTO addressBaseDTO);
}

