package com.melo.lzmall.member.service.repository.converter;


import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.entity.po.MemberPO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface MemberRepositoryConverter {

    MemberBaseDTO convertToMemberBaseDTO(MemberPO memberPO);
}

