package com.melo.lzmall.member.service.service;


import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.dto.SaveMemberAddressDTO;

public interface MemberAddressService {
    MemberAddressBaseDTO getMemberDefaultAddress();

    Page<MemberAddressBaseDTO> getMemberAddressPage(Integer pageIndex,Integer pageSize);

    void addMemberAddress(SaveMemberAddressDTO saveMemberAddressDTO);

    void updateMemberAddress(SaveMemberAddressDTO saveMemberAddressDTO);

    MemberAddressBaseDTO getMemberAddress(Long id);

    void deleteMemberAddress(Long valueOf);
}

