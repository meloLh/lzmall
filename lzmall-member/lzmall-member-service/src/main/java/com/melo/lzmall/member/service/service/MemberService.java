package com.melo.lzmall.member.service.service;


import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;

public interface MemberService {
    MemberBaseDTO getMemberByMemberCode(String memberCode);

    MemberBaseDTO getMemberByAccountCode(String accountCode);
}

