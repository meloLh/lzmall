package com.melo.lzmall.member.service.service.impl;

import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.member.service.entity.base.MemberAddressBaseDTO;
import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.entity.dto.SaveMemberAddressDTO;
import com.melo.lzmall.member.service.repository.MemberAddressRepository;
import com.melo.lzmall.member.service.repository.MemberRepository;
import com.melo.lzmall.member.service.service.MemberAddressService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MemberAddressServiceImpl  implements MemberAddressService {

    @Autowired
    private MemberAddressRepository memberAddressRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public MemberAddressBaseDTO getMemberDefaultAddress() {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberBaseDTO memberBaseDTO = memberRepository.getMemberByAccountCode(authAccount.getAccountCode());
        return memberAddressRepository.getMemberDefaultAddress(memberBaseDTO.getMemberCode());
    }

    @Override
    public Page<MemberAddressBaseDTO> getMemberAddressPage(Integer pageIndex,Integer pageSize) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberBaseDTO memberBaseDTO = memberRepository.getMemberByAccountCode(authAccount.getAccountCode());
        return memberAddressRepository.getMemberAddressPage(memberBaseDTO.getMemberCode(),pageIndex,pageSize);
    }

    @Override
    public void addMemberAddress(SaveMemberAddressDTO saveMemberAddressDTO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberBaseDTO memberBaseDTO = memberRepository.getMemberByAccountCode(authAccount.getAccountCode());
        MemberAddressBaseDTO addressBaseDTO =  buildMemberAddressBaseDTO(saveMemberAddressDTO,memberBaseDTO.getMemberCode());
        memberAddressRepository.insert(addressBaseDTO);
    }

    @Override
    public void updateMemberAddress(SaveMemberAddressDTO saveMemberAddressDTO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberBaseDTO memberBaseDTO = memberRepository.getMemberByAccountCode(authAccount.getAccountCode());
        MemberAddressBaseDTO addressBaseDTO =  buildMemberAddressBaseDTO(saveMemberAddressDTO,memberBaseDTO.getMemberCode());
        if(Objects.equals(addressBaseDTO.getIsDefault(),Boolean.TRUE)){
            memberAddressRepository.clearMemberDefaultAddress(memberBaseDTO.getMemberCode());
        }
        memberAddressRepository.updateById(addressBaseDTO);
    }

    @Override
    public MemberAddressBaseDTO getMemberAddress(Long id) {
        return memberAddressRepository.getMemberAddressById(id);
    }

    @Override
    public void deleteMemberAddress(Long id) {
         memberAddressRepository.deleteMemberAddressById(id);
    }

    private MemberAddressBaseDTO buildMemberAddressBaseDTO(SaveMemberAddressDTO saveMemberAddressDTO,
                                                           String memberCode) {
        MemberAddressBaseDTO addressBaseDTO = new MemberAddressBaseDTO();
        if(StringUtils.isNotBlank(saveMemberAddressDTO.getId())){
            addressBaseDTO.setId(Long.valueOf(saveMemberAddressDTO.getId()));
        }
        addressBaseDTO.setMemberCode(memberCode);
        addressBaseDTO.setAddressDetail(saveMemberAddressDTO.getAddressDetail());
        addressBaseDTO.setIsDefault(saveMemberAddressDTO.getIsDefault());
        addressBaseDTO.setName(saveMemberAddressDTO.getName());
        addressBaseDTO.setMobile(saveMemberAddressDTO.getMobile());
        addressBaseDTO.setAddressCodePath(String.join(",", saveMemberAddressDTO.getAddressCodePath()));
        addressBaseDTO.setAddressNamePath(String.join(",", saveMemberAddressDTO.getAddressNamePath()));
        fillAddressPath(saveMemberAddressDTO, addressBaseDTO);
        return addressBaseDTO;
    }

    private void fillAddressPath(SaveMemberAddressDTO saveMemberAddressDTO, MemberAddressBaseDTO addressBaseDTO) {
        if(CollectionUtils.size(saveMemberAddressDTO.getAddressCodePath()) == 3){
            addressBaseDTO.setProvinceCode(saveMemberAddressDTO.getAddressCodePath().get(0));
            addressBaseDTO.setCityCode(saveMemberAddressDTO.getAddressCodePath().get(0));
            addressBaseDTO.setDistrictCode(saveMemberAddressDTO.getAddressCodePath().get(1));
            addressBaseDTO.setTownCode(saveMemberAddressDTO.getAddressCodePath().get(2));
        }else{
            addressBaseDTO.setProvinceCode(saveMemberAddressDTO.getAddressCodePath().get(0));
            addressBaseDTO.setCityCode(saveMemberAddressDTO.getAddressCodePath().get(1));
            addressBaseDTO.setDistrictCode(saveMemberAddressDTO.getAddressCodePath().get(2));
            addressBaseDTO.setTownCode(saveMemberAddressDTO.getAddressCodePath().get(3));
        }
        if(CollectionUtils.size(saveMemberAddressDTO.getAddressNamePath()) == 3){
            addressBaseDTO.setProvinceName(saveMemberAddressDTO.getAddressNamePath().get(0));
            addressBaseDTO.setCityName(saveMemberAddressDTO.getAddressNamePath().get(0));
            addressBaseDTO.setDistrictName(saveMemberAddressDTO.getAddressNamePath().get(1));
            addressBaseDTO.setTownName(saveMemberAddressDTO.getAddressNamePath().get(2));
        }else {
            addressBaseDTO.setProvinceName(saveMemberAddressDTO.getAddressNamePath().get(0));
            addressBaseDTO.setCityName(saveMemberAddressDTO.getAddressNamePath().get(1));
            addressBaseDTO.setDistrictName(saveMemberAddressDTO.getAddressNamePath().get(2));
            addressBaseDTO.setTownName(saveMemberAddressDTO.getAddressNamePath().get(3));
        }
    }
}