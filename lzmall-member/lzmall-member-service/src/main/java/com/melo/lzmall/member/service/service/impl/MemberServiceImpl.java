package com.melo.lzmall.member.service.service.impl;

import com.melo.lzmall.member.service.entity.base.MemberBaseDTO;
import com.melo.lzmall.member.service.repository.MemberRepository;
import com.melo.lzmall.member.service.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MemberServiceImpl  implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public MemberBaseDTO getMemberByMemberCode(String memberCode) {
        return memberRepository.getMemberByMemberCode(memberCode);
    }

    @Override
    public MemberBaseDTO getMemberByAccountCode(String accountCode) {
        return memberRepository.getMemberByAccountCode(accountCode);
    }
}