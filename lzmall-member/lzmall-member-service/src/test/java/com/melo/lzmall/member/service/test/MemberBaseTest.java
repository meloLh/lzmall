package com.melo.lzmall.member.service.test;

import com.melo.lzmall.member.service.MemberApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = MemberApplication.class)
@RunWith(SpringRunner.class)
public class MemberBaseTest {
}
