package com.melo.lzmall.member.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.member.service.controller.toc.MemberTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class MemberTocControllerTest extends MemberBaseTest {

    @Autowired
    private MemberTocController controller;

    @Test
    public void queryAllCategory(){
        Result<MemberInfoResponse> result = controller.getInitMemberInfo();
        log.info(JSON.toJSONString(result));
    }
}
