package com.melo.lzmall.order.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.order.api.request.CreateOrderCodeRequest;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.order.api.response.createSaleOrderResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "lzmall-order",contextId = "lzmall-member-orderFeignClient")
public interface OrderFeignClient {

    @ApiOperation("提交订单")
    @GetMapping("/api/order/toc/createSaleOrder")
    Result<createSaleOrderResponse> createSaleOrder(@RequestBody CreateOrderRequest request);

    @ApiOperation("获取订单号")
    @PostMapping("/api/order/toc/createOrderCode")
    Result<List<CreateOrderCodeResponse>> createOrderCode(@RequestBody CreateOrderCodeRequest request);
}
