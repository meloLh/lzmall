package com.melo.lzmall.order.api.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class CreateOrderCodeRequest implements Serializable {

    @NotBlank
    private String memberCode;

    @NotEmpty
    private List<String> shopCodeList;
}
