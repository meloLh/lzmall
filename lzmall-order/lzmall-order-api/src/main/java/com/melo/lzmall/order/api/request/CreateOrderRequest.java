package com.melo.lzmall.order.api.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class CreateOrderRequest implements Serializable {

    /**
     * 会员编码
     */
    private String memberCode;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 订单备注
     */
    private String orderRemark;


    /**
     * 运费
     */
    private BigDecimal freightAmount;


    /**
     * 交易单号
     */
    private String tradeCode;


    private List<CreateOrderSkuRequest> skuList;
}
