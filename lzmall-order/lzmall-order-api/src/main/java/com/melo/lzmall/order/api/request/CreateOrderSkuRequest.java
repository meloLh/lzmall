package com.melo.lzmall.order.api.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CreateOrderSkuRequest implements Serializable {

    /**
     * 店铺ID
     */
    @NotBlank
    private String shopCode;


    @NotBlank
    private String orderCode;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * spu编码
     */
    private String spuCode;

    /**
     * sku编码
     */
    @NotBlank
    private String skuCode;

    /**
     * 购买数量
     */
    @NotNull
    private Integer buyCount;

    /**
     * sku名称
     */
    private String productName;

    /**
     * 商品主图
     */
    private String mainPicture;

    /**
     * 一级分类编号
     */
    private String categoryCode1;

    /**
     * 二级分类编号
     */
    private String categoryCode2;

    /**
     * 三级分类编号
     */
    private String categoryCode3;

    /**
     * 成本价
     */
    @NotNull
    @Min(0)
    private BigDecimal costPrice;

    /**
     * 商品价
     */
    @NotNull
    @Min(0)
    private BigDecimal salePrice;

    /**
     * 市场价
     */
    @NotNull
    @Min(0)
    private BigDecimal marketPrice;


    @NotNull
    @Min(0)
    private BigDecimal payableAmount;

    @NotNull
    @Min(0)
    private BigDecimal discountAmount;

    @NotNull
    @Min(0)
    private BigDecimal couponAmount;

    /**
     * 原始价格总额
     */
    private BigDecimal saleAmount;

    /**
     * 原始价格总额
     */
    private BigDecimal costAmount;

    /**
     * 单位
     */
    private String unit;

    /**
     * 规格json
     */
    private String specJson;
}
