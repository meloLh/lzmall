package com.melo.lzmall.order.api.request;

import com.melo.lzmall.common.core.dto.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class QuerySaleOrderPageRequest extends PageRequest implements Serializable {

    @ApiModelProperty(value = "ALL:全部," +
            "WAIT_PAY:待付款," +
            "WAIT_ROG:待收货," +
            "CANCELLED:已取消," +
            "COMPLETE:已完成")
    private String tag;
}
