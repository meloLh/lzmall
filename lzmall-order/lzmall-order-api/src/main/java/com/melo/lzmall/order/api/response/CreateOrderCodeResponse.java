package com.melo.lzmall.order.api.response;

import lombok.Data;

import java.io.Serializable;
@Data
public class CreateOrderCodeResponse implements Serializable {

    private String memberCode;

    private String shopCode;

    private String orderCode;
}
