package com.melo.lzmall.order.api.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class QuerySaleOrderDetailPageResponse implements Serializable {

    /**
     * spu编码
     */
    private String spuCode;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * 购买数量
     */
    private Integer buyCount;
    /**
     * sku名称
     */
    private String productName;
    /**
     * 商品主图
     */
    private String mainPicture;
    /**
     * 应付总额
     */
    private BigDecimal payableAmount;

    /**
     * 零售价
     */
    private BigDecimal salePrice;
}
