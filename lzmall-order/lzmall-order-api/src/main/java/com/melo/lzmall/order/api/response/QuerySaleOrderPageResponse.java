package com.melo.lzmall.order.api.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class QuerySaleOrderPageResponse implements Serializable {

    private String tradeCode;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 订单来源
     */
    private String clientType;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 会员编码
     */
    private String memberCode;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 原价总额
     */
    private BigDecimal orderAmount;
    /**
     * 运费
     */
    private BigDecimal freightAmount;
    /**
     * 优惠券优惠总额
     */
    private BigDecimal couponAmount;
    /**
     * 折扣活动优惠总额
     */
    private BigDecimal discountAmount;
    /**
     * 应付总额
     */
    private BigDecimal payableAmount;
    /**
     * 实付总额
     */
    private BigDecimal paymentAmount;
    /**
     * 成本总额
     */
    private BigDecimal costAmount;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 支付状态
     */
    private String payStatus;

    private List<QuerySaleOrderDetailPageResponse> detailList;

    private AllowOperationResponse allowOperation;
}
