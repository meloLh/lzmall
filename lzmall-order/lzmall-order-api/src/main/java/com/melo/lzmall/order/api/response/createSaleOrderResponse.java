package com.melo.lzmall.order.api.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class createSaleOrderResponse implements Serializable {

    private String tradeCode;

    private String memberCode;

    private List<String> orderCodeList;
}
