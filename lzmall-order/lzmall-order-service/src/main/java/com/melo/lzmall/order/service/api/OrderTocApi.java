package com.melo.lzmall.order.service.api;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.order.api.feign.OrderFeignClient;
import com.melo.lzmall.order.api.request.CreateOrderCodeRequest;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.order.api.response.createSaleOrderResponse;
import com.melo.lzmall.order.service.converter.SaleOrderFaceConverter;
import com.melo.lzmall.order.service.service.SaleOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@Validated
public class OrderTocApi implements OrderFeignClient {

    @Autowired
    private SaleOrderService saleOrderService;

    @Autowired
    private SaleOrderFaceConverter converter;

    @ApiOperation("批量查询sku信息")
    @GetMapping("/api/order/toc/createSaleOrder")
    public Result<createSaleOrderResponse> createSaleOrder(@RequestBody CreateOrderRequest request) {
        createSaleOrderResponse response = saleOrderService.createSaleOrder(request);
        return Result.success(response);
    }

    @ApiOperation("获取订单号")
    @PostMapping("/api/order/toc/createOrderCode")
    public Result<List<CreateOrderCodeResponse>> createOrderCode(@RequestBody CreateOrderCodeRequest request) {
        return Result.success(saleOrderService.createOrderCode(request.getMemberCode(),request.getShopCodeList()));
    }
}
