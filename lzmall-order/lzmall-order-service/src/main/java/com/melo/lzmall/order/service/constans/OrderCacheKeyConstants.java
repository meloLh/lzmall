package com.melo.lzmall.order.service.constans;

public class OrderCacheKeyConstants {

    private static final String PREFIX = "LZMALL_ORDER_CACHE:";

    public static final String CREATE_ORDER_CODE_KEY = PREFIX+"CREATE_ORDER_CODE_";
}
