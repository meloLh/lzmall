package com.melo.lzmall.order.service.constans;

public class OrderLockKeyConstants {

    private static final String PREFIX = "LZMALL_ORDER_LOCK:";

    public static final String CREATE_ORDER_LOCK_KEY = PREFIX+"CREATE_ORDER_LOCK_KEY_";
}
