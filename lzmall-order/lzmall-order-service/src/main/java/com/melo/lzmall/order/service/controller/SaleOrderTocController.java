package com.melo.lzmall.order.service.controller;

import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.order.api.request.QuerySaleOrderPageRequest;
import com.melo.lzmall.order.api.response.AllowOperationResponse;
import com.melo.lzmall.order.api.response.QuerySaleOrderPageResponse;
import com.melo.lzmall.order.api.response.SaleOrderDetailTocResponse;
import com.melo.lzmall.order.service.converter.SaleOrderFaceConverter;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.bo.SaleOrderDetailBO;
import com.melo.lzmall.order.service.entity.dto.QuerySaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;
import com.melo.lzmall.order.service.service.SaleOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
@Validated
public class SaleOrderTocController {
    @Autowired
    private SaleOrderService saleOrderService;

    @Autowired
    private SaleOrderFaceConverter converter;

    @ApiOperation("查询订单列表")
    @GetMapping("/buyer/order/querySaleOrderPage")
    @MallAuth
    public Result<Page<QuerySaleOrderPageResponse>> querySaleOrderPage(QuerySaleOrderPageRequest request) {
        QuerySaleOrderPageDTO querySaleOrderPageDTO = converter.convertToQuerySaleOrderPageDTO(request);
        Page<SaleOrderPageDTO> page =  saleOrderService.querySaleOrderPage(querySaleOrderPageDTO);
        Page<QuerySaleOrderPageResponse> responsePage = converter.convertToCreateOrderCodeResponsePage(page);
        return Result.success(responsePage);
    }

    @ApiOperation("查询订单详情")
    @GetMapping("/buyer/order/getSaleOrderDetail")
    @MallAuth
    public Result<SaleOrderDetailTocResponse> getSaleOrderDetail(@RequestParam("orderCode") String orderCode) {
        SaleOrderDetailBO saleOrderBaseDTO = saleOrderService.getSaleOrderDetail(orderCode);
        SaleOrderDetailTocResponse response = converter.convertToSaleOrderDetailTocResponse(saleOrderBaseDTO);

        AllowOperationResponse allowOperationResponse = new AllowOperationResponse();
        allowOperationResponse.setCanCancel(true);
        allowOperationResponse.setCanPay(true);
        allowOperationResponse.setShowLogistics(false);
        allowOperationResponse.setCanReceive(false);

        response.setAllowOperation(allowOperationResponse);
        return Result.success(response);
    }
}
