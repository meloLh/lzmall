package com.melo.lzmall.order.service.converter;


import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.order.api.request.QuerySaleOrderPageRequest;
import com.melo.lzmall.order.api.response.QuerySaleOrderPageResponse;
import com.melo.lzmall.order.api.response.SaleOrderDetailTocResponse;
import com.melo.lzmall.order.service.entity.bo.SaleOrderDetailBO;
import com.melo.lzmall.order.service.entity.dto.QuerySaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface  SaleOrderFaceConverter {

    QuerySaleOrderPageDTO convertToQuerySaleOrderPageDTO(QuerySaleOrderPageRequest request);

    Page<QuerySaleOrderPageResponse> convertToCreateOrderCodeResponsePage(Page<SaleOrderPageDTO> page);

    SaleOrderDetailTocResponse convertToSaleOrderDetailTocResponse(SaleOrderDetailBO saleOrderBaseDTO);
}

