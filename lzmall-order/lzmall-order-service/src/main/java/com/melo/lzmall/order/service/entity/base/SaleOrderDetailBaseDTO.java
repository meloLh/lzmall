package com.melo.lzmall.order.service.entity.base;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 订单明细表
 */
@Data
public class SaleOrderDetailBaseDTO implements Serializable {

        /**
     * ID
     */
    private Long id;

        /**
     * 订单编号
     */
    private String orderCode;

        /**
     * spu编码
     */
    private String spuCode;

    private String shopCode;

    private String shopName;

        /**
     * sku编码
     */
    private String skuCode;

        /**
     * 购买数量
     */
    private Integer buyCount;

        /**
     * sku名称
     */
    private String productName;

        /**
     * 商品主图
     */
    private String mainPicture;

        /**
     * 一级分类编号
     */
    private String categoryCode1;

        /**
     * 二级分类编号
     */
    private String categoryCode2;

        /**
     * 三级分类编号
     */
    private String categoryCode3;

        /**
     * 成本价
     */
    private BigDecimal costPrice;

        /**
     * 商品价
     */
    private BigDecimal salePrice;

        /**
     * 市场价
     */
    private BigDecimal marketPrice;

        /**
     * 原始价格总额
     */
    private BigDecimal saleAmount;

        /**
     * 优惠券优惠总额
     */
    private BigDecimal couponAmount;

        /**
     * 折扣活动优惠总额
     */
    private BigDecimal discountAmount;

        /**
     * 应付总额
     */
    private BigDecimal payableAmount;

        /**
     * 成本总额
     */
    private BigDecimal costAmount;

        /**
     * 单位
     */
    private String unit;

        /**
     * 规格json
     */
    private String specJson;

        /**
     * 排序
     */
    private Integer sort;

        /**
     * 乐观锁
     */
    private Integer version;

        /**
     * 创建人
     */
    private String creator;

        /**
     * 创建人编号
     */
    private String creatorCode;

        /**
     * 创建时间
     */
    private Date createdTime;

        /**
     * 更新人
     */
    private String modifier;

        /**
     * 更新人编号
     */
    private String modifierCode;

        /**
     * 更新时间
     */
    private Date modifiedTime;

    
}
