package com.melo.lzmall.order.service.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单可进行的操作
 */
@Data
public class AllowOperationDTO implements Serializable {

    private static final long serialVersionUID = -5109440403955543227L;

    @ApiModelProperty(value = "可以取消")
    private Boolean canCancel = false;

    @ApiModelProperty(value = "可以支付")
    private Boolean canPay = false;

    @ApiModelProperty(value = "可以收货")
    private Boolean canReceive = false;

    @ApiModelProperty(value = "可以收货")
    private Boolean showLogistics = false;
}
