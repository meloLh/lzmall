package com.melo.lzmall.order.service.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class QuerySaleOrderPageDTO implements Serializable {

    @ApiModelProperty(value = "ALL:全部," +
            "WAIT_PAY:待付款," +
            "WAIT_ROG:待收货," +
            "CANCELLED:已取消," +
            "COMPLETE:已完成")
    private String tag;

    @ApiModelProperty(value = "下单开始时间")
    private Date startTime;

    @ApiModelProperty(value = "下单结束时间")
    private Date endTime;

    private String memberCode;

    private Integer pageIndex = 1;

    private Integer pageSize = 10;
}
