package com.melo.lzmall.order.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 订单表
 */
@Data
@TableName("sale_order")
public class SaleOrderPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Long id;

    private String tradeCode;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 订单来源
     */
    private String clientType;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 会员编码
     */
    private String memberCode;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 原价总额
     */
    private BigDecimal orderAmount;
    /**
     * 运费
     */
    private BigDecimal freightAmount;
    /**
     * 优惠券优惠总额
     */
    private BigDecimal couponAmount;
    /**
     * 折扣活动优惠总额
     */
    private BigDecimal discountAmount;
    /**
     * 应付总额
     */
    private BigDecimal payableAmount;
    /**
     * 实付总额
     */
    private BigDecimal paymentAmount;
    /**
     * 成本总额
     */
    private BigDecimal costAmount;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 支付状态
     */
    private String payStatus;
    /**
     * 支付方式(ONLINE 线下支付  ONLINE线上支付)
     */
    private String payMode;
    /**
     * 支付方式(现金，支付宝，微信)
     */
    private String payType;
    /**
     * 支付单号
     */
    private String payCode;
    /**
     * 支付时间
     */
    private Date payTime;
    /**
     * 取消时间
     */
    private Date cancelTime;
    /**
     * 退款状态
     */
    private String refundStatus;
    /**
     * 退款成功时间
     */
    private Date refundTime;
    /**
     * 取消原因
     */
    private String cancelReason;
    /**
     * 订单备注
     */
    private String orderRemark;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
