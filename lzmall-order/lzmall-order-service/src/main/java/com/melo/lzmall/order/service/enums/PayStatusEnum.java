package com.melo.lzmall.order.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PayStatusEnum {

    WAIT_PAY("待支付","WAIT_PAY"),

    PAY_SUCCESS("待发货","PAY_SUCCESS"),
    ;


    private final String name;

    private final String value;
}
