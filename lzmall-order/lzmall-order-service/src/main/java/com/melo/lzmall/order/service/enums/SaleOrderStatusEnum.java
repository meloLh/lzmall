package com.melo.lzmall.order.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SaleOrderStatusEnum {

    CREATED("已提交","CREATED"),

    WAIT_DELIVER("待发货","WAIT_DELIVER"),

    WAIT_RECEIVE("待收货","WAIT_RECEIVE"),

    COMPLETE("已完成","COMPLETED"),

    CANCEL("已取消","CANCELLED")
    ;


    private final String name;

    private final String value;
}
