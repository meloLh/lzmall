package com.melo.lzmall.order.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.order.service.entity.po.SaleOrderPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface SaleOrderMapper extends BaseMapper<SaleOrderPO> {
	
}
