package com.melo.lzmall.order.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.helper.MybatisBatchHelper;
import com.melo.lzmall.order.service.entity.base.SaleOrderDetailBaseDTO;
import com.melo.lzmall.order.service.entity.po.SaleOrderDetailPO;
import com.melo.lzmall.order.service.mapper.SaleOrderDetailMapper;
import com.melo.lzmall.order.service.repository.converter.SaleOrderDetailRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class SaleOrderDetailRepository {

    @Autowired
    private SaleOrderDetailMapper saleOrderDetailMapper;

    @Autowired
    private MybatisBatchHelper mybatisBatchHelper;

    @Autowired
    private SaleOrderDetailRepositoryConverter converter;


    public void batchInsert(List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOS) {
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(saleOrderDetailBaseDTOS),"参数异常");
        List<SaleOrderDetailPO> saleOrderDetailPOS = converter.convertToSaleOrderDetailPO(saleOrderDetailBaseDTOS);
        mybatisBatchHelper.batchSave(saleOrderDetailPOS,SaleOrderDetailMapper.class,(record,mapper)->mapper.insert(record));
    }

    public List<SaleOrderDetailBaseDTO> queryByOrderCodeList(List<String> orderCodeList) {
        if(CollectionUtils.isEmpty(orderCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<SaleOrderDetailPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SaleOrderDetailPO::getOrderCode,orderCodeList);
        List<SaleOrderDetailPO> saleOrderDetailPOS = saleOrderDetailMapper.selectList(queryWrapper);
        return converter.convertToSaleOrderDetailBaseDTO(saleOrderDetailPOS);
    }

    public List<SaleOrderDetailBaseDTO> queryByOrderCode(String orderCode) {
        if(StringUtils.isBlank(orderCode)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<SaleOrderDetailPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SaleOrderDetailPO::getOrderCode,orderCode);
        List<SaleOrderDetailPO> saleOrderDetailPOS = saleOrderDetailMapper.selectList(queryWrapper);
        return converter.convertToSaleOrderDetailBaseDTO(saleOrderDetailPOS);
    }
}

