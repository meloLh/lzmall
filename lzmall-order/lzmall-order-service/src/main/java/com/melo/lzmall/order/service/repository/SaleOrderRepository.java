package com.melo.lzmall.order.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.helper.MybatisBatchHelper;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.dto.QuerySaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.po.SaleOrderPO;
import com.melo.lzmall.order.service.enums.PayStatusEnum;
import com.melo.lzmall.order.service.enums.SaleOrderStatusEnum;
import com.melo.lzmall.order.service.mapper.SaleOrderMapper;
import com.melo.lzmall.order.service.repository.converter.SaleOrderRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class SaleOrderRepository {

    @Autowired
    private SaleOrderMapper saleOrderMapper;

    @Autowired
    private MybatisBatchHelper mybatisBatchHelper;

    @Autowired
    private SaleOrderRepositoryConverter converter;

    public void batchInsert(List<SaleOrderBaseDTO> saleOrderBaseDTOS) {
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(saleOrderBaseDTOS),"参数异常");
        List<SaleOrderPO> saleOrderPOList = converter.convertToSaleOrderPO(saleOrderBaseDTOS);
        mybatisBatchHelper.batchSave(saleOrderPOList,SaleOrderMapper.class,(record,mapper)->mapper.insert(record));
    }

    public List<SaleOrderBaseDTO> queryByTradeCode(String tradeCode) {
        if(StringUtils.isBlank(tradeCode)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<SaleOrderPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SaleOrderPO::getTradeCode,tradeCode);
        List<SaleOrderPO> saleOrderPOS = saleOrderMapper.selectList(queryWrapper);
       return converter.convertToSaleOrderBaseDTOList(saleOrderPOS);
    }

    public Page<SaleOrderPageDTO> querySaleOrderPage(QuerySaleOrderPageDTO querySaleOrderPageDTO) {
        AssertUtil.isTrue(StringUtils.isNotBlank(querySaleOrderPageDTO.getMemberCode()), "参数异常");
        LambdaQueryWrapper<SaleOrderPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SaleOrderPO::getMemberCode, querySaleOrderPageDTO.getMemberCode());
        if (StringUtils.isNotBlank(querySaleOrderPageDTO.getTag()) && !Objects.equals(querySaleOrderPageDTO.getTag(), "ALL")) {
            if (Objects.equals(querySaleOrderPageDTO.getTag(), "WAIT_PAY")) {
                queryWrapper.eq(SaleOrderPO::getPayStatus, PayStatusEnum.WAIT_PAY.getValue())
                        .eq(SaleOrderPO::getOrderStatus, SaleOrderStatusEnum.CREATED.getValue());
            }else {
                queryWrapper.eq(SaleOrderPO::getOrderStatus, querySaleOrderPageDTO.getTag());
            }
        }
        if (Objects.nonNull(querySaleOrderPageDTO.getStartTime()) && Objects.nonNull(querySaleOrderPageDTO.getEndTime())) {
            queryWrapper.le(SaleOrderPO::getCreatedTime, querySaleOrderPageDTO.getEndTime())
                    .ge(SaleOrderPO::getCreatedTime, querySaleOrderPageDTO.getStartTime());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SaleOrderPO> pageObj = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(querySaleOrderPageDTO.getPageIndex(), querySaleOrderPageDTO.getPageSize());
        IPage<SaleOrderPO> page = saleOrderMapper.selectPage(pageObj, queryWrapper);
        Page<SaleOrderPageDTO> resultPage = converter.convertToPage(page);
        resultPage.setPageSize((int) page.getSize());
        List<SaleOrderPageDTO> saleOrderPageDTOList = converter.convertToSaleOrderPageDTOList(page.getRecords());
        resultPage.setList(saleOrderPageDTOList);
        return resultPage;
    }

    public SaleOrderBaseDTO getByOrderCode(String orderCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(orderCode),"参数异常");
        LambdaQueryWrapper<SaleOrderPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SaleOrderPO::getOrderCode,orderCode);
        SaleOrderPO saleOrderPO = saleOrderMapper.selectOne(queryWrapper);
        return converter.convertToSaleOrderBaseDTO(saleOrderPO);
    }
}

