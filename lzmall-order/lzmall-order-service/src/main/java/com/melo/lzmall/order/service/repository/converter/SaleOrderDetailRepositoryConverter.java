package com.melo.lzmall.order.service.repository.converter;


import com.melo.lzmall.order.service.entity.base.SaleOrderDetailBaseDTO;
import com.melo.lzmall.order.service.entity.po.SaleOrderDetailPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface SaleOrderDetailRepositoryConverter {

    List<SaleOrderDetailPO> convertToSaleOrderDetailPO(List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOS);

    List<SaleOrderDetailBaseDTO> convertToSaleOrderDetailBaseDTO(List<SaleOrderDetailPO> saleOrderDetailPOS);
}

