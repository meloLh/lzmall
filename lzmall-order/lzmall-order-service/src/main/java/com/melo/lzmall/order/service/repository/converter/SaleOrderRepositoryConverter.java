package com.melo.lzmall.order.service.repository.converter;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.po.SaleOrderPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface SaleOrderRepositoryConverter {
    
    List<SaleOrderPO> convertToSaleOrderPO(List<SaleOrderBaseDTO> saleOrderBaseDTOS);

    List<SaleOrderPageDTO> convertToSaleOrderPageDTOList(List<SaleOrderPO> records);

    Page<SaleOrderPageDTO> convertToPage(IPage<SaleOrderPO> page);

    SaleOrderBaseDTO convertToSaleOrderBaseDTO(SaleOrderPO saleOrderPO);

    List<SaleOrderBaseDTO> convertToSaleOrderBaseDTOList(List<SaleOrderPO> saleOrderPOS);
}

