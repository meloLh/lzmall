package com.melo.lzmall.order.service.service;


import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.order.api.response.createSaleOrderResponse;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.bo.SaleOrderDetailBO;
import com.melo.lzmall.order.service.entity.dto.QuerySaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;

import java.util.List;

public interface SaleOrderService {
    /**
     * @Description 创建订单
     * @author liuhu
     * @param request
     * @date 2024/6/3 13:23
     * @return com.melo.lzmall.order.api.response.createSaleOrderResponse
     */
    createSaleOrderResponse createSaleOrder(CreateOrderRequest request);

    /**
     * @Description 获取订单号
     * @author liuhu
     * @param memberCode
     * @param shopCodeList
     * @date 2024/6/3 16:28
     * @return java.util.List<com.melo.lzmall.order.api.response.CreateOrderCodeResponse>
     */
    List<CreateOrderCodeResponse> createOrderCode(String memberCode, List<String> shopCodeList);

    /**
     * @Description 查询分页
     * @author liuhu
     * @param querySaleOrderPageDTO
     * @date 2024/6/5 10:05
     * @return com.melo.lzmall.common.core.dto.Page<java.util.List<java.util.List<com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO>>>
     */
    Page<SaleOrderPageDTO> querySaleOrderPage(QuerySaleOrderPageDTO querySaleOrderPageDTO);

    SaleOrderDetailBO getSaleOrderDetail(String orderCode);
}

