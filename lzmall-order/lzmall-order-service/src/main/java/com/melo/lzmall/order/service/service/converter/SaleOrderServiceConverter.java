package com.melo.lzmall.order.service.service.converter;


import com.melo.lzmall.order.api.request.CreateOrderSkuRequest;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.base.SaleOrderDetailBaseDTO;
import com.melo.lzmall.order.service.entity.bo.SaleOrderDetailBO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface  SaleOrderServiceConverter {

    SaleOrderDetailBaseDTO convertToSaleOrderDetailBaseDTO(CreateOrderSkuRequest sku);

    SaleOrderDetailBO convertToSaleOrderDetailBO(SaleOrderBaseDTO saleOrderBaseDTO);
}

