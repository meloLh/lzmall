package com.melo.lzmall.order.service.service.impl;

import com.melo.lzmall.order.service.repository.SaleOrderDetailRepository;
import com.melo.lzmall.order.service.service.SaleOrderDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SaleOrderDetailServiceImpl  implements SaleOrderDetailService {

    @Autowired
    private SaleOrderDetailRepository saleOrderDetailRepository;
}