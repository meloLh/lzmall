package com.melo.lzmall.order.service.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.request.CreateOrderSkuRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.order.api.response.createSaleOrderResponse;
import com.melo.lzmall.order.service.constans.OrderCacheKeyConstants;
import com.melo.lzmall.order.service.constans.OrderLockKeyConstants;
import com.melo.lzmall.order.service.entity.base.SaleOrderBaseDTO;
import com.melo.lzmall.order.service.entity.base.SaleOrderDetailBaseDTO;
import com.melo.lzmall.order.service.entity.bo.SaleOrderDetailBO;
import com.melo.lzmall.order.service.entity.dto.AllowOperationDTO;
import com.melo.lzmall.order.service.entity.dto.QuerySaleOrderPageDTO;
import com.melo.lzmall.order.service.entity.dto.SaleOrderPageDTO;
import com.melo.lzmall.order.service.enums.PayStatusEnum;
import com.melo.lzmall.order.service.enums.SaleOrderStatusEnum;
import com.melo.lzmall.order.service.repository.SaleOrderDetailRepository;
import com.melo.lzmall.order.service.repository.SaleOrderRepository;
import com.melo.lzmall.order.service.service.SaleOrderService;
import com.melo.lzmall.order.service.service.converter.SaleOrderServiceConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SaleOrderServiceImpl  implements SaleOrderService {

    @Autowired
    private SaleOrderRepository saleOrderRepository;

    @Autowired
    private SaleOrderDetailRepository saleOrderDetailRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private SaleOrderServiceConverter converter;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private RedisHelper redisHelper;

    @Override
    public createSaleOrderResponse createSaleOrder(CreateOrderRequest request) {

        RLock lock = redissonClient.getLock(OrderLockKeyConstants.CREATE_ORDER_LOCK_KEY + request.getTradeCode());
        try {
            boolean tryLock = lock.tryLock(500, 3000L, TimeUnit.SECONDS);
            AssertUtil.isTrue(tryLock,"请勿重复提交");
            List<SaleOrderBaseDTO> saleOrderBaseDTOS = saleOrderRepository.queryByTradeCode(request.getTradeCode());
            AssertUtil.isTrue(CollectionUtils.isEmpty(saleOrderBaseDTOS),"当前订单已存在");
            List<SaleOrderDetailBaseDTO> allDetailList = new ArrayList<>();
            List<SaleOrderBaseDTO> all = new ArrayList<>();
            List<String> orderCodeList  = new ArrayList<>();
            // 按照店铺维度拆分
            Map<String, List<CreateOrderSkuRequest>> shopProductGroupMap = request.getSkuList().stream().collect(Collectors.groupingBy(CreateOrderSkuRequest::getShopCode));
            splitShopOrderByShopCode(request, allDetailList, all, orderCodeList, shopProductGroupMap);
            // 保存数据
            saveShopOrderInfo(allDetailList, all);
            return createSaleOrderResponse.builder()
                    .memberCode(request.getMemberCode())
                    .tradeCode(request.getTradeCode())
                    .orderCodeList(orderCodeList).build();
        } catch (BusinessException e) {
           throw e;
        } catch (Exception e){
           log.info("创建订单失败,req:{}", JSON.toJSONString(request),e);
           throw new BusinessException("500","创建订单失败");
        } finally {
            // 删除缓存订单号
            if(lock.isLocked() && lock.isHeldByCurrentThread()){
                lock.unlock();
            }

        }
    }

    @Override
    public List<CreateOrderCodeResponse> createOrderCode(String memberCode, List<String> shopCodeList) {
        List<CreateOrderCodeResponse> list = new ArrayList<>();
        for (String shopCode : shopCodeList) {
            String orderCode = IdUtil.getSnowflakeNextIdStr();
            CreateOrderCodeResponse response = new CreateOrderCodeResponse();
            response.setShopCode(shopCode);
            response.setOrderCode(orderCode);
            response.setMemberCode(memberCode);
            list.add(response);
        }
        return list;
    }

    @Override
    public Page<SaleOrderPageDTO> querySaleOrderPage(QuerySaleOrderPageDTO querySaleOrderPageDTO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        querySaleOrderPageDTO.setMemberCode(authAccount.getAccountCode());
        Date now = new Date();
        querySaleOrderPageDTO.setEndTime(now);
        querySaleOrderPageDTO.setStartTime(DateUtil.offsetMonth(now,-6));
        Page<SaleOrderPageDTO> page = saleOrderRepository.querySaleOrderPage(querySaleOrderPageDTO);
        List<String> orderCodeList = page.getList().stream().map(SaleOrderPageDTO::getOrderCode).collect(Collectors.toList());
        List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOList = saleOrderDetailRepository.queryByOrderCodeList(orderCodeList);
        Map<String, List<SaleOrderDetailBaseDTO>> saleOrderDetailMap = saleOrderDetailBaseDTOList.stream().collect(Collectors.groupingBy(SaleOrderDetailBaseDTO::getOrderCode));
        for (SaleOrderPageDTO orderPageDTO : page.getList()) {
            List<SaleOrderDetailBaseDTO> orderDetailBaseDTOS = saleOrderDetailMap.get(orderPageDTO.getOrderCode());
            orderPageDTO.setDetailList(orderDetailBaseDTOS);
            AllowOperationDTO allowOperationDTO = buildAllowOperationDTO(orderPageDTO);
            orderPageDTO.setAllowOperation(allowOperationDTO);
        }
        return page;
    }

    @Override
    public SaleOrderDetailBO getSaleOrderDetail(String orderCode) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        SaleOrderBaseDTO saleOrderBaseDTO = saleOrderRepository.getByOrderCode(orderCode);
        AssertUtil.isTrue(Objects.nonNull(saleOrderBaseDTO),"订单信息不存在");
        AssertUtil.isTrue(Objects.equals(saleOrderBaseDTO.getCreatorCode(),authAccount.getAccountCode()),"越权异常");
        List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOList = saleOrderDetailRepository.queryByOrderCode(orderCode);
        SaleOrderDetailBO saleOrderDetailBO =  converter.convertToSaleOrderDetailBO(saleOrderBaseDTO);
        saleOrderDetailBO.setSkuList(saleOrderDetailBaseDTOList);
        return saleOrderDetailBO;
    }

    private AllowOperationDTO buildAllowOperationDTO(SaleOrderPageDTO orderPageDTO) {
        AllowOperationDTO allowOperationDTO = new AllowOperationDTO();
        if(Objects.equals(orderPageDTO.getPayStatus(), PayStatusEnum.WAIT_PAY.getValue()) &&
                Objects.equals(orderPageDTO.getOrderStatus(), SaleOrderStatusEnum.CREATED.getValue())){
            allowOperationDTO.setCanCancel(true);
            allowOperationDTO.setCanPay(true);
        }
        if(Objects.equals(orderPageDTO.getOrderStatus(), SaleOrderStatusEnum.WAIT_RECEIVE.getValue())){
            allowOperationDTO.setCanReceive(true);
            allowOperationDTO.setShowLogistics(true);
        }
        return allowOperationDTO;
    }

    private void splitShopOrderByShopCode(CreateOrderRequest request,
                      List<SaleOrderDetailBaseDTO> allDetailList,
                      List<SaleOrderBaseDTO> all,
                      List<String> orderCodeList,
                      Map<String, List<CreateOrderSkuRequest>> shopProductGroupMap) {
        for (Map.Entry<String, List<CreateOrderSkuRequest>> entry : shopProductGroupMap.entrySet()) {
            List<CreateOrderSkuRequest> skuList = entry.getValue();
            String orderCode = skuList.get(0).getOrderCode();
            List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOS = buildSaleOrderDetailBaseDTO(skuList, request, orderCode);
            SaleOrderBaseDTO saleOrderBaseDTO = buildSaleOrderBaseDTO(request, saleOrderDetailBaseDTOS, orderCode);
            all.add(saleOrderBaseDTO);
            allDetailList.addAll(saleOrderDetailBaseDTOS);
            orderCodeList.add(orderCode);
        }
    }

    private void saveShopOrderInfo(List<SaleOrderDetailBaseDTO> allDetailList, List<SaleOrderBaseDTO> all) {
        transactionTemplate.execute(ts -> {
            saleOrderRepository.batchInsert(all);
            saleOrderDetailRepository.batchInsert(allDetailList);
            return null;
        });
    }

    private SaleOrderBaseDTO buildSaleOrderBaseDTO(CreateOrderRequest orderRequest,
                                                   List<SaleOrderDetailBaseDTO> saleOrderDetailBaseDTOS,
                                                   String orderCode) {
        SaleOrderDetailBaseDTO detailBaseDTO = saleOrderDetailBaseDTOS.get(0);

        SaleOrderBaseDTO saleOrderBaseDTO = new SaleOrderBaseDTO();
        saleOrderBaseDTO.setOrderCode(orderCode);
        saleOrderBaseDTO.setTradeCode(orderRequest.getTradeCode());
        saleOrderBaseDTO.setCreator(orderRequest.getMemberName());
        saleOrderBaseDTO.setCreatorCode(orderRequest.getMemberCode());
        saleOrderBaseDTO.setModifier(orderRequest.getMemberName());
        saleOrderBaseDTO.setModifierCode(orderRequest.getMemberCode());
        saleOrderBaseDTO.setClientType("CUSTOM");
        saleOrderBaseDTO.setMemberName(orderRequest.getMemberName());
        saleOrderBaseDTO.setMemberCode(orderRequest.getMemberCode());
        saleOrderBaseDTO.setShopCode(detailBaseDTO.getShopCode());
        saleOrderBaseDTO.setShopName(detailBaseDTO.getShopName());
        saleOrderBaseDTO.setPayMode("ONLINE");
        saleOrderBaseDTO.setPayStatus("WAIT_PAY");
        saleOrderBaseDTO.setOrderRemark(orderRequest.getOrderRemark());
        saleOrderBaseDTO.setFreightAmount(orderRequest.getFreightAmount());

        BigDecimal orderAmount = saleOrderDetailBaseDTOS.stream().map(SaleOrderDetailBaseDTO::getSaleAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        saleOrderBaseDTO.setOrderAmount(orderAmount);

        BigDecimal couponAmount = saleOrderDetailBaseDTOS.stream().map(SaleOrderDetailBaseDTO::getCouponAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        saleOrderBaseDTO.setCouponAmount(couponAmount);

        BigDecimal discountAmount = saleOrderDetailBaseDTOS.stream().map(SaleOrderDetailBaseDTO::getDiscountAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        saleOrderBaseDTO.setDiscountAmount(discountAmount);

        BigDecimal payableAmount = orderAmount.subtract(couponAmount).subtract(discountAmount).subtract(orderRequest.getFreightAmount());
        saleOrderBaseDTO.setPayableAmount(payableAmount);

        BigDecimal costAmount = saleOrderDetailBaseDTOS.stream().map(SaleOrderDetailBaseDTO::getCostAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        saleOrderBaseDTO.setCostAmount(costAmount);

        return saleOrderBaseDTO;
    }

    private List<SaleOrderDetailBaseDTO> buildSaleOrderDetailBaseDTO(List<CreateOrderSkuRequest> skuList,
                                                                     CreateOrderRequest orderRequest,
                                                                     String orderCode) {
        return skuList.stream().map(sku->{
            SaleOrderDetailBaseDTO detailBaseDTO = converter.convertToSaleOrderDetailBaseDTO(sku);
            detailBaseDTO.setCreator(orderRequest.getMemberName());
            detailBaseDTO.setCreatorCode(orderRequest.getMemberCode());
            detailBaseDTO.setModifier(orderRequest.getMemberName());
            detailBaseDTO.setModifierCode(orderRequest.getMemberCode());
            detailBaseDTO.setOrderCode(orderCode);
            return detailBaseDTO;
        }).collect(Collectors.toList());
    }
}