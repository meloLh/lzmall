package com.melo.lzmall.order.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.order.api.request.QuerySaleOrderPageRequest;
import com.melo.lzmall.order.api.response.QuerySaleOrderPageResponse;
import com.melo.lzmall.order.service.controller.SaleOrderTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class MemberTocControllerTest extends OrderBaseTest {

    @Autowired
    private SaleOrderTocController controller;

    @Test
    public void queryAllCategory(){
        QuerySaleOrderPageRequest request = new QuerySaleOrderPageRequest();
        request.setPageIndex(1);
        request.setPageSize(10);
        Result<Page<QuerySaleOrderPageResponse>> result = controller.querySaleOrderPage(request);
        log.info(JSON.toJSONString(result));
    }
}
