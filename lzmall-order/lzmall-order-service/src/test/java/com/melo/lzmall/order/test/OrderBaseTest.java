package com.melo.lzmall.order.test;

import com.melo.lzmall.order.service.OrderApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = OrderApplication.class)
@RunWith(SpringRunner.class)
public class OrderBaseTest {
}
