package com.melo.lzmall.product.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "lzmall-product",contextId = "lzmall-product-productFeignClient")
public interface ProductFeignClient {

    @ApiOperation("批量查询sku信息")
    @GetMapping("/api/product/queryBySkuCodeList")
    Result<List<ProductSkuDetailResponse>> queryBySkuCodeList(@RequestParam(name = "skuCodeList") List<String> skuCodeList);
}
