package com.melo.lzmall.product.api.feign;


import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockRequest;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "lzmall-product",contextId = "lzmall-product-productStockFeignClient")
public interface ProductStockFeignClient {

    @ApiOperation("批量冻结库存")
    @PostMapping("/api/product/stock/batchFreezingProductStock")
     Result<Void> batchFreezingProductStock(@RequestBody List<BatchFreezingProductStockRequest> request);
}
