package com.melo.lzmall.product.api.request.search;

import com.melo.lzmall.common.core.dto.PageRequest;
import lombok.Data;

import java.io.Serializable;
@Data
public class SearchRecommendProductListRequest  extends PageRequest implements Serializable {

    private String categoryCode3;

    private String brandCode;

    private String keyword;

    private String currentSkuCode;
}
