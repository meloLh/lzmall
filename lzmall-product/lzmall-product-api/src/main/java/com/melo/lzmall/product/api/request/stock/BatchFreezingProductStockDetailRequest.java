package com.melo.lzmall.product.api.request.stock;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class BatchFreezingProductStockDetailRequest implements Serializable {

    @NotBlank
    private String skuCode;

    private String skuName;

    @NotNull
    private Integer stockNum;

}
