package com.melo.lzmall.product.api.request.stock;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
public class BatchFreezingProductStockRequest implements Serializable {

    @NotBlank
    private String orderCode;

    @NotBlank
    private String operateCode;

    private String operateName;

    @NotEmpty
    private List<BatchFreezingProductStockDetailRequest> skuList;

}
