package com.melo.lzmall.product.api.response.category;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("小程序首页分类")
public class QueryCategoryListTocResponse implements Serializable {

    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 分类编码
     */
    private String categoryCode;
    /**
     * 父级分类编码
     */
    private String parentCategoryCode;
    /**
     * 层级
     */
    private Integer level;
    /**
     * 分类图标
     */
    private String image;
    /**
     * 排序值
     */
    private Integer sort;


    private List<QueryCategoryListTocResponse> childrenList;
}
