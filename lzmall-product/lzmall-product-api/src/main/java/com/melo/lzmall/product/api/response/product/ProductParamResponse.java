package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;

/**
 * 商品参数表
 */
@Data
public class ProductParamResponse implements Serializable {

	/**
	 * 参数项
	 */
	private String paramName;
	/**
	 * 参数值
	 */
	private String paramValue;
}
