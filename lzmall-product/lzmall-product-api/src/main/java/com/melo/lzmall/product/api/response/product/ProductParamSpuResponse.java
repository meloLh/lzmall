package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品参数表
 */
@Data
public class ProductParamSpuResponse implements Serializable {

	/**
	 * 参数编码
	 */
	private String paramCode;
	/**
	 * 参数分组名称
	 */
	private String paramGroupName;


	private List<ProductParamResponse> paramList;
}
