package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductSkuDetailResponse implements Serializable {
    /**
     * spu编码
     */
    private String spuCode;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * sku名称
     */
    private String productName;
    /**
     * 品牌编码
     */
    private String brandCode;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 一级分类编号
     */
    private String categoryCode1;
    /**
     * 二级分类编号
     */
    private String categoryCode2;
    /**
     * 三级分类编号
     */
    private String categoryCode3;
    /**
     * 分类路径
     */
    private String categoryCodePath;
    /**
     * 店铺分类
     */
    private String shopCategoryCodePath;
    /**
     * 成本价
     */
    private BigDecimal costPrice;
    /**
     * 商品价
     */
    private BigDecimal salePrice;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private String status;
    /**
     * 排序
     */
    private Integer sort;

    //--------------------图文详情
    /**
     * 商品视频
     */
    private String video;
    /**
     * 商品详情
     */
    private String detailHtml;
    /**
     * 商品移动端详情
     */
    private String mobileHtml;

    /**
     * 移动端商品描述
     */
    private String mobileIntro;
    /**
     * 卖点
     */
    private String sellingPoint;
    /**
     * 原图路径
     */
    private String original;

    /**
     * 缩略图路径
     */
    private String thumbnail;

    // --------------------------库存信息
    /**
     * 实际库存
     */
    private Integer stockNum;

    // 规格信息
    private List<ProductSpecResponse> specList;

}
