package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;


@Data
public class ProductSpecResponse implements Serializable {

	/**
	 * 规格编号
	 */
	private String specCode;
	/**
	 * 规格项名称
	 */
	private String specName;
	/**
	 * 规格值名称
	 */
	private String specValue;
}
