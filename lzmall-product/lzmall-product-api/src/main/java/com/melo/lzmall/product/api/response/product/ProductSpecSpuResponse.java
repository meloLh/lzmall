package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class ProductSpecSpuResponse implements Serializable {

    /**
     * 规格编号
     */
    private String skuCode;
    /**
     * 规格项名称
     */
    private Integer stockNum;

    /**
     * sku规格信息
     */
    private List<ProductSpecResponse> specValues;
}
