package com.melo.lzmall.product.api.response.product;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ProductSpuDetailResponse implements Serializable {

    /**
     * sku信息
     */
    private ProductSkuDetailResponse sku;

    /**
     * 规格信息
     */
    private List<ProductSpecSpuResponse> specs;

    /**
     * 商品属性
     */
    private List<ProductParamSpuResponse> paramList;

    /**
     * 营销信息
     */
    private Map<String, Object> promotionMap = new HashMap<>();

    private List<Object> wholesaleList = new ArrayList<>();

    private List<String> goodsGalleryList;
}
