package com.melo.lzmall.product.api.response.product;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("小程序商品搜索")
public class SearchIndexProductResponse implements Serializable {

    /**
     * spu编码
     */
    private String spuCode;

    private String skuCode;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 一级分类编号
     */
    private String categoryCode1;
    /**
     * 二级分类编号
     */
    private String categoryCode2;
    /**
     * 三级分类编号
     */
    private String categoryCode3;
    /**
     * 分类路径
     */
    private String categoryCodePath;
    /**
     * 店铺分类
     */
    private String shopCategoryCodePath;
    /**
     * 品牌ID
     */
    private String brandCode;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 关键词
     */
    private String keyword;
    /**
     * 单位
     */
    private String unit;

    /**
     * 购买数量
     */
    private Integer buyCount = 999;

    /**图片*/
    private String thumbnail;

    /**
     * 评论数量
     */
    private Integer commentCount = 500;
    /**
     * 成本价
     */
    private BigDecimal costPrice;
    /**
     * 商品价
     */
    private BigDecimal salePrice;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;

}
