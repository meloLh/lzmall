package com.melo.lzmall.product.api.response.search;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProductEvaluationResponse implements Serializable {

    private String id;

    private Date createTime;

    private String memberId;

    private String memberName;

    private String memberProfile;

    private String goodsName;

    private String content;

    private String grade;

    private String skuId;


    private String storeId;
    private String storeName;


}
