package com.melo.lzmall.product.api.response.search;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("品牌相关")
public class SearchProductRelatedBrandResponse implements Serializable {

    private String name;

    private String url;

    private String value;
}
