package com.melo.lzmall.product.api.response.search;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("小程序商品搜索")
public class SearchProductRelatedContentResponse implements Serializable {

     private List<SearchProductRelatedBrandResponse> brands;

     private List<SearchProductRelatedCategoryResponse> categories;

     private List<SearchProductRelatedParamsResponse>  paramOptions;
}
