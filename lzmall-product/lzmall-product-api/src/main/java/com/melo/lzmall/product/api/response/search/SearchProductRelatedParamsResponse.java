package com.melo.lzmall.product.api.response.search;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("品牌相关")
public class SearchProductRelatedParamsResponse implements Serializable {

    private String key;

    private List<String> values;
}
