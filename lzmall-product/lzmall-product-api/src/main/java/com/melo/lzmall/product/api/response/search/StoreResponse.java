package com.melo.lzmall.product.api.response.search;

import lombok.Data;

import java.io.Serializable;
@Data
public class StoreResponse implements Serializable {

    private String storeId;

    private String storeDisable;

    private String storeDesc;

    private String storeName;

    private Integer collectionNum;

    private Integer goodsNum;
}
