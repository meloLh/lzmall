package com.melo.lzmall.product.service.api;

import cn.hutool.core.util.IdUtil;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.product.api.response.product.ProductSpuDetailResponse;
import com.melo.lzmall.product.service.converter.ProductFaceConverter;
import com.melo.lzmall.product.service.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "C端搜索")
@RestController
@RequestMapping
public class ProductApi {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductFaceConverter converter;

    @ApiOperation("批量查询sku信息")
    @GetMapping("/api/product/queryBySkuCodeList")
    public Result<List<ProductSkuDetailResponse>> queryBySkuCodeList(@RequestParam(name = "skuCodeList") List<String> skuCodeList) {
        List<ProductSkuDetailResponse> responseList = productService.queryBySkuCodeList(skuCodeList);
        return Result.success(responseList);
    }
}
