package com.melo.lzmall.product.service.api;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.feign.ProductStockFeignClient;
import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockRequest;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.product.service.converter.ProductFaceConverter;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import com.melo.lzmall.product.service.service.ProductService;
import com.melo.lzmall.product.service.service.ProductStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "库存api")
@RestController
@RequestMapping
@Validated
public class ProductStockApi implements ProductStockFeignClient {

    @Autowired
    private ProductStockService productStockService;

    @Autowired
    private ProductFaceConverter converter;

    @ApiOperation("批量冻结库存")
    @PostMapping("/api/product/stock/batchFreezingProductStock")
    public Result<Void> batchFreezingProductStock(@RequestBody List<BatchFreezingProductStockRequest> request) {
        List<OperateProductStockBO> productStockBOList = converter.convertToOperateProductStockBOList(request);
        productStockService.batchFreezingProductStock(productStockBOList);
        return Result.success();
    }
}
