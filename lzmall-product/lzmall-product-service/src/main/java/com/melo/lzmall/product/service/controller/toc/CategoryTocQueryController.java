package com.melo.lzmall.product.service.controller.toc;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.category.QueryCategoryListTocResponse;
import com.melo.lzmall.product.service.converter.CategoryTocConverter;
import com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO;
import com.melo.lzmall.product.service.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "C端分类")
@RestController
@RequestMapping
public class CategoryTocQueryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryTocConverter converter;

    @ApiOperation("查询首页全部分类")
    @GetMapping("/buyer/category/queryAllCategoryList")
    public Result<List<QueryCategoryListTocResponse>> queryAllCategory() {
        List<CategoryTreeBaseDTO> categoryList = categoryService.queryAllCategory();
        List<QueryCategoryListTocResponse> responseList = converter.convertToQueryCategoryListTocResponseList(categoryList);
        return Result.success(responseList);
    }

}
