package com.melo.lzmall.product.service.controller.toc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.request.product.SearchIndexProductRequest;
import com.melo.lzmall.product.api.request.search.SearchRecommendProductListRequest;
import com.melo.lzmall.product.api.response.search.*;
import com.melo.lzmall.product.service.converter.ProductSearchTocConverter;
import com.melo.lzmall.product.service.entity.dto.SearchRecommendProductListDTO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;
import com.melo.lzmall.product.service.service.ProductSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "C端搜索")
@RestController
@RequestMapping
public class ProductSearchTocController {

    @Autowired
    private ProductSearchService productSearchService;

    @Autowired
    private ProductSearchTocConverter converter;

    @ApiOperation("搜索页接口")
    @GetMapping("/buyer/search/searchProduct")
    public Result<Page<SearchIndexProductResponse>> searchProduct(SearchIndexProductRequest request) {
        SearchIndexProductDTO searchIndexProductDTO =  converter.convertToSearchIndexProductDTO(request);
        Page<SearchIndexProductResponse> page = productSearchService.searchProduct(searchIndexProductDTO);
        return Result.success(page);
    }

    @ApiOperation("搜索商品后获取推荐分类、品牌等")
    @GetMapping("/buyer/search/searchProductRelatedContend")
    public Result<SearchProductRelatedContentResponse>  searchProductRelatedContend(SearchIndexProductRequest request) {
        SearchIndexProductDTO searchIndexProductDTO =  converter.convertToSearchIndexProductDTO(request);
        SearchProductRelatedContentResponse response = productSearchService.searchProductRelatedContend(searchIndexProductDTO);
        return Result.success(response);
    }

    @ApiOperation("商品热搜词")
    @GetMapping("/buyer/search/searchHotWords")
    public Result<List<String>> searchHotWords() {
        String json = "[\n" +
                "  \" 大哥大手机\",\n" +
                "  \"手机\",\n" +
                "  \"测试clc\",\n" +
                "  \"薯条\",\n" +
                "  \"平板电脑\",\n" +
                "  \"大牌\",\n" +
                "  \"口红\",\n" +
                "  \"wwwwwwwwwwwwwwwwwwwwwww\",\n" +
                "  \"AABB\",\n" +
                "  \"2qqqqqqqq\"\n" +
                "]";
        return Result.success(JSON.parseArray(json,String.class));
    }

    @ApiOperation("推荐商品搜索")
    @GetMapping("/buyer/search/searchRecommendProductList")
    public Result<Page<SearchRecommendProductListResponse>> searchRecommendProductList(SearchRecommendProductListRequest request) {
        SearchRecommendProductListDTO productListBO =  converter.convertToSearchRecommendProductListBO(request);
        Page<SearchRecommendProductListResponse> page = productSearchService.searchRecommendProductList(productListBO);
        return Result.success(page);
    }

    @ApiOperation("推荐商品搜索")
    @GetMapping("buyer/shop/getShopDetail")
    public Result<Object> getShopDetail(@RequestParam(name = "shopCode")String shopCode) {
        String json = "{\n" +
                "  \"storeId\": \"1376433565247471616\",\n" +
                "  \"storeName\": \"小7\",\n" +
                "  \"storeDisable\": \"OPEN\",\n" +
                "  \"companyAddressPath\": null,\n" +
                "  \"storeLogo\": \"\",\n" +
                "  \"storeDesc\": \"小店铺卖好货\",\n" +
                "  \"pcPageData\": null,\n" +
                "  \"mobilePageData\": null,\n" +
                "  \"selfOperated\": \"1\",\n" +
                "  \"goodsNum\": 10387,\n" +
                "  \"collectionNum\": 307,\n" +
                "  \"yzfSign\": \"37ef9b97807d03c6741298ed4eb5b536d2d238e08a3c00fb01fe48f03a569974c99ad767e72c04b3165ef29aca2c488b505fe4ca\",\n" +
                "  \"yzfMpSign\": \"32b8ff6f8d1c240be8d7fe51bdd6d44a6776ea86930afbe5c3c342825e942c914fc6126b6be1f003ab04aee1af9f442d2c33e1427529300671588866edaa4b12\",\n" +
                "  \"merchantEuid\": \"小强\",\n" +
                "  \"pageShow\": \"0\"\n" +
                "}";
        return Result.success(JSON.parseObject(json, StoreResponse.class));
    }



    @ApiOperation("推荐商品搜索")
    @GetMapping("buyer/member/evaluation/getProductEvaluation")
    public Result<Object> getProductEvaluation(@RequestParam(name = "spuCode")String spuCode) {
        String json = "{\n" +
                "  \"list\": [\n" +
                "    {\n" +
                "      \"id\": \"1658897102296801281\",\n" +
                "      \"createBy\": \"SYSTEM\",\n" +
                "      \"createTime\": \"2023-05-18 02:07:44\",\n" +
                "      \"updateBy\": null,\n" +
                "      \"updateTime\": null,\n" +
                "      \"deleteFlag\": false,\n" +
                "      \"memberId\": \"1623263074457878529\",\n" +
                "      \"storeId\": \"1376433565247471616\",\n" +
                "      \"storeName\": \"小7\",\n" +
                "      \"goodsId\": \"1512009118243758081\",\n" +
                "      \"skuId\": \"1512009118382170114\",\n" +
                "      \"memberName\": \"微信用户\",\n" +
                "      \"memberProfile\": \"https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132\",\n" +
                "      \"goodsName\": \"aa 111\",\n" +
                "      \"goodsImage\": \"https://lilishop-oss.oss-cn-beijing.aliyuncs.com/a9a154d5f9ea4a4d93ca6a2b8b69e408.jpg?x-oss-process=style/400X400\",\n" +
                "      \"orderNo\": \"O202302081623263893005979649\",\n" +
                "      \"grade\": \"GOOD\",\n" +
                "      \"content\": \"系统默认好评\",\n" +
                "      \"images\": null,\n" +
                "      \"status\": \"OPEN\",\n" +
                "      \"reply\": null,\n" +
                "      \"replyImage\": null,\n" +
                "      \"haveImage\": false,\n" +
                "      \"haveReplyImage\": null,\n" +
                "      \"replyStatus\": false,\n" +
                "      \"deliveryScore\": 5,\n" +
                "      \"serviceScore\": 5,\n" +
                "      \"descriptionScore\": 5\n" +
                "    }\n" +
                "  ],\n" +
                "  \"total\": 1,\n" +
                "  \"size\": 10,\n" +
                "  \"current\": 1,\n" +
                "  \"pages\": 1\n" +
                "}";
        return Result.success(JSON.parseObject(json, new TypeReference<Page<ProductEvaluationResponse>>(){}));
    }

}
