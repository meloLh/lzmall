package com.melo.lzmall.product.service.controller.toc;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.product.ProductSpuDetailResponse;
import com.melo.lzmall.product.service.converter.ProductFaceConverter;
import com.melo.lzmall.product.service.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "C端搜索")
@RestController
@RequestMapping
public class ProductTocController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductFaceConverter converter;

    @ApiOperation("搜索页接口")
    @GetMapping("/buyer/product/getProductSpuDetail")
    public Result<ProductSpuDetailResponse> getProductSpuDetail(@RequestParam(name = "spuCode") String spuCode,
                                                                @RequestParam(name = "skuCode") String skuCode) {
        ProductSpuDetailResponse response = productService.getProductSpuDetail(spuCode,skuCode);
        return Result.success(response);
    }
}
