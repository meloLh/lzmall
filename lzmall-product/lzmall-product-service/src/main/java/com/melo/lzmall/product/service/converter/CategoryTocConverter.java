package com.melo.lzmall.product.service.converter;

import com.melo.lzmall.product.api.response.category.QueryCategoryListTocResponse;
import com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CategoryTocConverter {

    List<QueryCategoryListTocResponse> convertToQueryCategoryListTocResponseList(List<CategoryTreeBaseDTO> categoryList);
}

