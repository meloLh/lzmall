package com.melo.lzmall.product.service.converter;

import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockRequest;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductFaceConverter {
    OperateProductStockBO convertToOperateProductStockBO(BatchFreezingProductStockRequest request);

    List<OperateProductStockBO> convertToOperateProductStockBOList(List<BatchFreezingProductStockRequest> request);
}
