package com.melo.lzmall.product.service.converter;

import com.melo.lzmall.product.api.request.product.SearchIndexProductRequest;
import com.melo.lzmall.product.api.request.search.SearchRecommendProductListRequest;
import com.melo.lzmall.product.service.entity.dto.SearchRecommendProductListDTO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ProductSearchTocConverter {

    SearchIndexProductDTO convertToSearchIndexProductDTO(SearchIndexProductRequest request);

    SearchRecommendProductListDTO convertToSearchRecommendProductListBO(SearchRecommendProductListRequest request);
}

