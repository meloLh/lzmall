package com.melo.lzmall.product.service.core;

import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.product.service.core.converter.ProductQueryCoreConverter;
import com.melo.lzmall.product.service.entity.base.*;
import com.melo.lzmall.product.service.entity.bo.product.*;
import com.melo.lzmall.product.service.entity.po.ProductParamPO;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import com.melo.lzmall.product.service.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ProductQueryCore {

    @Autowired
    private ProductSpuRepository productSpuRepository;

    @Resource(name = "productTaskThreadPool")
    private ThreadPoolTaskExecutor executor;

    @Autowired
    private ProductSkuRepository productSkuRepository;

    @Autowired
    private ProductStockRepository productStockRepository;

    @Autowired
    private ProductGraphicDetailRepository graphicDetailRepository;

    @Autowired
    private ProductParamRepository productParamRepository;

    @Autowired
    private ProductSpecRepository productSpecRepository;

    @Autowired
    private ProductQueryCoreConverter converter;

    /**
     * @Description 查询spu详情
     * @author liuhu
     * @param spuCodeList
     * @date 2024/4/25 18:05
     * @return java.util.List<com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO>
     */
    public List<ProductSpuBO> queryProductSpuList(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        // spu信息
        List<ProductSpuBaseDTO> productSpuBaseDTOS = productSpuRepository.queryBySpuCodeList(spuCodeList);
        List<ProductSpuBO> productSpuBOList = converter.convertToProductSpuBO(productSpuBaseDTOS);
        // sku信息
        List<ProductSkuBaseDTO> productSkuBaseDTOS = productSkuRepository.queryBySpuCodeList(spuCodeList);
        List<String> skuCodeList = productSkuBaseDTOS.stream().map(ProductSkuBaseDTO::getSkuCode).collect(Collectors.toList());
        CompletableFuture<List<ProductSkuBO>> skuFuture = CompletableFuture.supplyAsync(() -> this.queryProductSkuList(skuCodeList), executor);
        // spu扩展信息
        CompletableFuture<List<ProductSpuExtendBO>> spuExtendFuture = CompletableFuture.supplyAsync(() -> this.queryProductSpuExtend(spuCodeList), executor);

        CompletableFuture.allOf(spuExtendFuture,skuFuture)
                         .exceptionally(e->{
                             log.error("查询spu详情异常,spuCode:{}",spuCodeList,e);
                             throw new BusinessException("500","查询spu详情异常");
                         }).join();
        // 构建
        return buildProductSpuBOList(productSpuBOList,skuFuture.join(),spuExtendFuture.join());
    }

    /**
     * @Description 查询spu扩展信息 包含属性
     * @author liuhu
     * @param spuCodeList
     * @date 2024/4/25 17:45
     * @return java.util.List<com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO>
     */
    private List<ProductSpuExtendBO> queryProductSpuExtend(List<String> spuCodeList) {
        List<ProductSpuExtendBO> extendBOList = new ArrayList<>();

        List<ProductParamBaseDTO> productParamBaseDTOS = productParamRepository.queryBySpuCodeList(spuCodeList);
        Map<String, List<ProductParamBaseDTO>> spuParamMap = productParamBaseDTOS.stream().collect(Collectors.groupingBy(ProductParamBaseDTO::getSpuCode));

        for (Map.Entry<String, List<ProductParamBaseDTO>> entry : spuParamMap.entrySet()) {
            ProductSpuExtendBO spuExtendBO = new ProductSpuExtendBO();
            spuExtendBO.setSpuCode(entry.getKey());
            List<ProductParamBO> paramList = converter.convertToProductParamBOList(entry.getValue());
            spuExtendBO.setParamList(paramList);
            extendBOList.add(spuExtendBO);
        }
       return extendBOList;
    }

    private List<ProductSpuBO> buildProductSpuBOList(List<ProductSpuBO> productSpuBOList,
                                                     List<ProductSkuBO> productSkuBOList,
                                                     List<ProductSpuExtendBO> spuExtendBOList) {
        Map<String, ProductSpuExtendBO> spuExtendBOMap = spuExtendBOList.stream().collect(Collectors.toMap(ProductSpuExtendBO::getSpuCode, Function.identity(), (k1, k2) -> k1));

        Map<String, List<ProductSkuBO>> skuMap = productSkuBOList.stream().collect(Collectors.groupingBy(ProductSkuBO::getSpuCode));

        for (ProductSpuBO spuBO : productSpuBOList) {
            ProductSpuExtendBO spuExtendBO = spuExtendBOMap.get(spuBO.getSpuCode());
            if(Objects.nonNull(spuExtendBO)){
                spuBO.setParamList(spuExtendBO.getParamList());
            }
            List<ProductSkuBO> productSkuBOS = skuMap.get(spuBO.getSpuCode());
            AssertUtil.isTrue(CollectionUtils.isNotEmpty(productSkuBOS), "获取商品sku详情异常");
            spuBO.setSkuList(productSkuBOS);

            List<ProductSpecBO> spuSpecBOS = productSkuBOS.stream().flatMap(sku -> sku.getSpecList().stream())
                    .sorted(Comparator.comparing(ProductSpecBO::getSpecName).reversed()).collect(Collectors.toList());
            spuBO.setSpecList(spuSpecBOS);

            List<String> thumbnailList = productSkuBOS.stream().map(ProductSkuBO::getThumbnail).collect(Collectors.toList());
            spuBO.setThumbnailList(thumbnailList);

        }
        return productSpuBOList;
    }

    /**
     * @Description 查询sku详情 聚合
     * @author liuhu
     * @param skuCodeList
     * @date 2024/4/25 17:43
     * @return java.util.List<com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO>
     */
    public List<ProductSkuBO> queryProductSkuList(List<String> skuCodeList) {
        // 基本信息
        CompletableFuture<List<ProductSkuBO>> productBaseInfoFuture = CompletableFuture.supplyAsync(() -> getProductSkuBaseInfoList(skuCodeList), executor);
        // 库存信息
        CompletableFuture<List<ProductStockBaseDTO>> stockFuture = CompletableFuture.supplyAsync(() -> productStockRepository.queryBySkuCodeList(skuCodeList), executor);

        CompletableFuture.allOf(productBaseInfoFuture, stockFuture)
                .exceptionally(e -> {
                    log.error("查询sku详情异常,spuCode:{}", skuCodeList, e);
                    throw new BusinessException("500", "查询sku详情异常");
                });
        return buildProductSkuBO(productBaseInfoFuture.join(),stockFuture.join());
    }

    private List<ProductSkuBO> buildProductSkuBO(List<ProductSkuBO> productSkuBOList,
                                                 List<ProductStockBaseDTO> productStockBaseDTOS) {
        Map<String, ProductStockBaseDTO> stockBaseDTOMap = productStockBaseDTOS.stream()
                .collect(Collectors.toMap(ProductStockBaseDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));

        for (ProductSkuBO productSkuBO : productSkuBOList) {
            ProductStockBaseDTO productStockBaseDTO = stockBaseDTOMap.get(productSkuBO.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(productStockBaseDTO), "获取商品库存信息异常");
            productSkuBO.setAvailableStock(productStockBaseDTO.getAvailableStock());
        }
        return productSkuBOList;
    }

    /**
     * @Description 查询sku基本信息
     * @author liuhu
     * @param skuCodeList
     * @date 2024/4/25 17:37
     * @return java.util.List<com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO>
     */
    private List<ProductSkuBO> getProductSkuBaseInfoList(List<String> skuCodeList) {
        // sku基本信息
        List<ProductSkuBaseDTO> productSkuBaseDTOS = productSkuRepository.queryBySkuCodeList(skuCodeList);
        // 图文详情
        List<ProductGraphicDetailBaseDTO> graphicDetailBaseDTOList = graphicDetailRepository.queryBySkuCodeList(skuCodeList);
        Map<String, ProductGraphicDetailBaseDTO> graphicDetailBaseDTOMap = graphicDetailBaseDTOList.stream().collect(Collectors.toMap(ProductGraphicDetailBaseDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));
        // 规格
        List<ProductSpecBaseDTO> productSpecBaseDTOList = productSpecRepository.queryBySkuCodeList(skuCodeList);
        Map<String, List<ProductSpecBaseDTO>> specMap = productSpecBaseDTOList.stream().collect(Collectors.groupingBy(ProductSpecBaseDTO::getSkuCode));
        return productSkuBaseDTOS.stream().map(sku -> {
            ProductGraphicDetailBaseDTO graphicDetailBaseDTO = graphicDetailBaseDTOMap.get(sku.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(graphicDetailBaseDTO), "获取商品图文详情异常");
            List<ProductSpecBaseDTO> specBaseDTOS = specMap.get(sku.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(specBaseDTOS), "获取商品规格详情异常");
            ProductSkuBO productSkuBO = converter.convertToProductSkuBO(sku, graphicDetailBaseDTO);
            List<ProductSpecBO> productSpecBOList = converter.convertToProductSpecBO(specBaseDTOS);
            productSkuBO.setSpecList(productSpecBOList);
            return productSkuBO;
        }).collect(Collectors.toList());
    }
}
