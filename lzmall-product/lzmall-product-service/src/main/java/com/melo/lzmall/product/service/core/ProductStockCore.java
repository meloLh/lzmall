package com.melo.lzmall.product.service.core;

import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.product.service.entity.base.ProductStockBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductStockRecordBaseDTO;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockDetailBO;
import com.melo.lzmall.product.service.entity.dto.OperateProductStockDTO;
import com.melo.lzmall.product.service.enums.ProductStockBizTypeEnum;
import com.melo.lzmall.product.service.mapper.ProductStockMapper;
import com.melo.lzmall.product.service.repository.ProductStockRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ProductStockCore {

    @Autowired
    private ProductStockRepository productStockRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private RedissonClient redissonClient;

    public void batchFreezingProductStock(List<OperateProductStockBO> productStockBOList) {
        List<String> skuCodeList = productStockBOList.stream()
                .flatMap(v->v.getSkuList().stream().map(OperateProductStockDetailBO::getSkuCode).collect(Collectors.toList()).stream())
                .collect(Collectors.toList());
        //1.商品信息校验
        List<ProductStockBaseDTO> stockBaseDTOList = productStockRepository.queryBySkuCodeList(skuCodeList);
        validProductStock(stockBaseDTOList,skuCodeList);
        //1.业务单库存流水校验
        List<String> orderCodeList = productStockBOList.stream().map(OperateProductStockBO::getOrderCode).collect(Collectors.toList());
        List<ProductStockRecordBaseDTO> productStockRecordBaseDTOS =  productStockRepository.queryRecordByBizCodeList(orderCodeList);
        validStockRecord(productStockRecordBaseDTOS,productStockBOList);
        List<OperateProductStockDTO> stockDetailBOS = buildOperateProductStockDTOS(productStockBOList);
        // 利用mysql 行锁处理并发
        transactionTemplate.execute(ts -> {
            productStockRepository.batchFreezingProductStock(stockDetailBOS);
            // 事务未提交 当前库存不会被其他线程更改 当前查询的库存是修改后的  往前反推操作前库存
            List<ProductStockBaseDTO> productStockBaseDTOS = productStockRepository.queryBySkuCodeList(skuCodeList);
            List<ProductStockRecordBaseDTO> productStockRecordBaseDTOList = buildProductStockRecordBaseDTO(productStockBaseDTOS,productStockBOList);
            productStockRepository.batchInsert(productStockRecordBaseDTOList);
            return null;
        });
    }

    private void validStockRecord(List<ProductStockRecordBaseDTO> productStockRecordBaseDTOS,
                                  List<OperateProductStockBO> productStockBOList) {
        if(CollectionUtils.isEmpty(productStockRecordBaseDTOS)){
            return;
        }
        Map<String, ProductStockRecordBaseDTO> recordBaseDTOMap = productStockRecordBaseDTOS.stream().collect(Collectors.toMap(ProductStockRecordBaseDTO::getBizCode, Function.identity(), (k1, k2) -> k1));
        for (OperateProductStockBO productStockBO : productStockBOList) {
            ProductStockRecordBaseDTO productStockRecordBaseDTO = recordBaseDTOMap.get(productStockBO.getOrderCode());
            AssertUtil.isTrue(Objects.isNull(productStockRecordBaseDTO),"当前业务单已处理");
        }
    }

    private void validProductStock(List<ProductStockBaseDTO> stockBaseDTOList, List<String> skuCodeList) {
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(stockBaseDTOList),"库存信息不存在");
        List<String> existSkuCodeList = stockBaseDTOList.stream().map(ProductStockBaseDTO::getSkuCode).collect(Collectors.toList());
        AssertUtil.isTrue(existSkuCodeList.containsAll(skuCodeList),"商品库存不存在");
    }

    private List<ProductStockRecordBaseDTO> buildProductStockRecordBaseDTO(List<ProductStockBaseDTO> existProductStockList,
                                                                           List<OperateProductStockBO> productStockBOList) {
        Map<String, ProductStockBaseDTO> stockBaseDTOMap = existProductStockList.stream().collect(Collectors.toMap(ProductStockBaseDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));

       return productStockBOList.stream().flatMap(orderStock->{
            List<ProductStockRecordBaseDTO> productStockRecordBaseDTOS = orderStock.getSkuList().stream().map(sku -> {
                ProductStockBaseDTO productStockBaseDTO = stockBaseDTOMap.get(sku.getSkuCode());
                AssertUtil.isTrue(Objects.nonNull(productStockBaseDTO), "库存信息不存在" + sku.getSkuCode());
                ProductStockRecordBaseDTO dto = new ProductStockRecordBaseDTO();
                dto.setSkuCode(sku.getSkuCode());
                dto.setBizCode(orderStock.getOrderCode());
                dto.setBizType(ProductStockBizTypeEnum.CREATE_ORDER.getValue());
                dto.setOperateQty(sku.getStockNum());
                dto.setBeforeRealStock(productStockBaseDTO.getRealStock());
                dto.setAfterRealStock(productStockBaseDTO.getRealStock());
                dto.setBeforeFreezeStock(productStockBaseDTO.getFreezeStock() - sku.getStockNum());
                dto.setAfterFreezeStock(productStockBaseDTO.getFreezeStock());
                dto.setBeforeAvailableStock(dto.getBeforeRealStock() - dto.getBeforeFreezeStock());
                dto.setAfterAvailableStock(productStockBaseDTO.getAvailableStock());

                dto.setCreator(orderStock.getOperateName());
                dto.setCreatorCode(orderStock.getOperateCode());
                dto.setModifier(orderStock.getOperateName());
                dto.setModifierCode(orderStock.getOperateCode());
                return dto;
            }).collect(Collectors.toList());
            return productStockRecordBaseDTOS.stream();
        }).collect(Collectors.toList());
    }

    private List<OperateProductStockDTO> buildOperateProductStockDTOS(List<OperateProductStockBO> productStockBOList) {
        return productStockBOList.stream().flatMap(v->v.getSkuList().stream()).map(sku -> {
            OperateProductStockDTO stockDTO = new OperateProductStockDTO();
            stockDTO.setStockNum(sku.getStockNum());
            stockDTO.setSkuCode(sku.getSkuCode());
            return stockDTO;
        }).collect(Collectors.toList());
    }
}
