package com.melo.lzmall.product.service.core.converter;

import com.melo.lzmall.product.service.entity.base.*;
import com.melo.lzmall.product.service.entity.bo.product.ProductParamBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpecBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductQueryCoreConverter {
    @Mappings({
            @Mapping(source = "sku.spuCode",target = "spuCode"),
            @Mapping(source = "sku.skuCode",target = "skuCode")
    })
    ProductSkuBO convertToProductSkuBO(ProductSkuBaseDTO sku, ProductGraphicDetailBaseDTO graphicDetailBaseDTO);

    List<ProductSpecBO> convertToProductSpecBO(List<ProductSpecBaseDTO> specBaseDTOS);
    
    List<ProductSpuBO> convertToProductSpuBO(List<ProductSpuBaseDTO> productSpuBaseDTOS);

    List<ProductParamBO> convertToProductParamBOList(List<ProductParamBaseDTO> value);
}
