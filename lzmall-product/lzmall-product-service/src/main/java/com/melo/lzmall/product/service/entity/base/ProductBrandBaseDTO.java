package com.melo.lzmall.product.service.entity.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 品牌表
 */
@Data
public class ProductBrandBaseDTO implements Serializable {

    /**
     * ID
     */
    private Long id;
    /**
     * 品牌编码
     */
    private String brandCode;
    /**
     * 品牌图标
     */
    private String logo;
    /**
     * 品牌名称
     */
    private String name;
    /**
     * 品牌故事
     */
    private String brandStory;
    /**
     *
     */
    private String description;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
