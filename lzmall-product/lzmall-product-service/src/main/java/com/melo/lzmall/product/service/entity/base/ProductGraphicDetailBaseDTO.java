package com.melo.lzmall.product.service.entity.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品图文详情表
 */
@Data
public class ProductGraphicDetailBaseDTO implements Serializable {

    /**
     * ID
     */
    private Long id;
    /**
     * spu编码
     */
    private String spuCode;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * 商品视频
     */
    private String video;
    /**
     * 商品详情
     */
    private String detailHtml;
    /**
     * 商品移动端详情
     */
    private String mobileHtml;
    /**
     * 移动端商品描述
     */
    private String mobileIntro;
    /**
     * 卖点
     */
    private String sellingPoint;
    /**
     * 原图路径
     */
    private String original;
    /**
     * 小图路径
     */
    private String small;
    /**
     * 缩略图路径
     */
    private String thumbnail;
}
