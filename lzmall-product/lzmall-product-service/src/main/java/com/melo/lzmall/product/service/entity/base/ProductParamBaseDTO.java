package com.melo.lzmall.product.service.entity.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品参数表
 */
@Data
public class ProductParamBaseDTO implements Serializable {

    private String spuCode;
    /**
     * 参数编码
     */
    private String paramCode;
    /**
     * 参数分组名称
     */
    private String paramGroupName;
    /**
     * 参数项
     */
    private String paramName;
    /**
     * 参数值
     */
    private String paramValue;

}
