package com.melo.lzmall.product.service.entity.base;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品sku表
 */
@Data
public class ProductSkuBaseDTO implements Serializable {

    /**
     * ID
     */
    private Long id;
    /**
     * spu编码
     */
    private String spuCode;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * sku名称
     */
    private String productName;
    /**
     * 品牌编码
     */
    private String brandCode;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 一级分类编号
     */
    private String categoryCode1;
    /**
     * 二级分类编号
     */
    private String categoryCode2;
    /**
     * 三级分类编号
     */
    private String categoryCode3;
    /**
     * 分类路径
     */
    private String categoryCodePath;
    /**
     * 店铺分类
     */
    private String shopCategoryCodePath;
    /**
     * 成本价
     */
    private BigDecimal costPrice;
    /**
     * 商品价
     */
    private BigDecimal salePrice;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private String status;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
