package com.melo.lzmall.product.service.entity.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品规格表
 */
@Data
public class ProductSpecBaseDTO implements Serializable {

    private String skuCode;

    private String spuCode;
    /**
     * 规格编号
     */
    private String specCode;
    /**
     * 规格项名称
     */
    private String specName;
    /**
     * 规格值名称
     */
    private String specValue;
}
