package com.melo.lzmall.product.service.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 库存信息表
 */
@Data
public class ProductStockBaseDTO implements Serializable {

    /**
     * id
     */
    private Long id;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * spu编码
     */
    private String spuCode;
    /**
     * 实际库存
     */
    private Integer realStock;
    /**
     * 可用库存
     */
    private Integer availableStock;
    /**
     * 冻结库存
     */
    private Integer freezeStock;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
