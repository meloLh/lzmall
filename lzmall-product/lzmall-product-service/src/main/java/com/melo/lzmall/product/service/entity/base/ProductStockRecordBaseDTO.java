package com.melo.lzmall.product.service.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 库存流水记录表
 */
@Data
public class ProductStockRecordBaseDTO implements Serializable {

    /**
     * id
     */
    private Long id;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * spu编码
     */
    private String spuCode;
    /**
     * 业务类型
     */
    private String bizType;
    /**
     * 业务单号
     */
    private String bizCode;
    /**
     * 操作数量
     */
    private Integer operateQty;
    /**
     * 操作前实际库存
     */
    private Integer beforeRealStock;
    /**
     * 操作后实际库存
     */
    private Integer afterRealStock;
    /**
     * 操作前可用库存
     */
    private Integer beforeAvailableStock;
    /**
     * 操作后可用库存
     */
    private Integer afterAvailableStock;
    /**
     * 操作前冻结库存
     */
    private Integer beforeFreezeStock;
    /**
     * 操作后冻结库存
     */
    private Integer afterFreezeStock;
    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
