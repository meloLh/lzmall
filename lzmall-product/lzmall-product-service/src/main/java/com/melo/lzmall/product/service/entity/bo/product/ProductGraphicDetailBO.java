package com.melo.lzmall.product.service.entity.bo.product;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品图文详情表
 */
@Data

public class ProductGraphicDetailBO implements Serializable {

	/**
	 * spu编码
	 */
	private String spuCode;
	/**
	 * sku编码
	 */
	private String skuCode;
	/**
	 * 商品视频
	 */
	private String video;
	/**
	 * 商品详情
	 */
	private String detailHtml;
	/**
	 * 商品移动端详情
	 */
	private String mobileHtml;
	/**
	 * 卖点
	 */
	private String sellingPoint;
	/**
	 * 原图路径
	 */
	private String original;
	/**
	 * 小图路径
	 */
	private String small;
	/**
	 * 缩略图路径
	 */
	private String thumbnail;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
