package com.melo.lzmall.product.service.entity.bo.product;

import lombok.Data;

import java.io.Serializable;

/**
 * 商品参数表
 */
@Data
public class ProductParamBO implements Serializable {

	/**
	 * 参数编码
	 */
	private String paramCode;
	/**
	 * 参数分组名称
	 */
	private String paramGroupName;
	/**
	 * 参数项
	 */
	private String paramName;
	/**
	 * 参数值
	 */
	private String paramValue;
}
