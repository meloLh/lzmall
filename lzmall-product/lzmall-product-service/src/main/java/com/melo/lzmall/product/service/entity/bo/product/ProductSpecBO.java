package com.melo.lzmall.product.service.entity.bo.product;

import lombok.Data;

import java.io.Serializable;


@Data
public class ProductSpecBO implements Serializable {

	private String spuCode;

	private String skuCode;

	/**
	 * 规格编号
	 */
	private String specCode;
	/**
	 * 规格项名称
	 */
	private String specName;
	/**
	 * 规格值名称
	 */
	private String specValue;
}
