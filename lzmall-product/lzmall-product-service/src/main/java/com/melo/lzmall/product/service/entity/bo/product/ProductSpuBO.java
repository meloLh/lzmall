package com.melo.lzmall.product.service.entity.bo.product;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@Data
public class ProductSpuBO implements Serializable {

    /**
     * spu编码
     */
    private String spuCode;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 一级分类编号
     */
    private String categoryCode1;
    /**
     * 二级分类编号
     */
    private String categoryCode2;
    /**
     * 三级分类编号
     */
    private String categoryCode3;
    /**
     * 分类路径
     */
    private String categoryCodePath;
    /**
     * 店铺分类
     */
    private String shopCategoryCodePath;
    /**
     * 品牌ID
     */
    private String brandCode;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 关键词
     */
    private String keyword;
    /**
     * 单位
     */
    private String unit;
    /**
     * 销售模式
     */
    private String salesModel;

    private Date createdTime;
    /**
     * spu对应sku信息
     */
    private List<ProductSkuBO> skuList;
    /**
     * spu规格信息
     */
    private List<ProductSpecBO> specList;

    /**图片集合*/
    private List<String> thumbnailList;

    /**
     * spu对应sku信息
     */
    private List<ProductParamBO> paramList;
}
