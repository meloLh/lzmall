package com.melo.lzmall.product.service.entity.bo.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductSpuExtendBO implements Serializable {

    private String spuCode;
    /**
     * spu对应sku信息
     */
    private List<ProductParamBO> paramList;
}
