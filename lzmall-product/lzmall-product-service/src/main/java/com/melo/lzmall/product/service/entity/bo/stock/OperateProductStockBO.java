package com.melo.lzmall.product.service.entity.bo.stock;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OperateProductStockBO implements Serializable {

    private String orderCode;

    private String operateCode;

    private String operateName;

    private List<OperateProductStockDetailBO> skuList;

}
