package com.melo.lzmall.product.service.entity.bo.stock;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class OperateProductStockDetailBO implements Serializable {

    private String skuCode;

    private String skuName;

    private Integer stockNum;

}
