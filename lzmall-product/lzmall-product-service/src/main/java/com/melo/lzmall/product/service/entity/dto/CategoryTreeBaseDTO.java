package com.melo.lzmall.product.service.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品分类表
 */
@Data
public class CategoryTreeBaseDTO implements Serializable {

    /**
     * ID
     */
    private Long id;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 父级分类编码
     */
    private String categoryCode;
    /**
     * 父级分类编码
     */
    private String parentCategoryCode;
    /**
     * 层级
     */
    private Integer level;
    /**
     * 分类图标
     */
    private String image;
    /**
     * 排序值
     */
    private Integer sort;


    private List<CategoryTreeBaseDTO> childrenList;

}
