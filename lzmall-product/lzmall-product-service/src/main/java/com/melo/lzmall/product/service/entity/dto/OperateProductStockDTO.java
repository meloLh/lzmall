package com.melo.lzmall.product.service.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OperateProductStockDTO implements Serializable {

    private String operateCode;

    private String operateName;

    private String skuCode;

    private String skuName;

    private Integer stockNum;

}
