package com.melo.lzmall.product.service.entity.dto;

import com.melo.lzmall.common.core.dto.PageRequest;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SearchIndexProductDTO extends PageRequest implements Serializable {

    /**
     * 搜索关键词
     */
    private String keyword;

    /**
     * 三级分类编号
     */
    private String categoryCode3;

    /**
     * 品牌
     */
    private String brandCode;

    /**
     * 最小销售价
     */
    private BigDecimal minSalePrice;

    /**
     * 最大销售价
     */
    private BigDecimal maxSalePrice;


    /**
     * 排序字段
     */
    private String sort;

    /**
     * 升序降序
     */
    private Boolean desc;

}
