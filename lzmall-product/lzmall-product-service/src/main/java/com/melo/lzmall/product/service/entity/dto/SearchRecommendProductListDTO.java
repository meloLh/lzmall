package com.melo.lzmall.product.service.entity.dto;

import com.melo.lzmall.common.core.dto.PageRequest;
import lombok.Data;

import java.io.Serializable;

@Data
public class SearchRecommendProductListDTO extends PageRequest implements Serializable {

    private String categoryCode3;

    private String brandCode;

    private String keyword;

    private String currentSpuCode;
}
