package com.melo.lzmall.product.service.entity.es.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldNameConstants
public class ProductSearchEsPO implements Serializable {

    /**
     * spu编码
     */
    private String spuCode;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * sku名称
     */
    private String productName;
    /**
     * 品牌编码
     */
    private String brandCode;

    private String brandName;
    /**
     * 店铺ID
     */
    private String shopCode;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 一级分类编号
     */
    private String categoryCode1;

    private String categoryName1;
    /**
     * 二级分类编号
     */
    private String categoryCode2;

    private String categoryName2;
    /**
     * 三级分类编号
     */
    private String categoryCode3;

    private String categoryName3;
    /**
     * 成本价
     */
    private BigDecimal costPrice;
    /**
     * 商品价
     */
    private BigDecimal salePrice;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private String status;
    /**
     * 排序
     */
    private Integer sort;


    private Integer availableStock;

    private Date createdTime;
}
