package com.melo.lzmall.product.service.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品分类表
 */
@Data
@TableName("product_category")
public class ProductCategoryPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 分类名称
	 */
	private String categoryName;
	/**
	 * 分类编码
	 */
	private String categoryCode;
	/**
	 * 父级分类编码
	 */
	private String parentCategoryCode;
	/**
	 * 层级
	 */
	private Integer level;
	/**
	 * 分类图标
	 */
	private String image;
	/**
	 * 排序值
	 */
	private Integer sort;
	/**
	 * 状态 ENABLE/DISABLE
	 */
	private String status;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
