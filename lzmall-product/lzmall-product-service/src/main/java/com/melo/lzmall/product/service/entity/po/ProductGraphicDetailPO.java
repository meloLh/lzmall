package com.melo.lzmall.product.service.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品图文详情表
 */
@Data
@TableName("product_graphic_detail")
public class ProductGraphicDetailPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * spu编码
	 */
	private String spuCode;
	/**
	 * sku编码
	 */
	private String skuCode;
	/**
	 * 商品视频
	 */
	private String video;
	/**
	 * 商品详情
	 */
	private String detailHtml;
	/**
	 * 商品移动端详情
	 */
	private String mobileHtml;
	/**
	 * 卖点
	 */
	private String sellingPoint;

	/**
	 * 移动端商品描述
	 */
	private String mobileIntro;
	/**
	 * 原图路径
	 */
	private String original;
	/**
	 * 小图路径
	 */
	private String small;
	/**
	 * 缩略图路径
	 */
	private String thumbnail;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
