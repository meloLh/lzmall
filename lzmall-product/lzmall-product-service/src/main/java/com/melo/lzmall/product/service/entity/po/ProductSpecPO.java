package com.melo.lzmall.product.service.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品规格表
 */
@Data
@TableName("product_spec")
public class ProductSpecPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 规格编号
	 */
	private String specCode;
	/**
	 * 规格项名称
	 */
	private String specName;
	/**
	 * 规格值名称
	 */
	private String specValue;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
