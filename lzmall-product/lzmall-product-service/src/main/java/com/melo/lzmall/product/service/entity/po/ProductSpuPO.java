package com.melo.lzmall.product.service.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 商品spu表
 */
@Data
@TableName("product_spu")
public class ProductSpuPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * spu编码
	 */
	private String spuCode;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 一级分类编号
	 */
	private String categoryCode1;
	/**
	 * 二级分类编号
	 */
	private String categoryCode2;
	/**
	 * 三级分类编号
	 */
	private String categoryCode3;
	/**
	 * 分类路径
	 */
	private String categoryCodePath;
	/**
	 * 店铺分类
	 */
	private String shopCategoryCodePath;
	/**
	 * 品牌ID
	 */
	private String brandCode;
	/**
	 * 店铺ID
	 */
	private String shopCode;
	/**
	 * 店铺名称
	 */
	private String shopName;
	/**
	 * 关键词
	 */
	private String keyword;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 销售模式
	 */
	private String salesModel;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
