package com.melo.lzmall.product.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductEsEnum {

    LZMALL_PRODUCT_SEARCH("lzmall_product_search", "lzmall_product_search_alias","商品搜索使用"),
    ;

    private final String indexName;

    private final String alias;


    private final String desc;


}
