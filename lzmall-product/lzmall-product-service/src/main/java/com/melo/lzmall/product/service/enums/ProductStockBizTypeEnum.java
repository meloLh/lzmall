package com.melo.lzmall.product.service.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductStockBizTypeEnum {

    CREATE_ORDER("CREATE_ORDER", "下单冻结库存（实际库存不变  冻结库存增加  可用库存减少）"),

    DELIVERY_ORDER("DELIVERY_ORDER", "发货扣减实际库存（实际库存减少  冻结库存减少  可用库存不变）"),

    RETURN_ORDER_CONFIRM("RETURN_ORDER_CONFIRM", "退货确认归还实际库存（实际库存增加  冻结库存不变  可用库存增加）"),
    ;

    private final String value;

    private final String name;


}
