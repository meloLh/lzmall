package com.melo.lzmall.product.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.product.service.entity.po.ProductParamRefPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ProductParamRefMapper extends BaseMapper<ProductParamRefPO> {

    List<String> queryGroupParam(@Param("spuCodeList") List<String> spuCodeList);
}
