package com.melo.lzmall.product.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ProductSpuMapper extends BaseMapper<ProductSpuPO> {

    List<String> queryGroupBrand(@Param("spuCodeList") List<String> spuCodeList);

    List<String> queryGroupCategory(@Param("spuCodeList")List<String> spuCodeList);
}
