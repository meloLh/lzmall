package com.melo.lzmall.product.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import com.melo.lzmall.product.service.entity.dto.OperateProductStockDTO;
import com.melo.lzmall.product.service.entity.po.ProductStockPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface ProductStockMapper extends BaseMapper<ProductStockPO> {

    /**
     * @Description 增加冻结库存 减少可用库存  实际库存不变
     * @author liuhu
     * @param stockDTO
     * @date 2024/6/3 14:15
     * @return int
     */
    int batchFreezingProductStock(@Param("entity") OperateProductStockDTO stockDTO);
}
