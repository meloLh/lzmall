package com.melo.lzmall.product.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.product.service.entity.po.ProductStockRecordPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ProductStockRecordMapper extends BaseMapper<ProductStockRecordPO> {
	
}
