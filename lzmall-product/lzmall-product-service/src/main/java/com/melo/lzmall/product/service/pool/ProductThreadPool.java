package com.melo.lzmall.product.service.pool;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadPoolExecutor;
/**
 *  @Description pool
 *  @author liuhu
 *  @Date 2023/12/18 17:10
 */
@Component
public class ProductThreadPool {

    @Bean("productTaskThreadPool")
    public ThreadPoolTaskExecutor productTaskThreadPool(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(8);
        executor.setMaxPoolSize(20);
        executor.setQueueCapacity(500);
        executor.setThreadFactory(new CustomizableThreadFactory("LZMALL_TASK_PRODUCT_"));
        executor.setKeepAliveSeconds(60*60);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}
