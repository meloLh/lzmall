package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.product.service.entity.base.ProductBrandBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductBrandPO;
import com.melo.lzmall.product.service.mapper.BrandMapper;
import com.melo.lzmall.product.service.repository.converter.ProductBrandRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ProductBrandRepository {

    @Autowired
    private BrandMapper brandMapper;

    @Autowired
    private ProductBrandRepositoryConverter converter;

    public List<ProductBrandBaseDTO> queryByBrandCode(List<String> brandCodeList) {
        if(CollectionUtils.isEmpty(brandCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductBrandPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductBrandPO::getBrandCode,brandCodeList);
        List<ProductBrandPO> productBrandPOS = brandMapper.selectList(queryWrapper);
        return converter.convertToProductBrandBaseDTOList(productBrandPOS);
    }
}

