package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.product.service.entity.base.ProductCategoryBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductCategoryPO;
import com.melo.lzmall.product.service.mapper.CategoryMapper;
import com.melo.lzmall.product.service.repository.converter.CategoryRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Slf4j
@Component
public class ProductCategoryRepository {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryRepositoryConverter converter;

    public List<ProductCategoryBaseDTO> queryAllCategory() {
        LambdaQueryWrapper<ProductCategoryPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ProductCategoryPO::getStatus,"ENABLE").orderByDesc(ProductCategoryPO::getSort);
        List<ProductCategoryPO> productCategoryPOS = categoryMapper.selectList(queryWrapper);
        return converter.convertToCategoryBaseDTOList(productCategoryPOS);
    }

    public List<ProductCategoryBaseDTO> queryByCategoryCodeList(List<String> categoryCodeList) {
        LambdaQueryWrapper<ProductCategoryPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .in(ProductCategoryPO::getCategoryCode,categoryCodeList)
                .eq(ProductCategoryPO::getStatus,"ENABLE")
                .orderByDesc(ProductCategoryPO::getSort);
        List<ProductCategoryPO> productCategoryPOS = categoryMapper.selectList(queryWrapper);
        return converter.convertToCategoryBaseDTOList(productCategoryPOS);
    }
}

