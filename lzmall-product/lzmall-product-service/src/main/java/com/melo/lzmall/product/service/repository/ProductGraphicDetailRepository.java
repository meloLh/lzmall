package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.product.service.entity.base.ProductGraphicDetailBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductGraphicDetailPO;
import com.melo.lzmall.product.service.entity.po.ProductParamPO;
import com.melo.lzmall.product.service.mapper.ProductGraphicDetailMapper;
import com.melo.lzmall.product.service.repository.converter.ProductGraphicDetailRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ProductGraphicDetailRepository {

    @Autowired
    private ProductGraphicDetailMapper productGraphicDetailMapper;

    @Autowired
    private ProductGraphicDetailRepositoryConverter converter;

    public List<ProductGraphicDetailBaseDTO> queryBySkuCodeList(List<String> skuCodeList) {
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductGraphicDetailPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductGraphicDetailPO::getSkuCode,skuCodeList);
        List<ProductGraphicDetailPO> productGraphicDetailPOS = productGraphicDetailMapper.selectList(queryWrapper);
        return   converter.convertToProductGraphicDetailBaseDTOList(productGraphicDetailPOS);
    }
}

