package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.product.service.entity.base.ProductParamBaseDTO;
import com.melo.lzmall.product.service.entity.bo.product.ProductParamBO;
import com.melo.lzmall.product.service.entity.po.ProductParamPO;
import com.melo.lzmall.product.service.entity.po.ProductParamRefPO;
import com.melo.lzmall.product.service.mapper.ProductParamMapper;
import com.melo.lzmall.product.service.mapper.ProductParamRefMapper;
import com.melo.lzmall.product.service.repository.converter.ProductParamRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ProductParamRepository {

    @Autowired
    private ProductParamMapper productParamMapper;

    @Autowired
    private ProductParamRefMapper productParamRefMapper;

    @Autowired
    private ProductParamRepositoryConverter converter;

    public List<ProductParamBaseDTO> queryBySpuCodeList(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        List<ProductParamRefPO> productParamRefPOS = queryProductParamRefBySpuCodeList(spuCodeList);
        List<String> paramCodeList = productParamRefPOS.stream().map(ProductParamRefPO::getParamCode).distinct().collect(Collectors.toList());
        List<ProductParamPO> productParamPOS = queryByParamCodeList(paramCodeList);
        Map<String, ProductParamPO> productParamPOMap = productParamPOS.stream().collect(Collectors.toMap(ProductParamPO::getParamCode, Function.identity(), (k1, k2) -> k1));

       return productParamRefPOS.stream().map(ref->{
            ProductParamPO productParamPO = productParamPOMap.get(ref.getParamCode());
            if(Objects.isNull(productParamPO)){
                return null;
            }
            ProductParamBaseDTO baseDTO = converter.convertToProductParamBaseDTO(productParamPO);
            baseDTO.setSpuCode(ref.getSpuCode());
            return baseDTO;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<ProductParamPO> queryByParamCodeList(List<String> paramCodeList){
        if(CollectionUtils.isEmpty(paramCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductParamPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductParamPO::getParamCode,paramCodeList);
        return productParamMapper.selectList(queryWrapper);
    }

    public List<ProductParamBaseDTO> queryBaseDTOByParamCodeList(List<String> paramCodeList){
        if(CollectionUtils.isEmpty(paramCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductParamPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductParamPO::getParamCode,paramCodeList);
        List<ProductParamPO> productParamPOS = productParamMapper.selectList(queryWrapper);
        return converter.convertToProductParamBaseDTOList(productParamPOS);
    }

    private List<ProductParamRefPO> queryProductParamRefBySpuCodeList(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductParamRefPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductParamRefPO::getSpuCode,spuCodeList);
        return productParamRefMapper.selectList(queryWrapper);
    }

    public List<String> queryGroupParam(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
       return productParamRefMapper.queryGroupParam(spuCodeList);
    }
}

