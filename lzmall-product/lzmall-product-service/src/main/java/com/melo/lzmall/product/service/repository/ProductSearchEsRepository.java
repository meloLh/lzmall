package com.melo.lzmall.product.service.repository;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.es.dto.EsPageCommonParamDTO;
import com.melo.lzmall.common.es.dto.EsSortParamDTO;
import com.melo.lzmall.common.es.helper.EsHelper;
import com.melo.lzmall.product.service.entity.es.po.ProductSearchEsPO;
import com.melo.lzmall.product.service.enums.ProductEsEnum;
import com.melo.lzmall.product.service.repository.dto.ProductSearchEsDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;

@Component
@Slf4j
public class ProductSearchEsRepository {

    @Autowired
    private EsHelper esHelper;

    public void upsert(ProductSearchEsPO productSearchEsPO) {
        esHelper.upsert(ProductEsEnum.LZMALL_PRODUCT_SEARCH.getIndexName(),productSearchEsPO.getSpuCode(), JSON.toJSONString(productSearchEsPO));

    }

    public void createIndex() {
        String mapping = "{\n" +
                "\t\"mappings\": {\n" +
                "\t\t\"properties\": {\n" +
                "\t\t\t\"spuCode\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"productName\": {\n" +
                "\t\t\t\t\"type\": \"text\",\n" +
                "\t\t\t\t\"analyzer\": \"ik_max_word\",\n" +
                "\t\t\t\t\"fields\": {\n" +
                "\t\t\t\t\t\"keyword\": {\n" +
                "\t\t\t\t\t\t\"type\": \"keyword\",\n" +
                "\t\t\t\t\t\t\"ignore_above\": 256\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t}\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryCode1\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryName1\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryCode2\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryName2\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryCode3\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"categoryName3\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"brandCode\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"brandName\": {\n" +
                "\t\t\t\t\"type\": \"text\",\n" +
                "\t\t\t\t\"analyzer\": \"ik_max_word\",\n" +
                "\t\t\t\t\"fields\": {\n" +
                "\t\t\t\t\t\"keyword\": {\n" +
                "\t\t\t\t\t\t\"type\": \"keyword\",\n" +
                "\t\t\t\t\t\t\"ignore_above\": 256\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t}\n" +
                "\t\t\t},\n" +
                "\t\t\t\"shopCode\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"shopName\": {\n" +
                "\t\t\t\t\"type\": \"text\",\n" +
                "\t\t\t\t\"analyzer\": \"ik_max_word\",\n" +
                "\t\t\t\t\"fields\": {\n" +
                "\t\t\t\t\t\"keyword\": {\n" +
                "\t\t\t\t\t\t\"type\": \"keyword\",\n" +
                "\t\t\t\t\t\t\"ignore_above\": 256\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t}\n" +
                "\t\t\t},\n" +
                "\t\t\t\"keyword\": {\n" +
                "\t\t\t\t\"type\": \"text\",\n" +
                "\t\t\t\t\"analyzer\": \"ik_max_word\",\n" +
                "\t\t\t\t\"fields\": {\n" +
                "\t\t\t\t\t\"keyword\": {\n" +
                "\t\t\t\t\t\t\"type\": \"keyword\",\n" +
                "\t\t\t\t\t\t\"ignore_above\": 256\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t}\n" +
                "\t\t\t},\n" +
                "\t\t\t\"status\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"creatorCode\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"creator\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"createdTime\": {\n" +
                "\t\t\t\t\"type\": \"date\",\n" +
                "\t\t\t\t\"format\": \"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"skuCode\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"costPrice\": {\n" +
                "\t\t\t\t\"type\": \"scaled_float\",\n" +
                "                \"scaling_factor\": 100\n" +
                "\t\t\t},\n" +
                "\t\t\t\"salePrice\": {\n" +
                "\t\t\t\t\"type\": \"scaled_float\",\n" +
                "                \"scaling_factor\": 100\n" +
                "\t\t\t},\n" +
                "\t\t\t\"marketPrice\": {\n" +
                "\t\t\t\t\"type\": \"scaled_float\",\n" +
                "\t\t\t\t\"scaling_factor\": 100\n" +
                "\t\t\t},\n" +
                "\t\t\t\"unit\": {\n" +
                "\t\t\t\t\"type\": \"keyword\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"availableStock\": {\n" +
                "                 \"type\": \"integer\"\n" +
                "            }\n" +
                "\t\t}\n" +
                "\t}\n" +
                "}";
        esHelper.createIndex(ProductEsEnum.LZMALL_PRODUCT_SEARCH.getIndexName(),ProductEsEnum.LZMALL_PRODUCT_SEARCH.getAlias(), mapping);
    }

    public Page<ProductSearchEsPO> searchProductPage(ProductSearchEsDTO productSearchEsDTO) {
        BoolQueryBuilder queryBuilder = getProductSearchBoolQueryBuilder(productSearchEsDTO);

        ArrayList<EsSortParamDTO> esSortParamDTOS = Lists.newArrayList(EsSortParamDTO.builder().column(ProductSearchEsPO.Fields.createdTime).desc(true).build());
        if(StringUtils.isNotBlank(productSearchEsDTO.getSortField())){
            EsSortParamDTO sortParamDTO = EsSortParamDTO.builder().column(productSearchEsDTO.getSortField()).desc(productSearchEsDTO.getDesc()).build();
            esSortParamDTOS.add(sortParamDTO);
        }

        EsPageCommonParamDTO commonParamDTO = EsPageCommonParamDTO.builder()
                .pageIndex(productSearchEsDTO.getPageIndex())
                .pageSize(productSearchEsDTO.getPageSize())
                .indexName(ProductEsEnum.LZMALL_PRODUCT_SEARCH.getAlias())
                .esSortParamList(esSortParamDTOS)
                .build();
       return esHelper.queryPage(queryBuilder,commonParamDTO,ProductSearchEsPO.class);
    }

    private BoolQueryBuilder getProductSearchBoolQueryBuilder(ProductSearchEsDTO productSearchEsDTO) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if(StringUtils.isNotBlank(productSearchEsDTO.getBrandCode())){
            queryBuilder.filter(QueryBuilders.termQuery(ProductSearchEsPO.Fields.brandCode, productSearchEsDTO.getBrandCode()));
        }
        if(StringUtils.isNotBlank(productSearchEsDTO.getCategoryCode3())){
            queryBuilder.filter(QueryBuilders.termQuery(ProductSearchEsPO.Fields.categoryCode3, productSearchEsDTO.getCategoryCode3()));
        }
        if(Objects.nonNull(productSearchEsDTO.getMinSalePrice()) && Objects.nonNull(productSearchEsDTO.getMaxSalePrice())){
            queryBuilder.filter(QueryBuilders.rangeQuery(ProductSearchEsPO.Fields.salePrice)
                    .lte(productSearchEsDTO.getMaxSalePrice())
                    .gte(productSearchEsDTO.getMinSalePrice()));
        }

        if(StringUtils.isNotBlank(productSearchEsDTO.getKeyword())){
            queryBuilder.filter(QueryBuilders.multiMatchQuery(productSearchEsDTO.getKeyword(),
                    ProductSearchEsPO.Fields.productName,ProductSearchEsPO.Fields.brandName));
        }

        return queryBuilder;
    }
}
