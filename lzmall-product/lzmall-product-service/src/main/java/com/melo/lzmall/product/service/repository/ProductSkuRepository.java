package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.product.service.entity.base.ProductSkuBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductSkuPO;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import com.melo.lzmall.product.service.mapper.ProductSkuMapper;
import com.melo.lzmall.product.service.repository.converter.ProductSkuRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ProductSkuRepository {

    @Autowired
    private ProductSkuMapper productSkuMapper;

    @Autowired
    private ProductSkuRepositoryConverter converter;


    public List<ProductSkuBaseDTO> queryBySpuCodeList(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductSkuPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductSkuPO::getSpuCode,spuCodeList);
        List<ProductSkuPO> productSkuPOS = productSkuMapper.selectList(queryWrapper);
        return converter.convertToProductSkuBaseDTOList(productSkuPOS);
    }

    public List<ProductSkuBaseDTO> queryBySkuCodeList(List<String> skuCodeList) {
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductSkuPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductSkuPO::getSkuCode,skuCodeList);
        List<ProductSkuPO> productSkuPOS = productSkuMapper.selectList(queryWrapper);
        return converter.convertToProductSkuBaseDTOList(productSkuPOS);
    }
}

