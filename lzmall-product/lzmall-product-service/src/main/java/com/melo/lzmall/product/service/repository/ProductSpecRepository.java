package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.product.service.entity.base.ProductParamBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductSpecBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductParamPO;
import com.melo.lzmall.product.service.entity.po.ProductParamRefPO;
import com.melo.lzmall.product.service.entity.po.ProductSpecPO;
import com.melo.lzmall.product.service.entity.po.ProductSpecRefPO;
import com.melo.lzmall.product.service.mapper.ProductSpecMapper;
import com.melo.lzmall.product.service.mapper.ProductSpecRefMapper;
import com.melo.lzmall.product.service.repository.converter.ProductSpecRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ProductSpecRepository {

    @Autowired
    private ProductSpecMapper productSpecMapper;

    @Autowired
    private ProductSpecRefMapper productSpecRefMapper;

    @Autowired
    private ProductSpecRepositoryConverter converter;

    public List<ProductSpecBaseDTO> queryBySkuCodeList(List<String> skuCodeList) {
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        List<ProductSpecRefPO> productSpecRefPOList = queryProductSpecRefBySkuCodeList(skuCodeList);
        List<String> specCodeList = productSpecRefPOList.stream().map(ProductSpecRefPO::getSpecCode).distinct().collect(Collectors.toList());
        List<ProductSpecPO> productSpecPOList = queryBySpecCodeList(specCodeList);
        Map<String, ProductSpecPO> productSpecPOMap = productSpecPOList.stream().collect(Collectors.toMap(ProductSpecPO::getSpecCode, Function.identity(), (k1, k2) -> k1));

        return productSpecRefPOList.stream().map(ref->{
            ProductSpecPO productSpecPO = productSpecPOMap.get(ref.getSpecCode());
            if(Objects.isNull(productSpecPO)){
                return null;
            }
            ProductSpecBaseDTO baseDTO = converter.convertToProductSpecBaseDTO(productSpecPO);
            baseDTO.setSpuCode(ref.getSpuCode());
            baseDTO.setSkuCode(ref.getSkuCode());
            return baseDTO;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<ProductSpecPO> queryBySpecCodeList(List<String> specCodeList){
        if(CollectionUtils.isEmpty(specCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductSpecPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductSpecPO::getSpecCode,specCodeList);
        return productSpecMapper.selectList(queryWrapper);
    }

    private List<ProductSpecRefPO> queryProductSpecRefBySkuCodeList(List<String> skuCodeList) {
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductSpecRefPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductSpecRefPO::getSkuCode,skuCodeList);
        return productSpecRefMapper.selectList(queryWrapper);
    }
}

