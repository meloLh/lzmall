package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.datasource.utils.PageUtil;
import com.melo.lzmall.product.service.entity.base.ProductSpuBaseDTO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import com.melo.lzmall.product.service.mapper.ProductSpuMapper;
import com.melo.lzmall.product.service.repository.converter.ProductSpuRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ProductSpuRepository {

    @Autowired
    private ProductSpuMapper productSpuMapper;

    @Autowired
    private ProductSpuRepositoryConverter converter;

    public com.melo.lzmall.common.core.dto.Page<ProductSpuBaseDTO> querySearchPage(SearchIndexProductDTO searchIndexProductDTO) {
        IPage<ProductSpuPO> page = new Page<>(searchIndexProductDTO.getPageIndex(),searchIndexProductDTO.getPageSize());
        IPage<ProductSpuPO> iPage = productSpuMapper.selectPage(page, new QueryWrapper<>());
        com.melo.lzmall.common.core.dto.Page pageNodata = PageUtil.copyIPageNodata(iPage);
        List<ProductSpuBaseDTO> spuBaseDTOList = converter.convertToProductSpuBaseDTOList(iPage.getRecords());
        pageNodata.setList(spuBaseDTOList);
        return pageNodata;
    }

    public List<ProductSpuBaseDTO> queryBySpuCodeList(List<String> spuCodeList) {
        if(CollectionUtils.isEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductSpuPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductSpuPO::getSpuCode,spuCodeList);
        List<ProductSpuPO> productSpuPOList = productSpuMapper.selectList(queryWrapper);
        return converter.convertToProductSpuBaseDTOList(productSpuPOList);
    }

    public List<String> queryGroupBrand(List<String> spuCodeList) {
        if(CollectionUtils.isNotEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
       return productSpuMapper.queryGroupBrand(spuCodeList);
    }

    public List<String> queryGroupCategory(List<String> spuCodeList) {
        if(CollectionUtils.isNotEmpty(spuCodeList)){
            return Lists.newArrayList();
        }
        return productSpuMapper.queryGroupCategory(spuCodeList);
    }
}

