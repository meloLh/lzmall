package com.melo.lzmall.product.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.datasource.helper.MybatisBatchHelper;
import com.melo.lzmall.product.service.entity.base.ProductStockBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductStockRecordBaseDTO;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import com.melo.lzmall.product.service.entity.dto.OperateProductStockDTO;
import com.melo.lzmall.product.service.entity.po.ProductSkuPO;
import com.melo.lzmall.product.service.entity.po.ProductStockPO;
import com.melo.lzmall.product.service.entity.po.ProductStockRecordPO;
import com.melo.lzmall.product.service.mapper.ProductStockMapper;
import com.melo.lzmall.product.service.mapper.ProductStockRecordMapper;
import com.melo.lzmall.product.service.repository.converter.ProductStockRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ProductStockRepository {

    @Autowired
    private ProductStockMapper productStockMapper;

    @Autowired
    private ProductStockRecordMapper productStockRecordMapper;

    @Autowired
    private MybatisBatchHelper mybatisBatchHelper;

    @Autowired
    private ProductStockRepositoryConverter converter;

    public List<ProductStockBaseDTO> queryBySkuCodeList(List<String> skuCodeList) {
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductStockPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductStockPO::getSkuCode,skuCodeList);
        List<ProductStockPO> productStockPOS = productStockMapper.selectList(queryWrapper);
        return converter.convertToProductStockBaseDTOList(productStockPOS);
    }

    public void batchFreezingProductStock(List<OperateProductStockDTO> stockDTOList) {
        boolean allMatch = stockDTOList.stream().allMatch(stock -> StringUtils.isNotBlank(stock.getSkuCode()));
        AssertUtil.isTrue(allMatch,"参数异常");
        int rows = mybatisBatchHelper.batchSave(stockDTOList, ProductStockMapper.class, (record, mapper) -> {
            productStockMapper.batchFreezingProductStock(record);
        });
//        AssertUtil.isTrue(CollectionUtils.size(stockDTOList) == rows,"扣减库存失败");
    }

    public void batchInsert(List<ProductStockRecordBaseDTO> recordBaseDTOList) {
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(recordBaseDTOList),"参数异常");
        mybatisBatchHelper.batchSave(recordBaseDTOList, ProductStockMapper.class, (record, mapper) -> {
            ProductStockRecordPO stockRecordPO = converter.convertToProductStockRecordPO(record);
            productStockRecordMapper.insert(stockRecordPO);
        });
    }

    public List<ProductStockRecordBaseDTO> queryRecordByBizCode(String bizCode) {
        if(StringUtils.isBlank(bizCode)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductStockRecordPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ProductStockRecordPO::getBizCode,bizCode);
        List<ProductStockRecordPO> productStockRecordPOS = productStockRecordMapper.selectList(queryWrapper);
        return converter.convertToProductStockRecordBaseDTO(productStockRecordPOS);
    }

    public List<ProductStockRecordBaseDTO> queryRecordByBizCodeList(List<String> orderCodeList) {
        if(CollectionUtils.isEmpty(orderCodeList)){
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<ProductStockRecordPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ProductStockRecordPO::getBizCode,orderCodeList);
        List<ProductStockRecordPO> productStockRecordPOS = productStockRecordMapper.selectList(queryWrapper);
        return converter.convertToProductStockRecordBaseDTO(productStockRecordPOS);
    }
}

