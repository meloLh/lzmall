package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductCategoryBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductCategoryPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  CategoryRepositoryConverter {

    List<ProductCategoryBaseDTO> convertToCategoryBaseDTOList(List<ProductCategoryPO> productCategoryPOS);
}

