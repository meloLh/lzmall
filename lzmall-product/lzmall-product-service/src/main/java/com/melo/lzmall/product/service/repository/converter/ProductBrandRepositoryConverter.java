package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductBrandBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductBrandPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface ProductBrandRepositoryConverter {

    List<ProductBrandBaseDTO> convertToProductBrandBaseDTOList(List<ProductBrandPO> productBrandPOS);
}

