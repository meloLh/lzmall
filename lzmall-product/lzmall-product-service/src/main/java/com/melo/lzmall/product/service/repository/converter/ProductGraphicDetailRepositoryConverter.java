package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductGraphicDetailBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductGraphicDetailPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  ProductGraphicDetailRepositoryConverter {

    List<ProductGraphicDetailBaseDTO> convertToProductGraphicDetailBaseDTOList(List<ProductGraphicDetailPO> productGraphicDetailPOS);
}

