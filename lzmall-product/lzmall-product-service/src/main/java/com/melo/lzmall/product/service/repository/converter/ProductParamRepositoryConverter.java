package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductParamBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductParamPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  ProductParamRepositoryConverter {

    ProductParamBaseDTO convertToProductParamBaseDTO(ProductParamPO productParamPO);

    List<ProductParamBaseDTO> convertToProductParamBaseDTOList(List<ProductParamPO> productParamPOS);
}

