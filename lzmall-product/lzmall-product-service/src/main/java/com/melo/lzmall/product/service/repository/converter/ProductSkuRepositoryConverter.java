package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductSkuBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductSkuPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  ProductSkuRepositoryConverter {

    List<ProductSkuBaseDTO> convertToProductSkuBaseDTOList(List<ProductSkuPO> productSkuPOS);
}

