package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductSpecBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductSpecPO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface  ProductSpecRepositoryConverter {

    ProductSpecBaseDTO convertToProductSpecBaseDTO(ProductSpecPO productSpecPO);
}

