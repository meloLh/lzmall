package com.melo.lzmall.product.service.repository.converter;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.product.service.entity.base.ProductSpuBaseDTO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import com.melo.lzmall.product.service.entity.es.po.ProductSearchEsPO;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  ProductSpuRepositoryConverter {

    Page<ProductSpuBaseDTO> convertToProductSpuBaseDTOPage(IPage<ProductSpuPO> iPage);

    List<ProductSpuBaseDTO> convertToProductSpuBaseDTOList(List<ProductSpuPO> records);

    ProductSearchEsPO convertToProductSearchEsPO(ProductSpuBO spu);
}

