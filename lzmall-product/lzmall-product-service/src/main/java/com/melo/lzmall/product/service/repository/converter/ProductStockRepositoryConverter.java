package com.melo.lzmall.product.service.repository.converter;

import com.melo.lzmall.product.service.entity.base.ProductStockBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductStockRecordBaseDTO;
import com.melo.lzmall.product.service.entity.po.ProductStockPO;
import com.melo.lzmall.product.service.entity.po.ProductStockRecordPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface  ProductStockRepositoryConverter {

    List<ProductStockBaseDTO> convertToProductStockBaseDTOList(List<ProductStockPO> productStockPOS);

    ProductStockRecordPO convertToProductStockRecordPO(ProductStockRecordBaseDTO record);

    List<ProductStockRecordBaseDTO> convertToProductStockRecordBaseDTO(List<ProductStockRecordPO> productStockRecordPOS);
}

