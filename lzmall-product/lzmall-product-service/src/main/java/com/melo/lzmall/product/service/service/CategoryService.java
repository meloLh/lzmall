package com.melo.lzmall.product.service.service;


import com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO;

import java.util.List;

public interface CategoryService {
    /**
     * @Description 查询分类树
     * @author liuhu
     * @date 2024/4/25 10:33
     * @return java.util.List<com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO>
     */
    List<CategoryTreeBaseDTO> queryAllCategory();
}

