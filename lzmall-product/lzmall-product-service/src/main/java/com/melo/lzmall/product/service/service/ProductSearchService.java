package com.melo.lzmall.product.service.service;

import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.product.api.response.search.SearchIndexProductResponse;
import com.melo.lzmall.product.api.response.search.SearchProductRelatedContentResponse;
import com.melo.lzmall.product.api.response.search.SearchRecommendProductListResponse;
import com.melo.lzmall.product.service.entity.dto.SearchRecommendProductListDTO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;

import java.util.List;

public interface ProductSearchService {
    /**
     * @Description 商品搜索
     * @author liuhu
     * @param searchIndexProductDTO
     * @date 2024/4/25 20:18
     * @return com.melo.lzmall.common.core.dto.Page<com.melo.lzmall.product.api.response.search.SearchIndexProductResponse>
     */
    Page<SearchIndexProductResponse> searchProduct(SearchIndexProductDTO searchIndexProductDTO);
    /**
     * @Description 推荐商品
     * @author liuhu
     * @param productListBO
     * @date 2024/4/26 14:43
     * @return java.util.List<com.melo.lzmall.product.api.response.search.SearchRecommendProductListResponse>
     */
    Page<SearchRecommendProductListResponse> searchRecommendProductList(SearchRecommendProductListDTO productListBO);

    /**
     * @Description 获取商品关联内容
     * @author liuhu
     * @param searchIndexProductDTO
     * @date 2024/6/19 16:24
     * @return void
     */
    SearchProductRelatedContentResponse searchProductRelatedContend(SearchIndexProductDTO searchIndexProductDTO);
}
