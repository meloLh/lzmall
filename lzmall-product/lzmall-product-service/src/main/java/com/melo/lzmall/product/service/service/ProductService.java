package com.melo.lzmall.product.service.service;

import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.product.api.response.product.ProductSpuDetailResponse;

import java.util.List;

public interface ProductService {

    /**
     * @Description 查询商品spu详情
     * @author liuhu
     * @param spuCode
     * @param skuCode
     * @date 2024/4/25 21:00
     * @return com.melo.lzmall.product.api.response.product.ProductSpuDetailResponse
     */
    ProductSpuDetailResponse getProductSpuDetail(String spuCode, String skuCode);

    List<ProductSkuDetailResponse> queryBySkuCodeList(List<String> skuCodeList);
}
