package com.melo.lzmall.product.service.service;


import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;

import java.util.List;

public interface ProductStockService {
    /**
     * @Description 下单冻结库存
     * @author liuhu
     * @param productStockBOList
     * @date 2024/6/3 14:05
     * @return void
     */
    void batchFreezingProductStock(List<OperateProductStockBO> productStockBOList );
}

