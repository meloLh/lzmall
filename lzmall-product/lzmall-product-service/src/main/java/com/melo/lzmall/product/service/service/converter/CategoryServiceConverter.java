package com.melo.lzmall.product.service.service.converter;

import com.melo.lzmall.product.service.entity.base.ProductCategoryBaseDTO;
import com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CategoryServiceConverter {

    CategoryTreeBaseDTO convertToCategoryTreeBaseDTO(ProductCategoryBaseDTO category);

    List<CategoryTreeBaseDTO> convertToCategoryTreeBaseDTOList(List<CategoryTreeBaseDTO> childrenCategoryList);
}

