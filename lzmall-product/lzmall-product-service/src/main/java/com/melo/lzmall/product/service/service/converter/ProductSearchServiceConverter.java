package com.melo.lzmall.product.service.service.converter;

import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.product.api.response.search.SearchIndexProductResponse;
import com.melo.lzmall.product.api.response.search.SearchRecommendProductListResponse;
import com.melo.lzmall.product.service.entity.base.ProductSpuBaseDTO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;
import com.melo.lzmall.product.service.entity.es.po.ProductSearchEsPO;
import com.melo.lzmall.product.service.repository.dto.ProductSearchEsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring")
public interface ProductSearchServiceConverter {
    
    SearchIndexProductResponse convertToSearchIndexProductResponse(ProductSpuBO productSpuBO);

    @Mapping(source = "availableStock",target = "stockNum")
    ProductSkuDetailResponse convertToProductSkuDetailResponse(ProductSkuBO productSkuBO);

    Page<SearchRecommendProductListResponse> convertToSearchRecommendProductListResponsePage(Page<ProductSpuBaseDTO> spuBaseDTOPage);

    SearchRecommendProductListResponse convertToSearchRecommendProductListResponse(ProductSkuBO productSkuBO);

    List<ProductSkuDetailResponse> convertToProductSkuDetailResponseList(List<ProductSkuBO> productSkuBOS);

    ProductSearchEsDTO convertToProductSearchEsDTO(SearchIndexProductDTO searchIndexProductDTO);

    Page<SearchIndexProductResponse> convertToSearchIndexProductResponsePage(Page<ProductSearchEsPO> productSearchEsPage);
}

