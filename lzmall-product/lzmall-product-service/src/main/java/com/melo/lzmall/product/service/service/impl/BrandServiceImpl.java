package com.melo.lzmall.product.service.service.impl;

import com.melo.lzmall.product.service.repository.ProductBrandRepository;
import com.melo.lzmall.product.service.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class BrandServiceImpl  implements BrandService {

    @Autowired
    private ProductBrandRepository productBrandRepository;
}