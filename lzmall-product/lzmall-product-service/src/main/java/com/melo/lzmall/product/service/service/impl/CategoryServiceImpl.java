package com.melo.lzmall.product.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import com.melo.lzmall.product.service.constants.ProductCacheConstant;
import com.melo.lzmall.product.service.entity.base.ProductCategoryBaseDTO;
import com.melo.lzmall.product.service.entity.dto.CategoryTreeBaseDTO;
import com.melo.lzmall.product.service.repository.ProductCategoryRepository;
import com.melo.lzmall.product.service.service.CategoryService;
import com.melo.lzmall.product.service.service.converter.CategoryServiceConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Slf4j
@Service
public class CategoryServiceImpl  implements CategoryService {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private CategoryServiceConverter converter;

    @Autowired
    private RedisHelper redisHelper;

    @Override
    public List<CategoryTreeBaseDTO> queryAllCategory() {
        String key = ProductCacheConstant.BUYER_INDEX_CATEGORY_CACHE_KEY;
        String cacheCategory = redisHelper.get(key);
        if(StringUtils.isNotBlank(cacheCategory)){
            return JSON.parseArray(cacheCategory,CategoryTreeBaseDTO.class);
        }
        List<CategoryTreeBaseDTO> categoryTreeBaseDTOList = queryAllCategoryTree();
        redisHelper.set(key,JSON.toJSONString(categoryTreeBaseDTOList),1L, TimeUnit.HOURS);
        return categoryTreeBaseDTOList;
    }

    private List<CategoryTreeBaseDTO> queryAllCategoryTree() {
        List<ProductCategoryBaseDTO> allCategoryList = productCategoryRepository.queryAllCategory();
        List<ProductCategoryBaseDTO> parentCategoryList = allCategoryList.stream().filter(dto -> Objects.equals(dto.getParentCategoryCode(), "0")).collect(Collectors.toList());

        return parentCategoryList.stream().map(parentCategory->{
             CategoryTreeBaseDTO categoryTreeBaseDTO = converter.convertToCategoryTreeBaseDTO(parentCategory);
             List<CategoryTreeBaseDTO> categoryTreeBaseDTOList = buildChildrenCategoryList(parentCategory, allCategoryList);
             categoryTreeBaseDTO.setChildrenList(categoryTreeBaseDTOList);
             return categoryTreeBaseDTO;
         }).collect(Collectors.toList());
    }

    private List<CategoryTreeBaseDTO> buildChildrenCategoryList(ProductCategoryBaseDTO parentCategory,
                                                                List<ProductCategoryBaseDTO> allCategoryList) {
       return allCategoryList.stream().filter(category->Objects.equals(category.getParentCategoryCode(),parentCategory.getCategoryCode()))
                .map(category->{
                    CategoryTreeBaseDTO categoryTreeBaseDTO = converter.convertToCategoryTreeBaseDTO(category);
                    List<CategoryTreeBaseDTO> childrenCategoryList = buildChildrenCategoryList(category, allCategoryList);
                    categoryTreeBaseDTO.setChildrenList(childrenCategoryList);
                    return categoryTreeBaseDTO;
                }).collect(Collectors.toList());
    }
}