package com.melo.lzmall.product.service.service.impl;

import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.product.api.response.search.*;
import com.melo.lzmall.product.service.core.ProductQueryCore;
import com.melo.lzmall.product.service.entity.base.ProductBrandBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductCategoryBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductParamBaseDTO;
import com.melo.lzmall.product.service.entity.base.ProductSpuBaseDTO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import com.melo.lzmall.product.service.entity.dto.SearchRecommendProductListDTO;
import com.melo.lzmall.product.service.entity.dto.SearchIndexProductDTO;
import com.melo.lzmall.product.service.entity.es.po.ProductSearchEsPO;
import com.melo.lzmall.product.service.repository.*;
import com.melo.lzmall.product.service.repository.dto.ProductSearchEsDTO;
import com.melo.lzmall.product.service.service.ProductSearchService;
import com.melo.lzmall.product.service.service.converter.ProductSearchServiceConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductSearchServiceImpl implements ProductSearchService {

    @Autowired
    private ProductSpuRepository productSpuRepository;

    @Autowired
    private ProductQueryCore productQueryCore;

    @Autowired
    private ProductSearchEsRepository productSearchEsRepository;

    @Autowired
    private ProductParamRepository productParamRepository;

    @Autowired
    private ProductBrandRepository productBrandRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductSearchServiceConverter converter;

    @Override
    public Page<SearchIndexProductResponse> searchProduct(SearchIndexProductDTO searchIndexProductDTO) {
        //1.查询ES获取分页 spu编号
        ProductSearchEsDTO productSearchEsDTO = converter.convertToProductSearchEsDTO(searchIndexProductDTO);
        Page<ProductSearchEsPO> productSearchEsPage = productSearchEsRepository.searchProductPage(productSearchEsDTO);
        List<String> spuCodeList = productSearchEsPage.getList().stream().map(ProductSearchEsPO::getSpuCode).collect(Collectors.toList());
        //2.查询spu数据 组装返回
        List<ProductSpuBO> productSpuBOList = productQueryCore.queryProductSpuList(spuCodeList);
        Page<SearchIndexProductResponse> page =  converter.convertToSearchIndexProductResponsePage(productSearchEsPage);
        List<SearchIndexProductResponse> responseList = buildSearchIndexProductResponse(productSpuBOList);
        page.setList(responseList);
        return page;
    }

    @Override
    public Page<SearchRecommendProductListResponse> searchRecommendProductList(SearchRecommendProductListDTO productListBO) {
        SearchIndexProductDTO dto = new SearchIndexProductDTO();
        dto.setPageIndex(productListBO.getPageIndex());
        dto.setPageSize(productListBO.getPageSize());
        Page<ProductSpuBaseDTO> spuBaseDTOPage = productSpuRepository.querySearchPage(dto);

        List<String> spuCodeList = spuBaseDTOPage.getList().stream().map(ProductSpuBaseDTO::getSpuCode).collect(Collectors.toList());
        List<ProductSpuBO> productSpuBOList = productQueryCore.queryProductSpuList(spuCodeList);
        Page<SearchRecommendProductListResponse> page =  converter.convertToSearchRecommendProductListResponsePage(spuBaseDTOPage);
        List<SearchRecommendProductListResponse> responseList = buildSearchRecommendProductListResponse(productSpuBOList);
        page.setList(responseList);
        return page;
    }

    @Override
    public SearchProductRelatedContentResponse searchProductRelatedContend(SearchIndexProductDTO searchIndexProductDTO) {
        //1.查询ES获取分页 spu编号
        ProductSearchEsDTO productSearchEsDTO = converter.convertToProductSearchEsDTO(searchIndexProductDTO);
        productSearchEsDTO.setPageSize(1000);
        Page<ProductSearchEsPO> productSearchEsPage = productSearchEsRepository.searchProductPage(productSearchEsDTO);
        List<String> spuCodeList = productSearchEsPage.getList().stream().map(ProductSearchEsPO::getSpuCode).collect(Collectors.toList());

        List<String> brandCodeList = productSpuRepository.queryGroupBrand(spuCodeList);
        List<String> categoryCodeList = productSpuRepository.queryGroupCategory(spuCodeList);
        List<String> groupParamCodeList = productParamRepository.queryGroupParam(spuCodeList);

        SearchProductRelatedContentResponse response = new SearchProductRelatedContentResponse();
        List<SearchProductRelatedBrandResponse> brands = getSearchProductRelatedBrandResponse(brandCodeList);
        List<SearchProductRelatedCategoryResponse> categories = getSearchProductRelatedCategoryResponse(categoryCodeList);
        List<SearchProductRelatedParamsResponse>  paramOptions = getSearchProductRelatedParamsResponse(groupParamCodeList);
        response.setBrands(brands);
        response.setCategories(categories);
        response.setParamOptions(paramOptions);
        return response;
    }

    private List<SearchProductRelatedParamsResponse> getSearchProductRelatedParamsResponse(List<String> groupParamCodeList) {
        List<SearchProductRelatedParamsResponse> list = new ArrayList<>();

        List<ProductParamBaseDTO> productParamBaseDTOS = productParamRepository.queryBaseDTOByParamCodeList(groupParamCodeList);
        Map<String, List<ProductParamBaseDTO>> map = productParamBaseDTOS.stream().collect(Collectors.groupingBy(ProductParamBaseDTO::getParamGroupName));
        for (Map.Entry<String, List<ProductParamBaseDTO>> entry : map.entrySet()) {
            SearchProductRelatedParamsResponse response = new SearchProductRelatedParamsResponse();
            response.setKey(entry.getKey());
            List<String> paramValues = entry.getValue().stream().map(ProductParamBaseDTO::getParamName).collect(Collectors.toList());
            response.setValues(paramValues);
            list.add(response);
        }
        return list
    }

    private List<SearchProductRelatedCategoryResponse> getSearchProductRelatedCategoryResponse(List<String> categoryCodeList) {
        List<ProductCategoryBaseDTO> categoryBaseDTOS = productCategoryRepository.queryByCategoryCodeList(categoryCodeList);
        return categoryBaseDTOS.stream().map(category->{
            SearchProductRelatedCategoryResponse response = new SearchProductRelatedCategoryResponse();
            response.setName(category.getCategoryName());
            response.setValue(category.getCategoryCode());
            response.setUrl(category.getImage());
            return response;
        }).collect(Collectors.toList());
    }

    private List<SearchProductRelatedBrandResponse> getSearchProductRelatedBrandResponse(List<String> brandCodeList) {
        List<ProductBrandBaseDTO> productBrandBaseDTOS = productBrandRepository.queryByBrandCode(brandCodeList);
        return productBrandBaseDTOS.stream().map(brand->{
            SearchProductRelatedBrandResponse response = new SearchProductRelatedBrandResponse();
            response.setName(brand.getName());
            response.setValue(brand.getBrandCode());
            response.setUrl(brand.getLogo());
            return response;
        }).collect(Collectors.toList());
    }

    private List<SearchRecommendProductListResponse> buildSearchRecommendProductListResponse(List<ProductSpuBO> productSpuBOList) {
        return productSpuBOList.stream().map(spu->{
            ProductSkuBO productSkuBO = spu.getSkuList().get(0);
            SearchRecommendProductListResponse recommendProductListResponse = converter.convertToSearchRecommendProductListResponse(productSkuBO);
            return recommendProductListResponse;
        }).collect(Collectors.toList());
    }


    private List<SearchIndexProductResponse> buildSearchIndexProductResponse(List<ProductSpuBO> productSpuBOList) {
        return productSpuBOList.stream().map(productSpuBO -> {
            SearchIndexProductResponse response =  converter.convertToSearchIndexProductResponse(productSpuBO);
            ProductSkuBO productSkuBO = productSpuBO.getSkuList().get(0);
            response.setSkuCode(productSkuBO.getSkuCode());
            response.setThumbnail(productSkuBO.getThumbnail());
            response.setSalePrice(productSkuBO.getSalePrice());
            response.setCostPrice(productSkuBO.getCostPrice());
            response.setMarketPrice(productSkuBO.getMarketPrice());
            return  response;
        }).collect(Collectors.toList());
    }
}
