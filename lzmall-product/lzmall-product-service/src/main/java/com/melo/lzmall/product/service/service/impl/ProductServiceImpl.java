package com.melo.lzmall.product.service.service.impl;

import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.product.api.response.product.*;
import com.melo.lzmall.product.service.core.ProductQueryCore;
import com.melo.lzmall.product.service.entity.bo.product.ProductParamBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpecBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import com.melo.lzmall.product.service.repository.ProductSpuRepository;
import com.melo.lzmall.product.service.service.ProductService;
import com.melo.lzmall.product.service.service.converter.ProductSearchServiceConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductSpuRepository productSpuRepository;

    @Autowired
    private ProductQueryCore productQueryCore;

    @Autowired
    private ProductSearchServiceConverter converter;

    @Override
    public ProductSpuDetailResponse getProductSpuDetail(String spuCode, String skuCode) {
        List<ProductSpuBO> productSpuBOList = productQueryCore.queryProductSpuList(Lists.newArrayList(spuCode));
        ProductSpuBO productSpuBO = productSpuBOList.get(0);
        AssertUtil.isTrue(Objects.nonNull(productSpuBO),"商品详情不存在");
        List<ProductSkuBO> defaultSkuList = productSpuBO.getSkuList().stream()
                .filter(v -> Objects.equals(skuCode, v.getSkuCode())).collect(Collectors.toList());
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(defaultSkuList),"商品详情不存在");

        ProductSpuDetailResponse response = new ProductSpuDetailResponse();
        ProductSkuDetailResponse skuDetailResponse = converter.convertToProductSkuDetailResponse(defaultSkuList.get(0));
        response.setSku(skuDetailResponse);

        response.setSpecs(buildProductSpecSpuResponse(productSpuBO));
        response.setParamList(buildProductParamSpuResponse(productSpuBO));

        List<String> goodsGalleryList = productSpuBO.getSkuList().stream().map(ProductSkuBO::getThumbnail).collect(Collectors.toList());
        response.setGoodsGalleryList(goodsGalleryList);
        return response;
    }

    @Override
    public List<ProductSkuDetailResponse> queryBySkuCodeList(List<String> skuCodeList) {
        List<ProductSkuBO> productSkuBOS = productQueryCore.queryProductSkuList(skuCodeList);
        return converter.convertToProductSkuDetailResponseList(productSkuBOS);
    }

    private List<ProductParamSpuResponse> buildProductParamSpuResponse(ProductSpuBO productSpuBO) {
        if(CollectionUtils.isEmpty(productSpuBO.getParamList())){
            return Lists.newArrayList();
        }

        Map<String, List<ProductParamBO>> map = productSpuBO.getParamList().stream().collect(Collectors.groupingBy(ProductParamBO::getParamGroupName));

        List<ProductParamSpuResponse> list = new ArrayList<>();
        for (Map.Entry<String, List<ProductParamBO>> entry : map.entrySet()) {
            ProductParamSpuResponse response = new ProductParamSpuResponse();
            response.setParamCode(entry.getKey());
            response.setParamGroupName(entry.getValue().get(0).getParamGroupName());

            List<ProductParamResponse> paramResponses = entry.getValue().stream().map(bo -> {
                ProductParamResponse paramResponse = new ProductParamResponse();
                paramResponse.setParamName(bo.getParamName());
                paramResponse.setParamValue(bo.getParamValue());
                return paramResponse;
            }).collect(Collectors.toList());

            response.setParamList(paramResponses);

            list.add(response);
        }
        return list;
    }

    private List<ProductSpecSpuResponse> buildProductSpecSpuResponse(ProductSpuBO productSpuBO) {

        Map<String, ProductSkuBO> skuBOMap = productSpuBO.getSkuList().stream().collect(Collectors.toMap(ProductSkuBO::getSkuCode, Function.identity(), (k1, k2) -> k1));

        Map<String, List<ProductSpecBO>> map =
                productSpuBO.getSpecList().stream().collect(Collectors.groupingBy(ProductSpecBO::getSkuCode));


        List<ProductSpecSpuResponse> list = new ArrayList<>();
        for (Map.Entry<String, List<ProductSpecBO>> entry : map.entrySet()) {
            ProductSpecSpuResponse response = new ProductSpecSpuResponse();
            response.setSkuCode(entry.getKey());

            ProductSkuBO currentSku = skuBOMap.get(entry.getKey());
            AssertUtil.isTrue(Objects.nonNull(currentSku),"商品详情不存在");
            response.setStockNum(currentSku.getAvailableStock());

            List<ProductSpecResponse> productSpecResponses = entry.getValue().stream().map(bo -> {
                ProductSpecResponse specResponse = new ProductSpecResponse();
                specResponse.setSpecCode(bo.getSpecCode());
                specResponse.setSpecName(bo.getSpecName());
                specResponse.setSpecValue(bo.getSpecValue());
                return specResponse;
            }).collect(Collectors.toList());
            response.setSpecValues(productSpecResponses);

            list.add(response);
        }
        return list;
    }
}
