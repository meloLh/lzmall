package com.melo.lzmall.product.service.service.impl;

import com.melo.lzmall.product.service.repository.ProductSkuRepository;
import com.melo.lzmall.product.service.service.ProductSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Slf4j
@Service
public class ProductSkuServiceImpl  implements ProductSkuService {

    @Autowired
    private ProductSkuRepository productSkuRepository;
}