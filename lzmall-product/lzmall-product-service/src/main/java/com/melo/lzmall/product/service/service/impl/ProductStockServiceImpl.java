package com.melo.lzmall.product.service.service.impl;

import com.melo.lzmall.product.service.core.ProductStockCore;
import com.melo.lzmall.product.service.entity.bo.stock.OperateProductStockBO;
import com.melo.lzmall.product.service.service.ProductStockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class ProductStockServiceImpl  implements ProductStockService {

    @Autowired
    private ProductStockCore productStockCore;

    @Override
    public void batchFreezingProductStock(List<OperateProductStockBO> productStockBOList) {
        productStockCore.batchFreezingProductStock(productStockBOList);
    }
}