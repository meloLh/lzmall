package com.melo.lzmall.product.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.category.QueryCategoryListTocResponse;
import com.melo.lzmall.product.service.controller.toc.CategoryTocQueryController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
@Slf4j
public class CategoryTocQueryControllerTest extends ProductBaseTest{

    @Autowired
    private CategoryTocQueryController controller;

    @Test
    public void queryAllCategory(){
        Result<List<QueryCategoryListTocResponse>> result = controller.queryAllCategory();
        log.info(JSON.toJSONString(result));
    }
}
