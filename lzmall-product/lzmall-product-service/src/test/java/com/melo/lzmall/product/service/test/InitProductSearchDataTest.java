package com.melo.lzmall.product.service.test;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.melo.lzmall.product.service.core.ProductQueryCore;
import com.melo.lzmall.product.service.entity.bo.product.ProductSkuBO;
import com.melo.lzmall.product.service.entity.bo.product.ProductSpuBO;
import com.melo.lzmall.product.service.entity.es.po.ProductSearchEsPO;
import com.melo.lzmall.product.service.entity.po.ProductSpuPO;
import com.melo.lzmall.product.service.mapper.ProductSpuMapper;
import com.melo.lzmall.product.service.repository.ProductSearchEsRepository;
import com.melo.lzmall.product.service.repository.converter.ProductSpuRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class InitProductSearchDataTest extends ProductBaseTest{

   @Autowired
   private ProductSpuMapper productSpuMapper;

    @Autowired
    private ProductQueryCore productQueryCore;

    @Autowired
    private ProductSearchEsRepository productSearchEsRepository;

    @Autowired
    private ProductSpuRepositoryConverter converter;

    @Test
    public void initProductEsData(){
        Integer pageIndex = 1;
        List<ProductSpuPO> productSpuPOList;
        do {
            LambdaQueryWrapper<ProductSpuPO> queryWrapper = new LambdaQueryWrapper<>();
            Page<ProductSpuPO> page = new Page<>(pageIndex, 50);
            IPage<ProductSpuPO> productSpuPage = productSpuMapper.selectPage(page, queryWrapper);
            productSpuPOList = productSpuPage.getRecords();
            if(CollectionUtils.isNotEmpty(productSpuPOList)){
                List<String> spuCodeList = productSpuPOList.stream().map(ProductSpuPO::getSpuCode).collect(Collectors.toList());
                List<ProductSpuBO> productSpuBOS = productQueryCore.queryProductSpuList(spuCodeList);
                List<ProductSearchEsPO> productSearchEsPOList = buildProductSearchEsPOList(productSpuBOS);
                for (ProductSearchEsPO productSearchEsPO : productSearchEsPOList) {
                    productSearchEsRepository.upsert(productSearchEsPO);
                }
            }
            pageIndex++;
        }while (CollectionUtils.isNotEmpty(productSpuPOList));
    }

    private List<ProductSearchEsPO> buildProductSearchEsPOList(List<ProductSpuBO> productSpuBOS) {
       return productSpuBOS.stream().map(spu->{
            ProductSearchEsPO productSearchEsPO =  converter.convertToProductSearchEsPO(spu);
            ProductSkuBO defaultSku = spu.getSkuList().get(0);

            productSearchEsPO.setSkuCode(defaultSku.getSkuCode());
            productSearchEsPO.setSalePrice(defaultSku.getSalePrice());
            productSearchEsPO.setCostPrice(defaultSku.getCostPrice());
            productSearchEsPO.setMarketPrice(defaultSku.getMarketPrice());
            productSearchEsPO.setAvailableStock(defaultSku.getAvailableStock());
            return productSearchEsPO;
        }).collect(Collectors.toList());
    }


    @Test
    public void initProductEsIndex(){
        productSearchEsRepository.createIndex();
    }
}
