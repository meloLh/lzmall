package com.melo.lzmall.product.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Page;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.request.product.SearchIndexProductRequest;
import com.melo.lzmall.product.api.response.search.SearchIndexProductResponse;
import com.melo.lzmall.product.api.response.search.SearchProductRelatedContentResponse;
import com.melo.lzmall.product.service.controller.toc.ProductSearchTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ProductSearchTocControllerTest extends ProductBaseTest{

    @Autowired
    private ProductSearchTocController controller;

    @Test
    public void queryAllCategory(){
        SearchIndexProductRequest request = new SearchIndexProductRequest();
        request.setPageIndex(1);
        request.setPageSize(10);
        Result<Page<SearchIndexProductResponse>> result = controller.searchProduct(request);
        log.info(JSON.toJSONString(result));
    }

    @Test
    public void searchProductRelatedContend(){

        Result<SearchProductRelatedContentResponse> result = controller.searchProductRelatedContend(null);
        log.info(JSON.toJSONString(result));
    }
}
