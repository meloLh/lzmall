package com.melo.lzmall.product.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.product.api.response.product.ProductSpuDetailResponse;
import com.melo.lzmall.product.service.controller.toc.ProductTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ProductTocControllerTest extends ProductBaseTest{

    @Autowired
    private ProductTocController controller;

    @Test
    public void queryAllCategory(){
        Result<ProductSpuDetailResponse> result = controller.getProductSpuDetail("PU123", "KU123");
        log.info(JSON.toJSONString(result));
    }
}
