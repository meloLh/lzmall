package com.melo.lzmall.trade.api.request.cart;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
@ApiModel("新增购物车")
public class AddCartRequest implements Serializable {

    @NotBlank(message = "商品编码不能为空")
    private String skuCode;

    @Min(message = "架构数量不能小于0",value = 1)
    @NotNull
    private Integer buyCount;
}
