package com.melo.lzmall.trade.api.request.cart;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class DeleteCartSkuListRequest implements Serializable {

    @NotEmpty
    private List<String> skuCodeList;
}
