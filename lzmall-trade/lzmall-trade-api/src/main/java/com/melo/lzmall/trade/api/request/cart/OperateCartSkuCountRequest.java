package com.melo.lzmall.trade.api.request.cart;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class OperateCartSkuCountRequest implements Serializable {

    @NotBlank
    private String skuCode;

    /**操作后数量*/
    @Min(1)
    @NotNull
    private Integer buyCount;
}
