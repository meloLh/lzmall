package com.melo.lzmall.trade.api.request.coupon;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
public class ChooseCouponRequest implements Serializable {

    @NotBlank
    private String instanceCode;

    @NotBlank
    private String activityCode;

    @NotNull
    private Boolean checked;
}
