package com.melo.lzmall.trade.api.request.order;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SubmitOrderCartPriceRequest implements Serializable {

    /**
     * 商品总额
     */
    private BigDecimal saleAmount;

    /**
     * 可使用优惠券抵扣金额
     */
    private BigDecimal couponAmount;

    /**
     * 优惠活动价格
     */
    private BigDecimal discountAmount;

    /**
     * 应付总额
     */
    private BigDecimal payableAmount;

}
