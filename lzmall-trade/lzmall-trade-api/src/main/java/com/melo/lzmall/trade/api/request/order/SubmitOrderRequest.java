package com.melo.lzmall.trade.api.request.order;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SubmitOrderRequest implements Serializable {

    private String orderRemark;

    private String instanceCode;

    /**
     * 商品信息
     */
    private List<SubmitOrderCartRequest> skuList;


    /**
     * 价格信息
     */
    private SubmitOrderCartPriceRequest cartPrice;
}
