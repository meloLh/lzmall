package com.melo.lzmall.trade.api.response;

import lombok.Data;

import java.io.Serializable;
@Data
public class MemberAddressResponse implements Serializable {

    private String id;

    private String consigneeAddressPath;

    private Boolean isDefault;

    private String mobile;

    private String name;


    private String detail;

    private String consigneeAddressIdPath;


}
