package com.melo.lzmall.trade.api.response.cart;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
public class CartSkuCouponResponseResponse implements Serializable {

    /**
     * 活动编号
     */
    private String activityCode;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 优惠券类型  满减券  无门槛
     */
    private String couponType;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;
}
