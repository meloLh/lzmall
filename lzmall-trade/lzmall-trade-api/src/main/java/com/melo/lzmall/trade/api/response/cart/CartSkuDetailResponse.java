package com.melo.lzmall.trade.api.response.cart;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class CartSkuDetailResponse implements Serializable {

    /**
     * 是否选中，要去结算 0:未选中 1:已选中，默认
     */
    @ApiModelProperty(value = "是否选中，要去结算")
    private Boolean checked;

    @ApiModelProperty(value = "是否失效 ")
    private Boolean invalid;

    /**sku基本信息*/
    private CartSkuInfoResponse sku;

    private Integer buyCount;

    private String skuCode;

    /**
     * sku维度的价格
     */
    private CartSkuPriceDetailResponse skuPrice;

    private Map<String,Object> notFilterPromotionMap = new HashMap<>();;

    private Map<String,Object> promotionMap = new HashMap<>();;
}
