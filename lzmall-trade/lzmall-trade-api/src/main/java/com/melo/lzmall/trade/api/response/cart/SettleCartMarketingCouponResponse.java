package com.melo.lzmall.trade.api.response.cart;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SettleCartMarketingCouponResponse implements Serializable {

    /**
     * 活动编号
     */
    private String activityCode;

    /**
     * 优惠券实例编号
     */
    private String instanceCode;

    /**
     * 优惠券来源 平台券  店铺券
     */
    private String couponSource;

    /**
     * 主体编号 店铺券  是店铺编号
     */
    private String subjectCode;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 优惠券类型  满减券  无门槛
     */
    private String couponType;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 优惠券生效开始时间
     */
    private Date instanceStartTime;

    /**
     * 优惠券生效结束时间
     */
    private Date instanceEndTime;

    /**
     * 方法类型 1.主动领取 2.订单满赠 3.新用户发放 4.指定用户发放
     */
    private String sendType;

    /**
     * 1.指定商品可用 2.指定分类可用 3.订单满减
     */
    private String useType;

    /**
     * 限领张数
     */
    private Integer userSendLimit;

    /**
     * 每次发几张优惠券
     */
    private Integer sendCount;

    /**
     * 优惠券面值
     */
    private BigDecimal couponAmount;

    /**
     * 优惠券使用门槛金额
     */
    private BigDecimal couponFullAmount;

    /**
     * 是否与其他活动互斥
     */
    private String mutexFlag;


    /**
     * 订单原价总额
     */
    private BigDecimal totalOriginAmount;

    /**
     * 订单优惠总额
     */
    private BigDecimal totalCouponPromotionAmount;
}
