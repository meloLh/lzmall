package com.melo.lzmall.trade.api.response.cart;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class SettleCartResponse implements Serializable {

    private String memberCode;

    private String memberName;

    /**
     * 选择优惠券
     */
    private String instanceCode;

    /**
     * 优惠券信息
     */
    private List<SettleCartMarketingCouponResponse> canUseCoupons;

    private List<SettleCartMarketingCouponResponse> cantUseCoupons;

    /**
     * 店铺维度商品信息
     */
    private List<SettleShopCartResponse> cartList;

    /**
     * 购物车价格
     */
    private CartPriceDetailResponse cartPrice;

    /**
     * 优惠券信息
     */
    private Map<String, Object> skuPromotionDetail = new HashMap<>();
}
