package com.melo.lzmall.trade.api.response.cart;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class SettleCartSkuDetailResponse implements Serializable {
    /**sku基本信息*/
    private CartSkuInfoResponse sku;

    private Integer buyCount;

    /**
     * sku维度的价格
     */
    private CartSkuPriceDetailResponse skuPrice;

    private Map<String,Object> notFilterPromotionMap = new HashMap<>();;

    private Map<String,Object> promotionMap = new HashMap<>();;
}
