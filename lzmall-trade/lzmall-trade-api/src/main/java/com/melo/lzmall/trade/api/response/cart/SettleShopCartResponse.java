package com.melo.lzmall.trade.api.response.cart;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("店铺维度商品数据")
public class SettleShopCartResponse implements Serializable {

    private String shopNo;

    private String shopName;

    /**
     * sku详情
     */
    private List<CartSkuDetailResponse> skuList;

}
