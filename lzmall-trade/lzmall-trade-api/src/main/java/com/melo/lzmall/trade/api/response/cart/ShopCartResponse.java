package com.melo.lzmall.trade.api.response.cart;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@ApiModel("店铺维度商品数据")
public class ShopCartResponse implements Serializable {

    private String shopNo;

    private String shopName;

    private Boolean checked = false;


    private List<CartSkuCouponResponseResponse> canReceiveCouponList;

    /**
     * sku详情
     */
    private List<CartSkuDetailResponse> skuList;

}
