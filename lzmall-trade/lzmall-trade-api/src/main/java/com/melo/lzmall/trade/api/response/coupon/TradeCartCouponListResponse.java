package com.melo.lzmall.trade.api.response.coupon;

import com.melo.lzmall.trade.api.response.cart.CartSkuCouponResponseResponse;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class TradeCartCouponListResponse implements Serializable {


    private List<CartSkuCouponResponseResponse> canReceiveCouponList;


    private List<CartSkuCouponResponseResponse> cantReceiveCouponList;
}
