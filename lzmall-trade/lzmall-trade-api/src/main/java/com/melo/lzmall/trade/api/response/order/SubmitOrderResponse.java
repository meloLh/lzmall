package com.melo.lzmall.trade.api.response.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubmitOrderResponse implements Serializable {

    private String tradeCode;

    private String memberCode;
}
