package com.melo.lzmall.trade.api.response.order;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class TradeOrderDetailResponse implements Serializable {

    private BigDecimal walletValue = new BigDecimal("1000");

    private String memberCode;

    private String memberName;

    private Long autoCancel = 1717494431552L;
    /**
     * 交易编码
     */
    private String tradeCode;

    /**
     * 支付状态 WAIT_PAY
     */
    private String payStatus;

    /**
     * 支付方式(现金，支付宝，微信)
     */
    private String payType;
    /**
     * 支付单号
     */
    private String payCode;

    /**
     * 应付金额
     */
    private BigDecimal payableAmount;

    /**
     * 实付金额
     */
    private BigDecimal paymentAmount;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 交易备注
     */
    private String remark;

    private List<String> support;
}
