package com.melo.lzmall.trade.service;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.melo.lzmall.trade.service.mapper")
@EnableFeignClients(basePackages = {"com.melo.lzmall.*.api.feign"})
public class TradeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradeApplication.class,args);
    }
}
