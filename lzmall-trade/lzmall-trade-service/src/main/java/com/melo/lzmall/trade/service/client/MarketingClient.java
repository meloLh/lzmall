package com.melo.lzmall.trade.service.client;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.marketing.api.feign.CouponTocFeignClient;
import com.melo.lzmall.marketing.api.feign.MarketingDiscountTocFeignClient;
import com.melo.lzmall.marketing.api.feign.MarketingIdentifyTocFeignClient;
import com.melo.lzmall.marketing.api.request.coupon.UsedCouponInstanceRequest;
import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class MarketingClient {

    @Autowired
    private MarketingIdentifyTocFeignClient marketingIdentifyTocFeignClient;

    @Autowired
    private MarketingDiscountTocFeignClient discountTocFeignClient;

    @Autowired
    private CouponTocFeignClient couponTocFeignClient;

    public SkuMarketingResponse querySkuMarketingList(QuerySkuMarketingRequest request){
        Result<SkuMarketingResponse> result = marketingIdentifyTocFeignClient.querySkuMarketingList(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","获取营销信息异常");
        }
        return result.getResult();
    }

    public MarketingSettleCartResponse settleCart(SettleCartRequest request){
        Result<MarketingSettleCartResponse> result = discountTocFeignClient.settleCart(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","获取营销信息异常");
        }
        return result.getResult();
    }

    public SubmitOrderResponse submitOrder(SubmitOrderRequest request){
        Result<SubmitOrderResponse> result = discountTocFeignClient.submitOrder(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","提交订单获取优惠券信息异常");
        }
        return result.getResult();
    }


    public void usedCouponInstance(UsedCouponInstanceRequest request){
        Result<Void> result = couponTocFeignClient.usedCouponInstance(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","使用优惠券异常");
        }
    }
}
