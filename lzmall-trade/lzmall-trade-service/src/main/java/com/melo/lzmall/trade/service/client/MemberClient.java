package com.melo.lzmall.trade.service.client;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.member.api.feign.MemberFeignClient;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class MemberClient {

    @Autowired
    private MemberFeignClient memberFeignClient;

    public MemberInfoResponse getMemberInfoByAccountCode(String accountCode){
        Result<MemberInfoResponse> result = memberFeignClient.getMemberInfoByAccountCode(accountCode);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","获取会员信息异常");
        }
        return result.getResult();
    }
}
