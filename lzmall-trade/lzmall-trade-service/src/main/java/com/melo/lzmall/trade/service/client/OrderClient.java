package com.melo.lzmall.trade.service.client;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.order.api.feign.OrderFeignClient;
import com.melo.lzmall.order.api.request.CreateOrderCodeRequest;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.order.api.response.createSaleOrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class OrderClient {

    @Autowired
    private OrderFeignClient orderFeignClient;

    public createSaleOrderResponse createSaleOrder(CreateOrderRequest request){
        Result<createSaleOrderResponse> result = orderFeignClient.createSaleOrder(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","创建订单异常");
        }
        return result.getResult();
    }

    public List<CreateOrderCodeResponse> createOrderCode(CreateOrderCodeRequest request){
        Result<List<CreateOrderCodeResponse>> result = orderFeignClient.createOrderCode(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","获取订单编号异常");
        }
        return result.getResult();
    }
}
