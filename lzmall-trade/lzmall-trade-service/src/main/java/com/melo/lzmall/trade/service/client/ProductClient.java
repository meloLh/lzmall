package com.melo.lzmall.trade.service.client;

import com.google.common.collect.Lists;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.product.api.feign.ProductFeignClient;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class ProductClient {

    @Autowired
    private ProductFeignClient productFeignClient;

    public List<ProductSkuDetailResponse> queryBySkuCodeList(List<String> skuCodeList){
        if(CollectionUtils.isEmpty(skuCodeList)){
            return Lists.newArrayList();
        }
        Result<List<ProductSkuDetailResponse>> result = productFeignClient.queryBySkuCodeList(skuCodeList);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","获取商品信息异常");
        }
        return result.getResult();
    }
}
