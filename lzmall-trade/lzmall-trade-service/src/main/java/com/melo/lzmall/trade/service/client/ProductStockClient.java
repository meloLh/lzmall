package com.melo.lzmall.trade.service.client;

import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.product.api.feign.ProductStockFeignClient;
import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class ProductStockClient {

    @Autowired
    private ProductStockFeignClient stockFeignClient;

    public void batchFreezingProductStock(List<BatchFreezingProductStockRequest> request){
        Result<Void> result = stockFeignClient.batchFreezingProductStock(request);
        if(Objects.isNull(result) || !result.getSuccess()){
            throw new BusinessException("500","扣减库存异常");
        }
    }
}
