package com.melo.lzmall.trade.service.constants;

public class CartRedisCacheConstant {

    private static final String LZMALL_TRADE_KEY_PREFIX = "LZMALL_TRADE_CACHE:";

    public static final String TOC_CHOOSE_COUPON_KEY_PREFIX = LZMALL_TRADE_KEY_PREFIX+"TOC_CHOOSE_COUPON_";
}
