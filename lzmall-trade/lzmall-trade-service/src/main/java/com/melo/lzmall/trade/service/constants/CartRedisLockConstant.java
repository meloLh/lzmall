package com.melo.lzmall.trade.service.constants;

public class CartRedisLockConstant {

    private static final String LZMALL_TRADE_KEY_PREFIX = "LZMALL_TRADE_LOCK:";

    public static final String CART_OPERATE_KEY_PREFIX = LZMALL_TRADE_KEY_PREFIX+"CART_OPERATE_";
}
