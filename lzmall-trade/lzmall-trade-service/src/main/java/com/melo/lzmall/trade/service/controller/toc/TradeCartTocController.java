package com.melo.lzmall.trade.service.controller.toc;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.trade.api.request.cart.AddCartRequest;
import com.melo.lzmall.trade.api.request.cart.CheckedCartSkuRequest;
import com.melo.lzmall.trade.api.request.cart.DeleteCartSkuListRequest;
import com.melo.lzmall.trade.api.request.cart.OperateCartSkuCountRequest;
import com.melo.lzmall.trade.api.response.MemberAddressResponse;
import com.melo.lzmall.trade.api.response.cart.CartAllResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.service.converter.CartFaceConverter;
import com.melo.lzmall.trade.service.entity.dto.AddCartDTO;
import com.melo.lzmall.trade.service.service.CartService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class TradeCartTocController {

    @Autowired
    private CartService cartService;

    @Autowired
    private CartFaceConverter converter;

    @ApiOperation("查询购物车列表")
    @GetMapping("/buyer/trade/cart/queryCartAll")
    @MallAuth
    public Result<CartAllResponse> queryCartAll() {
        CartAllResponse response = cartService.queryCartAll();
        return Result.success(response);
    }


    @ApiOperation("加入购物车")
    @PostMapping("/buyer/trade/cart/addCart")
    @MallAuth
    public Result<Void> addCart(@Validated @RequestBody AddCartRequest request) {
        AddCartDTO addCartDTO = converter.convertToAddCartDTO(request);
        cartService.addCart(addCartDTO);
        return Result.success();
    }

    @ApiOperation("查询购物商品数量")
    @GetMapping("/buyer/trade/cart/countMemberCart")
    @MallAuth
    public Result<Integer> countMemberCart() {
        Integer count = cartService.countMemberCart();
        return Result.success(count);
    }

    @ApiOperation("操作购物车商品数量")
    @PostMapping("/buyer/trade/cart/operateCartSkuCount")
    @MallAuth
    public Result<Void> operateCartSkuCount(@Validated @RequestBody OperateCartSkuCountRequest request) {
        cartService.operateCartSkuCount(request.getSkuCode(),request.getBuyCount());
        return Result.success();
    }

    @ApiOperation("删除购物车")
    @PostMapping("/buyer/trade/cart/deleteCartSku")
    @MallAuth
    public Result<Void> deleteCartSku(@Validated @RequestParam(name = "skuCode") String skuCode) {
        cartService.deleteCartSku(skuCode);
        return Result.success();
    }

    @ApiOperation("删除购物车")
    @PostMapping("/buyer/trade/cart/deleteCartSkuList")
    @MallAuth
    public Result<Void> deleteCartSkuList(@RequestBody DeleteCartSkuListRequest request) {
        cartService.deleteCartSkuList(request.getSkuCodeList());
        return Result.success();
    }


    @ApiOperation("购物车商品全选")
    @RequestMapping("/buyer/trade/cart/checkCartAll")
    @MallAuth
    public Result<Void> checkCartAll(Boolean checked){
        cartService.checkCartAll(checked);
        return Result.success();
    }

    @ApiOperation("购物车商品选中")
    @PostMapping("/buyer/trade/cart/checkCartSku")
    @MallAuth
    public Result<Void> checkCartSku(@Validated @RequestBody CheckedCartSkuRequest request) {
        cartService.checkCartSku(request.getSkuCode(),request.getChecked());
        return Result.success();
    }


    @ApiOperation("购物车商品选中")
    @GetMapping("/buyer/trade/cart/shippingMethodList")
    @MallAuth
    public Result<List<String>> shippingMethodList() {
        return Result.success(Lists.newArrayList("LOGISTICS"));
    }


    @ApiOperation("购物车商品选中")
    @GetMapping("/buyer/trade/cart/getMemberDefaultAddress")
    @MallAuth
    public Result<MemberAddressResponse> getMemberDefaultAddress() {
        String json = "{\n" +
                "  \"id\": \"1542505980977442818\",\n" +
                "  \"createBy\": \"15956514726\",\n" +
                "  \"createTime\": \"2022-06-30 21:50:58\",\n" +
                "  \"updateBy\": null,\n" +
                "  \"updateTime\": null,\n" +
                "  \"deleteFlag\": false,\n" +
                "  \"memberId\": \"1539954663151038464\",\n" +
                "  \"name\": \"啊是大多数\",\n" +
                "  \"mobile\": \"15956514721\",\n" +
                "  \"consigneeAddressPath\": \"辽宁省,葫芦岛市,葫芦巷\",\n" +
                "  \"consigneeAddressIdPath\": \"1401797451521721163,1401797451521721164,1522501311018512385\",\n" +
                "  \"detail\": \"12121\",\n" +
                "  \"isDefault\": true,\n" +
                "  \"alias\": \"212121\",\n" +
                "  \"lon\": \"120.85\",\n" +
                "  \"lat\": \"88\"\n" +
                "}";

        MemberAddressResponse response = JSON.parseObject(json, MemberAddressResponse.class);
        return Result.success(response);
    }
}
