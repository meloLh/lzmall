package com.melo.lzmall.trade.service.controller.toc;

import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.trade.api.request.coupon.ChooseCouponRequest;
import com.melo.lzmall.trade.service.service.TradeCouponService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@Validated
public class TradeCouponTocController {

    @Autowired
    private TradeCouponService tradeCouponService;

    @ApiOperation("提交订单")
    @PostMapping("/buyer/trade/coupon/chooseCoupon")
    @MallAuth
    public Result<Void> chooseCoupon(@Validated @RequestBody ChooseCouponRequest request) {
        tradeCouponService.chooseCoupon(request.getInstanceCode());
        return Result.success();
    }
}
