package com.melo.lzmall.trade.service.controller.toc;

import com.google.common.collect.Lists;
import com.melo.lzmall.auth.sdk.annotation.MallAuth;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.trade.api.request.order.SubmitOrderRequest;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.api.response.order.SubmitOrderResponse;
import com.melo.lzmall.trade.api.response.order.TradeOrderDetailResponse;
import com.melo.lzmall.trade.service.converter.TradeOrderTocFaceConverter;
import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.bo.order.SubmitOrderDTO;
import com.melo.lzmall.trade.service.service.TradeOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@Validated
public class TradeOrderTocController {

    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private TradeOrderTocFaceConverter converter;

    @ApiOperation("提交订单")
    @PostMapping("/buyer/trade/order/submitOrder")
    @MallAuth
    public Result<SubmitOrderResponse> submitOrder(@RequestBody SubmitOrderRequest request) {
        SubmitOrderDTO submitOrderDTO = tradeOrderService.submitOrder(request);
        SubmitOrderResponse response =  converter.convertToSubmitOrderResponse(submitOrderDTO);
        return Result.success(response);
    }

    @ApiOperation("购物车去结算")
    @PostMapping("/buyer/trade/order/settlementCart")
    @MallAuth
    public Result<SettleCartResponse> settlementCart() {
        SettleCartResponse response = tradeOrderService.settlementCart();
        return Result.success(response);
    }

    @ApiOperation("获取交易单详情")
    @GetMapping("/buyer/trade/order/getTradeOrderDetail")
    @MallAuth
    public Result<TradeOrderDetailResponse> getTradeOrderDetail(@RequestParam String tradeCode) {
        TradeOrderBaseDTO tradeOrderDetail = tradeOrderService.getTradeOrderDetail(tradeCode);
        TradeOrderDetailResponse response =  converter.convertToTradeOrderDetailResponse(tradeOrderDetail);
        response.setSupport(Lists.newArrayList("WALLET","ALIPAY","WECHAT"));
        return Result.success(response);
    }

    @ApiOperation("发起支付")
    @GetMapping("/buyer/trade/payment/pay")
    public Result<TradeOrderDetailResponse> pay() {
        return Result.success();
    }
}
