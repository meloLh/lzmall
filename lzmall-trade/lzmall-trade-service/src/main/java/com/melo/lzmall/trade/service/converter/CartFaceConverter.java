package com.melo.lzmall.trade.service.converter;


import com.melo.lzmall.trade.api.request.cart.AddCartRequest;
import com.melo.lzmall.trade.service.entity.dto.AddCartDTO;
import org.mapstruct.Mapper;

import java.util.Map;


@Mapper(componentModel = "spring")
public interface  CartFaceConverter {

    AddCartDTO convertToAddCartDTO(AddCartRequest request);
}

