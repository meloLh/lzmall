package com.melo.lzmall.trade.service.converter;


import com.melo.lzmall.marketing.api.response.discount.SettleCartCouponResponse;
import com.melo.lzmall.order.api.request.CreateOrderSkuRequest;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.trade.api.response.cart.CartPriceDetailResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartMarketingCouponResponse;
import com.melo.lzmall.trade.api.response.cart.SettleShopCartResponse;
import com.melo.lzmall.trade.service.entity.bo.CartPriceDetailBO;
import com.melo.lzmall.trade.service.entity.bo.CartSkuBaseInfoBO;
import com.melo.lzmall.trade.service.entity.bo.ShopCartBO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface TradeOrderServiceConverter {

    CreateOrderSkuRequest convertToCreateOrderSkuRequest(ProductSkuDetailResponse skuDetailResponse);

    List<SettleCartMarketingCouponResponse> convertToSettleCartMarketingCouponResponseList(List<SettleCartCouponResponse> availableCouponInstanceList);

    CartSkuBaseInfoBO convertToCartSkuBaseInfoBO(ProductSkuDetailResponse skuDetailResponse);

    List<SettleShopCartResponse> convertToSettleShopCartResponse(List<ShopCartBO> shopCartBOS);

    CartPriceDetailResponse convertToCartPriceDetailResponse(CartPriceDetailBO priceDetailBO);
}

