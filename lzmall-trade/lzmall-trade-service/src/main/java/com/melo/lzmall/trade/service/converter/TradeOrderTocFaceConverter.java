package com.melo.lzmall.trade.service.converter;


import com.melo.lzmall.trade.api.response.order.SubmitOrderResponse;
import com.melo.lzmall.trade.api.response.order.TradeOrderDetailResponse;
import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.bo.order.SubmitOrderDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TradeOrderTocFaceConverter {

    SubmitOrderResponse convertToSubmitOrderResponse(SubmitOrderDTO submitOrderDTO);

    TradeOrderDetailResponse convertToTradeOrderDetailResponse(TradeOrderBaseDTO tradeOrderDetail);
}

