package com.melo.lzmall.trade.service.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 交易单
 */
@Data
public class TradeOrderBaseDTO implements Serializable {

    /**
     * ID
     */
    private Long id;

    private String memberCode;

    private String memberName;
    /**
     * 交易编码
     */
    private String tradeCode;

    /**
     * 支付状态 WAIT_PAY
     */
    private String payStatus;

    /**
     * 支付方式(现金，支付宝，微信)
     */
    private String payType;
    /**
     * 支付单号
     */
    private String payCode;

    /**
     * 应付金额
     */
    private BigDecimal payableAmount;

    /**
     * 实付金额
     */
    private BigDecimal paymentAmount;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 交易备注
     */
    private String remark;

    /**
     * 乐观锁
     */
    private Integer version;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建人编号
     */
    private String creatorCode;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新人编号
     */
    private String modifierCode;
    /**
     * 更新时间
     */
    private Date modifiedTime;

}
