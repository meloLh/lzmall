package com.melo.lzmall.trade.service.entity.bo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CartSkuPriceDetailBO implements Serializable {

    /**
     * 售卖价
     */
    private BigDecimal salePrice;

    /**
     * 市场价  划线价格
     */
    private BigDecimal marketPrice;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 可使用优惠券抵扣金额
     */
    private BigDecimal couponAmount;

    /**
     * 优惠活动价格(折扣后)
     */
    private BigDecimal discountPrice;
}
