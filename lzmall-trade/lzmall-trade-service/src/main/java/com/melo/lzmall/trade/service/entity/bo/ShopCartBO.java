package com.melo.lzmall.trade.service.entity.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("店铺维度商品数据")
public class ShopCartBO implements Serializable {

    private String shopNo;

    private String shopName;

    private Boolean checked = false;

    /**
     * sku详情
     */
    private List<CartSkuDetailBO> skuList;

}
