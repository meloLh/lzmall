package com.melo.lzmall.trade.service.entity.context;

import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.trade.api.request.order.SubmitOrderRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class SubmitOrderContext implements Serializable {

    private String tradeCode;

    private String instanceCode;

    private SubmitOrderRequest submitOrderRequest;

    private MemberInfoResponse memberInfo;

    private AuthAccount authAccount;

    private List<ProductSkuDetailResponse> productDetailList;

    private Map<String,String> shopOrderCodeMap;

    private SubmitOrderResponse submitOrderResponse;
}
