package com.melo.lzmall.trade.service.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddCartDTO implements Serializable {

    private String skuCode;

    private Integer buyCount;
}
