package com.melo.lzmall.trade.service.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车表
 */
@Data
@TableName("cart")
public class CartPO implements Serializable {
private static final long serialVersionUID = 1L;

    /**
	 * ID
	 */
	private Long id;
	/**
	 * spu编码
	 */
	private String spuCode;
	/**
	 * sku编码
	 */
	private String skuCode;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 会员编码
	 */
	private String memberCode;
	/**
	 * 购买数量
	 */
	private Integer buyCount;
	/**
	 * 加购时售价
	 */
	private BigDecimal salePrice;
	/**
	 * 店铺ID
	 */
	private String shopCode;
	/**
	 * 店铺名称
	 */
	private String shopName;
	/**
	 * 是否选中
	 */
	private String checked;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 乐观锁
	 */
	private Integer version;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建人编号
	 */
	private String creatorCode;
	/**
	 * 创建时间
	 */
	private Date createdTime;
	/**
	 * 更新人
	 */
	private String modifier;
	/**
	 * 更新人编号
	 */
	private String modifierCode;
	/**
	 * 更新时间
	 */
	private Date modifiedTime;

}
