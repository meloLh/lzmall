package com.melo.lzmall.trade.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.trade.service.entity.po.CartPO;
import com.melo.lzmall.trade.service.repository.dto.UpdateCartProductNumDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface CartMapper extends BaseMapper<CartPO> {

    int decreaseCartProductNum(@Param("entity") UpdateCartProductNumDTO productNumDTO);

    int increaseCartProductNum(@Param("entity") UpdateCartProductNumDTO productNumDTO);
}
