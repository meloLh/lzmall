package com.melo.lzmall.trade.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melo.lzmall.trade.service.entity.po.TradeOrderPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface TradeOrderMapper extends BaseMapper<TradeOrderPO> {

}
