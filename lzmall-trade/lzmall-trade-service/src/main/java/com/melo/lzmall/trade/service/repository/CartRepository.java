package com.melo.lzmall.trade.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.trade.service.entity.base.CartBaseDTO;
import com.melo.lzmall.trade.service.entity.po.CartPO;
import com.melo.lzmall.trade.service.mapper.CartMapper;
import com.melo.lzmall.trade.service.repository.converter.CartRepositoryConverter;
import com.melo.lzmall.trade.service.repository.dto.UpdateCartProductNumDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class CartRepository {

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private CartRepositoryConverter converter;

    public List<CartBaseDTO> queryByMemberCode(String memberCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode);
        List<CartPO> cartPOS = cartMapper.selectList(queryWrapper);
        return converter.convertToCartBaseDTOList(cartPOS);
    }

    public CartBaseDTO getByMemberCodeAndSkuCode(String memberCode, String skuCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(memberCode,skuCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getSkuCode,skuCode);
        CartPO cartPO = cartMapper.selectOne(queryWrapper);
        return converter.convertToCartBaseDTO(cartPO);
    }

    public void insert(CartBaseDTO cartBaseDTO) {
        CartPO cartPO = converter.convertToCartPO(cartBaseDTO);
        cartMapper.insert(cartPO);
    }

    public void updateCartProductNum(UpdateCartProductNumDTO productNumDTO) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(productNumDTO.getMemberCode(),productNumDTO.getSkuCode())
                && Objects.nonNull(productNumDTO.getOperateNum()),"参数异常");
        int num = cartMapper.increaseCartProductNum(productNumDTO);
        AssertUtil.isTrue(num == 1,"加入购物车异常");
    }

    public Integer countMemberCart(String memberCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getMemberCode,memberCode);
        return cartMapper.selectCount(queryWrapper);
    }

    public void deleteCartSku(String memberCode,String skuCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(memberCode,skuCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getSkuCode,skuCode);
        int delete = cartMapper.delete(queryWrapper);
        AssertUtil.isTrue(delete == 1,"删除购物车异常");
    }

    public void deleteCartSkuList(String memberCode, List<String> skuCodeList) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode) && CollectionUtils.isNotEmpty(skuCodeList),"参数异常");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .in(CartPO::getSkuCode,skuCodeList);
        int delete = cartMapper.delete(queryWrapper);
        AssertUtil.isTrue(delete == CollectionUtils.size(skuCodeList),"删除购物车异常");
    }

    public void checkCartSku(String memberCode, String skuCode,Boolean checked) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(memberCode,skuCode) && Objects.nonNull(checked),"参数异常");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getSkuCode,skuCode);
        CartPO update = new CartPO();
        update.setChecked(checked?"Y":"N");
        int rows = cartMapper.update(update, queryWrapper);
    }

    public void checkCartAll(String memberCode,Boolean checked) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode);
        CartPO update = new CartPO();
        update.setChecked(checked?"Y":"N");
        cartMapper.update(update, queryWrapper);
    }

    public void operateCartSkuCount(String memberCode, String skuCode, Integer buyCount) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(memberCode,skuCode) && Objects.nonNull(buyCount),"参数异常");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getSkuCode,skuCode);
        CartPO update = new CartPO();
        update.setBuyCount(buyCount);
        int rows = cartMapper.update(update, queryWrapper);
    }

    public List<CartBaseDTO> queryByMemberCodeAndChecked(String memberCode, String checked) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode),"会员不能为空");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getChecked,checked);
        List<CartPO> cartPOS = cartMapper.selectList(queryWrapper);
        return converter.convertToCartBaseDTOList(cartPOS);
    }

    public void deleteCartChecked(String memberCode, Boolean checked) {
        AssertUtil.isTrue(StringUtils.isNotBlank(memberCode) && Objects.nonNull(checked),"参数异常");
        LambdaQueryWrapper<CartPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CartPO::getMemberCode,memberCode)
                .eq(CartPO::getChecked,checked?"Y":"N");
        int delete = cartMapper.delete(queryWrapper);
    }
}

