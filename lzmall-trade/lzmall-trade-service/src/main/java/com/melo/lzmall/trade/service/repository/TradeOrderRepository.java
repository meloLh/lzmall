package com.melo.lzmall.trade.service.repository;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.po.TradeOrderPO;
import com.melo.lzmall.trade.service.mapper.TradeOrderMapper;
import com.melo.lzmall.trade.service.repository.converter.TradeOrderRepositoryConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TradeOrderRepository {

    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private TradeOrderRepositoryConverter converter;

    public void createTradeOrder(TradeOrderBaseDTO tradeOrderBaseDTO) {
        TradeOrderPO tradeOrderPO =  converter.convertToTradeOrderPO(tradeOrderBaseDTO);
        tradeOrderMapper.insert(tradeOrderPO);
    }

    public TradeOrderBaseDTO getByTradeCode(String tradeCode) {
        AssertUtil.isTrue(StringUtils.isNotBlank(tradeCode),"参数异常");
        LambdaQueryWrapper<TradeOrderPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TradeOrderPO::getTradeCode,tradeCode);
        TradeOrderPO tradeOrderPO = tradeOrderMapper.selectOne(queryWrapper);
        return converter.convertToTradeOrderBaseDTO(tradeOrderPO);
    }
}

