package com.melo.lzmall.trade.service.repository.converter;


import com.melo.lzmall.trade.service.entity.base.CartBaseDTO;
import com.melo.lzmall.trade.service.entity.po.CartPO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CartRepositoryConverter {

    List<CartBaseDTO> convertToCartBaseDTOList(List<CartPO> cartPOS);

    CartBaseDTO convertToCartBaseDTO(CartPO cartPO);

    CartPO convertToCartPO(CartBaseDTO cartBaseDTO);
}

