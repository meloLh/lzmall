package com.melo.lzmall.trade.service.repository.converter;


import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.po.TradeOrderPO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TradeOrderRepositoryConverter {

    TradeOrderPO convertToTradeOrderPO(TradeOrderBaseDTO tradeOrderBaseDTO);

    TradeOrderBaseDTO convertToTradeOrderBaseDTO(TradeOrderPO tradeOrderPO);
}

