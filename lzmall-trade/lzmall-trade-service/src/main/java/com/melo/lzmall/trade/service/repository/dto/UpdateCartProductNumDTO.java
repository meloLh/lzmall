package com.melo.lzmall.trade.service.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateCartProductNumDTO implements Serializable {

    private String memberCode;

    private String skuCode;

    private Integer operateNum;
}
