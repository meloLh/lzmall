package com.melo.lzmall.trade.service.service;


import com.melo.lzmall.trade.api.response.cart.CartAllResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.service.entity.dto.AddCartDTO;

import java.util.List;

public interface CartService {
    void addCart(AddCartDTO addCartDTO);

    Integer countMemberCart();

    CartAllResponse queryCartAll();

    void deleteCartSku(String skuCode);

    void deleteCartSkuList(List<String> skuCodeList);

    void checkCartSku(String skuCode, Boolean checked);

    void checkCartAll(Boolean checked);

    void operateCartSkuCount(String skuCode, Integer buyCount);
}

