package com.melo.lzmall.trade.service.service;


public interface TradeCouponService {

    /**
     * @Description 选择优惠券
     * @author liuhu
     * @param instanceCode
     * @date 2024/6/11 14:24
     * @return void
     */
    void chooseCoupon(String instanceCode);

    /**
     * @Description 选择优惠券
     * @author liuhu
     * @date 2024/6/11 14:24
     * @return void
     */
    void cancelChooseCoupon(String accountCode);

    /**
     * @Description 获取选择得优惠券
     * @author liuhu
     * @param accountCode
     * @date 2024/6/11 14:24
     * @return java.lang.String
     */
    String getOrderChooseCoupon(String accountCode);
}

