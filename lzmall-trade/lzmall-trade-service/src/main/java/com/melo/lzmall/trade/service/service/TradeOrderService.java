package com.melo.lzmall.trade.service.service;


import com.melo.lzmall.trade.api.request.order.SubmitOrderRequest;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.bo.order.SubmitOrderDTO;

public interface TradeOrderService {

    /**
     * @Description 购物车去结算
     * @author liuhu
     * @date 2024/6/7 17:42
     * @return com.melo.lzmall.trade.api.response.cart.SettleCartResponse
     */
    SettleCartResponse settlementCart();

    /**
     * @Description 提交订单
     * @author liuhu
     * @param request
     * @date 2024/6/11 14:25
     * @return com.melo.lzmall.trade.service.entity.bo.order.SubmitOrderDTO
     */
    SubmitOrderDTO submitOrder(SubmitOrderRequest request);

    /**
     * @Description 获取交易单详情
     * @author liuhu
     * @param tradeCode
     * @date 2024/6/11 14:26
     * @return com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO
     */
    TradeOrderBaseDTO getTradeOrderDetail(String tradeCode);
}

