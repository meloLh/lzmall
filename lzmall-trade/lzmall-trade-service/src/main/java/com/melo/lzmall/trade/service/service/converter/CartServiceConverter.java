package com.melo.lzmall.trade.service.service.converter;


import com.melo.lzmall.marketing.api.response.discount.SettleCartCouponResponse;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingDetailResponse;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.trade.api.response.cart.*;
import com.melo.lzmall.trade.service.entity.bo.CartPriceDetailBO;
import com.melo.lzmall.trade.service.entity.bo.CartSkuBaseInfoBO;
import com.melo.lzmall.trade.service.entity.bo.ShopCartBO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CartServiceConverter {

    CartSkuInfoResponse convertToCartSkuInfoResponse(ProductSkuDetailResponse skuDetailResponse);
    
    CartSkuBaseInfoBO convertToCartSkuBaseInfoBO(ProductSkuDetailResponse skuDetailResponse);

    List<ShopCartResponse> convertToShopCartResponseList(List<ShopCartBO> shopCartBOS);
    
    List<SettleShopCartResponse> convertToSettleShopCartResponse(List<ShopCartBO> shopCartBOS);

    CartPriceDetailResponse convertToCartPriceDetailResponse(CartPriceDetailBO priceDetailBO);

    List<CartSkuCouponResponseResponse> convertToCartSkuCouponResponseResponseList(List<SkuMarketingDetailResponse> disableCouponActivityList);

    List<SettleCartMarketingCouponResponse> convertToSettleCartMarketingCouponResponseList(List<SettleCartCouponResponse> availableCouponInstanceList);
}

