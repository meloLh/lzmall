package com.melo.lzmall.trade.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SettleCartSkuRequest;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingDetailRequest;
import com.melo.lzmall.marketing.api.request.identify.QuerySkuMarketingRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingActivityResponse;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingDetailResponse;
import com.melo.lzmall.marketing.api.response.identify.SkuMarketingResponse;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.trade.api.response.cart.*;
import com.melo.lzmall.trade.service.client.MarketingClient;
import com.melo.lzmall.trade.service.client.MemberClient;
import com.melo.lzmall.trade.service.client.ProductClient;
import com.melo.lzmall.trade.service.constants.CartRedisLockConstant;
import com.melo.lzmall.trade.service.entity.base.CartBaseDTO;
import com.melo.lzmall.trade.service.entity.bo.*;
import com.melo.lzmall.trade.service.entity.dto.AddCartDTO;
import com.melo.lzmall.trade.service.repository.CartRepository;
import com.melo.lzmall.trade.service.repository.dto.UpdateCartProductNumDTO;
import com.melo.lzmall.trade.service.service.CartService;
import com.melo.lzmall.trade.service.service.TradeCouponService;
import com.melo.lzmall.trade.service.service.converter.CartServiceConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CartServiceImpl  implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private ProductClient productClient;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private MarketingClient  marketingClient;

    @Autowired
    private TradeCouponService tradeCouponService;

    @Autowired
    private CartServiceConverter converter;

    @Override
    public void addCart(AddCartDTO addCartDTO) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        String key = CartRedisLockConstant.CART_OPERATE_KEY_PREFIX+authAccount.getAccountCode();
        RLock lock = redissonClient.getLock(key);
        try {
            boolean tryLock = lock.tryLock(300, 500, TimeUnit.SECONDS);
            if (!tryLock) {
                throw new BusinessException("500", "请稍后重试");
            }
            MemberInfoResponse memberInfo = memberClient.getMemberInfoByAccountCode(authAccount.getAccountCode());
            AssertUtil.isTrue(Objects.nonNull(memberInfo), "会员信息不存在");
            ProductSkuDetailResponse productSkuDetail = validCartProduct(addCartDTO);
            CartBaseDTO cartBaseDTO = cartRepository.getByMemberCodeAndSkuCode(memberInfo.getMemberCode(), addCartDTO.getSkuCode());
            if (Objects.nonNull(cartBaseDTO)) {
                UpdateCartProductNumDTO productNumDTO = UpdateCartProductNumDTO.builder()
                        .memberCode(memberInfo.getMemberCode())
                        .skuCode(addCartDTO.getSkuCode())
                        .operateNum(addCartDTO.getBuyCount()).build();
                cartRepository.updateCartProductNum(productNumDTO);
            } else {
                CartBaseDTO insert = buildInsertCartBaseDTO(addCartDTO,productSkuDetail,memberInfo);
                cartRepository.insert(insert);
            }
        }catch (BusinessException e) {
            throw e;
        } catch (Exception e) {
            log.error("加入购物车失败,req:{}", JSON.toJSONString(addCartDTO),e);
            throw new BusinessException("500", "加购失败");
        } finally {
            if(lock.isHeldByCurrentThread() && lock.isLocked()){
                lock.unlock();
            }
        }
    }

    @Override
    public Integer countMemberCart() {
        MemberInfoResponse memberInfo = getMemberInfoResponse();
        return cartRepository.countMemberCart(memberInfo.getMemberCode());
    }

    private MemberInfoResponse getMemberInfoResponse() {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberInfoResponse memberInfo = memberClient.getMemberInfoByAccountCode(authAccount.getAccountCode());
        AssertUtil.isTrue(Objects.nonNull(memberInfo), "会员信息不存在");
        return memberInfo;
    }

    @Override
    public CartAllResponse queryCartAll() {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberInfoResponse memberInfo = memberClient.getMemberInfoByAccountCode(authAccount.getAccountCode());
        CartAllResponse response = new CartAllResponse();
        response.setMemberCode(memberInfo.getMemberCode());
        response.setMemberName(memberInfo.getMemberName());

        //1.查询当前会员购物车信息
        List<CartBaseDTO> cartBaseDTOS = cartRepository.queryByMemberCode(memberInfo.getMemberCode());
        if(CollectionUtils.isEmpty(cartBaseDTOS)){
            response.setCartList(Lists.newArrayList());
            return response;
        }
        // 构建店铺商品信息
        List<String> skuCodeList = cartBaseDTOS.stream().map(CartBaseDTO::getSkuCode).collect(Collectors.toList());
        List<ProductSkuDetailResponse> productDetailList = productClient.queryBySkuCodeList(skuCodeList);

        List<ShopCartBO> shopCartBOS = buildShopCartResponseList(cartBaseDTOS,productDetailList);
        List<ShopCartResponse> cartList = converter.convertToShopCartResponseList(shopCartBOS);
        // 购物车价格
        List<CartSkuDetailBO> cartSkuDetailResponses = shopCartBOS.stream()
                .flatMap(shopCart -> shopCart.getSkuList().stream()).collect(Collectors.toList())
                .stream().filter(v->Objects.equals(v.getChecked(),Boolean.TRUE)).collect(Collectors.toList());
        CartPriceDetailBO priceDetailBO = buildCartPriceDetailResponse(cartSkuDetailResponses);
        CartPriceDetailResponse cartPriceDetail = converter.convertToCartPriceDetailResponse(priceDetailBO);

        response.setCartList(cartList);
        response.setCartPrice(cartPriceDetail);
        // 构建可参数营销活动
        fillMarketingCouponInfo(cartList,productDetailList,authAccount);
        return response;
    }

    private void fillMarketingCouponInfo(List<ShopCartResponse> cartList,
                                         List<ProductSkuDetailResponse> productDetailList,
                                         AuthAccount authAccount) {
        SkuMarketingResponse marketingResponse = getSkuMarketingResponse(productDetailList, authAccount);
        Map<String, SkuMarketingActivityResponse> activityResponseMap = marketingResponse.getSkuActivityList()
                .stream().collect(Collectors.toMap(SkuMarketingActivityResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));

        for (ShopCartResponse shopCartResponse : cartList) {
            List<String> skuCodeList = shopCartResponse.getSkuList().stream().map(v -> v.getSku().getSkuCode()).collect(Collectors.toList());
            List<SkuMarketingDetailResponse> marketingActivityResponses = skuCodeList.stream().map(activityResponseMap::get).filter(Objects::nonNull)
                    .flatMap(v->v.getActivityList().stream()).collect(Collectors.toList());
            List<CartSkuCouponResponseResponse> canReceiveCouponList =  converter.convertToCartSkuCouponResponseResponseList(marketingActivityResponses);
            shopCartResponse.setCanReceiveCouponList(canReceiveCouponList);
        }
    }

    private SkuMarketingResponse getSkuMarketingResponse(List<ProductSkuDetailResponse> productDetailList, AuthAccount authAccount) {
        QuerySkuMarketingRequest request = new QuerySkuMarketingRequest();
        request.setAccountCode(authAccount.getAccountCode());
        request.setAccountName(authAccount.getAccountName());
        List<QuerySkuMarketingDetailRequest> marketingDetailRequests = productDetailList.stream().map(sku -> {
            QuerySkuMarketingDetailRequest detailRequest = new QuerySkuMarketingDetailRequest();
            detailRequest.setSkuCode(sku.getSkuCode());
            detailRequest.setShopCode(sku.getShopCode());
            detailRequest.setShopName(sku.getShopName());
            detailRequest.setCategoryCode3(sku.getCategoryCode3());
            return detailRequest;
        }).collect(Collectors.toList());
        request.setSkuList(marketingDetailRequests);
        return marketingClient.querySkuMarketingList(request);
    }

    @Override
    public void deleteCartSku(String skuCode) {
        MemberInfoResponse memberInfoResponse = getMemberInfoResponse();
        cartRepository.deleteCartSku(memberInfoResponse.getMemberCode(), skuCode);
    }

    @Override
    public void deleteCartSkuList(List<String> skuCodeList) {
        MemberInfoResponse memberInfoResponse = getMemberInfoResponse();
        cartRepository.deleteCartSkuList(memberInfoResponse.getMemberCode(), skuCodeList);
    }

    @Override
    public void checkCartSku(String skuCode, Boolean checked) {
        MemberInfoResponse memberInfoResponse = getMemberInfoResponse();
        cartRepository.checkCartSku(memberInfoResponse.getMemberCode(), skuCode,checked);
    }

    @Override
    public void checkCartAll(Boolean checked) {
        MemberInfoResponse memberInfoResponse = getMemberInfoResponse();
        cartRepository.checkCartAll(memberInfoResponse.getMemberCode(),checked);
    }

    @Override
    public void operateCartSkuCount(String skuCode, Integer buyCount) {
        MemberInfoResponse memberInfoResponse = getMemberInfoResponse();
        cartRepository.operateCartSkuCount(memberInfoResponse.getMemberCode(),skuCode,buyCount);
    }


    private CartPriceDetailBO buildCartPriceDetailResponse(List<CartSkuDetailBO> cartSkuDetailBOList) {
        CartPriceDetailBO priceDetailBO = new CartPriceDetailBO();
        BigDecimal saleAmount = cartSkuDetailBOList.stream().map(v -> new BigDecimal(v.getBuyCount()).multiply(v.getSkuPrice().getSalePrice()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        priceDetailBO.setSaleAmount(saleAmount);
        BigDecimal couponAmount = cartSkuDetailBOList.stream().map(v -> v.getSkuPrice().getCouponAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        priceDetailBO.setCouponAmount(couponAmount);
        BigDecimal discountAmount = cartSkuDetailBOList.stream().map(v -> {
            if(v.getSkuPrice().getDiscountPrice().compareTo(BigDecimal.ZERO) <=0){
                return BigDecimal.ZERO;
            }
            return new BigDecimal(v.getBuyCount()).multiply(v.getSkuPrice().getSalePrice().subtract(v.getSkuPrice().getDiscountPrice()));
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        priceDetailBO.setDiscountAmount(discountAmount);
        BigDecimal amount = saleAmount.subtract(couponAmount).subtract(discountAmount);
        BigDecimal payableAmount = amount.compareTo(BigDecimal.ZERO) >= 0 ? amount.subtract(discountAmount) : BigDecimal.ZERO;
        priceDetailBO.setPayableAmount(payableAmount);
        return priceDetailBO;
    }

    private List<ShopCartBO> buildShopCartResponseList(List<CartBaseDTO> cartBaseDTOS,
                                                       List<ProductSkuDetailResponse> productDetailList) {
        List<ShopCartBO> shopCartBOS = new ArrayList<>();
        Map<String, List<CartBaseDTO>> cartSkuShopMap = cartBaseDTOS.stream().collect(Collectors.groupingBy(CartBaseDTO::getShopCode));
        for (Map.Entry<String, List<CartBaseDTO>> entry : cartSkuShopMap.entrySet()) {
            String shopNo = entry.getKey();
            List<CartBaseDTO> cartBaseDTOList = entry.getValue();
            ShopCartBO shopCartBO = new ShopCartBO();
            shopCartBO.setShopNo(shopNo);
            shopCartBO.setShopName(cartBaseDTOList.get(0).getShopName());

            List<CartSkuDetailBO> cartSkuDetailBOS = buildCartSkuDetailResponseList(cartBaseDTOList,productDetailList);
            shopCartBO.setSkuList(cartSkuDetailBOS);
            shopCartBOS.add(shopCartBO);
        }
        return shopCartBOS;
    }

    private List<CartSkuDetailBO> buildCartSkuDetailResponseList(List<CartBaseDTO> cartBaseDTOList,
                                                                       List<ProductSkuDetailResponse> productDetailList) {
        Map<String, ProductSkuDetailResponse> skuDetailResponseMap = productDetailList.stream().collect(Collectors.toMap(ProductSkuDetailResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));
        return cartBaseDTOList.stream().map(cart->{
            ProductSkuDetailResponse skuDetailResponse = skuDetailResponseMap.get(cart.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(skuDetailResponse),"商品信息不存在"+cart.getSkuCode());
            CartSkuDetailBO response = new CartSkuDetailBO();
            response.setSkuCode(skuDetailResponse.getSkuCode());
            CartSkuBaseInfoBO skuInfoResponse =  converter.convertToCartSkuBaseInfoBO(skuDetailResponse);
            response.setInvalid(!Objects.equals(skuDetailResponse.getStatus(),"ON_SALE"));
            response.setChecked(Objects.equals(cart.getChecked(),"Y"));
            response.setBuyCount(cart.getBuyCount());
            response.setSku(skuInfoResponse);

            CartSkuPriceDetailBO priceDetail = new CartSkuPriceDetailBO();
            priceDetail.setCostPrice(skuDetailResponse.getCostPrice());
            priceDetail.setMarketPrice(skuDetailResponse.getMarketPrice());
            priceDetail.setSalePrice(skuDetailResponse.getSalePrice());
            priceDetail.setCouponAmount(BigDecimal.ZERO);
            priceDetail.setDiscountPrice(BigDecimal.ZERO);
            response.setSkuPrice(priceDetail);

            return response;

        }).collect(Collectors.toList());
    }

    private CartBaseDTO buildInsertCartBaseDTO(AddCartDTO addCartDTO,
                                               ProductSkuDetailResponse productSkuDetail,
                                               MemberInfoResponse memberInfo) {
        CartBaseDTO cartBaseDTO = new CartBaseDTO();
        cartBaseDTO.setSpuCode(productSkuDetail.getSpuCode());
        cartBaseDTO.setMemberCode(memberInfo.getMemberCode());
        cartBaseDTO.setSkuCode(addCartDTO.getSkuCode());
        cartBaseDTO.setProductName(productSkuDetail.getProductName());
        cartBaseDTO.setBuyCount(addCartDTO.getBuyCount());
        cartBaseDTO.setSalePrice(productSkuDetail.getSalePrice());
        cartBaseDTO.setShopCode(productSkuDetail.getShopCode());
        cartBaseDTO.setShopName(productSkuDetail.getShopName());
        return cartBaseDTO;
    }

    private ProductSkuDetailResponse validCartProduct(AddCartDTO addCartDTO) {
        List<ProductSkuDetailResponse> productDetailList = productClient.queryBySkuCodeList(Lists.newArrayList(addCartDTO.getSkuCode()));
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(productDetailList),"商品不存在");
        ProductSkuDetailResponse productSkuDetail = productDetailList.get(0);
        if(!Objects.equals(productSkuDetail.getStatus(),"ON_SALE")){
            throw new BusinessException("500", "商品【"+productSkuDetail.getProductName()+"】已失效");
        }
        if(Objects.isNull(productSkuDetail.getStockNum()) || productSkuDetail.getStockNum()<addCartDTO.getBuyCount()){
            throw new BusinessException("500", "商品【"+productSkuDetail.getProductName()+"】库存不足");
        }
        return productSkuDetail;
    }
}