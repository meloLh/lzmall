package com.melo.lzmall.trade.service.service.impl;

import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import com.melo.lzmall.trade.service.constants.CartRedisCacheConstant;
import com.melo.lzmall.trade.service.service.TradeCouponService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TradeCouponServiceImpl implements TradeCouponService {

    @Autowired
    private RedisHelper redisHelper;

    @Override
    public void chooseCoupon(String instanceCode) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        String key = CartRedisCacheConstant.TOC_CHOOSE_COUPON_KEY_PREFIX + authAccount.getAccountCode();
        redisHelper.set(key, instanceCode, 1L, TimeUnit.HOURS);
    }

    @Override
    public void cancelChooseCoupon(String accountCode) {
        String key = CartRedisCacheConstant.TOC_CHOOSE_COUPON_KEY_PREFIX + accountCode;
        redisHelper.del(key);
    }

    @Override
    public String getOrderChooseCoupon(String accountCode) {
        AssertUtil.isTrue(!StringUtils.isAnyBlank(accountCode),"参数异常");
        String key = CartRedisCacheConstant.TOC_CHOOSE_COUPON_KEY_PREFIX + accountCode;
        return redisHelper.get(key);
    }
}

