package com.melo.lzmall.trade.service.service.impl;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.melo.lzmall.auth.sdk.dto.AuthAccount;
import com.melo.lzmall.auth.sdk.utils.AccountContextUtil;
import com.melo.lzmall.common.core.exception.BusinessException;
import com.melo.lzmall.common.core.utils.AssertUtil;
import com.melo.lzmall.common.redis.helper.RedisHelper;
import com.melo.lzmall.marketing.api.request.coupon.UsedCouponInstanceRequest;
import com.melo.lzmall.marketing.api.request.discount.SettleCartRequest;
import com.melo.lzmall.marketing.api.request.discount.SettleCartSkuRequest;
import com.melo.lzmall.marketing.api.request.discount.SubmitOrderSkuRequest;
import com.melo.lzmall.marketing.api.response.discount.MarketingSettleCartResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderResponse;
import com.melo.lzmall.marketing.api.response.discount.SubmitOrderSkuCouponResponse;
import com.melo.lzmall.member.api.response.MemberInfoResponse;
import com.melo.lzmall.order.api.request.CreateOrderCodeRequest;
import com.melo.lzmall.order.api.request.CreateOrderRequest;
import com.melo.lzmall.order.api.request.CreateOrderSkuRequest;
import com.melo.lzmall.order.api.response.CreateOrderCodeResponse;
import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockDetailRequest;
import com.melo.lzmall.product.api.request.stock.BatchFreezingProductStockRequest;
import com.melo.lzmall.product.api.response.product.ProductSkuDetailResponse;
import com.melo.lzmall.trade.api.request.order.SubmitOrderCartRequest;
import com.melo.lzmall.trade.api.request.order.SubmitOrderRequest;
import com.melo.lzmall.trade.api.response.cart.CartPriceDetailResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartMarketingCouponResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.api.response.cart.SettleShopCartResponse;
import com.melo.lzmall.trade.service.client.*;
import com.melo.lzmall.trade.service.converter.TradeOrderServiceConverter;
import com.melo.lzmall.trade.service.entity.base.CartBaseDTO;
import com.melo.lzmall.trade.service.entity.base.TradeOrderBaseDTO;
import com.melo.lzmall.trade.service.entity.bo.*;
import com.melo.lzmall.trade.service.entity.bo.order.SubmitOrderDTO;
import com.melo.lzmall.trade.service.entity.context.SubmitOrderContext;
import com.melo.lzmall.trade.service.repository.CartRepository;
import com.melo.lzmall.trade.service.repository.TradeOrderRepository;
import com.melo.lzmall.trade.service.service.TradeCouponService;
import com.melo.lzmall.trade.service.service.TradeOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TradeOrderServiceImpl implements TradeOrderService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private TradeOrderRepository tradeOrderRepository;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private ProductClient productClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private ProductStockClient productStockClient;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private RedisHelper redisHelper;

    @Autowired
    private TradeCouponService tradeCouponService;

    @Autowired
    private MarketingClient marketingClient;

    @Autowired
    private TradeOrderServiceConverter converter;

    @Override
    public SettleCartResponse settlementCart() {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        MemberInfoResponse memberInfo = memberClient.getMemberInfoByAccountCode(authAccount.getAccountCode());
        //1.查询当前会员购物车信息
        List<CartBaseDTO> cartBaseDTOS = cartRepository.queryByMemberCodeAndChecked(memberInfo.getMemberCode(),"Y");
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(cartBaseDTOS),"购物车去结算异常");
        //2.构建店铺商品信息
        List<String> skuCodeList = cartBaseDTOS.stream().map(CartBaseDTO::getSkuCode).collect(Collectors.toList());
        List<ProductSkuDetailResponse> productDetailList = productClient.queryBySkuCodeList(skuCodeList);
        List<ShopCartBO> shopCartBOS = buildShopCartResponseList(cartBaseDTOS,productDetailList);
        List<SettleShopCartResponse> cartList = converter.convertToSettleShopCartResponse(shopCartBOS);
        //3.校验商品信息
        List<CartSkuDetailBO> cartSkuDetailBOList = shopCartBOS.stream()
                .flatMap(shopCart -> shopCart.getSkuList().stream()).collect(Collectors.toList());
        validShopCartProduct(cartSkuDetailBOList);
        //5.获取营销信息
        SettleCartResponse response = new SettleCartResponse();
        fillSettleCartMarketingCouponInfo(productDetailList,authAccount,response,cartBaseDTOS);
        //4.计算购物车价格
        CartPriceDetailBO priceDetailBO = buildCartPriceDetailResponse(cartSkuDetailBOList,response);
        CartPriceDetailResponse cartPriceDetail = converter.convertToCartPriceDetailResponse(priceDetailBO);
        response.setMemberCode(memberInfo.getMemberCode());
        response.setMemberName(memberInfo.getMemberName());
        response.setCartList(cartList);
        response.setCartPrice(cartPriceDetail);
        return response;
    }


    private CartPriceDetailBO buildCartPriceDetailResponse(List<CartSkuDetailBO> cartSkuDetailBOList,
                                                           SettleCartResponse response) {
        CartPriceDetailBO priceDetailBO = new CartPriceDetailBO();
        BigDecimal saleAmount = cartSkuDetailBOList.stream().map(v -> new BigDecimal(v.getBuyCount()).multiply(v.getSkuPrice().getSalePrice()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        priceDetailBO.setSaleAmount(saleAmount);
        BigDecimal couponAmount = BigDecimal.ZERO;
        // 如果选择了优惠券
        if(StringUtils.isNotBlank(response.getInstanceCode())){
            List<SettleCartMarketingCouponResponse> couponResponses = response.getCanUseCoupons().stream().filter(v -> Objects.equals(v.getInstanceCode(), response.getInstanceCode()))
                    .collect(Collectors.toList());
            if(CollectionUtils.isNotEmpty(couponResponses)){
                couponAmount =  couponResponses.get(0).getTotalCouponPromotionAmount();
            }
        }
        priceDetailBO.setCouponAmount(couponAmount);
        BigDecimal discountAmount = BigDecimal.ZERO;
        priceDetailBO.setDiscountAmount(discountAmount);
        BigDecimal amount = saleAmount.subtract(couponAmount).subtract(discountAmount);
        BigDecimal payableAmount = amount.compareTo(BigDecimal.ZERO) >= 0 ? amount : BigDecimal.ZERO;
        priceDetailBO.setPayableAmount(payableAmount);
        return priceDetailBO;
    }

    private List<ShopCartBO> buildShopCartResponseList(List<CartBaseDTO> cartBaseDTOS,
                                                       List<ProductSkuDetailResponse> productDetailList) {
        List<ShopCartBO> shopCartBOS = new ArrayList<>();
        Map<String, List<CartBaseDTO>> cartSkuShopMap = cartBaseDTOS.stream().collect(Collectors.groupingBy(CartBaseDTO::getShopCode));
        for (Map.Entry<String, List<CartBaseDTO>> entry : cartSkuShopMap.entrySet()) {
            String shopNo = entry.getKey();
            List<CartBaseDTO> cartBaseDTOList = entry.getValue();
            ShopCartBO shopCartBO = new ShopCartBO();
            shopCartBO.setShopNo(shopNo);
            shopCartBO.setShopName(cartBaseDTOList.get(0).getShopName());

            List<CartSkuDetailBO> cartSkuDetailBOS = buildCartSkuDetailResponseList(cartBaseDTOList,productDetailList);
            shopCartBO.setSkuList(cartSkuDetailBOS);
            shopCartBOS.add(shopCartBO);
        }
        return shopCartBOS;
    }

    private List<CartSkuDetailBO> buildCartSkuDetailResponseList(List<CartBaseDTO> cartBaseDTOList,
                                                                 List<ProductSkuDetailResponse> productDetailList) {
        Map<String, ProductSkuDetailResponse> skuDetailResponseMap = productDetailList.stream().collect(Collectors.toMap(ProductSkuDetailResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));
        return cartBaseDTOList.stream().map(cart->{
            ProductSkuDetailResponse skuDetailResponse = skuDetailResponseMap.get(cart.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(skuDetailResponse),"商品信息不存在"+cart.getSkuCode());
            CartSkuDetailBO response = new CartSkuDetailBO();
            response.setSkuCode(skuDetailResponse.getSkuCode());
            CartSkuBaseInfoBO skuInfoResponse =  converter.convertToCartSkuBaseInfoBO(skuDetailResponse);
            response.setInvalid(!Objects.equals(skuDetailResponse.getStatus(),"ON_SALE"));
            response.setChecked(Objects.equals(cart.getChecked(),"Y"));
            response.setBuyCount(cart.getBuyCount());
            response.setSku(skuInfoResponse);

            CartSkuPriceDetailBO priceDetail = new CartSkuPriceDetailBO();
            priceDetail.setCostPrice(skuDetailResponse.getCostPrice());
            priceDetail.setMarketPrice(skuDetailResponse.getMarketPrice());
            priceDetail.setSalePrice(skuDetailResponse.getSalePrice());
            priceDetail.setCouponAmount(BigDecimal.ZERO);
            priceDetail.setDiscountPrice(BigDecimal.ZERO);
            response.setSkuPrice(priceDetail);

            return response;

        }).collect(Collectors.toList());
    }

    private void fillSettleCartMarketingCouponInfo(List<ProductSkuDetailResponse> productDetailList,
                                                   AuthAccount authAccount,
                                                   SettleCartResponse response,
                                                   List<CartBaseDTO> cartBaseDTOS) {

        // 获取选中的优惠券
        String orderChooseCoupon = tradeCouponService.getOrderChooseCoupon(authAccount.getAccountCode());
        response.setInstanceCode(orderChooseCoupon);

        MarketingSettleCartResponse marketingSettleCartResponse = getMarketingSettleCartResponse(productDetailList, authAccount,cartBaseDTOS,orderChooseCoupon);
        List<SettleCartMarketingCouponResponse> canUseCoupons = converter.convertToSettleCartMarketingCouponResponseList(marketingSettleCartResponse.getAvailableCouponInstanceList());
        response.setCanUseCoupons(canUseCoupons);

        List<SettleCartMarketingCouponResponse> cantUseCoupons = converter.convertToSettleCartMarketingCouponResponseList(marketingSettleCartResponse.getDisableCouponInstanceList());
        response.setCantUseCoupons(cantUseCoupons);

    }

    private MarketingSettleCartResponse getMarketingSettleCartResponse(List<ProductSkuDetailResponse> productDetailList,
                                                                       AuthAccount authAccount,
                                                                       List<CartBaseDTO> cartBaseDTOS,
                                                                       String orderChooseCoupon) {
        SettleCartRequest request = new SettleCartRequest();
        request.setAccountCode(authAccount.getAccountCode());
        request.setAccountName(authAccount.getAccountName());
        request.setInstanceCode(orderChooseCoupon);
        Map<String, CartBaseDTO> cartBaseDTOMap = cartBaseDTOS.stream().collect(Collectors.toMap(CartBaseDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));
        List<SettleCartSkuRequest> marketingDetailRequests = productDetailList.stream().map(sku -> {
            SettleCartSkuRequest detailRequest = new SettleCartSkuRequest();
            detailRequest.setSkuCode(sku.getSkuCode());
            detailRequest.setProductName(sku.getProductName());
            detailRequest.setShopCode(sku.getShopCode());
            detailRequest.setShopName(sku.getShopName());
            detailRequest.setSalePrice(sku.getSalePrice());
            detailRequest.setCategoryCode3(sku.getCategoryCode3());
            CartBaseDTO cartBaseDTO = cartBaseDTOMap.get(sku.getSkuCode());
            detailRequest.setPurchaseNum(cartBaseDTO.getBuyCount());
            return detailRequest;
        }).collect(Collectors.toList());
        request.setSkuList(marketingDetailRequests);


        return marketingClient.settleCart(request);
    }

    private void validShopCartProduct(List<CartSkuDetailBO> cartSkuDetailBOList) {
        for (CartSkuDetailBO cartSkuDetailBO : cartSkuDetailBOList) {
            CartSkuBaseInfoBO skuDetailBOSku = cartSkuDetailBO.getSku();
            AssertUtil.isTrue(Objects.nonNull(cartSkuDetailBO.getSku()),"商品不存在");
            if(!Objects.equals(skuDetailBOSku.getStatus(),"ON_SALE")){
                throw new BusinessException("500", "商品【"+skuDetailBOSku.getProductName()+"】已失效");
            }
            if(Objects.isNull(skuDetailBOSku.getStockNum()) || skuDetailBOSku.getStockNum()< cartSkuDetailBO.getBuyCount()){
                throw new BusinessException("500", "商品【"+skuDetailBOSku.getProductName()+"】库存不足");
            }
        }
    }

    @Override
    public SubmitOrderDTO submitOrder(SubmitOrderRequest request) {
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();

        String key = DigestUtils.md5Hex(JSON.toJSONString(request)+authAccount.getAccountCode());
        Boolean block = redisHelper.setNx(key, "block", 3L, TimeUnit.SECONDS);
        AssertUtil.isTrue(block,"手速太快了，稍后重试！");

        MemberInfoResponse memberInfo = memberClient.getMemberInfoByAccountCode(authAccount.getAccountCode());
        //1.查询当前会员购物车信息
        List<CartBaseDTO> cartBaseDTOS = cartRepository.queryByMemberCodeAndChecked(memberInfo.getMemberCode(),"Y");
        AssertUtil.isTrue(CollectionUtils.isNotEmpty(cartBaseDTOS),"购物车去结算异常");
        //2.业务相关校验
        SubmitOrderContext context = validSubmitOrderBiz(request, authAccount,cartBaseDTOS,memberInfo);
        try {
            //3.生成交易单
            createTradeOrder(context);
            //4 获取订单号
            getOrderCodeList(context);
            //6.冻结库存
            freezeProductStock(context);
            //7.扣减优惠券、活动
            freezeJoinedMarketing(context);
            //8.创建订单
            createShopOrder(context);
            //9.清空购物车
            clearCartProduct(context);
            //TODO 4.发送订单创建成功消息
            return SubmitOrderDTO.builder().memberCode(memberInfo.getMemberCode()).tradeCode(context.getTradeCode()).build();
        } catch (Exception e) {
            log.error("提交订单异常,req:{}", JSON.toJSONString(request),e);
            //TODO 发送异常回滚消息 业务方需要幂等
            throw new BusinessException("500","创建订单异常");
        }finally {
            tradeCouponService.cancelChooseCoupon(authAccount.getAccountCode());
        }
    }

    @Override
    public TradeOrderBaseDTO getTradeOrderDetail(String tradeCode) {
        TradeOrderBaseDTO tradeOrderBaseDTO =  tradeOrderRepository.getByTradeCode(tradeCode);
        if(Objects.isNull(tradeOrderBaseDTO)){
            return null;
        }
        AuthAccount authAccount = AccountContextUtil.getAccountInfoThrowE();
        AssertUtil.isTrue(Objects.equals(tradeOrderBaseDTO.getMemberCode(),authAccount.getAccountCode()));
        return tradeOrderBaseDTO;
    }

    private void clearCartProduct(SubmitOrderContext context) {
        cartRepository.deleteCartChecked(context.getMemberInfo().getMemberCode(),Boolean.TRUE);
    }

    private void getOrderCodeList(SubmitOrderContext context) {
        List<String> shopCodeList = context.getProductDetailList().stream().map(ProductSkuDetailResponse::getShopCode).distinct().collect(Collectors.toList());
        CreateOrderCodeRequest request = new CreateOrderCodeRequest();
        request.setMemberCode(context.getMemberInfo().getMemberCode());
        request.setShopCodeList(shopCodeList);
        List<CreateOrderCodeResponse> createOrderCodeResponses = orderClient.createOrderCode(request);
        Map<String, String> orderCodeResponseMap = createOrderCodeResponses.stream().collect(Collectors.toMap(CreateOrderCodeResponse::getShopCode, CreateOrderCodeResponse::getOrderCode, (k1, k2) -> k1));
        context.setShopOrderCodeMap(orderCodeResponseMap);
    }

    private String createTradeOrder(SubmitOrderContext context) {
        TradeOrderBaseDTO tradeOrderBaseDTO = new TradeOrderBaseDTO();
        MemberInfoResponse memberInfo = context.getMemberInfo();
        tradeOrderBaseDTO.setCreator(memberInfo.getMemberName());
        tradeOrderBaseDTO.setCreatorCode(memberInfo.getMemberCode());
        tradeOrderBaseDTO.setModifier(memberInfo.getMemberName());
        tradeOrderBaseDTO.setModifierCode(memberInfo.getMemberCode());
        tradeOrderBaseDTO.setMemberCode(memberInfo.getMemberCode());
        tradeOrderBaseDTO.setMemberName(memberInfo.getMemberName());
        String tradeCode = IdUtil.getSnowflakeNextIdStr();
        tradeOrderBaseDTO.setTradeCode(tradeCode);
        tradeOrderBaseDTO.setPayStatus("WAIT_PAY");
        tradeOrderBaseDTO.setPayableAmount(context.getSubmitOrderRequest().getCartPrice().getPayableAmount());
        tradeOrderBaseDTO.setPaymentAmount(BigDecimal.ZERO);
        tradeOrderRepository.createTradeOrder(tradeOrderBaseDTO);
        context.setTradeCode(tradeCode);
        return tradeCode;
    }

    private void freezeJoinedMarketing(SubmitOrderContext context) {
        if(StringUtils.isBlank(context.getInstanceCode())){
           return;
        }
        UsedCouponInstanceRequest request = new UsedCouponInstanceRequest();
        request.setInstanceCode(context.getInstanceCode());
        request.setAccountCode(context.getAuthAccount().getAccountCode());
        request.setAccountName(context.getAuthAccount().getAccountName());
        marketingClient.usedCouponInstance(request);
    }

    private void freezeProductStock(SubmitOrderContext context) {
        List<BatchFreezingProductStockRequest> request = new ArrayList<>();
        Map<String, List<ProductSkuDetailResponse>> shopSkuMap = context.getProductDetailList().stream().collect(Collectors.groupingBy(ProductSkuDetailResponse::getShopCode));

        Map<String, SubmitOrderCartRequest> skuCartMap = context.getSubmitOrderRequest().getSkuList().stream().collect(Collectors.toMap(SubmitOrderCartRequest::getSkuCode,Function.identity(),(k1,k2)->k1));

        for (Map.Entry<String, List<ProductSkuDetailResponse>> entry : shopSkuMap.entrySet()) {
            BatchFreezingProductStockRequest productStockRequest = new BatchFreezingProductStockRequest();
            productStockRequest.setOperateCode(context.getMemberInfo().getMemberCode());
            productStockRequest.setOperateName(context.getMemberInfo().getMemberName());
            String orderCode = context.getShopOrderCodeMap().get(entry.getKey());
            AssertUtil.isTrue(StringUtils.isNotBlank(orderCode),"当前门店订单编号不存在"+entry.getKey());
            productStockRequest.setOrderCode(orderCode);
            List<BatchFreezingProductStockDetailRequest> detailRequests = entry.getValue().stream().map(sku -> {
                BatchFreezingProductStockDetailRequest stockDetailRequest = new BatchFreezingProductStockDetailRequest();

                SubmitOrderCartRequest cartRequest = skuCartMap.get(sku.getSkuCode());
                AssertUtil.isTrue(Objects.nonNull(cartRequest),"当前门店订单商品信息不存在"+entry.getKey());

                stockDetailRequest.setStockNum(cartRequest.getBuyCount());
                stockDetailRequest.setSkuCode(sku.getSkuCode());
                stockDetailRequest.setSkuName(sku.getProductName());
                return stockDetailRequest;
            }).collect(Collectors.toList());
            productStockRequest.setSkuList(detailRequests);
            request.add(productStockRequest);
        }

        productStockClient.batchFreezingProductStock(request);
    }

    private void createShopOrder(SubmitOrderContext context) {
        Map<String, ProductSkuDetailResponse> skuDetailResponseMap = context.getProductDetailList().stream().collect(Collectors.toMap(ProductSkuDetailResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));

        Map<String, SubmitOrderSkuCouponResponse> orderSkuCouponResponseMap = Maps.newHashMap();
        if(Objects.nonNull(context.getSubmitOrderResponse())){
             orderSkuCouponResponseMap = context.getSubmitOrderResponse().getSkuList().stream().collect(Collectors.toMap(SubmitOrderSkuCouponResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));
        }

        Map<String, SubmitOrderSkuCouponResponse> finalOrderSkuCouponResponseMap = orderSkuCouponResponseMap;

        List<CreateOrderSkuRequest> skuRequests = context.getSubmitOrderRequest().getSkuList().stream().map(sku -> {
            ProductSkuDetailResponse skuDetailResponse = skuDetailResponseMap.get(sku.getSkuCode());
            CreateOrderSkuRequest orderSkuRequest = converter.convertToCreateOrderSkuRequest(skuDetailResponse);
            String orderCode = context.getShopOrderCodeMap().get(skuDetailResponse.getShopCode());
            AssertUtil.isTrue(StringUtils.isNotBlank(orderCode),"当前门店订单编号不存在"+skuDetailResponse.getShopCode());
            orderSkuRequest.setOrderCode(orderCode);
            orderSkuRequest.setBuyCount(sku.getBuyCount());
            SubmitOrderSkuCouponResponse orderSkuResponse = finalOrderSkuCouponResponseMap.get(sku.getSkuCode());
            if(Objects.nonNull(orderSkuResponse)){
                orderSkuRequest.setCouponAmount(orderSkuResponse.getCouponPromotionAmount());
                orderSkuRequest.setDiscountAmount(BigDecimal.ZERO);
            }else {
                orderSkuRequest.setCouponAmount(BigDecimal.ZERO);
                orderSkuRequest.setDiscountAmount(BigDecimal.ZERO);
            }
            orderSkuRequest.setCostAmount(new BigDecimal(sku.getBuyCount()).multiply(skuDetailResponse.getCostPrice()));
            orderSkuRequest.setSaleAmount(new BigDecimal(sku.getBuyCount()).multiply(skuDetailResponse.getSalePrice()));
            orderSkuRequest.setPayableAmount(orderSkuRequest.getSaleAmount().subtract(orderSkuRequest.getCouponAmount()).subtract(orderSkuRequest.getDiscountAmount()));
            orderSkuRequest.setMainPicture(skuDetailResponse.getThumbnail());
            return orderSkuRequest;
        }).collect(Collectors.toList());

        CreateOrderRequest request = new CreateOrderRequest();
        request.setOrderRemark(context.getSubmitOrderRequest().getOrderRemark());
        request.setMemberCode(context.getMemberInfo().getMemberCode());
        request.setMemberName(context.getMemberInfo().getMemberName());
        request.setTradeCode(context.getTradeCode());
        request.setFreightAmount(BigDecimal.ZERO);
        request.setSkuList(skuRequests);
        orderClient.createSaleOrder(request);
    }

    private SubmitOrderContext validSubmitOrderBiz(SubmitOrderRequest request,
                                                   AuthAccount authAccount,
                                                   List<CartBaseDTO> cartBaseDTOS,
                                                   MemberInfoResponse memberInfo) {
        //2.校验购物车购买商品是否发生变化
        validCheckedCartProductHasChange(cartBaseDTOS, request.getSkuList());
        // 查询商品信息
        List<String> skuCodeList = request.getSkuList().stream().map(SubmitOrderCartRequest::getSkuCode).collect(Collectors.toList());
        List<ProductSkuDetailResponse> productDetailList = productClient.queryBySkuCodeList(skuCodeList);
        //3.校验商品信息是否合法
        validSubmitOrderProduct(productDetailList, request.getSkuList());

        SubmitOrderContext context = new SubmitOrderContext();
        context.setAuthAccount(authAccount);
        context.setProductDetailList(productDetailList);
        context.setSubmitOrderRequest(request);
        context.setMemberInfo(memberInfo);
        //4.校验营销信息
        SubmitOrderResponse submitOrderResponse = validOrderJoinMarketing(context);
        context.setSubmitOrderResponse(submitOrderResponse);
        return context;
    }

    private SubmitOrderResponse validOrderJoinMarketing(SubmitOrderContext context) {
        String orderChooseCoupon = tradeCouponService.getOrderChooseCoupon(context.getAuthAccount().getAccountCode());
        if (StringUtils.isBlank(orderChooseCoupon)) {
            return null;
        }
        context.setInstanceCode(orderChooseCoupon);
        SubmitOrderResponse submitOrderResponse = getSubmitOrderResponse(context.getAuthAccount(), context.getSubmitOrderRequest().getSkuList(), orderChooseCoupon);
        //1.校验金额是否相等
        if (submitOrderResponse.getTotalCouponPromotionAmount().compareTo(context.getSubmitOrderRequest().getCartPrice().getCouponAmount()) != 0) {
            throw new BusinessException("500", "优惠券已失效，请重新刷新");
        }
        if (submitOrderResponse.getTotalDiscountPromotionAmount().compareTo(context.getSubmitOrderRequest().getCartPrice().getDiscountAmount()) != 0) {
            throw new BusinessException("500", "活动已失效，请重新刷新");
        }
        BigDecimal payAbleAmount = submitOrderResponse.getTotalOriginAmount().subtract(submitOrderResponse.getTotalDiscountPromotionAmount()).subtract(submitOrderResponse.getTotalCouponPromotionAmount());
        if (payAbleAmount.compareTo(context.getSubmitOrderRequest().getCartPrice().getPayableAmount()) != 0) {
            throw new BusinessException("500", "活动已失效，请重新刷新");
        }
        return submitOrderResponse;
    }

    private SubmitOrderResponse getSubmitOrderResponse(AuthAccount authAccount, List<SubmitOrderCartRequest> skuList, String orderChooseCoupon) {
        Map<String, SubmitOrderCartRequest> orderCartRequestMap = skuList.stream().collect(Collectors.toMap(SubmitOrderCartRequest::getSkuCode, Function.identity(), (k1, k2) -> k1));

        com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest request = new com.melo.lzmall.marketing.api.request.discount.SubmitOrderRequest();
        request.setAccountCode(authAccount.getAccountCode());
        request.setAccountName(authAccount.getAccountName());
        request.setCouponInstanceCode(orderChooseCoupon);

        List<SubmitOrderSkuRequest> orderSkuRequests = skuList.stream().map(sku -> {
            SubmitOrderSkuRequest detail = new SubmitOrderSkuRequest();
            detail.setSkuCode(sku.getSkuCode());
            detail.setCategoryCode3(detail.getCategoryCode3());
            detail.setProductName(sku.getProductName());
            SubmitOrderCartRequest cartRequest = orderCartRequestMap.get(sku.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(cartRequest), sku.getSkuCode() + "商品编码不存在");
            detail.setPurchaseNum(cartRequest.getBuyCount());
            detail.setSalePrice(sku.getSalePrice());
            detail.setShopCode(sku.getSkuCode());
            return detail;
        }).collect(Collectors.toList());
        request.setSkuList(orderSkuRequests);
        return marketingClient.submitOrder(request);
    }

    private void validSubmitOrderProduct(List<ProductSkuDetailResponse> productDetailList,
                                   List<SubmitOrderCartRequest> skuList) {
        Map<String, ProductSkuDetailResponse> skuDetailResponseMap = productDetailList.stream().collect(Collectors.toMap(ProductSkuDetailResponse::getSkuCode, Function.identity(), (k1, k2) -> k1));
        for (SubmitOrderCartRequest cartRequest : skuList) {
            ProductSkuDetailResponse skuDetailResponse = skuDetailResponseMap.get(cartRequest.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(skuDetailResponse), "商品不存在");
            if (!Objects.equals(skuDetailResponse.getStatus(), "ON_SALE")) {
                throw new BusinessException("500", "商品【" + skuDetailResponse.getProductName() + "】已失效");
            }
            if (Objects.isNull(skuDetailResponse.getStockNum()) || skuDetailResponse.getStockNum() < cartRequest.getBuyCount()) {
                throw new BusinessException("500", "商品【" + skuDetailResponse.getProductName() + "】库存不足");
            }
            if (skuDetailResponse.getSalePrice().compareTo(cartRequest.getSalePrice()) != 0) {
                throw new BusinessException("500", "商品【" + skuDetailResponse.getProductName() + "】价格发生变化，请稍后重试");
            }
        }
    }

    private void validCheckedCartProductHasChange(List<CartBaseDTO> cartBaseDTOS,
                                                  List<SubmitOrderCartRequest> skuList) {
       AssertUtil.isTrue(CollectionUtils.size(cartBaseDTOS) == CollectionUtils.size(skuList) ,"购物车已更新，请刷新购物车重新购买");
        Map<String, CartBaseDTO> cartBaseDTOMap = cartBaseDTOS.stream().collect(Collectors.toMap(CartBaseDTO::getSkuCode, Function.identity(), (k1, k2) -> k1));
        for (SubmitOrderCartRequest cartRequest : skuList) {
            CartBaseDTO existCartBaseDTO = cartBaseDTOMap.get(cartRequest.getSkuCode());
            AssertUtil.isTrue(Objects.nonNull(existCartBaseDTO),"购物车已更新，请刷新购物车重新购买");
            AssertUtil.isTrue(Objects.equals(cartRequest.getBuyCount(),existCartBaseDTO.getBuyCount()),"购物车已更新，请刷新购物车重新购买");
        }
    }
}