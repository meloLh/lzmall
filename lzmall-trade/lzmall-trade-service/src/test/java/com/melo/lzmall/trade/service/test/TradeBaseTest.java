package com.melo.lzmall.trade.service.test;

import com.melo.lzmall.trade.service.TradeApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = TradeApplication.class)
@RunWith(SpringRunner.class)
public class TradeBaseTest {
}
