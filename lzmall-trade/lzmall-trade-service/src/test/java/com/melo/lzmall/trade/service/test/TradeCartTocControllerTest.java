package com.melo.lzmall.trade.service.test;

import com.alibaba.fastjson.JSON;
import com.melo.lzmall.common.core.dto.Result;
import com.melo.lzmall.trade.api.response.cart.CartAllResponse;
import com.melo.lzmall.trade.api.response.cart.SettleCartResponse;
import com.melo.lzmall.trade.service.controller.toc.TradeCartTocController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class TradeCartTocControllerTest extends TradeBaseTest{

    @Autowired
    private TradeCartTocController controller;

    @Test
    public void queryAllCategory(){
        Result<CartAllResponse> result = controller.queryCartAll();
        log.info(JSON.toJSONString(result));
    }

    @Test
    public void settlementCart(){
    }
}
